USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_ADD_TPP_TEACHER_SPECIAL_ACTIVITY]
	@daysPrior INT = 1 -- default, load all data from the previous day
AS

SET NOCOUNT ON

DECLARE @perfs TABLE(
	perf_no INT,
	perf_code VARCHAR(10),
	perf_desc VARCHAR(30),
	perf_dt datetime
)

-- NOTE: 
-- 1. This code follows the logic in LRP_NSCAN_ATTENDANCE_REPORT.
-- 2. The filter on 'Exhibit Halls' might later have to change when we later want to include ERC as an access point 
-- 3. Possibly make this a MERGE to check if record already exists???

INSERT INTO @perfs
SELECT p.perf_no, p.perf_code, i.description, p.perf_dt
	FROM [dbo].T_PERF p
	JOIN [dbo].T_INVENTORY i ON p.perf_no = i.inv_no
WHERE DATEDIFF(d,GETDATE(),p.perf_dt) = @daysPrior
	AND i.description = 'Exhibit Halls'

--SELECT * FROM dbo.TR_PERF_TYPE WHERE inactive = 'N'

INSERT INTO dbo.T_SPECIAL_ACTIVITY
(
    customer_no,
    sp_act,
    sp_act_dt,
    solicitor,
    perf,
    status,
    notes,
    create_loc,
    created_by,
    create_dt,
    last_updated_by,
    last_update_dt,
    num_attendees,
    worker_customer_no
)
SELECT 
	a.customer_no,
	56 AS sp_act, -- TPP Exhibit Halls 
	a.attend_dt AS sp_act_dt, 
	NULL AS solicitor, 
	CASE 
		WHEN DATEPART(hh,a.attend_dt) BETWEEN 8 AND 11 THEN 'Morning'
		WHEN DATEPART(hh,a.attend_dt) BETWEEN 12 AND 17 THEN 'Afternoon'
		WHEN DATEPART(hh,a.attend_dt) BETWEEN 18 AND 24 THEN 'Evening'
		ELSE NULL
	END AS perf, 
	3 AS status, -- Attended 
	NULL AS notes, 
	@@SERVERNAME,
	[dbo].[FS_USER](),
	GETDATE(),
	[dbo].[FS_USER](),
	GETDATE(),
	CASE a.admission_adult + a.admission_child + a.admission_other	
		WHEN 0 THEN 1
		ELSE a.admission_adult + a.admission_child + a.admission_other
	END AS num_attendees, -- for some reason, sometimes the sum = 0, so I set it to 1. 
	NULL AS worker_customer_no
FROM [dbo].T_ATTENDANCE a
INNER JOIN @perfs p 
	ON a.perf_no = p.perf_no
LEFT JOIN [dbo].T_MEMB_LEVEL ml 
	ON a.memb_level_no = ml.memb_level_no
WHERE DATEDIFF(d,GETDATE(),a.attend_dt) = @daysPrior
	AND ml.memb_org_no = 13 -- Teacher Partner


