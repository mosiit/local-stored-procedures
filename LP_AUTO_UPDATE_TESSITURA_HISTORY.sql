USE IMPRESARIO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_AUTO_UPDATE_TESSITURA_HISTORY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_AUTO_UPDATE_TESSITURA_HISTORY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_AUTO_UPDATE_TESSITURA_HISTORY]
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @fYear1 INT = 0, @fYear2 INT = 0
        DECLARE @season INT = 0, @season_str VARCHAR(100) = ''

    /*  Don't run this procedure before October 3, 2017
            --This line of code can be removed after October 3rd */

        IF CONVERT(DATE,GETDATE()) < '10-3-2017' GOTO DONE

    /*  Update the Built in Tessitura History Table based on season  */

        SELECT @fyear1 = ISNULL([dbo].[LF_GetFiscalYear] (GETDATE()),0)

        IF @fYear1 <= 0 GOTO DONE
    
    /* July to December: Second fiscal year is the previous fiscal year
        January to June: Second fiscal year is the next fiscal year  */

        IF DATEPART(MONTH,GETDATE()) >= 7 SELECT @fYear2 = (@fYear1 - 1) ELSE SELECT @fYear2 = (@fYear1 + 1)

    /*  Determine which seasons go with the two fiscal years  */

        DECLARE season_cursor INSENSITIVE CURSOR FOR
        SELECT [id] FROM [dbo].[TR_SEASON] WHERE [fyear] IN (@fYear1, @fYear2) ORDER BY [id]
        OPEN season_cursor
        BEGIN_SEASON_LOOP:

            FETCH NEXT FROM season_cursor INTO @season
            IF @@FETCH_STATUS = -1 GOTO END_SEASON_LOOP

            IF @season_str <> '' SELECT @season_str = @season_str + ','
            SELECT @season_str = @season_str + CONVERT(VARCHAR(20),@season)
            
            GOTO BEGIN_SEASON_LOOP

        END_SEASON_LOOP:
        CLOSE season_cursor
        DEALLOCATE season_cursor

    /* Execute the TP_UPDATE_TICKET_HISTORY using the gathered list of season id numbers  */
    
        IF @season_str = '' GOTO DONE

        EXECUTE [dbo].[TP_UPDATE_TICKET_HISTORY] @season_no = @season_str

    DONE:

END
GO
 
GRANT EXECUTE ON [dbo].[LP_AUTO_UPDATE_TESSITURA_HISTORY] TO ImpUsers
GO
