USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_FIX_NULL_CAMPAIGN_NO]    Script Date: 10/30/2015 17:28:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_FIX_NULL_CAMPAIGN_NO]
AS
SET XACT_ABORT ON
Set NoCount On  
/************************************************************************************************
Created 10/29/15 by Bryan Evans, Tessitura Network Support
This procedure was adapted from a script provided to fix NULL campaign_no values in the
T_TRANSACTION, T_SLI_DETAIL, and T_WEB_SLI_DETAIL tables as the result of a known defect
in Tessitura version 12.5.

The procedure gets the appropriate campaign_no from the corresponding T_PERF or T_PKG row and
updates the T_TRANSACTION, T_SLI_DETAIL, and T_WEB_SLI_DETAIL rows accordingly.


WO 89997 - Script to fix Tessitura defect 94581
Applied to MOS environment on Dec 2017 by DSJ
***********************************************************************************************/

-- Get list of transactions with null campaign_no
select sequence_no
into #transactions
from T_TRANSACTION
where campaign_no is null

-- Update transactions
update t
set campaign_no = c.campaign_no
from T_TRANSACTION t
join 
	(
	select tr.transaction_no, 
	tr.sequence_no, 
	isnull(min(pk.campaign_no),min(p.campaign_no)) as campaign_no 
	from T_TRANSACTION tr
	join T_LINEITEM l on tr.order_no = l.order_no 
		and tr.perf_no = l.perf_no
	join t_perf p on l.perf_no = p.perf_no
	left join t_pkg pk on l.pkg_no = pk.pkg_no	
	where tr.campaign_no is null 
	and tr.perf_no is not null
	group by tr.transaction_no, 
	tr.sequence_no
	) c on t.transaction_no = c.transaction_no 
		and t.sequence_no = c.sequence_no

-- Get list of SLI details with null campaign_no
select detail_sli_no
into #slids
from T_SLI_DETAIL
where campaign_no is null

-- Update SLI Details
update slid
set campaign_no = isnull(pk.campaign_no, p.campaign_no)
from T_SLI_DETAIL slid
join T_SUB_LINEITEM sli on slid.sli_no = sli.sli_no
join t_perf p on sli.perf_no = p.perf_no
left join t_pkg pk on sli.pkg_no = pk.pkg_no	
where slid.campaign_no is null 

-- Get list of web SLI details with null campaign_no
select detail_sli_no
into #webslids
from T_WEB_SLI_DETAIL
where campaign_no is null

-- Update Web SLI Details, just in case
update wslid
set campaign_no = isnull(pk.campaign_no, p.campaign_no)
from T_WEB_SLI_DETAIL wslid
join T_WEB_SUB_LINEITEM sli on wslid.sli_no = sli.sli_no
join t_perf p on sli.perf_no = p.perf_no
left join t_pkg pk on sli.pkg_no = pk.pkg_no	
where wslid.campaign_no is null 


select 'T_TRANSACTION' as table_updated,
x.sequence_no as pk,
t.campaign_no
from #transactions x
join T_TRANSACTION t on x.sequence_no = t.sequence_no 
	and t.campaign_no is not null
	
union

select 'T_SLI_DETAIL' as table_updated,
y.detail_sli_no as pk,
sd.campaign_no
from #slids y
join T_SLI_DETAIL sd on y.detail_sli_no = sd.detail_sli_no
	and sd.campaign_no is not null

union

select 'T_WEB_SLI_DETAIL' as table_updated,
z.detail_sli_no as pk,
wsd.campaign_no
from #webslids z
join T_WEB_SLI_DETAIL wsd on z.detail_sli_no = wsd.detail_sli_no
	and wsd.campaign_no is not null


RETURN


GO


