USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_GetProductData]    Script Date: 3/17/2020 11:24:11 AM ******/
DROP PROCEDURE [dbo].[LRP_GetProductData]
GO

/****** Object:  StoredProcedure [dbo].[LRP_GetProductData]    Script Date: 3/17/2020 11:24:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[LRP_GetProductData]
(@NumOfDays INT = 0 -- 0 for today's schedule
)
AS
SET NOCOUNT ON;

DECLARE @prods TABLE
(
    EventDate CHAR(10),
    EventTime VARCHAR(8),
    EventEnd VARCHAR(8),
    Venue VARCHAR(100),
    Location VARCHAR(100),
    SoftCapacity INT,
    Instock INT,
    EventNumber INT,
    MemberOnly INT,
    SchoolOnly INT,
    EventTitleLong VARCHAR(100),
    ItemLength INT
        UNIQUE CLUSTERED (
                             EventDate,
                             Venue,
                             MemberOnly, 
							 EventNumber,
							 EventTime
                         )
);

INSERT INTO @prods
(
    EventDate,
    EventTime,
    EventEnd,
    Venue,
    Location,
    SoftCapacity,
    Instock,
    EventNumber,
    MemberOnly,
    SchoolOnly,
    EventTitleLong,
    ItemLength
)
SELECT DISTINCT
       CONVERT(VARCHAR(10), perf.performance_dt, 111) AS EventDate,
       perf.performance_time AS EventTime,
       perf.performance_end_time AS EventEnd,
       CASE perf.title_name
           WHEN 'Hayden Planetarium' THEN 'Charles Hayden Planetarium'
           ELSE perf.title_name
       END AS Venue,
       CASE perf.title_name
           WHEN 'Drop-In Activities' THEN perf.performance_location
           WHEN 'Live Presentations' THEN perf.performance_location
           WHEN 'Hayden Planetarium' THEN 'Planetarium'
           ELSE perf.title_name
       END AS location,
	   [dbo].[LF_GetCapacity](perf.[performance_no], perf.[performance_zone], 'Capacity') AS SoftCapacity,
	   [dbo].[LF_GetCapacity](perf.[performance_no], perf.[performance_zone], 'Available') AS Instock,
	   --CASE
       --    WHEN perf.title_name = 'Drop-In Activities' THEN '30000'
       --    WHEN perf.title_name = 'Live Presentations' THEN '30000'
       --    ELSE ISNULL(cap.capacity, 0)
       --END AS SoftCapacity,
       --CASE
       --    WHEN perf.title_name = 'Drop-In Activities' THEN '30000'
       --    WHEN perf.title_name = 'Live Presentations' THEN '30000'
       --    ELSE ISNULL(cap.available, 0)
       --END AS Instock,
       perf.production_no AS EventNumber,
       CASE perf.performance_type_name WHEN 'Member Event' THEN 1 ELSE 0 END AS MemberOnly,
       CASE perf.performance_type_name WHEN 'School Only' THEN 1 ELSE 0 END AS SchoolOnly,
       perf.production_name_long AS EventTitleLong,
       DATEDIFF(mi, CAST(perf.performance_time AS DATETIME), CAST(perf.performance_end_time AS DATETIME)) AS ItemLength
FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE perf
--LEFT JOIN [dbo].[LV_PERFORMANCE_CAPACITY_AVAILABLITY] cap ON cap.production_no = perf.production_no
--                                                             AND cap.performance_date = perf.performance_date
--                                                             AND cap.performance_time = perf.performance_time
WHERE DATEDIFF(d, GETDATE(), performance_dt) >= 0
      AND DATEDIFF(d, GETDATE(), performance_dt) <= @NumOfDays
      AND perf.performance_name_long <> ''
      AND perf.title_name IN
          (
              SELECT venue_name FROM dbo.LT_DS_VENUES
          )
      AND
      (
          perf.performance_type_name <> 'Buyout'
          AND perf.production_name NOT LIKE '%Buyout%'
      )
      AND perf.is_generic_title = 'N';

--SELECT DISTINCT 'dates' AS [results], Venue, EventDate
SELECT DISTINCT 'dates' AS [results], EventDate
FROM	@prods
ORDER BY EventDate;

SELECT DISTINCT 'venues' AS [results], Venue, EventDate, MemberOnly
FROM	@prods
ORDER BY EventDate, Venue, MemberOnly;

SELECT DISTINCT 'shows' AS [results], x.Venue, x.EventDate, x.MemberOnly, x.EventNumber, x.ItemLength, x.Location, x.EventTitleLong --, apd.EventTitle, apd.EventURL
FROM	@prods x
--LEFT JOIN dbo.LT_ACQUIA_PRODUCTION_DATA apd
--ON		x.EventNumber = apd.EventNumber
ORDER BY x.EventDate, x.Venue, x.MemberOnly, x.EventNumber;

SELECT DISTINCT 'times' AS [results], Venue, EventDate, MemberOnly, EventNumber, EventEnd, Instock, SchoolOnly, EventTime
FROM	@prods
ORDER BY EventDate, Venue, MemberOnly, EventNumber;





GO


