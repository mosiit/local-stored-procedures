USE [impresario];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;

GO
--DROP PROCEDURE [dbo].[LRP_EXCEPTION_RETURNED_MEMBERSHIPS]

GO
CREATE PROCEDURE [dbo].[LRP_EXCEPTION_RETURNED_MEMBERSHIPS]
(
    @order_start_dt DATETIME,
    @order_end_last_update_dt DATETIME
)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;
-- =============================================
-- Author:		Aileen Duffy-Brown
-- Create date: 3/19/2018
-- Description:	Looks for returned memberships where  
-- the membership has been refunded but the membership 
-- record still remains and needs to be looked at and 
-- will likely be deleted by someone in membership manually.  
-- WO# 82787
-- =============================================
BEGIN

    SELECT DISTINCT
           ord.customer_no,
           ord.order_dt,
           ord.last_update_dt,
           ord.tot_paid_amt,
           sub.order_no,
           sub.sli_status,
           sub.due_amt,
           sub.paid_amt,
           zon.zone_no,
           zon.description AS Zone,
           mem.cust_memb_no,
           mem.init_dt,
           mem.expr_dt,
           mem.current_status,
           mcd.printed_dt
    FROM dbo.T_SUB_LINEITEM AS sub
        INNER JOIN dbo.T_ZONE AS zon
            ON sub.zone_no = zon.zone_no
        INNER JOIN T_ORDER AS ord
            ON sub.order_no = ord.order_no
               AND
               (
                   PATINDEX('%basic%', zon.description) > 0
                   OR PATINDEX('%premier%', zon.description) > 0 -- finding all basic and premier membership types
               )
        INNER JOIN dbo.TX_CUST_MEMBERSHIP AS mem
            ON ord.customer_no = mem.customer_no
               AND mem.expr_dt > GETDATE()
        LEFT OUTER JOIN LT_MOS_MEMBER_CARDS AS mcd
            ON ord.customer_no = mcd.customer_no
               AND mem.cust_memb_no = mcd.cust_memb_no
               AND mcd.printed_dt IS NULL -- the card print utility excludes returned orders so if a card is not printed it is a good sign that there is something amiss.
    WHERE sub.sli_status = 4 -- returned orders
          AND mem.memb_org_no = 4 -- houeshold
          AND ord.order_dt >= @order_start_dt
          AND ord.last_update_dt >= @order_end_last_update_dt;

END;

GO
GRANT EXECUTE ON [dbo].[LRP_EXCEPTION_RETURNED_MEMBERSHIPS] TO ImpUsers;
GO
