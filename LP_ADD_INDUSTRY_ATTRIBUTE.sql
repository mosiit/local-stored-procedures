USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_ADD_INDUSTRY_ATTRIBUTE]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- IF INDUSTRY EXISTS ON THE INDIVIDUAL RECORD BUT DOESN�T MATCH THE INDUSTRY ON THE EMPLOYER�S RECORD, 
-- UPDATE THE EXISTING INDUSTRY ATTRIBUTE VALUE ON THE INDIVIDUAL RECORD.

--SELECT c.customer_no, k2.key_value, k.key_value
UPDATE k2
SET k2.key_value = k.key_value
FROM TX_CUST_KEYWORD AS k2
INNER JOIN T_AFFILIATION AS a
    ON k2.customer_no = a.individual_customer_no
	AND a.affiliation_type_id = 10007 -- Employee_Main
INNER JOIN TX_CUST_KEYWORD AS k
    ON a.group_customer_no = k.customer_no
	AND k.keyword_no = 421 -- Industry
	AND SUBSTRING(k.key_value, 3, 1) = '_'
INNER JOIN T_CUSTOMER AS c
    ON a.individual_customer_no = c.customer_no
	AND c.name_status = 1
WHERE k2.keyword_no = 421
	AND k2.key_value <> k.key_value;


-- IF AN EMPLOYEE_MAIN RECORD (AFFILIATION_TYPE_ID = 10007) EXISTS ON THE CONSTITUENT'S RECORD BUT NO INDUSTRY (KEYWORD_NO = 421), 
-- ADD THE EMPLOYER�S INDUSTRY.

INSERT INTO dbo.TX_CUST_KEYWORD
(
    keyword_no,
    customer_no,
    key_value,
    create_loc,
    created_by,
    create_dt,
    last_updated_by,
    last_update_dt
)
SELECT 
    k.keyword_no,
	c.customer_no,
    k.key_value,
	@@SERVERNAME,
	dbo.FS_USER(),
	GETDATE(),
	dbo.FS_USER(),
	GETDATE()
FROM T_AFFILIATION AS a
INNER JOIN T_CUSTOMER AS c
    ON a.individual_customer_no = c.customer_no
	AND c.name_status = 1
INNER JOIN TX_CUST_KEYWORD AS k
    ON a.group_customer_no = k.customer_no
	AND k.keyword_no = 421 -- Industry
    AND SUBSTRING(k.key_value, 3, 1) = '_'
WHERE a.affiliation_type_id = 10007 -- Employee_Main
AND c.customer_no NOT IN
(
    SELECT customer_no FROM TX_CUST_KEYWORD WHERE keyword_no = 421 -- Industry
);

