USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_IMPORT_ATTENDANCE_BUDGET]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_IMPORT_ATTENDANCE_BUDGET] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_IMPORT_ATTENDANCE_BUDGET] TO impusers';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_IMPORT_ATTENDANCE_BUDGET] TO tessitura_app';
END;
GO

/*  This procedure assumes the most up to date information has already been uploaded into the attendance_budget table
    in the admin database.  That file will be periodically updated by Finance.  It is not a problem for the data in
    that table to be processed if no changes have been made.  If there is nothing to update.  The procuedure checks for
    updated data and makes adjustments only if necessary.  */

ALTER PROCEDURE [dbo].[LP_IMPORT_ATTENDANCE_BUDGET]
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


    /*  Step one is to update the data in attendance_budget with the appropriate title number from Tessitura.  
        The budget spreadsheet contains abbreviated versions of the title name.  These are stored in a content
        record on each title.  The content type is called BudgetAbbreviation.  Using that content, the procedure
        determines the specific Tessitura title name and number that goes with the abbreviation in the spreadsheet.
        Content Type 39 = BudgetAbbreviation
        Content Type 40 = TitleGroup  */

                WITH CTE_VENUES ([title_no], [title_name], [budget_abbrev], [title_group])
                AS (SELECT inv.[inv_no], 
                           inv.[description],
                           abr.[value],
                           grp.[value]
                    FROM [impresario].[dbo].[T_INVENTORY] AS inv
                         INNER JOIN [impresario].[dbo].[TX_INV_CONTENT] AS abr ON abr.[inv_no] = inv.[inv_no] AND abr.[content_type] = 39
                         INNER JOIN [impresario].[dbo].[TX_INV_CONTENT] AS grp ON grp.[inv_no] = inv.[inv_no] AND grp.[content_type] = 40)
        UPDATE bud
        SET bud.[budget_title] = cte.[title_name],
            bud.[budget_title_no] = cte.[title_no],
            bud.[budget_title_group] = cte.[title_group]
        FROM [admin].[dbo].[attendance_budget] AS bud
             INNER JOIN CTE_VENUES AS cte ON cte.[budget_abbrev] = bud.[budget_venue]

    /*  There are three venues that do not correspond to any specific title in Tessitura and have to be handled
        separately.  The Entertainment budget is a subset of the Planetarium and has been given a title number of 25
        because a unique number is needed to differentiate it and that number is not being used for anything else.  */

        UPDATE [admin].[dbo].[attendance_budget]
        SET [budget_title] = 'Entertainment',
            [budget_title_no] = 25,
            [budget_title_group] = 'Planetarium'
        WHERE [budget_venue] = 'Entertainment'

    /*  The Gross Attendance budget is given a title number of zero  */

        UPDATE [admin].[dbo].[attendance_budget]
        SET [budget_title] = 'Gross Attendance',
            [budget_title_no] = 0,
            [budget_title_group] = 'Entire Museum'
        WHERE [budget_venue] = 'Gross'

    /*  The Incremental attendance budget is given a title number of 24  */

        UPDATE [admin].[dbo].[attendance_budget]
        SET [budget_title] = 'Incremental',
            [budget_title_no] = 24,
            [budget_title_group] = 'Entire Museum'
        WHERE [budget_venue] = 'Incremental'
        

    /*  The first step is to insert any records into the LTR_ATTENDANCE_BUDGET_DATA table that do not
        already exist.  The unique identifier on the table is perf_dt and title_no.  Each venue and only have
        one record for each date.  There is a unique index on the table to prevent duplicates  */

        INSERT INTO [impresario].[dbo].[LTR_ATTENDANCE_BUDGET_DATA] ([perf_dt], [perf_day], [title_group], [title_no], [budget], [budget_revenue], [inactive])
        SELECT tmp.[budget_date],
               DATENAME(WEEKDAY,tmp.[budget_date]),
               tmp.[budget_title_group],
               tmp.[budget_title_no],
               tmp.[budget_number],
               tmp.[budget_revenue],
               'N'
        FROM [admin].[dbo].[attendance_budget] AS tmp
             LEFT OUTER JOIN [impresario].[dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS liv ON liv.[perf_dt] = tmp.[budget_date] AND liv.[title_no] = tmp.[budget_title_no]
        WHERE liv.[id] IS NULL

    /*  The second step is to update the existing records in LTR_ATTENDANCE_BUDGET_DATA that have changed.  
        If the budget number has not changed, the record is not updated with the same value.  
        That way the last updated date is more accurate  */

        UPDATE liv
        SET liv.[budget] = tmp.[budget_number],
            liv.[budget_revenue] = tmp.[budget_revenue]
        FROM [admin].[dbo].[attendance_budget] AS tmp
             LEFT OUTER JOIN [impresario].[dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS liv ON liv.[perf_dt] = tmp.[budget_date] AND liv.[title_no] = tmp.[budget_title_no]
        WHERE liv.[budget] <> tmp.[budget_number]


    /*  Deal with Incremental (if any)  */

        --Delete any incremental records more than a year old
        --After a year they don't need to be processed any more
        DELETE FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] 
        WHERE DATEDIFF(DAY,[perf_dt],GETDATE()) > 365 AND [title_no] = 24

        --Move the incremenral number into the same row as the gross attendance (if they exist)
        UPDATE att1
        SET att1.[incremental] = att2.[budget]
        FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS att1
             INNER JOIN [dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS att2 ON att2.[perf_dt] = att1.[perf_dt] AND att2.[title_no] = 24
        WHERE att1.[title_no] <> 24 AND att1.[title_no] = 0

        --Delete any remaining incremental records
        DELETE FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] 
        WHERE [title_no] = 24

        --Make sure there are no nulls in the incremental column
        UPDATE [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
        SET [incremental] = 0
        WHERE [incremental] IS NULL END

GO

EXECUTE [dbo].[LP_IMPORT_ATTENDANCE_BUDGET]

SELECT * FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] WHERE perf_dt >= '7-1-2020' ORDER BY [perf_dt], Title_group, [title_no]



