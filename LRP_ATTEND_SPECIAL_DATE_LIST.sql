USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTEND_SPECIAL_DATE_LIST]    Script Date: 11/26/2019 1:55:38 PM ******/
DROP PROCEDURE [dbo].[LRP_ATTEND_SPECIAL_DATE_LIST]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTEND_SPECIAL_DATE_LIST]    Script Date: 11/26/2019 1:55:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[LRP_ATTEND_SPECIAL_DATE_LIST]
	@WeekEnding	DATETIME,
	@WhichYear	VARCHAR(4) = 'This' -- 'This' or 'Last'
AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE	@StartDate	DATETIME,
		@EndDate	DATETIME,
		@StartTime	INT = 9,
		@EndTime	INT = 17;

IF @WhichYear = 'This'
	BEGIN

		SELECT	@StartDate = cur_week_start_dt,
				@EndDate = cur_week_end_dt
		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y');

		--SELECT 'debug',@StartDate,@EndDate,@WeekEnding
	END;

IF @WhichYear = 'Last'
	BEGIN

		SELECT	@StartDate = prv_week_start_dt,
				@EndDate = prv_week_end_dt
		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y');

	END;

	--SELECT 'debug', @StartDate, @EndDate

	DECLARE @cols AS NVARCHAR(MAX),
			@query  AS NVARCHAR(MAX)

	;WITH cte (datelist, maxdate) AS
	(
		SELECT DISTINCT @StartDate datelist, DATEADD(DAY, -1, @EndDate) maxdate
		FROM [dbo].[LTR_SPECIAL_DATES]
		WHERE	[Date] BETWEEN @StartDate AND @EndDate
		UNION ALL
		SELECT DATEADD(dd, 1, datelist), maxdate
		FROM cte
		WHERE datelist < maxdate
	) 
	SELECT ROW_NUMBER() OVER(ORDER BY c.datelist) AS RowNbr, c.datelist
	INTO #tempDates
	FROM cte c;

	--SELECT 'debug', * FROM #tempDates

	SELECT @cols = STUFF((SELECT DISTINCT ',' + QUOTENAME(CONVERT(CHAR(10), datelist, 120)) 
						FROM #tempDates
				FOR XML PATH(''), TYPE
				).value('.', 'NVARCHAR(MAX)') 
			,1,1,'');

	--SELECT @cols

	TRUNCATE TABLE [dbo].[LT_ATTEND_SPECIAL_DATE_MATRIX];

	SET @query = 'INSERT INTO [dbo].[LT_ATTEND_SPECIAL_DATE_MATRIX] SELECT Venue, Category, ' + @cols + ' from 
				 (
					select b.Venue, b.Category, d.datelist, convert(CHAR(10), datelist, 120) PivotDate
					from #tempDates d
					left join [dbo].[LTR_SPECIAL_DATES] b
						on d.datelist between b.[Date] and b.[Date]
				) x
				pivot 
				(
					count(datelist)
					for PivotDate in (' + @cols + ')
				) p';

	--SELECT @query;

	EXECUTE sp_executesql @query;

	-- Grab the notes as onerow
	DECLARE @notes VARCHAR(MAX);

	SELECT	@notes = COALESCE(@notes + ', ' + Notes, Notes)
	FROM	[dbo].[LTR_SPECIAL_DATES]
	WHERE	[Date] BETWEEN @StartDate AND @EndDate
	AND		ISNULL([Notes], '') <> '';

	SELECT DATEPART(YEAR, @StartDate) AS [Year],
		   sdm.[Venue],
		   sdm.[Category],
		   CASE WHEN sdm.[Date1] > 0 THEN '2' END AS [Date1],
		   CASE WHEN sdm.[Date2] > 0 THEN '2' END AS [Date2],
		   CASE WHEN sdm.[Date3] > 0 THEN '2' END AS [Date3],
		   CASE WHEN sdm.[Date4] > 0 THEN '2' END AS [Date4],
		   CASE WHEN sdm.[Date5] > 0 THEN '2' END AS [Date5],
		   CASE WHEN sdm.[Date6] > 0 THEN '2' END AS [Date6],
		   CASE WHEN sdm.[Date7] > 0 THEN '2' END AS [Date7],
		   @EndDate AS [EndDate],
		   'Notes: ' + @notes AS [Notes]
	FROM	[dbo].[LT_ATTEND_SPECIAL_DATE_MATRIX] sdm
	JOIN	[dbo].[LTR_SPECIAL_DATES_CATEGORIES] sdg
	ON		sdm.Category = sdg.Category
	--JOIN	[dbo].[LT_SPECIAL_DATES] sd
	--ON		sd.Venue = sdm.Venue
	--AND		sd.Category = sdm.Category
	--AND		sd.[Date] BETWEEN @StartDate AND @EndDate
	ORDER BY sdg.SortOrder;
	



GO


