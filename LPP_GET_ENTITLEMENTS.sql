USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LPP_GET_ENTITLEMENTS]    Script Date: 9/22/2017 10:16:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LPP_GET_ENTITLEMENTS]

@memb_level_no int = NULL, /* if a membership in the cart */
@constituency_str varchar(255) = NULL,
@customer_no int = NULL, /* if a logged in web user or chosen @customer_no */
@perf_no int, /* the thing we're trying to see if entitled */
@price_type INT = NULL, /* the input price type we're trying to buy with */
@price_type_group INT = NULL, /* either price type or price type group must be entered, if both given we use PT. */
@order_date DATETIME = NULL,
@return_results char(1) = 'Y',
@modify_prices CHAR(1) = 'N',
@cust_memb_no_out INT = NULL OUTPUT,
@mem_level_no_out INT = NULL OUTPUT,
@mem_level_desc_out varchar(30) = NULL OUTPUT,
@entitlement_no_out int = NULL OUTPUT,
@entitlement_key_out VARCHAR(255) = NULL OUTPUT,
@num_used_out int = NULL OUTPUT,
@num_ent_out int = NULL OUTPUT,
@num_unused_out int = NULL OUTPUT,
@upgrade_downgrade_out char(1) = NULL OUTPUT,
@target_price_type_out int = NULL OUTPUT,
@exceeded_price_type_out int = NULL OUTPUT,
@debug char(1) = 'N'

AS

SET NOCOUNT ON

/************************************************************************
3/20/2014 Michael Reisman
All rules happen here

--customer given
EXEC dbo.LPP_GET_ENTITLEMENTS @customer_no = 70017, @perf_no = 126, @price_type =  8, @order_date = '9/1/2015'

MODIFIED 
4/21/2015 MIR: Complete overhaul
4/30/2015 MIR: Adding the cart membership to the top half of this proc, along with other current memberships (UNION with TX_CUST_MEMBERSHIP)
5/8/2015 MIR: For N-Scan customization: Optional @price_type_group as input, @cust_memb_no as output.
5/11/2015 MIR: Reset type and entitlement key set properly for in-cart membership only cases.  This is rarely if ever hit
6/2/2015 MIR: Bypass this functionality for perf = 0 (Fixed seat packages)
6/12/2015 MIR: Attempt to fix case where 2nd membership is added after entitlements already used against first.
6/17/2015 MIR Respecting Perf-reset entitlement overrides from the LTX_CUST_ENTITLEMENT table.
6/22/2015 MIR Deactivated memberships should not be granted entitlements!
		SELECT * FROM TX_CUST_MEMBERSHIP where current_status NOT IN (2,3)
6/29/2015 MIR Corrected logic for removing dups in D(aily),P(erf) cases: num_remain_ent was wrong
7/8/2015 MIR Using distinct view of keywords [LV_ENT...]
11/24/2015 MIR adding Virtual Constituency as input to get the proper membership entitlements.
12/7/2015 MIR Respect new checkboxes in LTR_ENTITLEMENT for:
			 Inactive ("entitlement is inactive") 
			 Decline benefits ("This rule should not be granted if member has declined beneifts")
1/11/2016 MIR: Always use the stroke of 00:00 of the init date and 23:59:59 of the expire date.
1/11/2016 MIR: Two new functional columns in LTR_ENTITLEMENT:
			1. Checkbox "allow_lapsed" allows entitlements for lapsed members, according to period defined in T_MEMB_LEVEL
			2. Option to use performance date or order date to validate entitlements.  The date chosen must fall between the init & expire date.
1/29/2016 MIR: Order date is assumed to be this moment if not provided.
*********************************************************************/

DECLARE @cust_num_used int = 0
DECLARE @perf_calendar_dt datetime = (SELECT CAST(CONVERT(varchar(10),perf_dt,101) as datetime) FROM dbo.T_PERF WHERE perf_no = @perf_no)
DECLARE @num_of_pt_matches int
DECLARE @upgraded_price_type int
DECLARE @orig_price_type INT = @price_type --preserving input param for 8/13 changes:  may not be best to change @price_type mid-stream.

DECLARE @validation_message VARCHAR(255) = (SELECT dbo.FS_GET_DEFAULT_VALUE(NULL, 'Entitlements','VALIDATION_MESSAGE'))

DECLARE
		@mem_level_no INT,
		@mem_level_desc varchar(30),
		@entitlement_no int,
		@cust_memb_no int,
		@reset_type char(1),
		@num_ent int,
		@num_used int,
		@num_unused int,
		@upgrade_downgrade char(1),
		@target_price_type int,
		@exceeded_price_type INT

SET @order_date = ISNULL(@order_date,CURRENT_TIMESTAMP)

IF @memb_level_no IS NULL AND ISNULL(@constituency_str,'') <> ''
  BEGIN
	SET @memb_level_no = (SELECT TOP 1 memb_level_no 
							FROM dbo.T_MEMB_LEVEL l
							JOIN dbo.FT_SPLIT_LIST(@constituency_str,',') cs ON l.constituency = CAST(cs.Element AS INT))
  END

/* 6/2/2015 for zero perf (package) rows: abort with bypass (blank) values  */
IF @perf_no = 0 
  GOTO BYPASS
	
/* We have a customer */
IF NULLIF(@customer_no,0) IS NOT NULL
  BEGIN
	/* Find existing current membership */
	SELECT 
		cm.memb_org_no,
		init_dt,
		expr_dt,
		ml.renewal,
		ml.expiry,
		cm.declined_ind
	INTO #active_membs
	FROM TX_CUST_MEMBERSHIP cm
		JOIN T_MEMB_LEVEL ml ON ml.memb_level = cm.memb_level AND ml.memb_org_no = cm.memb_org_no
	WHERE customer_no = @customer_no
	AND current_status = 2

	/* See if it's a member AND is trying to buy an entitlement-relevant event based on entitlement rules  */
	SELECT
		mem_level_no = ml.memb_level_no,
		mem_level_desc = ml.description,
		entitlement_no = e.entitlement_no,
		cust_memb_no = cm.cust_memb_no,
		reset_type = e.reset_type,
		num_used = ISNULL(coe.cust_num_used,0),
		num_start_ent = COALESCE(ce.num_ent,e.num_ent),
		num_remain_ent = COALESCE(ce.num_ent,e.num_ent) - ISNULL(coe.cust_num_used,0),
		ent_price_type_group = e.ent_price_type_group,
		cm.init_dt,
		price_type_id = pt.id,
		membership_status = cm.current_status
	INTO #memb_ent
	FROM dbo.LTR_ENTITLEMENT e
		JOIN dbo.TR_PRICE_TYPE pt ON e.ent_price_type_group = pt.price_type_group
		JOIN dbo.T_MEMB_LEVEL ml ON e.memb_level_no = ml.memb_level_no
		JOIN dbo.LV_ENTITLEMENT_INV_TKW_LIST v ON v.tkw = e.ent_tkw_id
		JOIN (
			SELECT
				customer_no,
				cust_memb_no,
				memb_org_no,
				memb_level,
				init_dt,
				expr_dt,
				current_status,
				declined_ind
			FROM TX_CUST_MEMBERSHIP 
			WHERE customer_no = @customer_no
			
			UNION 

			/* simulated cart membership */
			SELECT 
				customer_no = @customer_no,
				cust_memb_no = 0,
				ml.memb_org_no,
				memb_level,
				init_dt = CASE 
							/* within renewal period = renewal */
							WHEN CURRENT_TIMESTAMP BETWEEN DATEADD(MONTH,-am.renewal,am.expr_dt) AND am.expr_dt
								THEN DATEADD(DAY,1,CAST(CONVERT(varchar(10),am.expr_dt,101) as datetime))
							/* Before renewal period = upgrade */
							WHEN CURRENT_TIMESTAMP < DATEADD(MONTH,-am.renewal,am.expr_dt)
								THEN am.init_dt
							/* otherwise it's today */
							ELSE 
								CAST(CONVERT(varchar(10),CURRENT_TIMESTAMP,101) as datetime)
							END,
				expr_dt = CASE 
							/* within renewal period = renewal: expr date is (new expiry) months after (init dt above) (minus a second)  */
							WHEN CURRENT_TIMESTAMP BETWEEN DATEADD(MONTH,-am.renewal,am.expr_dt) AND am.expr_dt 
								THEN DATEADD(SECOND,
											-1,
											(DATEADD(MONTH,
													ml.expiry,
													(DATEADD(DAY,
															1,
															CAST(CONVERT(varchar(10),am.expr_dt,101) as datetime)))))
											)
							/* before renewal period = upgrade */
							WHEN CURRENT_TIMESTAMP < DATEADD(MONTH,-am.renewal,am.expr_dt)
								THEN am.expr_dt
							/* otherwise it's (expiry months) after today (minus a second) */
							ELSE 
								DATEADD(SECOND,-1,(DATEADD(MONTH,ml.expiry,CURRENT_TIMESTAMP)))
							END,
				current_status = CASE 
							/* within renewal period = renewal */
							WHEN CURRENT_TIMESTAMP BETWEEN DATEADD(MONTH,-am.renewal,am.expr_dt) AND am.expr_dt
								THEN 3
							ELSE 2 
							END,
				am.declined_ind
			FROM (SELECT memb_level_no = @memb_level_no) cart_mem
				JOIN T_MEMB_LEVEL ml ON ml.memb_level_no = cart_mem.memb_level_no
				LEFT JOIN #active_membs am ON am.memb_org_no = ml.memb_org_no /* same org */
				) cm
			ON (ml.memb_org_no = cm.memb_org_no AND ml.memb_level = cm.memb_level)
			/* override */
			LEFT JOIN dbo.LTX_CUST_ENTITLEMENT ce 
				ON (e.entitlement_no = ce.entitlement_no 
				AND ce.customer_no = cm.customer_no
				AND ce.cust_memb_no = cm.cust_memb_no
				AND (e.reset_type = 'M'
					OR 
					(e.reset_type = 'D' AND CONVERT(VARCHAR(10),@perf_calendar_dt,101) = ce.entitlement_date)
					OR 
					(e.reset_type = 'P' AND CAST(@perf_no as varchar(255)) = ce.entitlement_perf_no) /* MIR 6/17/2015 */
					)
					)
			LEFT JOIN (
				SELECT
					lcoe.customer_no,
					lcoe.cust_memb_no,
					lcoe.entitlement_no, 
					entitlement_key = CASE
										WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
										WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
										WHEN le.reset_type = 'M' THEN NULL
										ELSE NULL
										END,
					cust_num_used = ISNULL(SUM(lcoe.num_used),0)
				FROM dbo.LTX_CUST_ORDER_ENTITLEMENT lcoe
					JOIN dbo.LTR_ENTITLEMENT le ON lcoe.entitlement_no = le.entitlement_no
				GROUP BY lcoe.customer_no, lcoe.cust_memb_no, lcoe.entitlement_no, CASE
										WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
										WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
										WHEN le.reset_type = 'M' THEN NULL
										ELSE NULL
										END) coe
			ON (
				coe.customer_no = cm.customer_no 
				AND coe.cust_memb_no = cm.cust_memb_no
				AND coe.entitlement_no = e.entitlement_no
				AND (
					(e.reset_type = 'D' AND coe.entitlement_key = CONVERT(varchar(10),@perf_calendar_dt,101)) /* 5/12/2015 */
					OR e.reset_type = 'M'
					OR (e.reset_type = 'P' AND coe.entitlement_key = CAST(@perf_no AS varchar(255))) /* 3/16/2015 */
					)
				)
	WHERE v.perf_no = @perf_no
	AND cm.customer_no = @customer_no
	AND ( /* 5/8/2015 */
		(@price_type IS NOT NULL AND pt.id = @price_type)
		OR
		(@price_type_group IS NOT NULL AND pt.price_type_group = @price_type_group))
	AND /* current, pending, or lapsed logic */ 
		( 
		   (cm.current_status = 2 
			AND CASE WHEN e.date_to_compare = 'O' THEN @order_date ELSE @perf_calendar_dt END BETWEEN CAST(CONVERT(varchar(10),cm.init_dt,101) as datetime) AND CAST(CONVERT(varchar(10),cm.expr_dt,101) as datetime) + '23:59:59')
		OR (cm.current_status = 3
			AND CASE WHEN e.date_to_compare = 'O' THEN @order_date ELSE @perf_calendar_dt END BETWEEN DATEADD(MONTH,0-ml.renewal,CAST(CONVERT(varchar(10),cm.init_dt,101) as datetime)) AND CAST(CONVERT(varchar(10),cm.expr_dt,101) as datetime) + '23:59:59')
		OR (cm.current_status = 7 AND ISNULL(e.allow_lapsed,'Y') = 'Y'
			AND  CASE WHEN e.date_to_compare = 'O' THEN @order_date ELSE @perf_calendar_dt END BETWEEN CAST(CONVERT(varchar(10),cm.init_dt,101) as datetime) AND dbo.FS_ADD_MONTHS(CAST(CONVERT(varchar(10),cm.expr_dt,101) as datetime) + '23:59:59',ISNULL(ml.lapse_susp,0)))
		)
	/* Do not grant if membership benefits are declined, and a declinable benefit  */
	AND NOT (
		ISNULL(cm.declined_ind,'N') = 'Y'
		AND 
		ISNULL(e.decline_benefit,'Y') = 'Y')
	/* is an active entitlement */
	AND ISNULL(e.inactive,'N') = 'N'

	IF @debug = 'Y' SELECT 1, * FROM #memb_ent

	/* Where there are multiple memberships (pendings, overlaps etc.) decide what to do:
		Member-term Pendings are additive;
		With membership Overlaps we choose the highest  */
	IF ISNULL((SELECT COUNT(*) FROM #memb_ent),0) > 1
	  BEGIN
			DECLARE @daily_unused INT = 
					ISNULL((SELECT MAX(num_remain_ent) FROM #memb_ent WHERE reset_type IN ('D','P')),0)

			/* 6/12/2015 */
			DECLARE @daily_used INT = 
					ISNULL((SELECT MAX(num_used) FROM #memb_ent WHERE reset_type IN ('D','P')),0)

			DECLARE @daily_ent INT = 
					ISNULL((SELECT MAX(num_start_ent) FROM #memb_ent WHERE reset_type IN ('D','P')),0)
			
			DECLARE @membership_unused INT = 
					ISNULL((SELECT MAX(num_remain_ent) FROM #memb_ent WHERE membership_status = 3 AND reset_type = 'M'),0)
				+	ISNULL((SELECT MAX(num_remain_ent) FROM #memb_ent WHERE membership_status IN (2,7) AND reset_type = 'M'),0)

			DECLARE @membership_ent INT = 
					ISNULL((SELECT MAX(num_start_ent) FROM #memb_ent WHERE membership_status = 3 AND reset_type = 'M'),0)
				+	ISNULL((SELECT MAX(num_start_ent) FROM #memb_ent WHERE membership_status IN (2,7) AND reset_type = 'M'),0)

			IF @debug = 'Y' SELECT daily_unused = @daily_unused, daily_ent = @daily_ent, daily_used = @daily_used, membership_unused = @membership_unused, membership_ent = @membership_ent

			/* for member pendings, add the ents together */
			UPDATE #memb_ent
			SET num_remain_ent = @membership_unused,
				num_start_ent = @membership_ent
			WHERE reset_type = 'M'
			AND num_remain_ent < @membership_unused
			AND EXISTS (SELECT TOP 1 * FROM #memb_ent WHERE membership_status = 3 AND reset_type = 'M')

			/* for D,P account for used 6/12/2015 */
			UPDATE #memb_ent
			SET num_remain_ent = @daily_unused, /* - @daily_used, 6/29 */
				num_start_ent = @daily_ent,
				num_used = @daily_used
			WHERE reset_type IN ('D','P')

			IF @debug = 'Y' SELECT 2, * FROM #memb_ent

			/* remove the row that isn't the highest. */
			DELETE #memb_ent
			WHERE num_remain_ent < CASE WHEN ISNULL(@membership_unused,0) > @daily_unused THEN @membership_unused ELSE @daily_unused END /* added @daily_used 6/12/2015 */
	
			IF @debug = 'Y' SELECT 3, * FROM #memb_ent
			-- should we set memb level to "multiple"?
	  END

	SELECT
		TOP 1
		@cust_memb_no = cust_memb_no,
		@mem_level_no = mem_level_no,
		@mem_level_desc = ISNULL(mem_level_desc,''), 
		@entitlement_no = ISNULL(entitlement_no,0),
		@reset_type = ISNULL(reset_type,''),
		@num_used =  ISNULL(num_start_ent,0) - ISNULL(num_remain_ent,0),
		@num_ent = ISNULL(num_start_ent,0),
		@num_unused = ISNULL(num_remain_ent,0),
		@upgrade_downgrade = 'D',
		@target_price_type = ISNULL(price_type_id,''),
		@exceeded_price_type = @exceeded_price_type
	FROM #memb_ent
	ORDER BY num_remain_ent DESC, membership_status DESC --init_dt ASC
 END

iF @debug = 'Y' SELECT mem_level_desc = @mem_level_desc, entitlement_no = @entitlement_no, reset_type = @reset_type, num_used = @num_used, num_ent = @num_ent, num_unused = @num_unused

/* Membership in cart: (cust membership was not already identified) */
IF NULLIF(@customer_no,0) IS NULL or (@mem_level_desc IS NULL AND @memb_level_no > 0)
  BEGIN
 	SELECT
  		@mem_level_desc = ml.description,
		@entitlement_no = e.entitlement_no,
		@reset_type = e.reset_type, /* MIR 5/11/2015 */
		@num_used = 0,
		@num_ent = e.num_ent,
		@num_unused = e.num_ent,
		@upgrade_downgrade = 'U',
		@target_price_type = @price_type,
		@exceeded_price_type = @exceeded_price_type
	FROM dbo.LTR_ENTITLEMENT e
		JOIN dbo.T_MEMB_LEVEL ml ON e.memb_level_no = ml.memb_level_no
		JOIN dbo.LV_ENTITLEMENT_INV_TKW_LIST v ON v.tkw = e.ent_tkw_id
		JOIN dbo.TR_PRICE_TYPE pt ON e.ent_price_type_group = pt.price_type_group
	WHERE v.perf_no = @perf_no -- plugin knows the perf they're trying to get
	AND pt.id = @price_type
	AND ml.memb_level_no = @memb_level_no
  END
  
/* 8/13/2014: Disallow ent price types for a nobody */
IF NULLIF(@mem_level_desc,'') IS NULL 
	AND NULLIF(@memb_level_no,0) IS NULL
	AND EXISTS (SELECT * 
				FROM TR_PRICE_TYPE pt 
						JOIN dbo.LTR_ENTITLEMENT_PRICE_TYPE_GROUP eptg ON pt.price_type_group = eptg.price_type_group
				WHERE pt.id = @orig_price_type) 
  BEGIN
	SELECT
  		@mem_level_desc = '',
		@entitlement_no = 0,
		@num_used = 0,
		@num_ent = 0,
		@num_unused = 0,
		@upgrade_downgrade = CASE WHEN @upgrade_downgrade = 'Z' THEN 'Z' ELSE 'N' END /* 12/4/2014 */
  END
 
BYPASS:
 
/* Return results */
IF @return_results = 'Y'
	SELECT 
		cust_memb_no = @cust_memb_no,
		mem_level_desc = ISNULL(@mem_level_desc,''),
		entitlement_no = ISNULL(@entitlement_no,0),
		entitlement_key = CASE
								WHEN @reset_type = 'D' THEN CONVERT(VARCHAR(10),@perf_calendar_dt,101)
								WHEN @reset_type = 'P' THEN CAST(@perf_no AS VARCHAR(255))
								WHEN @reset_type = 'M' THEN NULL
								ELSE NULL 
								END,
		num_used = ISNULL(@num_used,0),
		num_ent = ISNULL(@num_ent,0),
		num_unused = ISNULL(@num_unused,0),
		upgrade_downgrade = ISNULL(@upgrade_downgrade,''),
		target_price_type = ISNULL(@target_price_type,0),
		exceeded_price_type = ISNULL(@exceeded_price_type,0),
		validation_message = ISNULL(@validation_message,'')
  
/* Output Vars */
SELECT 
	@cust_memb_no_out = @cust_memb_no,
	@mem_level_no_out = @mem_level_no,
	@mem_level_desc_out = @mem_level_desc,
	@entitlement_no_out = @entitlement_no,
	@entitlement_key_out = CASE
								WHEN @reset_type = 'D' THEN CONVERT(VARCHAR(10),@perf_calendar_dt,101)
								WHEN @reset_type = 'P' THEN CAST(@perf_no AS VARCHAR(255))
								WHEN @reset_type = 'M' THEN NULL
								ELSE NULL 
								END,
	@num_used_out = @num_used,
	@num_ent_out = @num_ent,
	@num_unused_out = @num_unused,
	@upgrade_downgrade_out = @upgrade_downgrade,
	@target_price_type_out = @target_price_type,
	@exceeded_price_type_out = @exceeded_price_type

GO


