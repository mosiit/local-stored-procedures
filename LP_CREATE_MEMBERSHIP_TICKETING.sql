USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_CREATE_MEMBERSHIP_TICKETING]    Script Date: 10/18/2019 09:41:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LP_CREATE_MEMBERSHIP_TICKETING]
(
@batch_no int = 0,
@use_org_no int,
@decline_ben_ind char(1) = null,		
@memb_level_override char(3),	
@renew_upgrade_override char(1),
@creditee_no int = null,
@sli_no int,
@cust_memb_no int = NULL OUTPUT
)	

AS

SET NOCOUNT ON

/********************************************************************************************************************
Procedure based on TP_CREATE_MEMBERSHIPS used to create memberships from ticketing orders.

Written 6/13/2012 - David Woodall, Tessitura Network Consulting.

This procedure is called from LP_PROCESS_TICKETING_MEMBERSHIPS, which looks for unprocessed sub lineitems for a membership
performance, based on perf_type.  The Level and Renew/Upgrade are required and determined by the calling procedure
from the LTR_MEMB_ZONES local system table.

Modified 6/16/2014 to work with Recipient_no if provided
Modified 9/11/2015 MIR to include 'end of month' logic if expire_end_of_month is set to 'Y'
Modified 9/17/2015 MIR additional EOM logic
Modified 11/17/2015 to fix error calculating the recognition amount.
Modified 2/5/2016 DWW - Use today's date (not order_dt) for the base date for membership start. This is more consistent with 
						memberships created via the order/contribution tab.
Modified 11/18/2016 JWB - Updated code on line 367 to determine what current status should be rather than setting it always to 2.
Modified 12/17/2018 MIR to set the init date to the first minute of day after prev expiration (as opposed to one full "DATEADD" day after prev exp, which caused '1/1/2019 23:59:..') 
Modified 5/22/2019 MIR Calculating expr_dts with ms instead of seconds to match fix #6072 in TP_CREATE_MEMBERSHIPS
Modified 10/15/2019 BPedaci - add LP_CUSTOMER_RANK call to ensure ranking occurs
*********************************************************************************************************************/

Declare @err_msg varchar(255)

If @batch_no > 0 and not exists (select * from [dbo].t_batch where batch_no = @batch_no)
  Begin
	--Print 'Invalid Batch Number'		--FP1525.
	Select @err_msg = 'Invalid batch number'
	Raiserror(@err_msg, 11, 2)
	Return -999
  End

/*------------------------- the following added to edit new optional parameters  3/2007 lwl ---------*/
If  @decline_ben_ind is not null and @decline_ben_ind not in ('N','Y')
	select @decline_ben_ind = null

If	@memb_level_override not in (select memb_level from t_memb_level where memb_org_no = @use_org_no)
  Begin
	Select @err_msg = '@memb_level_override must be provided.'
	Raiserror(@err_msg, 11, 2)
  End

if	@renew_upgrade_override is not null and @renew_upgrade_override not in ('R','U')
  Begin
	Select @err_msg = '@renew_upgrade_override must be provided.'
	Raiserror(@err_msg, 11, 2)
  End

---------------------------------------------------------------------------------------------------------

Declare 
	@customer_no int,
	@cont_amt money,
	@memb_org_no int,
	@current_expr_dt datetime,
	@current_memb_level char(3),
	@memb_mode int,	-- 1 = create new, 2 = upgrade existing, 3 = already member, extend
	@parent_no int,
	@renewal_period int,
	@expire_period int,
	@expire_end_of_month char(1), /* 9/11/2015 MIR */
	@memb_level char(3),
	@expr_period int,
	@start_amt money,
	@expr_dt datetime,
	@current_memb_amt money,
	@current_init_dt datetime,
	@init_dt datetime,
	@upgrade_amt money,
	@current_rank int,
	@new_rank int,
	@trend int,
	@category_trend int,
	@error_point int,
	@renew_upgrade_ind char(1),
	@start_dt datetime,
	@initiation int,
	@decline_ind char(1),
	@campaign_no int,
	@current_memb_level_category int,
	@new_memb_level_category int,
	@base_amt money,
	@reinstate int,
	@nrr_status char(2),
	@num_issues int, 
	@next_issue int,
	@final_issue int,
	@first_issue_sent int,
	@recd_amt money,
	@current_cust_memb_no int,
	@parent_memb_level char(3),
	@parent_memb_level_category int,
	@parent_memb_amt money,
	@parent_rank int,
	@current_status int,
	@upgrade_pending char(1),
	@cont_dt datetime,
	@deactivate_on_adjustment char(1),		-- added 8/15/2007 RCreps
	@memb_recalculate char(1),			-- added 8/15/2007 RCreps
	@current_inception_dt datetime,			-- CWR 4/19/2010 FP1560.
	@ben_provider int,							-- RWC 6/3/2010 FP1525.
	@current_recog_amt money,
	@new_recog_amt money,
	@current_add_kids int

Select @upgrade_pending = 'N'
Select @ben_provider = 0	-- FP1525.  default to 0 if no creditor


--FP1525.  cannot pass @cust_memb_no in as we may need to upgrade an existing
If @cust_memb_no is NOT NULL
  Begin
	Select @err_msg = '@cust_memb_no cannot be passed a value'
	Raiserror(@err_msg, 11, 2)
  End

If @creditee_no > 0 and Not Exists (Select * From dbo.T_CUSTOMER Where customer_no = @creditee_no)
  Begin
	Select @err_msg = 'Creditee No is not a valid constituent.'
	Raiserror(@err_msg, 11, 2)
  End

-- Processing starts here!
  Begin
	If @sli_no is null 
		or (not exists (select * from [dbo].T_SUB_LINEITEM where sli_no =@sli_no))
	  Begin
		-- Print 'Invalid Contribution Ref No.'  --FP1525.
		Select @err_msg = 'Invalid Sub Lineitem'
		Raiserror(@err_msg, 11, 2)
		Return -999
	  End

--	Pull a lot of information from the sub lineitem data.	
	Select	@customer_no = CASE WHEN ISNULL(b.recipient_no,0) > 0 then b.recipient_no else o.customer_no END,
			@ben_provider = CASE WHEN b.recipient_no <> o.customer_no THEN o.customer_no ELSE 0 END, /* 6/16/2014 MIR */
			@init_dt = NULL,
			@expr_dt = NULL,
			@memb_org_no = @use_org_no,
			@memb_level = @memb_level_override,
			@renew_upgrade_ind = @renew_upgrade_override,
			@decline_ind = 'N',
			@cont_amt = b.paid_amt,
			@campaign_no = p.campaign_no,
			@recd_amt = b.paid_amt,
			@cont_dt = getdate()		-- was o.order_dt
	From	[dbo].T_SUB_LINEITEM b (nolock)
		join dbo.T_PERF p (NOLOCK) on b.perf_no = p.perf_no
		join dbo.T_ORDER o (NOLOCK) on b.order_no = o.order_no
	Where	b.sli_no = @sli_no


	--If @creditee_no > 0
	--  Begin
	--	Select @ben_provider = @customer_no
	--	Select @customer_no = @creditee_no
	--  End

	-- now start the membership stuff
	-- get the @memb_mode value	-- 1 = create new, 2 = upgrade existing, 3 = already member, extend
	If Not Exists (Select * From [dbo].tx_cust_membership 
			Where customer_no = @customer_no and memb_org_no = @memb_org_no 
				and (cur_record = 'Y' or current_status = 3))
	  Begin
		Select @memb_mode = 1	-- not a current member
	  End
	Else
	  Begin
		-- if they are current member or pending, are they within the renewal period?
		Select	@current_expr_dt = expr_dt,
				@current_memb_level = memb_level,
				@parent_no = parent_no,
				@current_cust_memb_no = cust_memb_no,
				@current_init_dt = init_dt,
				@first_issue_sent = first_issue_id,
				@current_status = current_status,	--added CWR 4/30/2004
				@current_inception_dt = inception_dt	-- CWR 4/19/2010 FP1560.
		From	[dbo].tx_cust_membership a
		Where	customer_no = @customer_no 
			and memb_org_no = @memb_org_no	
			and (cur_record = 'Y' or current_status = 3)	-- include pendings CWR 9/16/2003
			and expr_dt = (Select 	min(expr_dt) 	-- now that we include pendings, we have to make sure we're talking about the most recent one
					From 	[dbo].tx_cust_membership
					Where	customer_no = a.customer_no
						and memb_org_no = @memb_org_no
						and (cur_record = 'Y' or current_status = 3))

		Select	@renewal_period = renewal,
				@current_memb_level_category = category
		From	[dbo].t_memb_level
		Where	memb_level = @current_memb_level
			and memb_org_no = @memb_org_no
	
		-- if we're past the renewal period and there is a pending, then we want to upgrade that min pending row, so get the details for it
		If	(@renew_upgrade_ind = 'R' OR @cont_dt >= dbo.fs_add_months(@current_expr_dt, -1 * @renewal_period))	
			and Exists (Select * from [dbo].tx_cust_membership where customer_no = @customer_no and memb_org_no = @memb_org_no 
				and current_status = 3 and expr_dt > @current_expr_dt) 
		  Begin
			Select	@upgrade_pending = 'Y'

			Select	@current_expr_dt = expr_dt,
					@current_memb_level = memb_level,
					@parent_no = parent_no,
					@current_cust_memb_no = cust_memb_no,
					@current_init_dt = init_dt,
					@first_issue_sent = first_issue_id,
					@current_status = current_status	--added CWR 4/30/2004
			From	[dbo].tx_cust_membership a
			Where	customer_no = @customer_no 
				and memb_org_no = @memb_org_no	
				and current_status = 3	
				and expr_dt = (Select 	min(expr_dt) 	
						From 	[dbo].tx_cust_membership
						Where	customer_no = a.customer_no
							and memb_org_no = @memb_org_no
							and current_status = 3)
		  End


		------ New code to make renewal_upgrade_ind and memb_level_override work ---- 03/2007 lwl
		If Coalesce(@renew_upgrade_ind, '') = 'U'			-- added Coalesce CWR 5/19/2008 FP1211.
			OR (Coalesce(@renew_upgrade_ind, '') <> 'R' and @cont_dt < dbo.fs_add_months(@current_expr_dt, -1 * @renewal_period))		-- added Coalesce CWR 5/19/2008 FP1211.
	        OR @upgrade_pending = 'Y'
		  Begin
		    Select @memb_mode = 2 -- upgrade
		  End
		Else
		  Begin
			If @memb_level is null
			  Begin
				Select	TOP 1 @memb_level = memb_level			-- top 1 added CWR 11/20/2009 FP1458.
				From	[dbo].t_memb_level
				Where	memb_org_no = @memb_org_no
					and @cont_amt between start_amt and end_amt
					and Coalesce(inactive, 'N') = 'N' -- this line added CWR 11/5/2002
				Order by memb_level		-- added CWR 11/20/2009 FP1458.
		      End
			--possible sticking point for recalculating membership if a memb_level is found.  We don't want the membership to renew for a simple recalculation on levels. (RCreps 8/16/2007)
			If @memb_level is not null
			    Select @memb_mode = 3 -- renew
			Else -- we can't find a memb_level for this amount, so upgrade
			    Select @memb_mode = 2 -- upgrade
		  End	
	  End

	If @memb_mode = 1	-- create new membership
	  Begin
	
		-- get memb level information
		Select 	@start_amt = start_amt,
				@expire_period = expiry,
				@expire_end_of_month = expire_end_of_month, /* 9/11/2015 MIR */
				@start_dt = start_dt,
				@initiation = initiation,
				@base_amt = min_pmt_amt,
				@reinstate = reinstate,
				@num_issues = num_issues
		From	[dbo].t_memb_level
		Where	memb_org_no = @memb_org_no
			and memb_level = @memb_level


		-- get init date
		If @init_dt is null
		  Begin
			If @initiation = 1		-- first of next month
				If datepart(mm, @cont_dt) <> 12
					Select	@init_dt = Convert(datetime, convert(varchar, datepart(mm, @cont_dt) + 1) + '/1/' + convert(varchar, datepart(yy, @cont_dt)) )
				Else
					Select	@init_dt = Convert(datetime, '1/1/' + convert(varchar, datepart(yy, @cont_dt) + 1) )
			If @initiation = 2		-- today
				Select	@init_dt = Convert(datetime, convert(varchar, datepart(mm, @cont_dt)) + '/' + convert(varchar, datepart(dd, @cont_dt)) + '/' + convert(varchar, datepart(yy, @cont_dt)) )
			If @initiation = 3		-- first of this month
				Select	@init_dt = Convert(datetime, convert(varchar, datepart(mm, @cont_dt)) + '/1/' + convert(varchar, datepart(yy, @cont_dt)) )
			If @initiation = 4		-- specified date
				Select	@init_dt = @start_dt
		  End

		-- are they new or reinstate?
		If exists (select * from [dbo].tx_cust_membership where customer_no = @customer_no 
				and current_status not in (3,8)
				and memb_org_no = @memb_org_no			-- condition added CWR 7/3/2007
				and dbo.fs_add_months(expr_dt, @reinstate) > @init_dt)
			Select @nrr_status = 'RI'	--reinstate
		Else
		  Begin
			Select @nrr_status = 'NE'	-- consider them new
		  End
	
		-- get expiration date, if it wasn't specified in order_contribution table
		If @expr_dt is null
		  Begin /* EOM logic added 9/11/2015 */
			If @expire_end_of_month = 'Y'
			  Begin
				Select @expr_dt = Dateadd(ms,-3,Dateadd(mm, @expire_period, @init_dt))
				Select @expr_dt = dateadd(mm, 1, @expr_dt)
				Select @expr_dt = Dateadd(ms,-3,convert(datetime, convert(varchar, datepart(yy, @expr_dt)) + '-' + convert(varchar, datepart(mm, @expr_dt)) + '-1'))
			  End
			Else
			  Begin
				Select	@expr_dt = Dateadd(mm, @expire_period, @init_dt)
				Select	@expr_dt = dateadd(ms, -3, convert(varchar(10), @expr_dt, 121))  
			  End
		  End
	
		-- get next id for membership record
		If @memb_level is not null	-- This will always be true....
		  Begin
		  
			Exec @cust_memb_no = [dbo].ap_get_nextid_function 'CM'
		
			-- commented CWR 4/19/2010 FP1560.  Inception_dt should be the current date for a new membership
			--Select 	@inception_dt = min(inception_dt) 
			--From 	[dbo].tx_cust_membership 
			--Where	customer_no = @customer_no and memb_org_no = @memb_org_no and inception_dt is not null
		
			Insert	[dbo].tx_cust_membership(
				cust_memb_no,
				parent_no,
				customer_no,
				campaign_no,
				memb_org_no,
				memb_level,
				memb_amt,
				ben_provider,
				init_dt,
				expr_dt,
				current_status,
				NRR_status,
				memb_trend,
				declined_ind,
				AVC_amt,
				orig_expiry_dt,
				orig_memb_level,
				inception_dt,
				mir_lock,
				cur_record,
				recog_amt,
				category_trend,
				first_issue_id,
				final_issue_id,
				num_copies)
			Values(	@cust_memb_no,
				0,
				@customer_no,
				@campaign_no,
				@memb_org_no,
				@memb_level,
				@start_amt,
				@ben_provider,		-- replaces "0", so we can create a gift membership FP1525.
				@init_dt,
				@expr_dt,
				CASE WHEN @init_dt > @cont_dt THEN 3 ELSE CASE WHEN @recd_amt >= @base_amt THEN 2 ELSE 4 END END,--2,				-- New memberships are always active.
				@nrr_status,	-- new
				4,
				@decline_ind,
				CASE WHEN @cont_amt > @start_amt then @cont_amt - @start_amt else 0 end,	-- Allow cont_amt to be less than start_amt (discounts)
				@expr_dt,
				@memb_level,
				--Coalesce(@inception_dt, @init_dt), 
				GetDate(),		-- replaces above CWR 4/19/2010 FP1560.
				@batch_no,
				CASE WHEN @init_dt > @cont_dt THEN 'N' ELSE 'Y' END,
				@cont_amt,
				4,
				@next_issue,
				@final_issue,
				@num_issues)
	
			If @@error > 0
			  Begin
				Select @error_point = 4
				GOTO Error
			  End
		
		  End

	  End	-- New membership

	If @memb_mode = 2	-- upgrade existing membership  
	  Begin
		
		-- we're going to update the existing row
		Select 	@cust_memb_no = @current_cust_memb_no
	
		-- check to see if we deactivate the membership on an adjustment (8/15/2007 RCreps)
		Select @deactivate_on_adjustment = coalesce(deactivate_on_adjust_ind,'N')
		From [dbo].T_MEMB_ORG
		Where memb_org_no = @memb_org_no
		
		-- get memb amt spent so far, based on membership amounts, not the discounted amount in recog_amt.
		-- Also get the current recog amount, so we can up date it.
		-- Figure out if any kids have been added, by comparing the num_copies to the memb level
		Select 	@current_memb_amt = a.memb_amt+a.AVC_amt,
				@current_recog_amt = a.recog_amt,
				@current_add_kids = a.num_copies - ml.num_issues
		From	[dbo].tx_cust_membership a
			JOIN [dbo].T_MEMB_LEVEL ml on a.memb_level = ml.memb_level
		Where	a.cust_memb_no = @current_cust_memb_no
	
		--select memb_level = @memb_level, cont_amt = @cont_amt, current_amt = @current_memb_amt, cust_memb_no = @current_cust_memb_no	-- cwr debug

		-- get new memb level information that we are upgrading to
		Select 	@start_amt = start_amt,
				@expire_period = expiry,
				@expire_end_of_month = expire_end_of_month, /* 9/17/2015 MIR */
				@start_dt = start_dt,
				@initiation = initiation,
				@new_memb_level_category = category,
				@base_amt = min_pmt_amt,
				@num_issues = num_issues
		From	[dbo].t_memb_level
		Where	memb_org_no = @memb_org_no
			and memb_level = @memb_level

		-- The upgrade amount is set to the steart amount of the new membership. We also get a new recog amount by
		-- adding the new paid amount to the recog value in the membership. 
		Select	@upgrade_amt = @start_amt,
				@new_recog_amt = @current_recog_amt + @cont_amt

		-- if there is a parent_no, then get the parent_details and figure out the trends
		If Coalesce(@parent_no, 0) > 0 
		  Begin
			Select	@parent_memb_level = a.memb_level,
				@parent_memb_level_category = Coalesce(b.category, 0)
			From	[dbo].tx_cust_membership a
				JOIN [dbo].t_memb_level b ON a.memb_level = b.memb_level and a.memb_org_no = b.memb_org_no
			Where	cust_memb_no = @parent_no

			-- get parent memb_amounts again, based on member and AVC amounts.
			Select 	@parent_memb_amt = sum(a.memb_amt+a.AVC_amt)
				From	[dbo].tx_cust_membership a
				Where	a.cust_memb_no = @parent_no


			-- calculate macro trend
			Select 	@trend = 1

			If	@parent_memb_level <> @memb_level
			  Begin
				Select	@parent_rank = rank 
				From 	[dbo].t_memb_level 
				Where 	memb_org_no = @memb_org_no and memb_level = @parent_memb_level
		
				Select	@new_rank = rank 
				From 	[dbo].t_memb_level 
				Where 	memb_org_no = @memb_org_no and memb_level = @memb_level
		
				If @new_rank > @parent_rank
					Select @trend = 3
				If @new_rank < @parent_rank
					Select @trend = 2
			  End
			Else
			  Begin
				If @parent_memb_amt > @upgrade_amt
					Select @trend = 5		-- decrease
				If @parent_memb_amt < @upgrade_amt
					Select @trend = 6		-- increase
			  End
		
			-- calculate micro trend
			If	@parent_memb_level_category = @new_memb_level_category
			  Begin
				Select 	@category_trend = 1
				If	@parent_memb_level <> @memb_level
				  Begin
					If @new_rank > @parent_rank
						Select @category_trend = 3
					If @new_rank < @parent_rank
						Select @category_trend = 2
				  End
				Else
				  Begin
					If @parent_memb_amt > @upgrade_amt
						Select @category_trend = 5
					If @parent_memb_amt < @upgrade_amt
						Select @category_trend = 6
				  End
			  End
			Else
				--Select @trend = 4
				Select @category_trend = 4			-- changed from @trend CWR 10/23/2008 FP882.
		  End
		Else		-- there is no parent row, so we use the existing values on the row
		  Begin
			Select 	@category_trend = category_trend,
				@trend = memb_trend
			From	[dbo].tx_cust_membership
			Where	cust_memb_no = @cust_memb_no
		  End

		-- get expiration date
		If @expr_dt is null
		  Begin /* EOM logic added 9/11/2015 */
			If @expire_end_of_month = 'Y'
			  Begin
				Select @expr_dt = Dateadd(ms,-3,Dateadd(mm, @expire_period, @current_init_dt))
				Select @expr_dt = dateadd(mm, 1, @expr_dt)
				Select @expr_dt = Dateadd(ms,-3,convert(datetime, convert(varchar, datepart(yy, @expr_dt)) + '-' + convert(varchar, datepart(mm, @expr_dt)) + '-1'))
			  End
			Else
			  Begin
				Select	@expr_dt = Dateadd(mm, @expire_period, @current_init_dt)
				Select	@expr_dt = dateadd(ms, -3, convert(varchar(10), @expr_dt, 121))  
			  End
		  End


		-- Update the existing membership
		Update	[dbo].TX_CUST_MEMBERSHIP
		Set	memb_level = @memb_level,
			expr_dt = @expr_dt,
			memb_amt = @start_amt,
			avc_amt = 0,									-- Always use the start amount only for membership amounts.
			memb_trend = @trend,
			category_trend = @category_trend,
			num_copies = @num_issues + @current_add_kids,	-- Update the number of people, maintaining any extra add on kids.	
			recog_amt = @new_recog_amt,						-- This is the old recog + new paid amount
			ben_provider = @ben_provider /* 6/16/2014 MIR */
		Where cust_memb_no = @cust_memb_no
		
		If @@error > 0
		  Begin
			Select @error_point = 6
			GOTO Error
		  End

	  End	-- Upgrade existing membership
	
	
	If @memb_mode = 3	-- add new membership to already existing member
	  Begin
		
		-- get memb level information
		If @memb_level is null
			Select TOP 1 @memb_level = memb_level,		-- top 1 added CWR 11/20/2009 FP1458.
						@start_amt = start_amt,
						@expire_period = expiry,
						@start_dt = start_dt,
						@initiation = initiation,
						@new_memb_level_category = category,
						@base_amt = min_pmt_amt,
						@num_issues = num_issues
			From	[dbo].t_memb_level
			Where	memb_org_no = @memb_org_no
				and @cont_amt between start_amt and end_amt
				and Coalesce(inactive, 'N') = 'N'		-- CWR 11/15/2002
			Order by memb_level				-- CWR 11/20/2009 FP1458.
		Else	-- we have specified a memb_level so get the info from that level
			Select 	@start_amt = start_amt,
					@expire_period = expiry,
					@expire_end_of_month = expire_end_of_month, /* 9/17/2015 MIR */
					@start_dt = start_dt,
					@initiation = initiation,
					@new_memb_level_category = category,
					@base_amt = min_pmt_amt,
					@num_issues = num_issues
			From	[dbo].t_memb_level
			Where	memb_org_no = @memb_org_no
				and memb_level = @memb_level


		-- check to see if there are other pending memberships, if so then this one goes after existing ones		
		If Exists (Select * 
			From 	[dbo].tx_cust_membership 
			Where 	customer_no = @customer_no
				and memb_org_no = @memb_org_no
				and current_status = 3)
	
		  Begin

			Select 	@current_expr_dt = max(expr_dt) 
			From 	[dbo].tx_cust_membership 
			Where 	customer_no = @customer_no
				and memb_org_no = @memb_org_no
				and current_status = 3

			Select	@current_memb_level = memb_level,
					@current_cust_memb_no = cust_memb_no,
					@current_init_dt = init_dt,
					@final_issue = final_issue_id,
					@parent_no = parent_no
			From	[dbo].tx_cust_membership
			Where	customer_no = @customer_no 
				and memb_org_no = @memb_org_no	
				and expr_dt = @current_expr_dt			

			Select	@current_memb_level_category = category
			From	[dbo].t_memb_level
			Where	memb_level = @current_memb_level
				and memb_org_no = @memb_org_no

		  End


		-- get memb amt spent so far (to calculate trend)
		Select 	@current_memb_amt = sum(a.memb_amt+a.AVC_amt)
		From	[dbo].tx_cust_membership a
		Where	a.cust_memb_no = @current_cust_memb_no
	
		-- calculate macro trend
		Select 	@trend = 1
		If	@current_memb_level <> @memb_level
		  Begin
			Select	@current_rank = rank 
			From 	[dbo].t_memb_level 
			Where 	memb_org_no = @memb_org_no and memb_level = @current_memb_level
	
			Select	@new_rank = rank 
			From 	[dbo].t_memb_level 
			Where 	memb_org_no = @memb_org_no and memb_level = @memb_level
	
			If @new_rank > @current_rank
				Select @trend = 3
			If @new_rank < @current_rank
				Select @trend = 2
		  End
		Else
		  Begin
			If @current_memb_amt > @cont_amt
				Select @trend = 5
			If @current_memb_amt < @cont_amt
				Select @trend = 6
		  End



	
		-- calculate micro trend
		If	@current_memb_level_category = @new_memb_level_category
		  Begin
			Select 	@category_trend = 1
			If	@current_memb_level <> @memb_level
			  Begin
				If @new_rank > @current_rank
					Select @category_trend = 3
				If @new_rank < @current_rank
					Select @category_trend = 2
			  End
			Else
			  Begin
				If @current_memb_amt > @cont_amt
					Select @category_trend = 5
				If @current_memb_amt < @cont_amt
					Select @category_trend = 6
			  End
		  End
		Else
			Select @category_trend = 4			-- changed from @trend CWR 10/23/2008 FP882.


		--1           Same                           NULL                 metqa1     1997-06-30 08:41:24.180                 1997-08-05 13:10:14.210 N
		--2           Upgrade                        NULL                 metqa1     1997-06-30 08:41:24.240                 1997-08-05 13:10:14.210 N
		--3           Downgrade                      NULL                 metqa1     1997-06-30 08:41:24.240                 1997-08-05 13:10:14.210 N
		--4           New                            NULL                 metqa1     1997-06-30 08:41:24.240 creif           2006-01-05 15:17:34.177 Y
		--5           Decrease                       NULL                 metqa1     1997-06-30 08:41:24.290                 1997-08-05 13:10:14.260 N
		--6           Increase                       NULL                            1997-08-05 13:10:14.260                 1997-08-05 13:10:14.260 N

		-- get init date MIR 12/17/2018 set to the beginning of the day.
		--Select	@init_dt = Dateadd(dd, 1, @current_expr_dt)
		SELECT @init_dt = CAST(DATEADD(dd, 1, CAST(@current_expr_dt as DATE)) as DATETIME)
	
		-- get expiration date
		/* EOM logic added 9/11/2015 */
		If @expire_end_of_month = 'Y'
		  BEGIN
			Select @expr_dt = Dateadd(ms,-3,Dateadd(mm, @expire_period, @init_dt))
			Select @expr_dt = dateadd(mm, 1, @expr_dt)
			Select @expr_dt = Dateadd(ms,-3,convert(datetime, convert(varchar, datepart(yy, @expr_dt)) + '-' + convert(varchar, datepart(mm, @expr_dt)) + '-1'))
		  END
		Else
		  BEGIN
			Select	@expr_dt = Dateadd(mm, @expire_period, @init_dt)
			Select	@expr_dt = dateadd(ms, -3, convert(varchar(10), @expr_dt, 121))  -- CWR 9/9/2011 FP1817. replaces line above
		  END

		-- get next id for membership record
		Exec @cust_memb_no = [dbo].ap_get_nextid_function 'CM'
	
		-- this commented CWR 4/19/2010 FP1560. as we now get inception_dt from the renewed membership above
		--select 	@inception_dt = min(inception_dt) from [dbo].tx_cust_membership 
		--where	customer_no = @customer_no and memb_org_no = @memb_org_no and inception_dt is not null
	
		insert	dbo.TX_CUST_MEMBERSHIP(
			cust_memb_no,
			parent_no,
			customer_no,
			campaign_no,
			memb_org_no,
			memb_level,
			memb_amt,
			ben_provider,
			init_dt,
			expr_dt,
			current_status,
			NRR_status,
			memb_trend,
			declined_ind,
			AVC_amt,
			orig_expiry_dt,
			orig_memb_level,
			inception_dt,
			mir_lock,
			cur_record,
			recog_amt,
			category_trend,
			first_issue_id,
			final_issue_id,
			num_copies)
		values(	@cust_memb_no,
				@current_cust_memb_no,
				@customer_no,
				@campaign_no,
				@memb_org_no,
				@memb_level,
				@start_amt,
				@ben_provider,
				@init_dt,
				@expr_dt,
				CASE WHEN @init_dt > @cont_dt THEN 3 ELSE 2 END,	-- Pending or Active as status
				'RN',	-- renew
				@trend,
				@decline_ind,
				CASE WHEN @cont_amt > @start_amt then @cont_amt - @start_amt else 0 end,	-- Allow cont_amt to be less than start_amt (discounts)
				@expr_dt,
				@memb_level,
				Coalesce(@current_inception_dt, getdate()),		
				@batch_no,
				CASE WHEN @init_dt > @cont_dt THEN 'N' ELSE 'Y' END,	-- Current record?
				@cont_amt,
				@category_trend,
				@next_issue,
				@final_issue,
				@num_issues)
	
		If @@error > 0
		  Begin
			Select @error_point = 8
			GOTO Error
		  End
	
		-- If we just added a current record, then make any other cur records not current
		If (Select cur_record from [dbo].tx_cust_membership where cust_memb_no = @cust_memb_no) = 'Y'
		  Begin
			Update	[dbo].tx_cust_membership
			Set	current_status = 1,
				cur_record = 'N'
			Where	memb_org_no = @memb_org_no
				and cur_record = 'Y'
				and cust_memb_no <> @cust_memb_no 
				and customer_no = @customer_no		-- added CWR 11/19/2002 
		  End
	

		-- If the macro trend is upgrade (2) then we have to upgrade the existing membership, if it's still current
		-- added CWR 3/16/2005
		If @trend = 2 
		  Begin
			Update	dbo.TX_CUST_MEMBERSHIP
			Set	memb_level = @memb_level
			Where	memb_org_no = @memb_org_no
				and cur_record = 'Y'
				and cust_memb_no = @current_cust_memb_no 
				and customer_no = @customer_no		
				and (select Coalesce(retroactive_upgrade_ind, 'N') from [dbo].t_memb_org where memb_org_no = @memb_org_no) = 'Y'	-- CWR 1/29/2006
	
			
			If @@error > 0
			  Begin
				Select @error_point = 10
				GOTO Error
			  End
		  End
	  End		-- Add membership to existing member

-- ADDED LP_CUSTOMER_RANK CALL TN Support BPedaci 10/15/2019
EXEC LP_CUSTOMER_RANK @customer_no = @customer_no
	
	RETURN 0
END

Error:
	--FP1525.  Now raiserror here as well.
	Select @err_msg = Coalesce(@err_msg, 'An error occurred using ticket sli_no = ' + Coalesce(convert(varchar, @sli_no), '#N/A'))
	Raiserror(@err_msg, 11, 2)
	RETURN -999
