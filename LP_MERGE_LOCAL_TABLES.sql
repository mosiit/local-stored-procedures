USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_MERGE_LOCAL_TABLES]    Script Date: 3/21/2017 4:22:33 PM ******/
DROP PROCEDURE [dbo].[LP_MERGE_LOCAL_TABLES]
GO

/****** Object:  StoredProcedure [dbo].[LP_MERGE_LOCAL_TABLES]    Script Date: 3/21/2017 4:22:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_MERGE_LOCAL_TABLES]
       @merge_type CHAR(1) = 'I', -- (I)nitial, (D)ate
       @merge_date DATETIME,
       @results CHAR(1) = 'N' -- (N)o to omit results (as with a job), (Y)es to produce results
-- ===============================================================================================================
-- H. Sheridan, 1/11/2017
--
-- Until now, we hadn't coded local tables into the Tessitura merge process.  This left all local tables that
-- include 'customer_no' with the wrong ID's.  This stored proc was written to fix those values.  It calls
-- LP_CONST_MERGE, which has been coded to include all local tables with customer_no.  
--
-- When running this stored proc for the first time, don't include a value for the @merge_date parameter.  That
-- will ensure that all local tables not properly merged in the past will be fixed.
--
-- Although there's a mechanism in place to catch new local tables that use customer_no, we might need to run
-- this stored proc as a one-off to fix records already merged before we could change the merge code.  If that
-- happens, use the date that the local table went into production for @merge_date.  Repeat for each day if
-- it's been several days.
--
-- If you want to see the results of this stored proc, set @results to 'Y'.  The results include whether the
-- kept/deleted ID's were successful or failed in the local table merge process.  If running this as a job, 
-- set @results to 'N'.
--
-- Sample calls (the value for @results doesn't matter, exepct if run as a job):  
--
--		Initial:  EXECUTE LP_MERGE_LOCAL_TABLES @merge_date = NULL, @results = 'Y'
--		New Table Added:  EXECUTE LP_MERGE_LOCAL_TABLES @merge_date = '2017-01-11 23:59:59.997', @results = 'N'
--
-- ===============================================================================================================
AS 
       BEGIN

			 SET NOCOUNT ON

             DECLARE @cnt INT,
                     @max INT,
                     @seq INT,
                     @kept INT,
                     @del INT

             DECLARE @msg VARCHAR(500)

             DECLARE @tblSuccess TABLE (row_nbr INT,
                                        seq_no INT,
                                        kept_id INT,
                                        delete_id INT)

             DECLARE @tblMatch TABLE (row_nbr INT,
                                      seq_no INT,
                                      kept_id INT,
                                      delete_id INT,
                                      multijump INT)

             DECLARE @tblMsgs TABLE ([status] VARCHAR(30),
                                     msg VARCHAR(500))	

             IF @merge_type = 'I' 
                INSERT  INTO @tblSuccess
                        SELECT  ROW_NUMBER() OVER (ORDER BY seq_no ASC) AS row_nbr,
                                seq_no,
                                kept_id,
                                delete_id
                        FROM    T_MERGED
                        WHERE   [status] = 'S'
                        ORDER BY seq_no

             IF @merge_type = 'D' 
                INSERT  INTO @tblSuccess
                        SELECT  ROW_NUMBER() OVER (ORDER BY seq_no ASC) AS row_nbr,
                                seq_no,
                                kept_id,
                                delete_id
                        FROM    T_MERGED
                        WHERE   [status] = 'S'
						-- Takes care of anything up to and beyond date local table was created
                        AND		merge_dt >= @merge_date
                        ORDER BY seq_no

             SELECT @cnt = 1

             SELECT @max = COUNT(row_nbr)
             FROM   @tblSuccess

			 SELECT @msg = 'Total = ' + CONVERT(VARCHAR(10), @max)
			 RAISERROR(@msg, 10,1) WITH NOWAIT

             WHILE @cnt <= @max 
                   BEGIN

                         SELECT @kept = kept_id,
                                @del = delete_id
                         FROM   @tblSuccess
                         WHERE  row_nbr = @cnt

                         BEGIN TRY

								-- Code that changes the table values
                               EXECUTE [dbo].[LP_CONST_MERGE] @merge_stage = 'A', @kept_id = @kept, @deleted_id = @del

                               INSERT   INTO @tblMsgs
                                        SELECT  'Success',
                                                'Merged kept ID ' + CONVERT(VARCHAR(30), @kept) + ' and delete ID ' + CONVERT(VARCHAR(30), @del)

                         END TRY

                         BEGIN CATCH

                               INSERT   INTO @tblMsgs
                                        SELECT  'Failed',
                                                'Failed to merge kept ID ' + CONVERT(VARCHAR(30), @kept) + ' and delete ID ' + CONVERT(VARCHAR(30), @del)

                         END CATCH
					
                         SELECT @cnt = @cnt + 1

						 IF @cnt%100 = 0 
							BEGIN
								SELECT @msg = 'Processed ' + CONVERT(VARCHAR(10), @cnt) + ' records'
								RAISERROR(@msg, 10,1) WITH NOWAIT
							END
                   END

			IF @results = 'Y'
				SELECT * FROM @tblMsgs

       END


GO


