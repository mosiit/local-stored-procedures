USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_SHOW_AND_GO_MOVE]    Script Date: 1/31/2019 12:09:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_SHOW_AND_GO_MOVE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_SHOW_AND_GO_MOVE] AS' 
END
GO

-- =============================================
-- Author:		Jonathan Smillie, Tessitura Network
-- Create date: April 23-26, 2016
-- Description:	
-- Updated 1/31/2019 - MS - Added doors_open and doors_close to the fields being updated
--                          Added @redo_today parameter for testing purposes so that today's record can be reprocessed
-- =============================================
ALTER PROCEDURE [dbo].[LP_SHOW_AND_GO_MOVE]
	@season_no INT = NULL, 
	@perf_type INT = NULL,
    @redo_today CHAR(1) = 'N'
AS BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF ISNULL(@perf_type,0) <= 0 OR ISNULL(@season_no,0) <= 0 RETURN;

    /*  CTE to find and update Show-and-Go performance based on performance date and performance type 
        Expects a setting for Show-and-Go performance type Looks for a performance in an active season 
        based on inactive flag and start/end date.  If it finds it, moves Show-and-Go performance date,
        as well as the doors open and closed dates to today's date  */
    WITH perf_to_change_cte 
    AS (SELECT perf_no, perf_dt, season 
        FROM t_perf a 
             JOIN tr_season b ON a.season = b.id 
        WHERE perf_type = @perf_type AND b.id = @season_no 
          AND (CAST(a.perf_dt AS DATE) >= CAST(b.start_dt AS DATE)   -- Specify value of performance type for Show-and-Go performances 
               AND CAST(a.perf_dt AS DATE)  < CAST(b.end_dt AS DATE)  
               AND (CAST (a.perf_dt AS DATE) <> CAST(GETDATE() AS DATE) OR @redo_today = 'Y')
               AND b.inactive = 'N') 
       )
    UPDATE t_perf
    SET perf_dt = CONVERT(VARCHAR,CAST(GETDATE() AS DATE)) + ' 23:30:00',
        doors_open = CAST(GETDATE() AS DATE),
        doors_close = CONVERT(VARCHAR,CAST(GETDATE() AS DATE)) + ' 23:59:59'
        FROM perf_to_change_cte a 
             JOIN t_perf b ON a.perf_no = b.perf_no;

    --IF @@ROWCOUNT <> 0
    --    SELECT perf_no, perf_dt, doors_open, doors_close FROM t_perf WHERE perf_type = @perf_type AND season = @season_no;
        
END

GO

    EXECUTE [dbo].[LP_SHOW_AND_GO_MOVE] @season_no = 6, @perf_type = 16, @redo_today = 'Y';
    
    --SELECT perf_dt, doors_open, doors_close FROM T_PERF WHERE perf_no = 43238
    --SELECT perf_no, perf_dt, doors_open, doors_close FROM t_perf WHERE perf_type = 16 AND season = 6;



    --SELECT perf_no, perf_dt, season 
    --FROM t_perf a 
    --    JOIN tr_season b ON a.season = b.id 
    --WHERE perf_type = 16 AND b.id = 6 
    --      AND CAST(a.perf_dt AS DATE) >= CAST(b.start_dt AS DATE)   -- Specify value of performance type for Show-and-Go performances 
    --      AND CAST(a.perf_dt AS DATE)  < CAST(b.end_dt AS DATE)  
    --      AND b.inactive = 'N'
          