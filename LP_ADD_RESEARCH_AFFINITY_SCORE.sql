USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_ADD_RESEARCH_AFFINITY_SCORE]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

CREATE TABLE #AffinityTbl
(
	customer_no INT, 
	membership_Score INT DEFAULT 0,
	attendance_Score INT DEFAULT 0,
	boardCommitteeVolunteer_Score INT DEFAULT 0,
	memberSociety_Score INT DEFAULT 0, 
	numOfGifts_Score INT DEFAULT 0,
	recencyOfGift_Score INT DEFAULT 0,
	totalScore INT DEFAULT 0,
	affinityScore INT DEFAULT 0 
)


-----------------------------------------------------------------------------------
-- GET THE MEMBERSHIP SCORE 
-- 30 points given if a currently a Member (Household or AF) AND has had a membership for 5 or more years
-----------------------------------------------------------------------------------

INSERT INTO #AffinityTbl
(
    customer_no,
    membership_Score
)
SELECT m.customer_no, 
	CASE 
		WHEN COUNT(DISTINCT per.fyear) < 5 THEN 0
		ELSE 30
	END AS membership_Score
FROM dbo.TX_CUST_MEMBERSHIP m
	INNER JOIN dbo.TR_Batch_Period per
		ON m.init_dt BETWEEN per.start_dt AND per.end_dt
	INNER JOIN 
	(	-- must currently have an Active Household or AF Membership
		SELECT customer_no FROM dbo.TX_CUST_MEMBERSHIP WHERE current_status = 2 AND memb_org_no IN (4, 5, 24, 26)
	) X
	ON X.customer_no = m.customer_no
	WHERE memb_org_no IN (4, 5, 24, 26) -- Household Membership, AF Membership, AF_L Membership, Innovator Membership
		AND current_status IN (1, 2, 9) -- Inactive, Active, Merged
GROUP BY m.customer_no;

-----------------------------------------------------------------------------------
-- GET THE ATTENDANCE SCORE 
-- 25 points if 4+ events/scans in past 365 days, for Individuals and Households
-----------------------------------------------------------------------------------
WITH AttendScore
AS 
(
	SELECT customer_no, COUNT(*) AS cnt,
		CASE 
			WHEN COUNT(*) < 4 THEN 0
			ELSE 25
		END AS attendance_Score 
	FROM
	(	-- Elevated Events Attendance 
		SELECT DISTINCT a.customer_no, CAST(e.event_dt AS DATE) event_dt
		FROM V_CUSTOMER_WITH_PRIMARY_AFFILIATES AS a
		INNER JOIN TX_EVENT_GUEST AS g
			ON a.expanded_customer_no = g.guest_cust_no
		INNER JOIN T_CAMPAIGN AS e
			ON g.campaign_no = e.campaign_no
		INNER JOIN T_CUSTOMER AS c
			ON a.customer_no = c.customer_no
			AND c.cust_type IN (1, 7) -- Individual, Household
		WHERE g.customer_no > 0
			AND (g.seat_no IS NULL OR g.seat_no <> 'N' )
			AND DATEDIFF(d, e.event_dt, GETDATE()) <= 365 -- e.event_dt in the last year

		UNION ALL 

		-- NSCAN Attendance 
		SELECT DISTINCT a.customer_no, CAST(a.attend_dt AS DATE) attend_dt
		FROM dbo.T_ATTENDANCE a
		INNER JOIN dbo.T_PERF p
			ON p.perf_no = a.perf_no 
			AND p.perf_type IN (2, 4)  -- Public, Member Event
		INNER JOIN T_CUSTOMER AS c
			ON a.customer_no = c.customer_no
			AND c.cust_type IN (1, 7) -- Individual, Household
		WHERE a.customer_no <> 0  
			AND DATEDIFF(d, a.attend_dt, GETDATE()) <= 365 -- attend_dt in the last year
	) X
	GROUP BY X.customer_no
)
MERGE #AffinityTbl Aff
USING AttendScore as atnd
ON (Aff.customer_no = atnd.customer_no)
WHEN MATCHED THEN 
	UPDATE SET Aff.attendance_Score = atnd.attendance_Score
WHEN NOT MATCHED BY TARGET THEN
	INSERT (customer_no, attendance_Score) 
	VALUES (atnd.customer_no, atnd.attendance_Score);


-----------------------------------------------------------------------------------
-- GET THE BOARD/COMMITTEE/VOLUNTEER SCORE 
-- 20 points given if currently either Board/Committee or a Volunteer
-----------------------------------------------------------------------------------
WITH BCVScore
AS
(	-- Volunteer
	SELECT DISTINCT a.customer_no, 20 AS boardCommitteeVolunteer_Score
	FROM V_CUSTOMER_WITH_PRIMARY_AFFILIATES AS a
	INNER JOIN dbo.TX_CONST_CUST c
		ON a.expanded_customer_no = c.customer_no
	WHERE constituency = 15 -- Volunteer
		AND end_dt IS NULL
		
	UNION 
	-- Board/Committee
	SELECT DISTINCT a.customer_no, 20 AS boardCommitteeVolunteer_Score
	FROM V_CUSTOMER_WITH_PRIMARY_AFFILIATES AS a
	INNER JOIN T_AFFILIATION aff
		ON a.expanded_customer_no = aff.individual_customer_no
		AND aff.affiliation_type_id IN (10086, 10088, 10087, 10090, 10089,  10096)
	--10086	Active Trustee
	--10087	Overseer Emeriti
	--10088	Active Overseer
	--10089	Life Trustee
	--10090	Trustee Emeriti
	--10096	Active Committee
)
MERGE #AffinityTbl Aff
USING BCVScore bcv
ON (Aff.customer_no = bcv.customer_no)
WHEN MATCHED THEN
	UPDATE SET Aff.boardCommitteeVolunteer_Score = bcv.boardCommitteeVolunteer_Score
WHEN NOT MATCHED BY TARGET THEN
	INSERT (customer_no, boardCommitteeVolunteer_Score)
	VALUES (bcv.customer_no, bcv.boardCommitteeVolunteer_Score);   

-----------------------------------------------------------------------------------
-- GET THE MEMBER_SOCIETY SCORE 
-- 10 points if currently member of any Society
-----------------------------------------------------------------------------------
WITH MembScore
AS
(
	SELECT DISTINCT customer_no, 10 AS memberSociety_Score 
	FROM dbo.TX_CUST_MEMBERSHIP
	WHERE memb_org_no IN (4, 5, 24, 26) -- Household Membership, AF Membership, AF_L Membership, Innovator Membership
	AND current_status = 2 
)
MERGE #AffinityTbl Aff
USING MembScore mem
ON (Aff.customer_no = mem.customer_no)
WHEN MATCHED THEN
	UPDATE SET Aff.memberSociety_Score = mem.memberSociety_Score
WHEN NOT MATCHED BY TARGET THEN
	INSERT (customer_no, memberSociety_Score)
	VALUES(mem.customer_no, mem.memberSociety_Score);

-----------------------------------------------------------------------------------
-- GET THE NUMBER_OF_GIFTS SCORE 
-- 10 points if given more than 3 gifts on distinct dates at any point in time
-----------------------------------------------------------------------------------
WITH NumGiftsScore
AS
(
	SELECT customer_no, COUNT(*) AS numOfGifts,
		CASE 
			WHEN COUNT(*) < 3 THEN 0
			ELSE 10
		END AS numOfGifts_Score 
	FROM 
	(
		SELECT DISTINCT cont.customer_no, cont.cont_dt
		FROM T_CONTRIBUTION AS cont
		INNER JOIN T_CAMPAIGN AS cam
			ON cont.campaign_no = cam.campaign_no
		WHERE cont.cont_amt > 0 
			AND cam.category <> 24 -- Skip
			AND DATEDIFF(d,cont.cont_dt, GETDATE()) >= 0 -- no future-dated gifts 

		UNION ALL 

		SELECT DISTINCT cred.creditee_no AS customer_no, cont.cont_dt
		FROM T_CREDITEE AS cred
		INNER JOIN T_CONTRIBUTION AS cont
			ON cred.ref_no = cont.ref_no
		INNER JOIN T_CAMPAIGN AS camp
			ON cont.campaign_no = camp.campaign_no
		WHERE cont.cont_amt > 0 
			AND camp.category <> 24 -- Skip
			AND cred.creditee_type NOT IN (8,11) -- In Memoriam, In Honor Of
			AND DATEDIFF(d,cont.cont_dt, GETDATE()) >= 0  -- no future-dated gifts
	) X
	GROUP BY customer_no 
)
MERGE #AffinityTbl Aff
USING NumGiftsScore gft
ON (Aff.customer_no = gft.customer_no)
WHEN MATCHED THEN
	UPDATE SET Aff.numOfGifts_Score = gft.numOfGifts_Score
WHEN NOT MATCHED BY TARGET THEN
	INSERT (customer_no, numOfGifts_Score)
	VALUES(gft.customer_no, gft.numOfGifts_Score);

-----------------------------------------------------------------------------------
-- GET THE RECENCY_OF_GIFTS SCORE 
-- 5 points if gift given in past 2 years
-----------------------------------------------------------------------------------

WITH RecencyGiftsScore
AS
(
	SELECT DISTINCT customer_no, 5 AS recencyGifts_Score 
	FROM 
	(
		SELECT c.customer_no, SUM(p.pmt_amt) sum_total
		FROM T_TRANSACTION AS trx
		INNER JOIN T_CONTRIBUTION AS c
			ON trx.ref_no = c.ref_no
			AND c.customer_no > 0
			AND c.cont_amt > 0
			AND c.customer_no <> '2653093' -- Stewardship Soft Credit 
		INNER JOIN T_CAMPAIGN AS camp
			ON c.campaign_no = camp.campaign_no
			AND camp.category <> 24 -- Skip
		INNER JOIN T_PAYMENT AS p
			ON trx.sequence_no = p.sequence_no
		INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
			ON camp.category = cc.id
		WHERE c.custom_1 IN ('(none)', 'Matching Gift Credit')
			  AND trx.trn_type IN ('3', '6') -- Pledge Payment Received, Adjustment to Payment
			  AND DATEDIFF(d, p.pmt_dt, GETDATE()) BETWEEN 0 AND 730 -- payment date between now and 2 years ago
		GROUP BY c.customer_no
		HAVING SUM(p.pmt_amt) > 0 

		UNION 

		SELECT cred.creditee_no customer_no, SUM(p.pmt_amt) sum_total 
		FROM T_TRANSACTION AS trx
		INNER JOIN T_CONTRIBUTION AS c
			ON trx.ref_no = c.ref_no
			AND c.customer_no > 0  
			AND c.cont_amt > 0  
		INNER JOIN T_CAMPAIGN AS camp
			ON c.campaign_no = camp.campaign_no
			AND camp.category <> 24 -- Skip
		INNER JOIN T_PAYMENT AS p
			ON trx.sequence_no = p.sequence_no
		INNER JOIN T_CREDITEE AS cred
			ON c.ref_no = cred.ref_no
			AND cred.creditee_type NOT IN (8,11) 
		INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
			ON camp.category = cc.id
		WHERE c.custom_1 IN ('Primary Soft Credit', 'Stewardship Soft Credit', 'Matching Gift Credit') 
			 AND trx.trn_type IN ('3','6') -- Pledge Payment Received, Adjustment to Payment
			 AND DATEDIFF(d, p.pmt_dt, GETDATE()) BETWEEN 0 AND 730 -- payment date between now and 2 years ago
		GROUP BY cred.creditee_no
		HAVING SUM(p.pmt_amt) > 0 

		UNION 

		SELECT DISTINCT cont.customer_no, 0 AS amt
		FROM T_CONTRIBUTION AS cont
		INNER JOIN T_CAMPAIGN AS cam
			ON cont.campaign_no = cam.campaign_no
		WHERE cont.cont_amt > 0 
			AND cam.category <> 24 -- Skip
			AND DATEDIFF(d, cont.cont_dt, GETDATE()) BETWEEN 0 AND 730 -- contributed between now and 2 years ago
 
		UNION 

		SELECT DISTINCT cred.creditee_no AS customer_no, 0 AS amt
		FROM T_CREDITEE AS cred
		INNER JOIN T_CONTRIBUTION AS cont
			ON cred.ref_no = cont.ref_no
		INNER JOIN T_CAMPAIGN AS camp
			ON cont.campaign_no = camp.campaign_no
		WHERE cont.cont_amt > 0 
			AND camp.category <> 24 -- Skip
			AND cred.creditee_type NOT IN (8,11) -- In Memoriam, In Honor Of
			AND DATEDIFF(d, cont.cont_dt, GETDATE()) BETWEEN 0 AND 730 -- contributed between now and 2 years ago
	) X
) 
MERGE #AffinityTbl Aff
USING RecencyGiftsScore rec
ON (Aff.customer_no = rec.customer_no)
WHEN MATCHED THEN
	UPDATE SET Aff.recencyOfGift_Score = rec.recencyGifts_Score
WHEN NOT MATCHED BY TARGET THEN
	INSERT (customer_no, recencyOfGift_Score)
	VALUES(rec.customer_no, rec.recencyGifts_Score);

-----------------------------------------------------------------------------------
-- UPDATE FINAL RESULTS
-----------------------------------------------------------------------------------
UPDATE #AffinityTbl
SET totalScore = membership_Score + attendance_Score + boardCommitteeVolunteer_Score + memberSociety_Score + numOfGifts_Score + recencyOfGift_Score,
affinityScore = CASE 
		WHEN membership_Score + attendance_Score + boardCommitteeVolunteer_Score + memberSociety_Score + numOfGifts_Score + recencyOfGift_Score < 10 
			THEN 5
		WHEN membership_Score + attendance_Score + boardCommitteeVolunteer_Score + memberSociety_Score + numOfGifts_Score + recencyOfGift_Score < 26 
			THEN 4
		WHEN membership_Score + attendance_Score + boardCommitteeVolunteer_Score + memberSociety_Score + numOfGifts_Score + recencyOfGift_Score < 49 
			THEN 3
		WHEN membership_Score + attendance_Score + boardCommitteeVolunteer_Score + memberSociety_Score + numOfGifts_Score + recencyOfGift_Score < 70 
			THEN 2
		ELSE 1 
	END;

/* FOR TESTING. THIS WILL HELP DETERMINE THE BREAKDOWN OF THE SCORES. 
SELECT aff.customer_no,
	membership_Score, 
	attendance_Score, 
	boardCommitteeVolunteer_Score, 
	memberSociety_Score, 
	numOfGifts_Score,
	recencyOfGift_Score,
	totalScore,
	affinityScore 
FROM #AffinityTbl aff
;
*/


-----------------------------------------------------------------------------------
-- CAN'T USE MERGE BECAUSE OF THE STUPID NEXT_ID
-- 1. UPDATE RECORDS IN T_CUST_RESEARCH THAT EXIST
-- 2. INSERT RECORDS INTO T_CUST_RESEARCH THAT DON'T EXIST
-----------------------------------------------------------------------------------

UPDATE r
SET r.research_source = aff.affinityScore,
	r.last_update_dt = GETDATE()
--SELECT *
FROM #AffinityTbl aff
INNER JOIN dbo.T_CUST_RESEARCH r
	ON r.customer_no = aff.customer_no
	AND r.research_type = 106


DECLARE @increment INT,
	@next_id INT

SELECT @increment = COUNT(*) + 1 
	FROM #AffinityTbl 
	WHERE customer_no NOT IN (SELECT customer_no FROM dbo.T_CUST_RESEARCH WHERE research_type = 106)

EXECUTE @next_id = dbo.AP_GET_NEXTID_function @type = 'CR', @increment = @increment
--SELECT @increment, @next_id


INSERT INTO dbo.T_CUST_RESEARCH
(
    cust_research_no,
    customer_no,
    research_date,
    research_type,
    researcher,
    research_source,
    create_loc,
    created_by,
    create_dt,
    last_updated_by,
    last_update_dt,
    worker_customer_no
)
SELECT 
	ROW_NUMBER() Over (Order by aff.customer_no) + @next_id,
	aff.customer_no,
	GETDATE() AS research_date,
	106 AS research_type,
	NULL AS researcher,
	aff.affinityScore AS research_source,
	@@SERVERNAME,
	dbo.FS_USER(),
	GETDATE(),
	dbo.FS_USER(),
	GETDATE(),
	NULL AS worker_customer_no
FROM #AffinityTbl aff
LEFT JOIN dbo.T_CUST_RESEARCH r
	ON r.customer_no = aff.customer_no
	AND r.research_type = 106
WHERE r.research_source IS NULL 


DROP TABLE #AffinityTbl