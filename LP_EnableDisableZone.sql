USE [impresario]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_EnableDisableZone]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_EnableDisableZone]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*
    Local Stored Procedure: LP_EnableDisableZone

    Written By: Mark Sherwood - Dec, 2015

    Enables or disables a specific time within a specific performance.
    Pass in the performance number and the time and the procedure will determine the zone id and disable it.

    You can disable ALL zones on a specific performance by passing the word All instead of a time
    You can NOT enable ALL zones at once.  They must be done one at a time.

    NOTE: THIS PROCEDURE WILL ALWAYS WORK TO *DISABLE* A SPECIFIC TIME ON A SPECIFIC PERFORMANCE
          HOWEVER, IT ONLY WORKS TO ENABLE A ZONE IF YOU WANT ALL PRICE TYPES ENABLED.
                                                 
*/
CREATE PROCEDURE [dbo].[LP_EnableDisableZone]
        @perf_no int,                       --ID number of the performance being affected
        @perf_time varchar(8),              --Performance time in HH:mm 24 hour time
        @is_enabled char(1),                --Y = Enable time / N = Disable Time
        @force_disable CHAR(1),             --If @is_enabled = N, Y = Disable even if tix sold / N = Do Not Disable if Tix Sold (N/A if @is_enabled = Y)
        @return_message varchar(100) OUTPUT --Output: Returns "success" if no problems or an error message if there is a problem
AS BEGIN

    /*  Procedure variables  */

    DECLARE @zone_no INT, @tix_sold INT

    SELECT @return_message = ''

    /*  Check date passed to procedure to be sure it is an actual date.
        Assumit it is, make sure it is in the HH:mm format (military time, no seconds, with leading zero if necessary)  */

    IF @perf_time <> 'all' BEGIN
        IF isdate(@perf_time) = 0 BEGIN
            SELECT @return_message = 'invalid time passed to procedure (' + @perf_time + ')'
            GOTO DONE
        END ELSE BEGIN
            SELECT @perf_time = left(convert(char(8),convert(datetime,@perf_time),108),5)
        END
    END ELSE IF @is_enabled = 'Y' BEGIN
        SELECT @return_message = 'Cannot enable ALL zones at once.  Pick a specific time.'
        GOTO DONE
    END
  
    IF @perf_time = 'all'
        DECLARE zone_cursor INSENSITIVE CURSOR FOR
        SELECT performance_zone FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_no = @perf_no
    ELSE
        DECLARE zone_cursor INSENSITIVE CURSOR FOR
        SELECT performance_zone 
        FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE_ALL_ZONES 
        WHERE performance_no = @perf_no 
          AND (performance_time = @perf_time OR performance_zone_text = @perf_time)

    OPEN zone_cursor
    BEGIN_ZONE_LOOP:

        FETCH NEXT FROM zone_cursor INTO @zone_no
        IF @@FETCH_STATUS = -1 GOTO END_ZONE_LOOP

        BEGIN TRY

            /*  Performance will not be disabled if tickets are sold to it unless you specifically tell the procedure to force it  */        

            IF @is_enabled = 'N' BEGIN
                SELECT @tix_sold = [dbo].[LF_GetCapacity](@perf_no, @zone_no, 'Sold')
                --EXECUTE [dbo].[LP_SS_GetPerformanceInfo] @perf_no = @perf_no, @perf_time = @perf_time,  @suppress_dataset = 'Y', @TixSold = @tix_sold OUTPUT
                IF @tix_sold > 0 AND @force_disable <> 'Y' GOTO BEGIN_ZONE_LOOP
            END
                     
                /*  T_PERF_PRICE table is updated with the new value  */

                UPDATE [dbo].[T_PERF_PRICE] SET [start_enabled] = @is_enabled WHERE [perf_no] = @perf_no and [zone_no] = @zone_no
                               
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'An error occurred while updating the T_PERF_PRICE table.'
            GOTO BEGIN_ZONE_LOOP
        
        END CATCH

        GOTO BEGIN_ZONE_LOOP

    END_ZONE_LOOP:
    CLOSE zone_cursor
    DEALLOCATE zone_cursor

    FINISHED:

        IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_EnableDisableZone] TO ImpUsers
GO

--DECLARE @rtn VARCHAR(100) EXECUTE [dbo].[LP_EnableDisableZone] @perf_no = 83044, @perf_time = '10:00', @is_enabled = 'Y', @Force_disable = 'N', @return_message = @rtn OUTPUT PRINT @rtn
--DECLARE @rtn VARCHAR(100) EXECUTE [dbo].[LP_EnableDisableZone] @perf_no = 83045, @perf_time = '11:00', @is_enabled = 'Y', @Force_disable = 'N', @return_message = @rtn OUTPUT PRINT @rtn
