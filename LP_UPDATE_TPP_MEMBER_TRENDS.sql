USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_UPDATE_TPP_MEMBER_TRENDS]    Script Date: 4/29/2019 3:55:46 PM ******/
DROP PROCEDURE [dbo].[LP_UPDATE_TPP_MEMBER_TRENDS]
GO

/****** Object:  StoredProcedure [dbo].[LP_UPDATE_TPP_MEMBER_TRENDS]    Script Date: 4/29/2019 3:55:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_UPDATE_TPP_MEMBER_TRENDS]
AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @tblcus TABLE (customer_no INT,
						cust_memb_no INT,
					   NRR_status VARCHAR(3),
					   memb_trend_MACRO TINYINT,
					   category_trend_MICRO TINYINT,
					   expr_dt DATETIME,
					   init_dt DATETIME,
					   RowNbr INT);

INSERT INTO @tblcus
			SELECT tpp.customer_no,
				   tpp.cust_memb_no,
				   tpp.NRR_status,
				   tpp.memb_trend AS memb_trend_MACRO,
				   tpp.category_trend AS category_trend_MICRO,
				   tpp.expr_dt,
				   tpp.init_dt,
				   ROW_NUMBER () OVER (PARTITION BY tpp.customer_no ORDER BY tpp.expr_dt) AS 'RowNbr'
			FROM   dbo.TX_CUST_MEMBERSHIP AS tpp
				   LEFT OUTER JOIN dbo.T_CAMPAIGN AS cam
								ON tpp.campaign_no = cam.campaign_no
			WHERE  tpp.memb_org_no = 13 --TPP
			AND	   cam.description LIKE '%Teacher Partner%';

-- Sets membership record as renewed instead of new
UPDATE dbo.TX_CUST_MEMBERSHIP
SET	   NRR_status = 'RN',
	   memb_trend = 1,
	   category_trend = 1
WHERE  cust_memb_no IN (
		   SELECT tpp.cust_memb_no
		   FROM	  @tblcus c
				  JOIN TX_CUST_MEMBERSHIP AS tpp
					ON	c.customer_no = tpp.customer_no
					AND c.expr_dt = tpp.expr_dt
		   WHERE  c.RowNbr > 1
		   AND	  c.NRR_status = 'NE'
	   );



GO


