USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tanya Hoffmann - Tessitura Network
-- Create date:  9/13/2016
-- Description:	Update ranks for schools
-- Chose the name LP_UPDATE_CUSTOMER_RANK so as not to be confused LP_CUSTOMER_RANK and other rank types can be added later.

-- Temporary table used here for testing

--Highest MA Schools 
--NH, RI Schools 
--VT, CT, ME Schools 
--All other states

-- =============================================
CREATE PROCEDURE [dbo].[LP_UPDATE_CUSTOMER_RANK]
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @rank_type INT = 4

	--Highest MA Schools 
	--NH, RI Schools 
	--VT, CT, ME Schools 
	--All other states

	--MOS to provide new rank type in TEST and LIVE. 
	--MOS to create new constituent type for ProxOne records.

	-- MA schools should have the highest rank
	INSERT dbo.T_CUST_RANK (customer_no, rank_type, rank)
	SELECT C.customer_no, @rank_type, 400 rank
	FROM dbo.T_CUSTOMER C
	JOIN dbo.T_ADDRESS ad ON ad.customer_no = C.customer_no AND ad.primary_ind = 'Y'
	WHERE cust_type IN( 21,22)
	AND ad.state = 'MA'
	AND ad.country = 1
	AND C.inactive = 1 -- active customer records only
	AND NOT EXISTS (SELECT * FROM T_CUST_RANK WHERE customer_no = C.customer_no AND rank_type = @rank_type)

	-- NH, RI schools next
	INSERT dbo.T_CUST_RANK (customer_no, rank_type, rank)
	SELECT C.customer_no, @rank_type, 300 rank
	FROM dbo.T_CUSTOMER C
	JOIN dbo.T_ADDRESS ad ON ad.customer_no = C.customer_no AND ad.primary_ind = 'Y'
	WHERE cust_type IN (21, 22)
	AND ad.state IN ('NH', 'RI')
	AND ad.country = 1
	AND C.inactive = 1 -- active customer records only
	AND NOT EXISTS (SELECT * FROM T_CUST_RANK WHERE customer_no = C.customer_no AND rank_type = @rank_type)

	-- VT, CT, ME schools next
	INSERT dbo.T_CUST_RANK (customer_no, rank_type, rank)
	SELECT C.customer_no, @rank_type, 200 rank
	FROM dbo.T_CUSTOMER C
	JOIN dbo.T_ADDRESS ad ON ad.customer_no = C.customer_no AND ad.primary_ind = 'Y'
	WHERE cust_type IN (21,22)
	AND ad.state IN ('VT', 'CT', 'ME') 
	AND ad.country = 1
	AND C.inactive = 1 -- active customer records only
	AND NOT EXISTS (SELECT * FROM T_CUST_RANK WHERE customer_no = C.customer_no AND rank_type = @rank_type)

	----All Other Schools in other states/countries
	INSERT dbo.T_CUST_RANK (customer_no, rank_type, rank)
	SELECT C.customer_no, @rank_type, 100 rank
	FROM dbo.T_CUSTOMER C
	JOIN dbo.T_ADDRESS ad ON ad.customer_no = C.customer_no AND ad.primary_ind = 'Y'
	LEFT OUTER JOIN dbo.T_CUST_RANK cr ON cr.customer_no = C.customer_no AND cr.rank_type = @rank_type
	WHERE cust_type IN (21,22)
	AND cr.customer_no IS NULL -- get rows for all other schools not yet ranked.
	AND C.inactive = 1 -- active customer records only
	AND NOT EXISTS (SELECT * FROM T_CUST_RANK WHERE customer_no = C.customer_no AND rank_type = @rank_type)


	-- delete if this rank exists on the wrong cust type

	DELETE cr
	--select cr.*, c.cust_type, ad.state,ad.country, c.lname, c.inactive
	FROM dbo.T_CUST_RANK cr
	JOIN T_CUSTOMER c ON cr.customer_no = c.customer_no
	JOIN dbo.T_ADDRESS ad ON ad.customer_no = c.customer_no AND ad.primary_ind = 'Y'
	WHERE cr.rank_type = @rank_type
	AND cust_type NOT IN (21,22)

END
GO


