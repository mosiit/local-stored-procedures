USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_AUTO_CANCEL_UNSOLD_PERFS]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_AUTO_CANCEL_UNSOLD_PERFS] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_AUTO_CANCEL_UNSOLD_PERFS] TO [impusers], [tessitura_app]';
GO

/*
    This procedure checks to see if any of today's performances need to be canceled due to no ticket sales.
    There is a content type called auto cancel minutes that can be added to any level of the product tree
    if the performance is within the number of minutes from starting with no ticket sales, that particular
    zone in that particular performance is disabled and blocked from any other sales.
*/

ALTER PROCEDURE [dbo].[LP_AUTO_CANCEL_UNSOLD_PERFS]
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @log_dt DATETIME = GETDATE();
        DECLARE @inserted_rows INT = 0;
        DECLARE @log_id INT, @perf_no INT, @zone_no INT; 
        DECLARE @perf_time VARCHAR(10), @rtn VARCHAR(100) = '';

    /*  No Parameters to Check  */


    /*  Look for any performances that need to be canceled and add them to the log table in the Admin database  */

        WITH [CTE_AUTO_CLOSE_PERFS] ([perf_no],[perf_code],[perf_dt],[prod_season_no],[prod_no],[title_no],[auto_close])
        AS (SELECT r.perf_no, r.[perf_code], CAST(r.[perf_dt] AS DATE), s.[prod_season_no], p.[prod_no], t.[title_no],
                   COALESCE(rc.[value], sc.[value], pc.[value], tc.[value], 0) * -1 AS [auto_close]
            FROM [dbo].[T_PERF] AS r
                 LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS rc ON rc.[inv_no] = r.[perf_no] AND rc.[content_type] = 43
                 INNER JOIN [dbo].[T_PROD_SEASON] AS s ON s.[prod_season_no] = r.[prod_season_no]
                 LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS sc ON sc.[inv_no] = s.[prod_season_no] AND sc.[content_type] = 43
                 INNER JOIN [dbo].[T_PRODUCTION] AS p ON p.[prod_no] = s.[prod_no]
                 LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS pc ON pc.[inv_no] = p.[prod_no] AND pc.[content_type] = 43
                 INNER JOIN [dbo].[T_TITLE] AS t ON t.[title_no] = p.[title_no]
                 LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS tc ON tc.[inv_no] = t.[title_no] AND tc.[content_type] = 43
            WHERE CAST(r.[perf_dt] AS DATE) >= CAST(GETDATE() AS DATE)
              AND COALESCE(rc.[value], sc.[value], pc.[value], tc.[value], 0) <> 0)
        INSERT INTO [admin].[dbo].[tessitura_auto_cancel_log] ([log_dt], [update_made], [performance_dt], [performance_no], [performance_zone], [title_no], 
                                                               [title_name], [production_no], [production_name], [performance_time], [update_time], 
                                                               [relative_minutes], [auto_close_minutes], [tickets_sold])
        SELECT @log_dt,
               'N',
               CAST(prf.[performance_dt] AS DATE) AS [performance_dt],
               prf.[performance_no],
               prf.[performance_zone],
               prf.[title_no], 
               prf.[title_name],
               prf.[production_no],
               prf.[production_name],
               prf.[performance_time],
               CAST(FORMAT(GETDATE(),'HH:mm') AS VARCHAR(30)) AS [current_time],
               DATEDIFF(MINUTE,CAST(prf.[performance_time] AS TIME),CAST(GETDATE() AS TIME)) AS [relative_minutes],
              cte.[auto_close],
              [dbo].[LF_GetCapacity] (prf.[performance_no],prf.[performance_zone],'sold') AS [tickets_sold]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
             LEFT OUTER JOIN [CTE_AUTO_CLOSE_perfs] cte ON cte.[perf_no] = prf.[performance_no]
        WHERE CAST(prf.[performance_dt] AS DATE) = CAST(GETDATE() AS DATE)
          AND cte.[auto_close] < 0
          AND [dbo].[LF_GetCapacity] (prf.[performance_no],prf.[performance_zone],'sold') = 0
          AND DATEDIFF(MINUTE,CAST(prf.[performance_time] AS TIME),CAST(GETDATE() AS TIME)) >= cte.[auto_close];

        SELECT @inserted_rows = @@ROWCOUNT;

    /*  If any records were added into the log, take them one at a time and run the LP_EnableDisableZone procedure  
        This procedure disables a specific zone (based on performance time) for a venue */

        IF @inserted_rows > 0 BEGIN
        
            DECLARE [update_cursor] INSENSITIVE CURSOR FOR
            SELECT [log_id], 
                   [performance_no], 
                   [performance_zone],
                   [performance_time]
            FROM [admin].[dbo].[tessitura_auto_cancel_log]
            WHERE [log_dt] = @log_dt 
              AND [update_made] = 'N';
            OPEN [update_cursor];

            BEGIN_UPDATE_LOOP:

                FETCH NEXT FROM [update_cursor] INTO @log_id, @perf_no, @zone_no, @perf_time;
                IF @@FETCH_STATUS = -1 GOTO END_UPDATE_LOOP;

                SELECT @log_id, @perf_no, @zone_no, @perf_time;

                EXECUTE [dbo].[LP_EnableDisableZone] 
                        @perf_no = @perf_no,
                        @perf_time = @perf_time, 
                        @is_enabled = 'N',              --N means disable zone (Y means enable)
                        @force_disable = 'N',           --N means don't disable if tickets are sold to it
                        @return_message = @rtn OUTPUT;

                --Update the log record to indicated change has been made
                UPDATE [Admin].[dbo].[tessitura_auto_cancel_log]
                   SET [update_made] = 'Y',
                       [update_result] = @rtn
                 WHERE [log_id] = @log_id

                GOTO BEGIN_UPDATE_LOOP

            END_UPDATE_LOOP:
            CLOSE [update_cursor];
            DEALLOCATE [update_cursor];

        END;

END
GO

--EXECUTE [dbo].[LP_AUTO_CANCEL_UNSOLD_PERFS]

