USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_GET_SKIDATA_INFO]    Script Date: 5/3/2016 3:15:39 PM ******/
DROP PROCEDURE [dbo].[LP_GET_SKIDATA_INFO]
GO

/****** Object:  StoredProcedure [dbo].[LP_GET_SKIDATA_INFO]    Script Date: 5/3/2016 3:15:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_GET_SKIDATA_INFO]
AS

BEGIN

	SET NOCOUNT ON

	DECLARE @card_pins TABLE	(
							customer_no				VARCHAR(10),
							pin						VARCHAR(10),
							garage_ticket_number	VARCHAR(50),
							member_level			VARCHAR(30),
							display_name			VARCHAR(100)
							)

	DECLARE @hdr TABLE	(
						software_ver		VARCHAR(10),
						mos_skidata_nbr		VARCHAR(10),
						run_date			VARCHAR(50),
						nbr_of_recs			VARCHAR(30),
						country_code		VARCHAR(100)
						)

	DECLARE	@nbr_recs INT

	-- Do this to avoid locking the tables on the Tessitura side
	INSERT INTO @card_pins
		SELECT	DISTINCT cmi.customer_no, 
				mmc.pin,
				CONVERT(VARCHAR(50), CONVERT(VARCHAR(10), cmi.customer_no) + CONVERT(VARCHAR(10), mmc.pin) + 43980465111040),
				cmi.memb_level_description, 
				-- H. Sheridan, 20160608 - SkiData name limit is 30 characters
				CONVERT(VARCHAR(30), CASE
					WHEN COALESCE(c.fname,'') <> '' THEN c.fname + ' '  + c.lname + 
					CASE WHEN s.id > 0 THEN ', ' + s.description ELSE '' END
					ELSE c.lname 
				END)
		FROM	[dbo].[LV_CURRENT_MEMBERSHIP_INFO] cmi
		JOIN	[dbo].[T_CUSTOMER] c WITH (NOLOCK)
		ON		cmi.customer_no = c.customer_no
		LEFT JOIN dbo.TR_PREFIX p WITH (NOLOCK) on c.prefix = p.id and p.id > 0
		LEFT JOIN dbo.TR_SUFFIX s WITH (NOLOCK) on c.suffix = s.id and s.id > 0	
		JOIN	[dbo].[LT_MOS_MEMBER_CARDS] mmc
		ON		cmi.customer_no = mmc.customer_no
		WHERE	mmc.inactive = 'N' and mmc.cust_memb_no > 0

	--SELECT * FROM @card_pins

	-- 2016/05/31, H. Sheridan - cannot truncate a replicated table but deleting should be okay
	DELETE FROM [dbo].[LT_SKIDATA_USER_EXPORT]
	--TRUNCATE TABLE [dbo].[LT_SKIDATA_USER_EXPORT]

	INSERT INTO [dbo].[LT_SKIDATA_USER_EXPORT]
		SELECT	'"_WGDC"', garage_ticket_number, '"CUSTOMERNO"', '"' + RIGHT(SUBSTRING('00000000000' + CONVERT(VARCHAR(10), cp.customer_no), 2, 99), 10) + '"', 0, 'N', 'System', GetDate(), 'SSRS', 'System', GetDate()
		FROM	@card_pins cp
		UNION
		SELECT	'"_WGDC"', garage_ticket_number, '"INFO1"', '"' + cp.member_level + '"', 0, 'N', 'System', GetDate(), 'SSRS', 'System', GetDate()
		FROM	@card_pins cp
		UNION
		-- 2016/10/24, H. Sheridan - handle double quotes in name as well as single quotes
		SELECT	'"_WGDC"',garage_ticket_number, '"NAME"', '"' + REPLACE(REPLACE(cp.display_name,'''',''), '"', '') + '"', 0, 'N', 'System', GetDate(), 'SSRS', 'System', GetDate()
		FROM	@card_pins cp

	-- 2016/05/31, H. Sheridan - cannot truncate a replicated table but deleting should be okay
	DELETE FROM [dbo].[LT_SKIDATA_CARD_EXPORT]
	--TRUNCATE TABLE [dbo].[LT_SKIDATA_CARD_EXPORT]

	INSERT INTO [dbo].[LT_SKIDATA_CARD_EXPORT]
		SELECT	'"_WGDC"', garage_ticket_number, '"' + CASE WHEN cp.member_level LIKE 'Basic%' THEN 'MBASIC' ELSE 'MPREMIER' END + '"', 0, 'N', 'System', GetDate(), 'SSRS', 'System', GetDate()
		FROM	@card_pins cp
END
GO

GRANT EXECUTE ON [dbo].[LP_GET_SKIDATA_INFO] TO impusers
GO


