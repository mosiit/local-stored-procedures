USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_UPDATE_CONTRIBUTIONS]
AS

SET NOCOUNT ON 

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

-- Updates requested by DMS - WO 82612

--
-- QUERY 1
--
UPDATE T_CONTRIBUTION
SET custom_1 = '(none)'
WHERE custom_1 IS NULL

--
-- QUERY 2
--
UPDATE T_CONTRIBUTION
SET custom_5 = 'N'
WHERE custom_5 IS NULL

--
-- QUERY 3
--
UPDATE ct
SET ct.credit_amt = c.cont_amt
FROM T_CREDITEE AS ct 
INNER JOIN T_CONTRIBUTION AS c 
	ON ct.ref_no = c.ref_no
WHERE ct.credit_amt <> c.cont_amt

--
-- QUERY 4
--
UPDATE ct
SET ct.credit_dt = c.cont_dt
FROM T_CREDITEE AS ct 
INNER JOIN T_CONTRIBUTION AS c 
	ON ct.ref_no = c.ref_no
WHERE DATEDIFF(d,ct.credit_dt ,c.cont_dt) <> 0

--
-- QUERY 5
--
UPDATE T_CONTRIBUTION
SET initiator_no = NULL
WHERE initiator_no IS NOT NULL

--
-- QUERY 6
--
UPDATE c
SET c.cont_designation = fd.designation
FROM T_CONTRIBUTION AS c
INNER JOIN LTR_FUND_DETAIL AS fd
	ON c.fund_no = fd.fund_no
WHERE c.cont_designation <> fd.designation

--
-- QUERY 7
--
UPDATE T_CONTRIBUTION
SET custom_3 = '(none)'
WHERE custom_3 IS NULL

UPDATE T_CONTRIBUTION
SET custom_4 = '(none)'
WHERE custom_4 IS NULL

UPDATE T_CONTRIBUTION
SET custom_8 = '(none)'
WHERE custom_8 IS NULL


