USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_ATTEND_SAVE_GOCITY_SCAN_DATA]    Script Date: 1/21/2020 12:40:55 PM ******/
DROP PROCEDURE [dbo].[LP_ATTEND_SAVE_GOCITY_SCAN_DATA]
GO

/****** Object:  StoredProcedure [dbo].[LP_ATTEND_SAVE_GOCITY_SCAN_DATA]    Script Date: 1/21/2020 12:40:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[LP_ATTEND_SAVE_GOCITY_SCAN_DATA]
AS
SET NOCOUNT ON;

BEGIN

	INSERT INTO [dbo].[LT_GOCITY_SCANS]
	(
		[file_name],
		--[go_id],
		[go_scan_dt],
		[go_confirmation_code],
		[go_pass_type],
		[go_product_type],
		[go_attraction]
	)
	SELECT	gcr.[file_name],
			--gcr.[go_id],
			gcr.[go_scan_dt],
			gcr.[go_confirmation_code],
			gcr.[go_pass_type],
			gcr.[go_product_type],
			gcr.[go_attraction]
	FROM	[dbo].[LT_GOCITY_SCANS_STAGING] gcr
	WHERE	gcr.[go_confirmation_code] <> ''
	AND		NOT EXISTS (SELECT 1 FROM [dbo].[LT_GOCITY_SCANS] gcs WHERE gcs.[go_confirmation_code] = gcr.[go_confirmation_code])

END





GO


