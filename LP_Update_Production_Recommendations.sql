USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_UPDATE_PRODUCT_RECOMMENDATIONS]    Script Date: 1/11/2019 1:36:38 PM ******/
SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_UPDATE_PRODUCT_RECOMMENDATIONS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_UPDATE_PRODUCT_RECOMMENDATIONS] AS';
END;
GO

ALTER PROCEDURE [dbo].[LP_UPDATE_PRODUCT_RECOMMENDATIONS]
        @delete_blank_recommendations CHAR(1) = NULL,
        @delete_nonexistent_recommendations CHAR(1) = NULL
AS BEGIN;

    SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @prod_no AS INT = 0,            @rec_prod_01 AS INT = 0,        @rec_prod_02 AS INT = 0,        @rec_prod_03 AS INT = 0, 
                @rec_prod_04 AS INT = 0,        @rec_prod_05 AS INT = 0,        @rec_prod_06 AS INT = 0,        @rec_prod_07 AS INT = 0, 
                @rec_prod_08 AS INT = 0,        @rec_prod_09 AS INT = 0,        @rec_prod_10 AS INT = 0,        @rec_weight AS INT = 0,
                @prod_season_no INT = 0,        @content_type_id AS INT = 0,    @content_weight_id AS INT = 0,  @start_dt DATETIME,
                @end_dt datetime;

        DECLARE @rec_type VARCHAR(30) = '',             @content_type_name AS VARCHAR(30) = '',     @recommend_string AS VARCHAR(255) = '',
                @return_message AS VARCHAR(255) = '';

    /*  Check Parameters  */

        SELECT @delete_blank_recommendations = ISNULL(@delete_blank_recommendations,'N');

        SELECT @delete_nonexistent_recommendations = ISNULL(@delete_nonexistent_recommendations,'N');

    /*  Get the Content Type Id for Recommendation Weight  */
    
        SELECT @content_weight_id = ISNULL([id],0) 
                                    FROM [dbo].[TR_INV_CONTENT] 
                                    WHERE [description] = 'Recommendation Weight';

    /*  Pull a list of all production recommendations where the current date is within the date range of the recommendation */

        DECLARE recommendation_cursor INSENSITIVE CURSOR FOR
        SELECT  [start_dt],
                [end_dt],  
                [production], 
                [rec_weight], 
                [rec_type], 
                [rec_prod_01], 
                [rec_prod_02], 
                [rec_prod_03], 
                [rec_prod_04], 
                [rec_prod_05], 
                [rec_prod_06], 
                [rec_prod_07], 
                [rec_prod_08], 
                [rec_prod_09], 
                [rec_prod_10]
        FROM [dbo].[LTR_PRODUCTION_RECOMMENDATIONS]
        WHERE CONVERT(DATE,GETDATE()) BETWEEN CONVERT(DATE,ISNULL([start_dt],'1900-1-1')) AND CONVERT(DATE,ISNULL([end_dt],'2999-12-31'))
          AND [inactive] = 'N';
        OPEN recommendation_cursor;
        BEGIN_RECOMMENDATION_LOOP:

            /*  Reset variables  */    

                SELECT @recommend_string = '';
            
            /*  Retrieve information about the next production and its recommendations from the cursor  */

                FETCH NEXT FROM recommendation_cursor INTO @start_dt, @end_dt, @prod_no, @rec_weight, @rec_type, @rec_prod_01, @rec_prod_02, 
                                                           @rec_prod_03, @rec_prod_04, @rec_prod_05, @rec_prod_06, @rec_prod_07, @rec_prod_08, 
                                                           @rec_prod_09, @rec_prod_10;
                IF @@FETCH_STATUS = -1 GOTO END_RECOMMENDATION_LOOP;


            /*  Step One: Update the recommendation weight for the product that the recommendations are being added to  */

                BEGIN TRY
        
                    /*  If the Recommendation Weight content exists, update it with the new weight.  If not, create the content record with the new weight
                        This is only done if there is a content type with the description of 'Recommendation Weight' which is the case if @content_type_id > 0  */

                        IF @content_weight_id > 0 BEGIN
                            IF NOT EXISTS(SELECT * FROM [dbo].[TX_INV_CONTENT] WHERE [inv_no] = @prod_no AND [content_type] = @content_weight_id)
                                INSERT INTO [dbo].[TX_INV_CONTENT] ([inv_no], [content_type], [value])
                                VALUES (@prod_no, @content_weight_id, CONVERT(VARCHAR(50),@rec_weight));
                            ELSE
                                UPDATE [dbo].[TX_INV_CONTENT] 
                                SET [value] = CONVERT(VARCHAR(50),@rec_weight)
                                WHERE [inv_no] = @prod_no AND [content_type] = @content_weight_id;
                        END;

                END TRY
                BEGIN CATCH

                    SELECT @return_message = ISNULL(left(Error_message(),100), 'error while processing recommendation weight.');
                    GOTO END_RECOMMENDATION_LOOP;

                END CATCH

        /*  Step Two: Parse out the recommendations and add them to the production content  */

            BEGIN TRY

                SELECT @content_type_name = 'Recommended Events (' + @rec_type + ')';
                SELECT @content_type_id = [id] FROM [dbo].[TR_INV_CONTENT] WHERE description = @content_type_name;
                SELECT @content_type_id = ISNULL(@content_type_id,0);

                /*  If no content type exitst (@content_type_id = 0), skip this record  */

                    IF @content_type_id = 0 GOTO BEGIN_RECOMMENDATION_LOOP;

                /*  One at a time, take the recommended production numbers and retrieve any current or future production seasons for each production.
                    These are stored in a table variable called @prod_season_table.  */ 

                    WITH CTE_production_seasons (prod_sea_no)
                    AS (SELECT DISTINCT [production_season_no]
                        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                        WHERE [performance_dt] >= GETDATE()
                          AND [production_no] IN (@rec_prod_01, @rec_prod_02, @rec_prod_03, @rec_prod_04, @rec_prod_05, 
                                                  @rec_prod_06, @rec_prod_07, @rec_prod_08, @rec_prod_09, @rec_prod_10)
                          AND LEFT([production_season_name],2) <> 'z-')
                    SELECT @recommend_string = COALESCE(@recommend_string + ',','') + CONVERT(VARCHAR(20),rec.[production_season_no])
                                               FROM [dbo].[LV_PRODUCTION_ELEMENTS_SEASON] AS rec
                                                     INNER JOIN CTE_production_seasons AS sea ON rec.[production_season_no] = sea.[prod_sea_no]
                                                     ;
                
                    IF @recommend_string = '' BEGIN;
                    
                        IF @delete_blank_recommendations = 'Y'
                            DELETE FROM [dbo].[TX_INV_CONTENT] 
                            WHERE [inv_no] = @prod_no 
                              AND [content_type] = @content_type_id;

                    END ELSE BEGIN;

                        --Remove leading comma (if there is one)
                        IF LEFT(LTRIM(@recommend_string),1) = ','
                            SELECT @recommend_string = RIGHT(LTRIM(RTRIM(@recommend_string)),LEN(LTRIM(RTRIM(@recommend_string))) - 1)
                    
                        IF EXISTS (SELECT * FROM [dbo].[TX_INV_CONTENT] WHERE [inv_no] = @prod_no AND [content_type] = @content_type_id)
                            UPDATE [dbo].[TX_INV_CONTENT] 
                            SET [value] = @recommend_string
                            WHERE [inv_no] = @prod_no AND [content_type] = @content_type_id;
                        ELSE
                            INSERT INTO [dbo].[TX_INV_CONTENT] ([inv_no], [content_type], [value])
                            VALUES (@prod_no, @content_type_id, @recommend_string);

                    END;

            END TRY
            BEGIN CATCH

                SELECT @return_message = ISNULL(left(Error_message(),100), 'error while processing recommendations.');
                GOTO END_RECOMMENDATION_LOOP;

            END CATCH

        GOTO BEGIN_RECOMMENDATION_LOOP;

    END_RECOMMENDATION_LOOP:
    CLOSE recommendation_cursor;
    DEALLOCATE recommendation_cursor;

    /*  Step Three: Delete recommendations that are no longer in the Production Recommendations table  */

        BEGIN TRY
    
            IF @delete_nonexistent_recommendations = 'Y'
                DELETE FROM [dbo].[TX_INV_CONTENT]
                WHERE [content_type] IN (SELECT [id] 
                                         FROM [dbo].[TR_INV_CONTENT] 
                                         WHERE [description] LIKE 'Recommended Events%')
                  AND [inv_no] NOT IN (SELECT [production]
                                       FROM [dbo].[LTR_PRODUCTION_RECOMMENDATIONS]);

        END TRY
    BEGIN CATCH

        SELECT @return_message = ISNULL(left(Error_message(),100), 'error while processing recommendations.');
        
    END CATCH


    FINISHED:

        IF @return_message <> ''
            RAISERROR (@return_message, 16, 1);

END;
GO

GRANT EXECUTE ON [dbo].[LP_UPDATE_PRODUCT_RECOMMENDATIONS] TO ImpUsers
GO

  --EXECUTE [dbo].[LP_UPDATE_PRODUCT_RECOMMENDATIONS]
  --                  @delete_blank_recommendations = 'Y',
  --                  @delete_nonexistent_recommendations = 'Y'



  