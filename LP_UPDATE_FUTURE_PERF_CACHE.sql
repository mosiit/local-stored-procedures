USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_UPDATE_FUTURE_PERF_CACHE]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_UPDATE_FUTURE_PERF_CACHE] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_UPDATE_FUTURE_PERF_CACHE] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LP_UPDATE_FUTURE_PERF_CACHE]
        @minute_threshold INT = 0
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    DECLARE @out_of_date_count INT = 0

    IF @minute_threshold = 0 
        SELECT @out_of_date_count = 1
    ELSE IF NOT EXISTS (SELECT * FROM [dbo].[LT_FUTURE_PERF_CACHE])
        SELECT @out_of_date_count = 1
    ELSE
        SELECT @out_of_date_count = COUNT(*) 
                                    FROM [dbo].[LT_FUTURE_PERF_CACHE] 
                                    WHERE DATEDIFF(MINUTE,[update_dt],GETDATE()) > @minute_threshold

    IF @out_of_date_count > 0 BEGIN

        TRUNCATE TABLE [dbo].[LT_FUTURE_PERF_CACHE];
        
        INSERT INTO [dbo].[LT_FUTURE_PERF_CACHE] ([perf_no],[zone_no], [perf_zone_id], [perf_code],[perf_date],[perf_time], [perf_end_time], 
                                                  [prod_season_no], [prod_season_name],[prod_no],[prod_name],[title_no],[title_name],[image_url])
            SELECT  prf.[performance_no],
                    prf.[performance_zone],
                    FORMAT(prf.[performance_no],'0000000000') + FORMAT(prf.[performance_zone],'0000000000'),
                    prf.[performance_code],
                    prf.[performance_date],
                    prf.[performance_Time],
                    prf.[performance_end_time],
                    prf.[production_season_no],
                    prf.[production_season_name],
                    prf.[production_no],
                    prf.[production_name],
                    prf.[title_no],
                    prf.[title_name],
                    ISNULL(pru.[value],'') AS [image_url]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
                 LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] pru ON pru.[inv_no] = prf.[production_no] AND pru.[content_type] = 14
            WHERE CAST(prf.[performance_dt] AS DATE) >= CAST(GETDATE() AS DATE);
    END;

END;
GO

--EXECUTE [dbo].[LP_UPDATE_FUTURE_PERF_CACHE] @minute_threshold = 0
--SELECT * FROM [dbo].[LT_FUTURE_PERF_CACHE]