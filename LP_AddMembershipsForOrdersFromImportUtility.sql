USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE LP_AddMembershipsForOrdersFromImportUtility
( @batch_no INT )
AS

-- MOS has Memberships as a Product Add-On.
-- The issue is with Membership records being created as expected in Tessitura, using this add-on functionality. 
-- But they do not create when the ticket orders are imported using the Import Utility
-- This script will take a Batch Number from the Import Utility and add a membership to each order_num. 

SET NOCOUNT ON 

DECLARE
	@id INT,
	@maxId INT,
	@order_no int

DECLARE @OrderNumTbl TABLE
(
	id INT IDENTITY,
	order_num INT
)

INSERT INTO @OrderNumTbl (order_num)
SELECT order_no
FROM dbo.T_ORDER
WHERE batch_no = @batch_no

SET @id = 1
SELECT @maxId = MAX(id) FROM @OrderNumTbl

-- Loop through each order_num and create a membership record
WHILE @id <= @maxId
BEGIN

	SELECT @order_no = order_num FROM @OrderNumTbl WHERE id = @id

	EXEC dbo.LP_PROCESS_TICKETING_MEMBERSHIPS @order_no = @order_no, @debug = 'N'

	SET @id = @id + 1
END
