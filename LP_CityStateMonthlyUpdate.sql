USE [impresario]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_CityStateMonthlyUpdate]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_CityStateMonthlyUpdate]
GO

/*  THIS PROCEDURE IS DEPENDENT ON A TABLE CALLED ZipCodeDataUpdateFinal IN THE TemporaryData DATABASE.
    EACH MONTH, DELETE OR TRUNCATE THAT TABLE THEN IMPORT THE NEW UPDATE MATERIAL INTO THE SAME TABLE.
    ONCE THE DATA HAS BEEN UPDATED,  RUN THIS PROCEDURE TO UPDATE THE TR_CITYSTATE TABLE IN IMPRESARIO  */
    
CREATE PROCEDURE [dbo].[LP_CityStateMonthlyUpdate]
        @leave_all_CAPS CHAR(1) = 'N' --Y = Yes, leave data all upper / N = No, change to upper and lower / O = Only change to upper and lower (don't reimport data first)
AS BEGIN

    /*  Procedure variables  */

        DECLARE @words TABLE ([word_no] INT, [city_word] VARCHAR(255))
        DECLARE @city_original VARCHAR(100), @city_changed VARCHAR(100), @city_word VARCHAR(100)
        DECLARE @bakup_table VARCHAR(100), @count_original INT, @count_new INT
        DECLARE @err_message VARCHAR(255), @complete_message VARCHAR(255)
                
    /*  Leave All Caps Defaults to No if a null or any other value other than a 'Y' is passed to the procedure  */

        SELECT @leave_all_CAPS = ISNULL(@leave_all_CAPS,'N')
        IF @leave_all_CAPS <> 'Y' SELECT @leave_all_CAPS = 'N'

        SELECT @err_message = ''


    /*  Create a backup of the current City State Table  */

        BEGIN TRY

            SELECT @bakup_table = 'TR_CITYSTATE_' + DATENAME(YEAR,GETDATE()) 
                                + CASE WHEN DATEPART(MONTH,GETDATE()) < 10 THEN '0' ELSE '' END + CONVERT(VARCHAR(2),DATEPART(MONTH,GETDATE()))
                                + CASE WHEN DATEPART(DAY,GETDATE()) < 10 THEN '0' ELSE '' END + DATENAME(DAY,GETDATE())
                                + CASE WHEN DATEPART(HOUR,GETDATE()) < 10 THEN '0' ELSE '' END + DATENAME(HOUR,GETDATE())
                            + CASE WHEN DATEPART(MINUTE,GETDATE()) < 10 THEN '0' ELSE '' END + DATENAME(MINUTE,GETDATE())

            SELECT @bakup_table = '[TemporaryData].[dbo].[' + LTRIM(RTRIM(@bakup_table)) + ']'

            EXECUTE ('SELECT * INTO ' + @bakup_table + ' FROM [impresario].[dbo].[TR_CITYSTATE]')

            SELECT @count_original = COUNT(*) FROM [impresario].[dbo].[TR_CITYSTATE]

        END TRY
        BEGIN CATCH

            SELECT @err_message = LEFT(ERROR_MESSAGE(),255)
            GOTO FINISHED

        END CATCH


    IF @leave_all_CAPS <> 'O' BEGIN

        /*  Check to make sure the ZipCodeDataUpdateFinal table is there and contains data */

        IF OBJECT_ID('TemporaryData..ZipCodeDataUpdateFinal') IS NULL BEGIN
            SELECT @err_message = 'The ZipCodeDataUpdateFinal table does not exist in the TemporaryData database.'
            GOTO FINISHED
        END

        IF (SELECT COUNT(*) FROM [TemporaryData].[dbo].[ZipCodeDataUpdateFinal]) = 0 BEGIN
            SELECT @err_message = 'The ZipCodeDataUpdateFinal table in the TemporaryData database contains no data.'
            GOTO FINISHED
        END
        
        BEGIN TRY
                
            /*  create temporary table to narrow down the millions of rows in ZipCOdeDataUpdateFinal into distinct city/state/zip code values  */

                IF OBJECT_ID('tempdb..#zip_code_table') IS NOT NULL DROP TABLE [#zip_code_table]
                CREATE TABLE [#zip_code_table] ([zip_code] VARCHAR(25), [city_name] VARCHAR(50), [state_abbreviation] VARCHAR(25))
                CREATE UNIQUE CLUSTERED INDEX [ix_zip_code_table_CSZ] ON [#zip_code_table] ([zip_code] ASC, [city_name] ASC, [state_abbreviation] ASC) ON [PRIMARY]
       
                INSERT INTO [#zip_code_table]
                SELECT DISTINCT [ZIPCode], [CityName], [StateAbbr] FROM [TemporaryData].[dbo].[ZipCodeDataUpdateFinal] (NOLOCK)

            /*  truncate the TR_CITYSTATE table to clear it out  */

                TRUNCATE TABLE [impresario].[dbo].[TR_CITYSTATE]

            /*  Insert the data from the temporary table into the TR_CITYSTATE table  
                Note: When attempting to insert data directly from the ZipCodeDataUpdate table, it was taking too long, most likely becuase of a combination of all the 
                      functions in the select statement and the 46 *MILLION* rows in the update table.  By putting the distinct values into a temp table first, it 
                      brought the run time down to a reasonable length. */    

                INSERT INTO [impresario].[dbo].[TR_CITYSTATE] ([zip5], [city], [state], [country], [usps_code], [inactive], [create_loc], [created_by], [create_dt], [last_updated_by], [last_update_dt], [default_ind])
                SELECT [zip_code], [city_name], [state_abbreviation], 1, [city_name], 'N', HOST_NAME(), [impresario].[dbo].[fs_user](), GETDATE(), [impresario].[dbo].[FS_USER](),GETDATE(),'Y'
                FROM [#zip_code_table]

        END TRY
        BEGIN CATCH

            SELECT @err_message = LEFT(ERROR_MESSAGE(),255)
            GOTO FINISHED

        END CATCH

    END

    /*  The data is moved inserted into the table as all capitals.  If so desired, the following code will change them all back to upper/lower case
        Note:  This part of the code takes five to ten minutes to run  */      

    IF @leave_all_CAPS <> 'Y' BEGIN

        /*  A cursor is generated containing a list of all distinct city values in TR_CITYSTATE  */

            DECLARE city_cursor INSENSITIVE CURSOR FOR 
            SELECT DISTINCT [city] FROM [impresario].[dbo].[TR_CITYSTATE] ORDER BY [city]
            OPEN city_cursor
            BEGIN_CITY_LOOP:

                /*  Refresh all the variables  */

                SELECT @city_original = '', @city_changed = ''
                DELETE FROM @words

                /* Pull the city names one at a time */

                FETCH NEXT FROM city_cursor INTO @city_original
                IF @@FETCH_STATUS = -1 GOTO END_CITY_LOOP


                BEGIN TRY
                
                    IF @city_original NOT LIKE '% %' BEGIN

                        /*  If the city name does not contain a space, simply use the one word, 
                        taking the first capital letter and changing the rest of the word to lower case  */

                        SELECT @city_changed = LEFT(@city_original,1) + LOWER(RIGHT(@city_original,LEN(@city_original) - 1))
        
                    END ELSE BEGIN

                        /*  If the city name does contain a space (multiple words), use the LFT_SPLIT_DILEMETED_STRING function to 
                            break out the city into its individual words which a returned in a table variable called @words  */

                        INSERT INTO @words SELECT * FROM [dbo].[LFT_SPLIT_DILEMETED_STRING](@city_Original, ' ')

                        /*  Update each word in the @words table, keeping the first capital letter and changing the rest to lower case  */

                        UPDATE @words SET [city_word] = LEFT([city_word],1) + LOWER(RIGHT([city_word],LEN([city_word]) - 1))

                        /*  Pull the individual words from @words into a second cursor to concatinate them */
                    
                        DECLARE changed_city_cursor INSENSITIVE CURSOR FOR
                        SELECT [city_word] FROM @words ORDER BY [word_no]
                        OPEN changed_city_cursor
                        BEGIN_CHANGED_CITY_LOOP:

                            FETCH NEXT FROM changed_city_cursor INTO @city_word
                            IF @@FETCH_STATUS = -1 GOTO END_CHANGED_CITY_LOOP

                            SELECT @city_changed = @city_changed + ' ' + @city_word

                            GOTO BEGIN_CHANGED_CITY_LOOP
                
                        END_CHANGED_CITY_LOOP:
                        CLOSE changed_city_cursor
                        DEALLOCATE changed_city_cursor

                    END

                    /*  Update the TR_CITYSTATE TABLE WITH THE NEW VALUE */

                    UPDATE [impresario].[dbo].[TR_CITYSTATE] SET [city] = LTRIM(@city_changed) WHERE [city] = @city_original

                END TRY
                BEGIN CATCH

                    SELECT @err_message = LEFT(ERROR_MESSAGE(),255)

                END CATCH

                IF @err_message = '' GOTO BEGIN_CITY_LOOP

            END_CITY_LOOP:
            CLOSE city_cursor
            DEALLOCATE city_cursor
        
    END

    FINISHED:
    
        SELECT @count_new = COUNT(*) FROM [impresario].[dbo].[TR_CITYSTATE]
        SELECT @complete_message = 'The procedure completed without errors.' + CHAR(10) + CONVERT(varchar(20),@count_original) + '/' + CONVERT(VARCHAR(20),@count_new)

        IF OBJECT_ID('tempdb..#zip_code_table') IS NOT NULL DROP TABLE [#zip_code_table]

        IF @err_message <> '' RAISERROR (@err_message, 16, 1)
        ELSE RAISERROR (@complete_message , 10, 1)

END
GO

GRANT EXECUTE ON [dbo].[LP_CityStateMonthlyUpdate] TO ImpUsers
GO
                
--EXECUTE [dbo].[LP_CityStateMonthlyUpdate] @leave_all_CAPS = 'N'

--SELECT * FROM dbo.TR_CITYSTATE
  
