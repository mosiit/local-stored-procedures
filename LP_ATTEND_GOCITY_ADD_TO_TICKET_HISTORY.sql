USE [impresario];
GO

/****** Object:  StoredProcedure [dbo].[LP_ATTEND_GOCITY_ADD_TO_TICKET_HISTORY]    Script Date: 1/21/2020 12:55:31 PM ******/
DROP PROCEDURE [dbo].[LP_ATTEND_GOCITY_ADD_TO_TICKET_HISTORY];
GO

/****** Object:  StoredProcedure [dbo].[LP_ATTEND_GOCITY_ADD_TO_TICKET_HISTORY]    Script Date: 1/21/2020 12:55:31 PM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO



CREATE PROCEDURE [dbo].[LP_ATTEND_GOCITY_ADD_TO_TICKET_HISTORY]
AS
BEGIN

    -- Inherited from LP_IMPORT_GOCITY_SCANS

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Inserts the scan information directly into the ticket history table so that the data will start showing up on the report.
        The join with LT_HISTORY_TICKET makes sure the same scan (based on scan date and time) will not be inserted into the table
        more than once.  */

    INSERT INTO [dbo].[LT_HISTORY_TICKET]
    (
        [attendance_type],
        [performance_type],
        [order_no],
        [sli_no], 
		[li_seq_no],
        [title_no],
        [title_name],
        [perf_no],
        [perf_date],
        [perf_time],
        [zone_no],
        [production_no],
        [production_name],
        [production_name_short],
        [production_name_long],
        [comp_code],
        [comp_code_name],
        [order_payment_status],
        [price_type],
        [price_type_name],
        [due_amt],
        [paid_amt],
        [sale_total],
        [performance_date],
        [performance_time],
        [scan_admission_date],
        [scan_admission_time],
        [scan_device],
        [scan_admission_adult],
        [scan_admission_child],
        [scan_admission_other],
        [scan_admission_auto],
        [scan_admission_total],
        [sli_status],
        [customer_no],
        [create_dt]
    )
    SELECT 'Gate B (Non-Ticketed)',                  --attendance_type
           'Public',                                 --performance_type
           0,                                        --order_no
           999999,                                   --In sli_no column
           goc.[go_id],                             --in line_seq_no column
           0,                                        --title_no
           'Show and Go',                            --title_name
           0,                                        --perf_no
           '',                                       --perf_date (report uses performance_date instead)
           '',                                       --perf_time (report used performance_time instead)
           0,                                        --zone_no
           0,                                        --production_np
           'GoBoston',                               --production_name
           'GoBoston',                               --production_name_short
           'GoBoston',                               --production_name_long
           0,                                        --comp_code
           'Free Admission',                         --comp_code_name
           'Free',                                   --order_payment_status
           0,                                        --price_type
           '',                                       --price_type_name
           0.0,                                      --due_amt
           0.0,                                      --paid_amt
           0,                                        --sale_total
           CONVERT(CHAR(10), goc.[go_scan_dt], 111), --perf_date
           '09:00:00',                               --perf_time
           CONVERT(CHAR(10), goc.[go_scan_dt], 111), --scan_admission_date
           CONVERT(CHAR(8), goc.[go_scan_dt], 108),  --scan_admission_time
           goc.[go_confirmation_code],               --scan_device
           0,                                        --scan_admission_adult
           0,                                        --scan_admission_child
           1,                                        --scan_admission_other (all show and go is other)
           0,                                        --scan_admission_auto
           1,                                        --scan_admission_total
           0,                                        --slu_status
           0,                                        --customer_no
           GETDATE()                                 --create_dt
    FROM [dbo].[LT_GOCITY_SCANS] AS goc
    LEFT OUTER JOIN [dbo].[LT_HISTORY_TICKET] AS tkt ON tkt.[scan_admission_date] = CONVERT(
                                                                                               CHAR(10),
                                                                                               goc.[go_scan_dt],
                                                                                               111
                                                                                           )
                                                        AND tkt.[scan_admission_time] = CONVERT(
                                                                                                   CHAR(8),
                                                                                                   goc.[go_scan_dt],
                                                                                                   108
                                                                                               )
                                                        AND tkt.[production_name] = 'GoBoston'
    WHERE tkt.history_no IS NULL;


    /*  Inserts the necessary visit count records  */

    INSERT INTO [dbo].[LT_HISTORY_VISIT_COUNT]
    (
        [run_dt],
        [history_dt],
        [history_date],
        [attendance_type],
        [transaction_count],
        [visit_count],
        [visit_count_scan]
    )
    SELECT GETDATE(),
           CAST(tik.[scan_admission_date] AS DATE),
           tik.[scan_admission_date],
           'GOBoston',
           1,
           SUM(tik.[scan_admission_total]),
           COUNT(*)
    FROM [dbo].[LT_HISTORY_TICKET] AS tik
    LEFT OUTER JOIN [dbo].[LT_HISTORY_VISIT_COUNT] AS vis ON vis.[history_date] = tik.[scan_admission_date]
                                                             AND vis.[attendance_type] = 'GOBoston'
    WHERE tik.[production_name] = 'GoBoston'
          AND vis.[history_date] IS NULL
    GROUP BY CAST(tik.[scan_admission_date] AS DATE),
             tik.[scan_admission_date];

END;










GO


