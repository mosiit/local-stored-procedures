USE [impresario]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_AddTwoExtraOmniPasses]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_AddTwoExtraOmniPasses]
GO

CREATE PROCEDURE [dbo].[LP_AddTwoExtraOmniPasses]
       @log_only char(1) = 'N',
       @return_message varchar(100) = null OUTPUT
AS BEGIN

    DECLARE @log_date datetime
    DECLARE @customer_no INT = 0, @ben_provider INT = 0, @eligible_month_N INT = 0, @eligible_month_Y INT = 0, @create_month INT = 0, 
            @create_year INT = 0, @eligible_year_N INT = 0, @eligible_year_Y INT = 0, @is_eligible char(1) = '', @is_gift char(1) = ''
    DECLARE @pen_memb_no INT = 0, @pen_expr_dt DATETIME, @pen_create_dt DATETIME, @pen_memb_type_no INT = 0, @pen_memb_type_name varchar(10) = '', 
            @act_memb_no INT = 0, @act_expr_dt DATETIME
    DECLARE @entitlement_no_om INT = 0, @entitlement_code_om varchar(30) = '', @entitlement_no_pl INT = 0, @entitlement_code_pl varchar(30) = '', 
            @entitlement_no_eh INT = 0, @entitlement_code_eh varchar(30) = ''
    DECLARE @entitlement_num_om INT = 0, @entitlement_num_pl INT = 0, @entitlement_num_eh INT = 0, @err_message varchar(255) = ''
  
    DECLARE @eligible_memberships table ([customer_no] INT, [ben_provider] int, [active_memb_no] int, [active_expr_dt] datetime, [eligible_month_N] int, [eligible_year_N] int, [eligible_month_Y] int,
                                         [eligble_year_Y] int, [is_gift] char(1), [pending_memb_no] int, [pending_membership_type_no] int, [pending_membership_type_name] varchar(10), 
                                         [pending_created_dt] datetime, [pending_created_month] int, [pending_created_year] int, [is_eligible] char(1))

    SELECT @log_date = getdate(), @return_message = ''

    /* Step 1: Determine Who Is Eligible */

    /*  Pull a list of all pending household memberships that have not already had passes added to them
        Memberships that have had the passes added are tracked in the tessitura_membership_extra_passes table in the admin database */

    DECLARE ExtraPassesCursor INSENSITIVE CURSOR FOR
    SELECT [customer_no], [ben_provider], [cust_memb_no], [memb_level], [expr_dt], [create_dt] 
    FROM [dbo].[TX_CUST_MEMBERSHIP]
    WHERE [memb_org_no] = 4 and current_status = 3 and NRR_status = 'RN'     --memb_org 4 = Household /status 3 = Pending / NRR_status RN = Renew
          and cust_memb_no not in (SELECT pen_membership_no FROM [admin].[dbo].[tessitura_membership_extra_passes] WHERE passes_added = 'Y') --AND ben_provider > 0 ORDER BY create_dt
    OPEN ExtraPassesCursor
    BEGIN_EXTRA_PASSES_LOOP:

        SELECT @act_expr_dt = null

        FETCH NEXT FROM ExtraPassesCursor INTO @customer_no, @ben_provider, @pen_memb_no, @pen_memb_type_name, @pen_expr_dt, @pen_create_dt
        IF @@FETCH_STATUS = -1 GOTO END_EXTRA_PASSES_LOOP

        IF @ben_provider IS NULL SELECT @ben_provider = 0
        IF @ben_provider > 0 SELECT @is_gift = 'Y' ELSE SELECT @is_gift = 'N'
 
        /*  Get further information about the pending membership.  */
        
        SELECT @pen_memb_type_name = IsNull(@pen_memb_Type_name, '')
        SELECT @pen_memb_type_no = memb_level_no FROM T_MEMB_LEVEL WHERE memb_level = @pen_memb_type_name
        SELECT @pen_memb_type_no = IsNull(@pen_memb_type_no, 0)

        IF @pen_memb_type_name = '' or @pen_memb_type_no <= 0 BEGIN

            INSERT INTO [admin].[dbo].[tessitura_membership_extra_passes] ([log_date], [log_time_stamp], [customer_no], [act_membership_no], [act_expire_dt], [eligible_month_n], [eligible_year_n], 
                                                                                   [eligible_month_y], [eligible_year_y], [pen_membership_no], [pen_membership_type], [pen_create_dt], [create_month], 
                                                                                   [create_year], [passes_added], [entitlement_no], [entitlement_code], [is_gift], [notes])
            VALUES (@log_date, getdate(), @customer_no, @act_memb_no, @act_expr_dt, @eligible_month_N, @eligible_year_n, @eligible_month_Y, @eligible_year_Y, @pen_memb_no, @pen_memb_type_name, 
                    @pen_create_dt, @create_month, @create_year, 'N', 0, '', @is_gift, 'Unable to determine membership type for pending membership.  No passes added.')
       
            GOTO BEGIN_EXTRA_PASSES_LOOP

        END

        /*  Get active membership information for current customer  */

        SELECT @act_memb_no = max([cust_memb_no]) FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [customer_no] = @customer_no and [memb_org_no] = 4 and current_status = 2 --memb_org 4 = Household /status 2 = Active
        SELECT @act_memb_no = Isnull(@act_memb_no, 0)
        
        IF @act_memb_no <= 0 BEGIN
        
            INSERT INTO [admin].[dbo].[tessitura_membership_extra_passes] ([log_date], [log_time_stamp], [customer_no], [act_membership_no], [act_expire_dt], [eligible_month_n], [eligible_year_n], 
                                                                                   [eligible_month_y], [eligible_year_y], [pen_membership_no], [pen_membership_type], [pen_create_dt], [create_month], 
                                                                                   [create_year], [passes_added], [entitlement_no], [entitlement_code], [is_gift], [notes])
                VALUES (@log_date, getdate(), @customer_no, @act_memb_no, @act_expr_dt, @eligible_month_N, @eligible_year_N, @eligible_month_Y, @eligible_year_Y, @pen_memb_no, @pen_memb_type_name, 
                        @pen_create_dt, @create_month, @create_year, 'N', 0, '', @is_gift, 'No active membership found.  Not a renewal.  No passes added.')
       
            GOTO BEGIN_EXTRA_PASSES_LOOP
        
        END

        /*  Get active membership expiration date  */

        SELECT @act_expr_dt = [expr_dt] FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [cust_memb_no] = @act_memb_no
                
        IF @act_expr_dt is null BEGIN
        
            INSERT INTO [admin].[dbo].[tessitura_membership_extra_passes] ([log_date], [log_time_stamp], [customer_no], [act_membership_no], [act_expire_dt], [eligible_month_n], [eligible_year_n], 
                                                                                   [eligible_month_y], [eligible_year_y], [pen_membership_no], [pen_membership_type], [pen_create_dt], [create_month], 
                                                                                   [create_year], [passes_added], [entitlement_no], [entitlement_code], [is_gift], [notes])
            VALUES (@log_date, getdate(), @customer_no, @act_memb_no, @act_expr_dt, @eligible_month_N, @eligible_year_N, @eligible_month_Y, @eligible_year_Y, @pen_memb_no, @pen_memb_type_name, 
                    @pen_create_dt, @create_month, @create_year, 'N', 0, '', @is_gift, 'Unable to determine expiration date of active membership (' + convert(varchar(20),@act_memb_no) + ').  No passes added.')

            GOTO BEGIN_EXTRA_PASSES_LOOP

        END

        /*  Determine eligibility and insert record into @eligible_memberships (N = Not Gift/Y = Is Gift)  */

        SELECT @create_year = datepart(year,@pen_create_dt), @create_month = datepart(month,@pen_create_dt)
        SELECT @eligible_year_N = datepart(year,@act_expr_dt), @eligible_month_N = (datepart(month,@act_expr_dt) - 2)
        SELECT @eligible_year_Y = datepart(year,@act_expr_dt), @eligible_month_Y = (datepart(month,@act_expr_dt) - 1)
        
             IF @eligible_month_N = 0 SELECT @eligible_month_N = 12, @eligible_year_N = (@eligible_year_N - 1)
        ELSE IF @eligible_month_N = -1 SELECT @eligible_month_N = 11, @eligible_year_N = (@eligible_year_N - 1)

             IF @eligible_month_Y = 0 SELECT @eligible_month_Y = 12, @eligible_year_Y = (@eligible_year_Y - 1)
        ELSE IF @eligible_month_Y = -1 SELECT @eligible_month_Y = 11, @eligible_year_Y = (@eligible_year_Y - 1)
        
        IF (@create_month = @eligible_month_N and @create_year = @eligible_year_N) SELECT @is_eligible = 'Y' 
        ELSE IF (@create_month = @eligible_month_Y and @create_year = @eligible_year_Y AND @is_gift = 'Y') SELECT @is_eligible = 'Y' 
        ELSE SELECT @is_eligible = 'N'

        INSERT INTO @eligible_memberships
        SELECT @customer_no, @ben_provider, @act_memb_no, @act_expr_dt, @eligible_month_N, @eligible_year_N, @eligible_month_Y, @eligible_year_Y, @is_gift,
               @pen_memb_no, @pen_memb_type_no, @pen_memb_type_name, @pen_create_dt, @create_month, @create_year, @is_eligible

        GOTO BEGIN_EXTRA_PASSES_LOOP

    END_EXTRA_PASSES_LOOP:
    CLOSE ExtraPassesCursor
    DEALLOCATE ExtraPassesCursor

    /*  Delete records that are not eligible for the extra passes */

    DELETE FROM @eligible_memberships WHERE [is_eligible] = 'N' 

    /*  Even though this was checked above, double-check to make sure no one is getting double-extra-passes  */

    DELETE FROM @eligible_memberships WHERE [pending_memb_no] in (SELECT [pen_membership_no] FROM [admin].[dbo].[tessitura_membership_extra_passes] WHERE [passes_added] = 'Y')


    --SELECT * FROM @eligible_memberships WHERE pending_created_month = eligible_month_Y
    --SELECT eligible_month_N, eligible_year_N, eligible_month_Y, eligble_year_Y, is_gift, pending_created_month, pending_created_year FROM @eligible_memberships

    /*  If no eligible records exist, insert a log record saying so, otherwise process the records one at a time  */    
    
   
   IF not exists (SELECT * FROM @eligible_memberships) BEGIN

        INSERT INTO [admin].[dbo].[tessitura_membership_extra_passes] ([log_date], [log_time_stamp], [notes])
        VALUES (@log_date, getdate(), 'No eligible pending memberships needing extra passes were found.')

   END ELSE BEGIN

        DECLARE PassesCursor INSENSITIVE CURSOR FOR
        SELECT [customer_no], [active_memb_no], [active_expr_dt], [eligible_month_N], [eligible_year_N], [eligible_month_Y], [eligble_year_Y], [pending_memb_no], [pending_membership_type_no], [pending_membership_type_name], 
               [pending_created_dt], [pending_created_month], [pending_created_year], [is_gift]
        FROM @eligible_memberships ORDER BY [pending_membership_type_name], [pending_memb_no]
        OPEN PassesCursor
        BEGIN_PASSES_LOOP:

            FETCH NEXT FROM PassesCursor INTO  @customer_no, @act_memb_no, @act_expr_dt, @eligible_month_N, @eligible_year_N, @eligible_month_Y, @eligible_year_Y, @pen_memb_no, @pen_memb_type_no, 
                                               @pen_memb_type_name, @pen_create_dt, @create_month, @create_year, @is_gift
            IF @@FETCH_STATUS = -1 GOTO END_PASSES_LOOP

            /*  This process will only work on standard memberships - the three basics and the three premiers */

            IF @pen_memb_type_name not in ('B2', 'B5', 'B8', 'P2', 'P5', 'P8') BEGIN

                INSERT INTO [admin].[dbo].[tessitura_membership_extra_passes] ([log_date], [log_time_stamp], [customer_no], [act_membership_no], [act_expire_dt], [eligible_month_n], [eligible_year_n], 
                                                                                   [eligible_month_y], [eligible_year_y], [pen_membership_no], [pen_membership_type], [pen_create_dt], [create_month], 
                                                                                   [create_year], [passes_added], [entitlement_no], [entitlement_code], [is_gift], [notes])
                VALUES (@log_date, getdate(), @customer_no, @act_memb_no, @act_expr_dt, @eligible_month_N, @eligible_year_N, @eligible_month_Y, @eligible_year_Y, @pen_memb_no, @pen_memb_type_name, 
                        @pen_create_dt, @create_month, @create_year, 'N', 0, '', @is_gift, 'Non-standard Membership level (' + @pen_memb_type_name + ') on membership ' + convert(varchar(20),@pen_memb_no) + '.  No passes added.')

                GOTO BEGIN_PASSES_LOOP

            END

            /*  Determine entitlement codes for the three basic entitlements and use the codes to get the entitlement numbers.  */
            
            SELECT @entitlement_code_om = ltrim(rtrim(@pen_memb_type_name)) + ' OM PASS', 
                   @entitlement_code_pl = ltrim(rtrim(@pen_memb_type_name)) + ' PL PASS', 
                   @entitlement_code_eh = ltrim(rtrim(@pen_memb_type_name)) + ' EH PASS'
         
            SELECT @entitlement_no_om = [entitlement_no], @entitlement_num_om = [num_ent] FROM [dbo].[LTR_ENTITLEMENT] WHERE [entitlement_code] = @entitlement_code_om
            SELECT @entitlement_no_om = IsNull(@entitlement_no_om, 0)
            SELECT @entitlement_num_om = IsNull(@entitlement_num_om, 0)

            SELECT @entitlement_no_pl = [entitlement_no], @entitlement_num_pl = [num_ent] FROM [dbo].[LTR_ENTITLEMENT] WHERE [entitlement_code] = @entitlement_code_pl
            SELECT @entitlement_no_pl = IsNull(@entitlement_no_pl, 0)
            SELECT @entitlement_num_pl = IsNull(@entitlement_num_pl, 0)

            SELECT @entitlement_no_eh = [entitlement_no], @entitlement_num_eh = [num_ent] FROM [dbo].[LTR_ENTITLEMENT] WHERE [entitlement_code] = @entitlement_code_eh
            SELECT @entitlement_no_eh = IsNull(@entitlement_no_eh, 0)
            SELECT @entitlement_num_eh = IsNull(@entitlement_num_eh, 0)

             IF @log_only = 'Y' BEGIN

                INSERT INTO [admin].[dbo].[tessitura_membership_extra_passes] ([log_date], [log_time_stamp], [customer_no], [act_membership_no], [act_expire_dt], [eligible_month_n], [eligible_year_n], 
                                                                                   [eligible_month_y], [eligible_year_y], [pen_membership_no], [pen_membership_type], [pen_create_dt], [create_month], 
                                                                                   [create_year], [passes_added], [entitlement_no], [entitlement_code], [is_gift], [notes])
                VALUES (@log_date, getdate(), @customer_no, @act_memb_no, @act_expr_dt, @eligible_month_N, @eligible_year_N, @eligible_month_Y, @eligible_year_Y, @pen_memb_no, @pen_memb_type_name, 
                        @pen_create_dt, @create_month, @create_year, 'N', @entitlement_no_om, @entitlement_code_om, @is_gift, '2 passes will be added to membership ' + convert(varchar(20),@pen_memb_no))

            END ELSE BEGIN

                /* If the entitlement records that do not already exist in the LTX_CUST_ENTITLEMENT table, create them.  */

                BEGIN TRY

                    BEGIN TRANSACTION

                        IF not exists (SELECT * FROM [dbo].[LTX_CUST_ENTITLEMENT] WHERE [customer_no] = @customer_no and [cust_memb_no] = @pen_memb_no and [entitlement_no] = @entitlement_no_eh)
                            INSERT INTO [dbo].[LTX_CUST_ENTITLEMENT] ([customer_no], [cust_memb_no], [entitlement_no], [entitlement_date], [entitlement_perf_no], [num_ent])
                            VALUES (@customer_no, @pen_memb_no, @entitlement_no_eh, null, null, @entitlement_num_eh)

                        IF not exists (SELECT * FROM [dbo].[LTX_CUST_ENTITLEMENT] WHERE [customer_no] = @customer_no and [cust_memb_no] = @pen_memb_no and [entitlement_no] = @entitlement_no_pl)
                            INSERT INTO [dbo].[LTX_CUST_ENTITLEMENT] ([customer_no], [cust_memb_no], [entitlement_no], [entitlement_date], [entitlement_perf_no], [num_ent])
                            VALUES (@customer_no, @pen_memb_no, @entitlement_no_pl, null, null, @entitlement_num_pl)

                        IF not exists (SELECT * FROM [dbo].[LTX_CUST_ENTITLEMENT] WHERE [customer_no] = @customer_no and [cust_memb_no] = @pen_memb_no and [entitlement_no] = @entitlement_no_om)
                            INSERT INTO [dbo].[LTX_CUST_ENTITLEMENT] ([customer_no], [cust_memb_no], [entitlement_no], [entitlement_date], [entitlement_perf_no], [num_ent])
                            VALUES (@customer_no, @pen_memb_no, @entitlement_no_om, null, null, @entitlement_num_om)

                        /*  Add 2 Omni Passes to Entitlements  */

                        UPDATE [dbo].[LTX_CUST_ENTITLEMENT] SET [num_ent] = (@entitlement_num_om + 2) 
                        WHERE [customer_no] = @customer_no and [cust_memb_no] = @pen_memb_no and entitlement_no = @entitlement_no_om

                        INSERT INTO [admin].[dbo].[tessitura_membership_extra_passes] ([log_date], [log_time_stamp], [customer_no], [act_membership_no], [act_expire_dt], [eligible_month_n], [eligible_year_n], 
                                                                                   [eligible_month_y], [eligible_year_y], [pen_membership_no], [pen_membership_type], [pen_create_dt], [create_month], 
                                                                                   [create_year], [passes_added], [entitlement_no], [entitlement_code], [is_gift], [notes])
                        VALUES (@log_date, getdate(), @customer_no, @act_memb_no, @act_expr_dt, @eligible_month_N, @eligible_year_N, @eligible_month_Y, @eligible_year_Y, @pen_memb_no, @pen_memb_type_name, 
                                @pen_create_dt, @create_month, @create_year, 'Y', @entitlement_no_om, @entitlement_code_om, @is_gift, '2 passes added to membership ' + convert(varchar(20),@pen_memb_no))

                    COMMIT TRANSACTION

                END TRY
                BEGIN CATCH

                    SELECT @err_message = left(Error_message(),255)
                    SELECT @err_message = IsNull(@err_message, 'error while processing pending membership ' + convert(varchar(20),@pen_memb_no) + '.')

                    INSERT INTO [admin].[dbo].[tessitura_membership_extra_passes] ([log_date], [log_time_stamp], [customer_no], [act_membership_no], [act_expire_dt], [eligible_month_n], [eligible_year_n], 
                                                                                   [eligible_month_y], [eligible_year_y], [pen_membership_no], [pen_membership_type], [pen_create_dt], [create_month], 
                                                                                   [create_year], [passes_added], [entitlement_no], [entitlement_code], [is_gift], [notes])
                    VALUES (@log_date, getdate(), @customer_no, @act_memb_no, @act_expr_dt, @eligible_month_N, @eligible_year_N, @eligible_month_Y, @eligible_year_Y, @pen_memb_no, @pen_memb_type_name, 
                            @pen_create_dt, @create_month, @create_year, 'Y', @entitlement_no_om, @entitlement_code_om, @is_gift, @err_message)
                            
                   ROLLBACK TRANSACTION

                END CATCH

            END

            GOTO BEGIN_PASSES_LOOP

        END_PASSES_LOOP:
        CLOSE PassesCursor
        DEALLOCATE PassesCursor

    END

    IF @log_only = 'Y' UPDATE [admin].[dbo].[tessitura_membership_extra_passes] SET [notes] = 'LOG ONLY - ' + [notes] WHERE [log_date] = @log_date
        
    DELETE FROM [admin].[dbo].[tessitura_membership_extra_passes] WHERE datediff(day, [log_date], getdate()) > 366

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:
    
        WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    
        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_AddTwoExtraOmniPasses] TO impusers
GO


--DECLARE @rtn VARCHAR(100)
--EXECUTE [dbo].[LP_AddTwoExtraOmniPasses] @log_only = 'Y', @return_message = @rtn OUTPUT
--PRINT @rtn
--SELECT * FROM Admin.dbo.tessitura_membership_extra_passes WHERE log_date = (SELECT MAX(log_date) FROM Admin.dbo.tessitura_membership_extra_passes)

