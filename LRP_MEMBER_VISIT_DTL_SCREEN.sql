USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MEMBER_VISIT_DTL_SCREEN]    Script Date: 6/28/2018 8:30:19 AM ******/
DROP PROCEDURE [dbo].[LRP_MEMBER_VISIT_DTL_SCREEN]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MEMBER_VISIT_DTL_SCREEN]    Script Date: 6/28/2018 8:30:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_MEMBER_VISIT_DTL_SCREEN]
	@customer_no		INT
AS

-- ================================================================================================
-- Created:  H. Sheridan (MOS), 3/21/2016
--
-- Work order 81839 - [SCRN] Create member attendance custom screen in Tessitura
--
-- Notes:
--
--		Created as a stored procedure - even though this is just two SELECT's - to make it easier
--		to do edits down the road.  
--
--		Included in SSRS project:  E:\SSRS\Tessitura\ConstituentCustomScreens
-- 
-- 6/9/17 DSJ - Changed the join to the T_ATTENDANCE table to inner join (from left outer join) so it will remove the blank row. 
--				Changed the Visit Date to a datetime, instead of a string 
--
-- 6/28/18 HS - WO 93402 - Adjust LRP_MEMBER_VISIT_DTL_SCREEN to avoid using local view
--				The previous version of this code used the LV_PRODUCTION_ELEMENTS_PERFORMANCE view.  We've had trouble with
--				performance when using that view, so I coded it to use the source tables instead.  
-- ================================================================================================

BEGIN

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	--
	-- PULLING NSCAN ATTENDANCE DATA
	--
	SELECT	DISTINCT @customer_no AS customer_no,
			0 AS id_key,
			att.[attend_dt] AS 'Visit Date',
			ISNULL(inv.[description], '') AS 'Scan Location',
			cmi.memb_level_description AS 'Membership Type',
			SUM(admission_adult) + SUM(admission_child) + SUM(admission_other) AS '# People'
	FROM	[dbo].[LV_CURRENT_MEMBERSHIP_INFO] cmi
	INNER JOIN	[dbo].[T_ATTENDANCE] att 
	ON		att.[cust_memb_no] = cmi.[customer_memb_no]
	AND		att.[attend_dt] BETWEEN cmi.[initiation_date] AND cmi.[expiration_date]
	AND		att.[cust_memb_no] IS NOT NULL
	LEFT OUTER JOIN [dbo].[T_PERF] AS prf 
	ON		att.[perf_no] = prf.[perf_no]
	JOIN	[dbo].[T_INVENTORY] AS inv
	ON		prf.[perf_no] = inv.[inv_no]
	WHERE	cmi.customer_no = @customer_no
	AND		inv.[type] = 'R'
	GROUP BY att.[attend_dt], inv.[description], cmi.memb_level_description

	UNION

	SELECT	DISTINCT @customer_no AS customer_no,
			0 AS id_key,
			att.[attend_dt] AS 'Visit Date',
			ISNULL(inv.[description], '') AS 'Scan Location',
			pmi.memb_level_description AS 'Membership Type',
			SUM(admission_adult) + SUM(admission_child) + SUM(admission_other) AS '# People'
	FROM	[dbo].[LV_PAST_MEMBERSHIP_INFO] pmi
	INNER JOIN	[dbo].[T_ATTENDANCE] att 
	ON		att.[cust_memb_no] = pmi.[customer_memb_no]
	AND		att.[attend_dt] BETWEEN pmi.[initiation_date] AND pmi.[expiration_date]
	AND		att.[cust_memb_no] IS NOT NULL
	LEFT OUTER JOIN [dbo].[T_PERF] AS prf 
	ON		att.[perf_no] = prf.[perf_no]
	JOIN	[dbo].[T_INVENTORY] AS inv
	ON		prf.[perf_no] = inv.[inv_no]
    WHERE	pmi.customer_no = @customer_no 
	AND		inv.[type] = 'R'
	GROUP BY pmi.customer_no, att.[attend_dt], inv.[description], pmi.memb_level_description
	ORDER BY 'Visit Date', 'Membership Type'

END


GO


