USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_GET_AVAILABLE_ENTITLEMENT_GIFTS]    Script Date: 10/2/2020 3:11:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tessitura Network Consulting Jon Ballinger
-- Create date: 5/7/2018
-- Description:	Used for reporting to return a list of records to be used in mail merge type applications.
-- =============================================
ALTER  PROCEDURE [dbo].[LP_GET_AVAILABLE_ENTITLEMENT_GIFTS]
	@customer_no INT = NULL,
	@entitlement_no INT = NULL,
	@list_no INT = NULL,
	-- H. Sheridan, 20201002 - Ticket SCTASK0001716 - Add date parameter to Entitlement Gift Output report
	@create_dt DATETIME
	
AS
BEGIN
	
	IF @customer_no IS NOT NULL OR @list_no IS NOT NULL
		BEGIN 
			if @customer_no is not null 
				begin
					select 
						b.id_key,
						a.entitlement_no,
						a.entitlement_desc,a.entitlement_code,
						b.num_items,
						b.init_dt,
						b.expr_dt,
						b.group_gift_code,
						b.gift_code,
						c.description as tkw_description,
						cust.customer_no,
						cust.lname,
						cust.fname
					from LTR_ENTITLEMENT a
					join LT_ENTITLEMENT_GIFT b on a.entitlement_no = b.entitlement_no
						and a.entitlement_no = coalesce(@entitlement_no,a.entitlement_no)
						and b.gift_status='C'
					join TR_TKW c on c.id = a.ent_tkw_id
					join T_CUSTOMER (nolock) cust on cust.customer_no = b.customer_no
					where b.customer_no=@customer_no
					-- H. Sheridan, 20201002 - Ticket SCTASK0001716
					AND b.create_dt > @create_dt

				end
			IF @list_no IS NOT NULL
				BEGIN
					SELECT 
						b.id_key,
						a.entitlement_no,
						a.entitlement_desc,a.entitlement_code,
						b.num_items,
						b.init_dt,
						b.expr_dt,
						b.group_gift_code,
						b.gift_code,
						c.description AS tkw_description,
						cust.customer_no,
						cust.lname,
						cust.fname
					FROM LTR_ENTITLEMENT a
					JOIN LT_ENTITLEMENT_GIFT b ON a.entitlement_no = b.entitlement_no
						AND a.entitlement_no = COALESCE(@entitlement_no,a.entitlement_no)
						AND b.gift_status='C'
					JOIN TR_TKW c ON c.id = a.ent_tkw_id
					JOIN T_LIST_CONTENTS (NOLOCK) d ON d.customer_no = b.customer_no
						AND d.list_no = @list_no
					JOIN T_CUSTOMER (NOLOCK) cust ON cust.customer_no = b.customer_no
					-- H. Sheridan, 20201002 - Ticket SCTASK0001716
					WHERE b.create_dt > @create_dt

				END

		END 
	ELSE
		BEGIN
			SELECT 
				b.id_key,
				a.entitlement_no,
				a.entitlement_desc,a.entitlement_code,
				b.num_items,
				b.init_dt,
				b.expr_dt,
				b.group_gift_code,
				b.gift_code,
				c.description AS tkw_description,
				cust.customer_no,
				cust.lname,
				cust.fname
			FROM LTR_ENTITLEMENT a
			JOIN LT_ENTITLEMENT_GIFT b ON a.entitlement_no = b.entitlement_no
				AND a.entitlement_no = COALESCE(@entitlement_no,a.entitlement_no)
				AND b.gift_status='C'
			JOIN TR_TKW c ON c.id = a.ent_tkw_id
			JOIN T_CUSTOMER (NOLOCK) cust ON cust.customer_no = b.customer_no
			-- H. Sheridan, 20201002 - Ticket SCTASK0001716
			WHERE b.create_dt > @create_dt
		END
END


GO


