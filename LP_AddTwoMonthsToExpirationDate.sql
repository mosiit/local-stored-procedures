USE [impresario]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_AddTwoMonthsToExpirationDate]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_AddTwoMonthsToExpirationDate]
GO

CREATE PROCEDURE [dbo].[LP_AddTwoMonthsToExpirationDate]
        @return_message varchar(100) = null
AS BEGIN

    DECLARE @log_date datetime  
    DECLARE @customer_no int, @activity_no int,@membership_no int
    DECLARE @orig_expire_dt datetime, @orig_expire_month int, @orig_expire_year int
    DECLARE @new_expire_dt datetime, @new_expire_month int, @new_expire_year int
    DECLARE @IsAc_no int, @completed_action_id int, @completed_action_notes varchar(255)
    
    SELECT @log_date = getdate()
    SELECT @completed_action_id = 2  -- 2 = 'Completed Request'
    SELECT @completed_action_notes = '', @return_message = ''

    /*  Compiles a list of all open CSI records that have an activity type of 67 (Telemarketing 2 Extra Months) and are not resolved.  */

    IF not exists (SELECT * FROM [dbo].[LV_CSI_ACTIONS] WHERE [activity_type_no] = 67 and [is_resolved] = 'N') BEGIN

        INSERT INTO [admin].[dbo].[tessitura_membership_extra_two_months]([log_date], [log_time_stamp], [activity_no], [customer_no], [membership_no],  [orig_expire_dt], [new_expire_dt], [action_no], [action_notes])
        VALUES (@log_date, getdate(), 0, 0, 0, @log_date, @log_date, 0, 'No records found - No Memberships Changed.')

    END ELSE BEGIN

        DECLARE ExtraTwoCursor INSENSITIVE CURSOR FOR
        SELECT [activity_no], [customer_no] FROM [dbo].[LV_CSI_ACTIONS] WHERE [activity_type_no] = 67 and [is_resolved] = 'N'
        OPEN ExtraTwoCursor
        BEGIN_EXTRA_TWO_LOOP:

            /*  Process records one at a time */

            FETCH NEXT FROM ExtraTwoCursor INTO @activity_no, @customer_no
            IF @@FETCH_STATUS = -1 GOTO END_EXTRA_TWO_LOOP

            /*  Determine the current membership record -  NOTE: If there are more than one current membership records, the change
                will be applied to the record most recently created (maximum cust_memb_no). */

            SELECT @membership_no = max([cust_memb_no]) FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [customer_no] = @customer_no and [memb_org_no] = 4 and [current_status] in (2, 3)
            SELECT @membership_no = IsNull(@membership_no, 0)

            /*  If no membership found, skip and move on to next customer  */

            IF @membership_no <= 0 GOTO BEGIN_EXTRA_TWO_LOOP

            /*  Retrive original expiration date from the membership record to be changed, as well as the month and year of that date.  */
                
            SELECT @orig_expire_dt = [expr_dt], @orig_expire_month = datepart(month,[expr_dt]), @orig_expire_year = datepart(year,[expr_dt]) 
            FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [cust_memb_no] = @membership_no
    
            /* If original date is november, then January of next year or if December than February of next year, otherwise Add 2 months in current year  */

                 IF @orig_expire_month = 11 SELECT @new_expire_month = 1, @new_expire_year = (@orig_expire_year + 1)
            ELSE IF @orig_expire_month = 12 SELECT @new_expire_month = 2, @new_expire_year = (@orig_expire_year + 1)
            ELSE SELECT @new_expire_month = (@orig_expire_month + 2), @new_expire_year = @orig_expire_year

            /*  SET Expiration date to last day of month using [LF_GetLastOfMonth] function. */

            SELECT @new_expire_dt = [dbo].[LF_GetLastOfMonth] (convert(datetime,convert(varchar(2),@new_expire_month) + '-1-' + convert(varchar(4),@new_expire_year)),null) + ' 23:59:59'
               
            BEGIN TRY

                /*  Open a transaction so that if any part of this process fails, it rolls everything back   */

                BEGIN TRANSACTION
         
                    /*  Change expiration date in membership    */

                    UPDATE [dbo].[TX_CUST_MEMBERSHIP] SET [expr_dt] = @new_expire_dt WHERE cust_memb_no = @membership_no
            
                    /*  Set action notes text to contain all pertinent information  */

                    SELECT @completed_action_notes = convert(varchar(50),getdate()) + ': Expiration date on membership ' + convert(varchar(10),@membership_no) 
                                                   + ' changed from ' + convert(varchar(50),@orig_expire_dt) + ' to ' + convert(varchar(50),@new_expire_dt) + '.'

                    /*  Get next id number for an issue action from the T_NEXT_ID TABLE    */

                    EXECUTE [dbo].[AP_GET_NEXTID_function] @type = 'IA', @nextid = @IsAc_no OUTPUT, @increment = 1
                
                    /*  Insert action record into T_ISSUE_ACTION as a setting the issue to resolved.    */

                    INSERT INTO [dbo].[T_ISSUE_ACTION] ([IsAc_no], [customer_no], [action_dt], [action], [res_ind], [notes], [activity_no])
                    VALUES (@IsAc_no, @customer_no, getdate(), @completed_action_id, 'Y', @completed_action_notes, @activity_no)

                    /*  Log the change that was made  */

                    INSERT INTO [admin].[dbo].[tessitura_membership_extra_two_months]([log_date], [log_time_stamp], [activity_no], [customer_no], [membership_no],  [orig_expire_dt], [new_expire_dt], [action_no], [action_notes])
                    VALUES (@log_date, getdate(), @activity_no, @customer_no, @membership_no, @orig_expire_dt, @new_expire_dt, @IsAc_no, @completed_action_notes)

                
                /*  Commit transaction for this membership    */

                COMMIT TRANSACTION
            
             END TRY
             BEGIN CATCH

                SELECT @return_message = left(Error_message(),100)
                SELECT @return_message = IsNull(@return_message, 'error while processing expiration dates.')
                ROLLBACK TRANSACTION

             END CATCH

            /*  Process next membership record  */
      
            GOTO BEGIN_EXTRA_TWO_LOOP

        END_EXTRA_TWO_LOOP:
        CLOSE ExtraTwoCursor
        DEALLOCATE ExtraTwoCursor

    END

    DELETE FROM [admin].[dbo].[tessitura_membership_extra_two_months] WHERE datediff(day, [log_date], getdate()) > 365
        
    IF @return_message = '' SELECT @return_message = 'success'

    DONE:
    
        WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    
        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_AddTwoMonthsToExpirationDate] to impusers
GO


--EXECUTE [dbo].[LP_AddTwoMonthsToExpirationDate]
--SELECT * FROM [admin].[dbo].[tessitura_membership_extra_two_months] WHERE log_date = (SELECT max(log_date) FROM [admin].[dbo].[tessitura_membership_extra_two_months])


