USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_ImportPresentationsAndActivities]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_ImportPresentationsAndActivities]
GO

/*  Local Procedure: LP_ImportPResentationsAndActivities
    Written By: Mark Sherwood (Feb, 2016)

    This procedure relies on a fresh copy of the Events database being moved from Quark to Kirk nightly.
    It uses a view LV_QUARK_EVENT_INFO which looks into the copied Events database and pulls a list of all presentations and drop-in activities.
    One by one, it checks to see if that program exists as a performance in Tessitura.  If not, it tries to create one.  If yes, it checks to 
    make sure the title doesn't need to be changed.

    In a second step, it pulls a list of all Tessitura performances in the Drop-In Activities and Live Presentations titles and checked to make
    sure they still exist in the LV_QUARK_EVENT_INFO view.  If not, it deletes them from Tessitura.

*/

CREATE PROCEDURE [dbo].[LP_ImportPresentationsAndActivities]
                 @display_results char(1) = 'N'
AS BEGIN

 /*  Char and Varchar variable declarations  */

    DECLARE @show_type varchar(30),                         @show_date char(10),                            @show_start_time varchar(8), 
            @show_end_time varchar(8),                      @tessitura_title_name varchar(30),              @tessitura_production_name varchar(30), 
            @tessitura_production_name_long varchar(255),   @tessitura_production_name_short varchar(20),   @tessitura_production_season_name varchar(30),  
            @tessitura_performance_code varchar(10),        @tessitura_perf_type_name varchar(30),          @Location_name varchar(50),
            @existing_production_name varchar(30),          @existing_prod_season_name varchar(30),         @delete_performance_code varchar(10),
            @existing_Performance_location varchar(50)
     
    /*  DateTime variable declarations   */

    DECLARE @activity_start_time datetime,                  @activity_end_time datetime,                    @existing_start_time datetime,
            @existing_end_time datetime

    /*  Numeric variable declarations   */

    DECLARE @duration int,                                  @fiscal_year int,                               @tessitura_title_no int, 
            @tessitura_production_no int,                   @tessitura_perf_type_no int,                    @tessitura_production_season_no int, 
            @existing_prod_season_no int,                   @existing_inv_no int,                           @new_inv_no int,
            @season_no int,                                 @delete_performance_no int,                     @location_content_id INT,
            @existing_prod_no int

    /*  Log and Administrative variable declarations    */

    DECLARE @log_date datetime,                             @log_message varchar(255),                      @log_status char(3), 
            @rtn_msg varchar(100)

    DECLARE @valid_code_table table ([performance_code] varchar(10))
            
    /*  The log date is the date and time the procedure starts  */

    SELECT @log_date = getdate()


    /***************************************************************************************************************************************************************/
    /********************************************************************  PART 1  *********************************************************************************/
    /*******************************************  DELETE CANCELED PRESENTATIONS/ACTIVITIES FROM TESSITURA  *********************************************************/
    /***************************************************************************************************************************************************************/
    
     /* A cursor is generated comparing what's found in the impresario databse using the LV_PRODUCTION_ELEMENTS_PERFORMANCE View  with what's found in the
        Events database using the LV_QUARK_EVENT_INFO view, returning records from LV_PRODUCTION_ELEMENTS_PERFORMANCE where the performance code does not
        exists in the LV_QUARK_EVENT_INFO.  
        
        The final list contains the presentations and activities (if any) that were canceled and deleted from the Events database on Quark since the last update.
        
        12/14/16: Changed process a little.  Procedure was having problems changing a performance that was on the same date and time and in the same location when
                  the title changed because technically the performance code already existed.  Took out the line that checked against the LV_QUARK_EVENT_INFO view
                  and added an IF EXISTS line that tells it to skip the delete process if that program exists in the Quark Info.  Also changed the order so that the
                  deletes happen first.  That way the old programs are deleted first and the new programs are added in after.
      */

    DECLARE PresentationDeleteCursor INSENSITIVE CURSOR FOR
    SELECT [performance_no], [performance_code], [performance_type_name], [performance_date], [performance_time], [performance_activity_end], [title_name], [title_no],
           [production_name], [production_name_long], [production_name_short], [production_no], [production_season_name], [production_season_no]
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
    WHERE [performance_date] >= convert(char(10),getdate(),111) AND title_name IN ('Drop-In Activities','Live Presentations') --and [performance_type_name] in ('Live Presentation', 'Drop-In Activity')
          --and [performance_code] not in (SELECT [tessitura_performance_code] FROM [dbo].[LV_QUARK_EVENT_INFO])
    ORDER BY [Performance_date], [Performance_Time], [title_name]
    OPEN PresentationDeleteCursor
    BEGIN_PRESENTATION_DELETE_LOOP:

        FETCH NEXT FROM PresentationDeleteCursor INTO @delete_performance_no, @delete_performance_code, @show_type, @show_date, @show_start_time, @show_end_time, 
                                                      @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, @tessitura_production_name_long, 
                                                      @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name,
                                                      @tessitura_production_season_no
        IF @@FETCH_STATUS = -1 GOTO END_PRESENTATION_DELETE_LOOP

        IF EXISTS (SELECT * FROM [dbo].[LV_QUARK_EVENT_INFO] 
                   WHERE [show_date] = @show_date AND [tessitura_production_name] = @tessitura_production_name AND [tessitura_performance_code] = @delete_performance_code)
            GOTO BEGIN_PRESENTATION_DELETE_LOOP

        BEGIN TRY

            BEGIN TRANSACTION

                DELETE FROM [dbo].[T_PERF] WHERE [perf_no] = @delete_performance_no

                DELETE FROM [dbo].[T_INVENTORY] WHERE [inv_no] = @delete_performance_no

            COMMIT TRANSACTION

                SELECT @log_message = @show_date + ' performance ' + @delete_performance_code + ' of production ' + @tessitura_production_name + ' at ' 
                                    + rtrim(@show_start_time) + ' has been deleted from the schedule.'
                SELECT @log_status = 'del'
                GOTO LOG_DELETE

        END TRY
        BEGIN CATCH

            WHILE @@TRANCOUNT > 0 ROLLBACK TRAN
            SELECT @log_message = 'Error deleting performance ' + @delete_performance_code + ' from the schedule.'
            SELECT @log_status = 'err'
            GOTO LOG_DELETE

        END CATCH

        LOG_DELETE:

            /*  Write the results for the current record to the log table.  */

            IF @log_message = '' SELECT @Log_message = 'Unknown Status.'
            IF @log_status = '' SELECT @log_status = 'err'
            INSERT INTO [admin].[dbo].[tessitura_events_import_log]
            SELECT @log_date, getdate(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                   @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                   @existing_production_name, @existing_prod_no, @existing_prod_season_no
         

        GOTO BEGIN_PRESENTATION_DELETE_LOOP

    END_PRESENTATION_DELETE_LOOP:
    CLOSE PresentationDeleteCursor
    DEALLOCATE PresentationDeleteCursor


    /***************************************************************************************************************************************************************/
    /********************************************************************  PART 2  *********************************************************************************/
    /************************************************  IMPORT PRESENTATIONS/ACTIVITIES INTO TESSITURA  *************************************************************/
    /***************************************************************************************************************************************************************/
    
    /* A cursor is generated using the LV_QUARK_EVENT_INFO View, which is located in the impresario database but retrieves information from the
       Events Database, which is moved over from the Quark server nightly.

       This scripts require that each "performance" have a unique code that is based on date and time and location of the presentation or activity.
       These codes are pre-comiled within the view and if there are any duplicate codes, that code is ignored and not selected.
       
       The view already filters on date to show onlt the current and future dates, nothing in the past, so dates are not necessary in this select statement      */

    INSERT INTO @valid_code_table SELECT [tessitura_performance_code] FROM [dbo].[LV_QUARK_EVENT_INFO] GROUP BY [tessitura_performance_code] HAVING count(*) = 1

    DECLARE PresentationCreateCursor INSENSITIVE CURSOR FOR
    SELECT [show_type], [show_date], [show_start_time], [show_end_time], [tessitura_title], [tessitura_production_name], [tessitura_production_name_long], 
           [tessitura_performance_code], [Location_name], [Duration]
    FROM [dbo].[LV_QUARK_EVENT_INFO]
    WHERE [tessitura_performance_code] in (SELECT [performance_code] FROM @valid_code_table)
    ORDER BY [show_date], [show_start_time], [location_name]
    OPEN PresentationCreateCursor
    BEGIN_PRESENTATION_CREATE_LOOP:

        /*  Reset certain variables to blank or null values to make sure values don't carry over from record to record  */

        SELECT @log_message = '', @log_status = ''
        SELECT @existing_inv_no = null, @existing_prod_season_no = null, @tessitura_perf_type_no = null
        SELECT @tessitura_production_no = null, @tessitura_production_season_no = null, @tessitura_title_no = null
        SELECT @tessitura_production_name_short = null, @existing_prod_season_name = null, @existing_production_name = NULL, @existing_prod_no = null

        /*  Records are pulled one at a time from the cursor and processed.  All performances are checked every time the procedure is run.  */

        FETCH NEXT FROM PresentationCreateCursor INTO @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_production_name, 
                                                      @tessitura_production_name_long, @tessitura_performance_code, @Location_name, @duration
        IF @@FETCH_STATUS = -1 GOTO END_PRESENTATION_CREATE_LOOP
        
        /*  @show_date must be a valid date - if not, error out and log results    */        

        IF isdate(@show_date) = 0 BEGIN
            SELECT @log_message = 'Invalid Show Date (' + @show_date + ').'
            SELECT @log_status = 'err'
            INSERT INTO [admin].[dbo].[tessitura_events_import_log]
            SELECT @log_date, getdate(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                   @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                   @existing_production_name, @existing_prod_no, @existing_prod_season_no

            GOTO BEGIN_PRESENTATION_CREATE_LOOP
        END

        /*  Convert activity start and end times to datetime (for later use)    */

        SELECT @activity_start_time = convert(datetime,(@show_date + ' ' + @show_start_time)),
               @activity_end_time = convert(datetime,(@show_date + ' ' + @show_end_time))

        /*  Verifies that the title (venue) is there by getting its id number.  If no id number is found, error out and log results   */
    
        SELECT @tessitura_title_no = [title_no] FROM [dbo].[LV_PRODUCTION_ELEMENTS_TITLE] WHERE [title_name] = @tessitura_title_name
        SELECT @tessitura_title_no = IsNull(@tessitura_title_no,0)
        
        IF @tessitura_title_no <= 0 BEGIN
            SELECT @log_message = @tessitura_title_name + ' Title does not exist in Tessitura.'
            SELECT @log_status = 'err'
            INSERT INTO [admin].[dbo].[tessitura_events_import_log]
            SELECT @log_date, getdate(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                   @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                   @existing_production_name, @existing_prod_no, @existing_prod_season_no

            GOTO BEGIN_PRESENTATION_CREATE_LOOP
        END

        /*  Verifies that the production (event title) is there by getting its id number and short title.  If no id number is found, error out and log results   */

        SELECT @tessitura_production_no = [production_no], @tessitura_production_name_short = [production_name_short] 
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [title_no] = @tessitura_title_no and [production_name] = @tessitura_production_name
        SELECT @tessitura_production_no = IsNull(@tessitura_production_no, 0)
        SELECT @tessitura_production_name_short = IsNull(@tessitura_production_name_short, '')

        IF @tessitura_production_no <= 0 BEGIN
            SELECT @log_message = @tessitura_production_name + ' Production does not exist under ' + @tessitura_title_name + ' in tessitura.'
            SELECT @log_status = 'err'
            INSERT INTO [admin].[dbo].[tessitura_events_import_log]
            SELECT @log_date, getdate(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                   @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                   @existing_production_name, @existing_prod_no, @existing_prod_season_no

            GOTO BEGIN_PRESENTATION_CREATE_LOOP
        END

        /*  Use the show_date to determine fiscal year, which runs July to June.  If month is >= 7, it's (year + 1) otherwise it's just year.   */

        IF datepart(month, @show_date) >= 7 SELECT @fiscal_year = (datepart(year, @show_Date) + 1)
        ELSE SELECT @fiscal_year = DATEPART(YEAR, @show_date)

        /*  Determines production season based on production name and fiscal year, then verifies it's there by getting its id number.  
            If no id number is found, an attempt is made to create the production season on the fly.
            If that attempt fails, error out and log results   */
        
        SELECT @tessitura_production_season_name = rtrim(@tessitura_production_name) + ' FY' + convert(varchar(4),@fiscal_year)
        SELECT @tessitura_production_season_no = [production_season_no] FROM [dbo].[LV_PRODUCTION_ELEMENTS_SEASON] WHERE [production_season_name] = @tessitura_production_season_name
        SELECT @tessitura_production_season_no = IsNull(@tessitura_production_season_no, 0)

        IF @tessitura_production_season_no <= 0 BEGIN
               
            BEGIN TRY

                EXECUTE [dbo].[AP_GET_NEXTID_function] @type = 'IV', @nextid = @tessitura_production_season_no OUTPUT, @increment = 1
                SELECT @tessitura_production_season_no = IsNull(@tessitura_production_season_no, 0)

                IF @tessitura_production_season_no > 0 BEGIN

                    SELECT @season_no = max(id) FROM TR_SEASON WHERE [description] = 'Fiscal Year ' + convert(varchar(4),@fiscal_year)
                    SELECT @season_no = IsNull(@season_no, 0)
                    
                    IF @season_no > 0 BEGIN

                        BEGIN TRANSACTION

                            INSERT INTO [dbo].[T_PROD_SEASON] ([prod_season_no], [prod_no], [season], [premiere], [allow_multiple_entry])
                            VALUES (@tessitura_production_season_no, @tessitura_production_no, @season_no, Null, 'N')

                            INSERT INTO [T_INVENTORY] ([inv_no], [description],[type])
                            VALUES (@tessitura_production_season_no, @tessitura_production_season_name,'S')

                        COMMIT TRANSACTION

                    END

                END

            END TRY
            BEGIN CATCH

                SELECT @tessitura_production_season_no = 0
                WHILE @@TRANCOUNT < 0 ROLLBACK TRAN

            END CATCH
           
        END
        
        IF @tessitura_production_season_no <= 0 BEGIN
            SELECT @log_message = @tessitura_production_season_name + ' Production Season does not exist under ' + @tessitura_title_name + ' in tessitura.'
            SELECT @log_status = 'err'
            INSERT INTO [admin].[dbo].[tessitura_events_import_log]
            SELECT @log_date, getdate(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                   @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                   @existing_production_name, @existing_prod_no, @existing_prod_season_no

            GOTO BEGIN_PRESENTATION_CREATE_LOOP
        END

        /*  Determine performance type (Live Presentation or Drop-In Activity) based on the show type from the events database.
            Get id number of the designated performance type for use later.  If no id number is found, error out and log results   */

        IF @show_type in ('CS&T', 'Programs') SELECT @tessitura_perf_type_name = 'Live Presentation' 
        ELSE IF @show_type = 'Drop In Activities' SELECT @tessitura_perf_type_name = 'Drop-In Activity'
        ELSE SELECT @tessitura_perf_type_name = ''

        IF @tessitura_perf_type_name <> '' SELECT @tessitura_perf_type_no = [id] FROM [dbo].[TR_PERF_TYPE] WHERE [description] = @tessitura_perf_type_name
        
        SELECT @tessitura_perf_type_no = IsNull(@tessitura_perf_type_no, 0)

        IF @tessitura_perf_type_no <= 0 BEGIN
            SELECT @log_message = 'Unable to find performance type for ' + @show_type + '.'
            SELECT @log_status = 'err'
            INSERT INTO [admin].[dbo].[tessitura_events_import_log]
            SELECT @log_date, getdate(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                   @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                   @existing_production_name, @existing_prod_no, @existing_prod_season_no

            GOTO BEGIN_PRESENTATION_CREATE_LOOP
        END

        /*  If a record with the designated performance code does not already exist, use the local procedure LP_SS_AddDailyPresentation to add it to the database.
            Log the procedure's results accordingly (cmp for completed or err for error).       */

        IF not exists (SELECT * FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [production_season_no] = @tessitura_production_season_no and performance_code = @tessitura_performance_code) BEGIN

            BEGIN TRY

                EXECUTE [dbo].[LP_SS_AddDailyPresentation]
                        @title_no = @tessitura_title_no,                    @prod_no = @tessitura_production_no,
                        @perf_type = @tessitura_perf_type_no,               @perf_date = @show_date,
                        @perf_time = @show_start_time,                      @perf_code = @tessitura_performance_code,
                        @activity_start = @show_start_time,                 @activity_end = @show_end_time,
                        @location_name = @Location_name,                    @new_inv_no = @new_inv_no OUTPUT,
                        @return_message = @rtn_msg OUTPUT

                SELECT @rtn_msg = isnull(@rtn_msg, 'Unknown Status')
                SELECT @log_message = @tessitura_performance_code + ' - inv_no: ' + convert(varchar(10),@new_inv_no) + ' - ' + @tessitura_production_name + ' - ' + @rtn_msg
                SELECT @log_status = 'cmp'
                                
            END TRY
            BEGIN CATCH

                SELECT @log_message = 'Error creating performance (' + @tessitura_performance_code + ').'
                SELECT @log_status = 'err'
               
            END CATCH

            /*  Write the results for the current record to the log table.  */

            IF @log_message = '' SELECT @Log_message = 'Unknown Status.'
            IF @log_status = '' SELECT @log_status = 'err'
            INSERT INTO [admin].[dbo].[tessitura_events_import_log]
            SELECT @log_date, getdate(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                   @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                   @existing_production_name, @existing_prod_no, @existing_prod_season_no

            GOTO BEGIN_PRESENTATION_CREATE_LOOP
 
        END

        /*  If there is already a performance with that code in the database, check for multiple records with the designated performance_code.
            This is a double-check of something that has already been checked once when selecting the records into the cursor.
            If there are multiple records in the database with the same code, error out and log results.        */

        IF (SELECT count(*) FROM [LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [production_season_no] = @existing_prod_season_no and [performance_code] = @tessitura_performance_code) > 1 BEGIN
            SELECT @log_message = 'Multiple performances found with the same code (' + @tessitura_performance_code + ').'
            SELECT @log_status = 'err'
        END ELSE BEGIN

            /*  Determine the existing production season number and production name by searching the LV_PRODUCTION_ELEMENTS_PERFORMANCE view for the
                performance code and retrieving the existing production season id number.  
                If no id number is found, error out and log results.
                If the existing production season number is different from the current production season number, update the T_PERF and T_INVENTORY records with the
                information from the new production and log the results.        */
    
            SELECT @existing_inv_no = [performance_no], @existing_prod_season_no = [production_season_no], @existing_prod_season_name = [production_season_name], 
                   @existing_prod_no = [production_no], @existing_production_name = [production_name], @existing_start_time = [performance_activity_start], @existing_end_time = [performance_activity_end],
                   @existing_Performance_location = [performance_location]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE performance_code = @tessitura_performance_code
          
            SELECT @existing_prod_season_no = IsNull(@existing_prod_season_no, 0)
            SELECT @existing_prod_season_name = IsNull(@existing_prod_season_name, '')
            SELECT @existing_prod_no = ISNULL(@existing_prod_no,0)
            SELECT @existing_production_name = IsNull(@existing_production_name, '')
            SELECT @existing_inv_no = IsNull(@existing_inv_no, 0)
            SELECT @existing_Performance_location = ISNULL(@existing_Performance_location, '')


        END

               
        IF @existing_prod_season_no <= 0 BEGIN
            SELECT @log_message = 'Cannot determine existing production season number (' + @tessitura_performance_code + ').'
            SELECT @log_status = 'err'
        END ELSE IF @existing_inv_no <= 0 BEGIN
            SELECT @log_message = 'Unable to determine inventory number for performance ' + @tessitura_performance_code + '.'
            SELECT @log_status = 'err'
        END ELSE IF @existing_prod_season_no = @tessitura_production_season_no AND @existing_Performance_location = @Location_name
                    AND NOT (@tessitura_perf_type_name = 'Drop-In Activity' AND @activity_end_time <> @existing_end_time) BEGIN
            SELECT @log_message = 'No Change', @log_status = 'skp'
        END ELSE BEGIN

            IF @existing_prod_season_no <> @tessitura_production_season_no BEGIN
     
                BEGIN TRY

                    BEGIN TRANSACTION

                        UPDATE [dbo].[T_PERF] SET [prod_season_no] = @tessitura_production_season_no WHERE perf_no = @existing_inv_no

                        IF @tessitura_production_name_short = '' SELECT @tessitura_production_name_short = left(@tessitura_production_name, 20)

                        UPDATE [dbo].[T_INVENTORY] SET [description] = @tessitura_production_name, [short_name] = @tessitura_production_name_short WHERE inv_no = @existing_inv_no

                    COMMIT TRANSACTION

                    SELECT @log_message = 'Production ' + rtrim(@tessitura_production_name) + ' changed from ' + @existing_production_name + '.'
                    SELECT @log_status = 'cmp'

                END TRY
                BEGIN CATCH

                    WHILE @@TRANCOUNT > 0 ROLLBACK TRAN
                    SELECT @log_message = 'Error changing production (' + convert(varchar(10), @existing_prod_season_no) + '/' + convert(varchar(10), @tessitura_production_season_no) + ').'
                    SELECT @log_status = 'err'
                                    
                END CATCH
                       
            END

            IF @log_message <> '' BEGIN
                IF @log_status = '' SELECT @log_status = 'err'
                INSERT INTO [admin].[dbo].[tessitura_events_import_log]
                SELECT @log_date, getdate(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                       @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                       @existing_production_name, @existing_prod_no, @existing_prod_season_no
                SELECT @log_status = '', @log_message = ''
            END


            IF @existing_Performance_location <> @Location_name BEGIN

                BEGIN TRY

                    BEGIN TRANSACTION

                        SELECT @location_content_id = [id] FROM [dbo].[TR_INV_CONTENT] WHERE [description] = 'Location'
                        SELECT @location_content_id = IsNull(@location_content_id, 0)

                        IF @location_content_id > 0 and @Location_name = '' BEGIN
                            DELETE FROM [dbo].[TX_INV_CONTENT] WHERE [inv_no] = @existing_inv_no and [content_type] = @location_content_id
                        END ELSE IF @location_content_id > 0 AND @Location_name <> '' BEGIN

                            IF NOT EXISTS (SELECT * FROM [dbo].[TX_INV_CONTENT] WHERE [inv_no] = @existing_inv_no AND [content_type] = @location_content_id)
                                INSERT INTO [dbo].[TX_INV_CONTENT] ([inv_no], [content_type], [value]) VALUES (@existing_inv_no, @location_content_id, @Location_name)
                            ELSE
                                UPDATE [dbo].[TX_INV_CONTENT] SET [value] = @Location_name WHERE [inv_no] = @existing_inv_no AND [content_type] = @location_content_id

                        END
                        SELECT @log_message = rtrim(@existing_production_name) + ' location changed from ' + CASE WHEN @existing_Performance_location = '' THEN 'Blank' ELSE @existing_Performance_location END
                                            + ' to ' + CASE WHEN @Location_name = '' THEN 'Blank' ELSE @Location_name END + '.'
                        SELECT @log_status = 'cmp'

                    COMMIT TRANSACTION
                    
                END TRY
                BEGIN CATCH

                    WHILE @@TRANCOUNT > 0 ROLLBACK TRAN
                    SELECT @log_message = 'Error changing ' + @existing_production_name + ' location to ' + @Location_name + '.'
                    SELECT @log_status = 'err'

                END CATCH


            END

            IF @log_message <> '' BEGIN
                IF @log_status = '' SELECT @log_status = 'err'
                INSERT INTO [admin].[dbo].[tessitura_events_import_log]
                SELECT @log_date, getdate(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                       @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                       @existing_production_name, @existing_prod_season_no, @existing_prod_season_no
                SELECT @log_status = '', @log_message = ''
            END

            /*  If this is a drop-in activity, check the end time.  if it has changed, change the doors_close column in the performance record.
                The start time will never be changed from here.  The start time is a part of the unique identifier on the record. 
                If the start time is changed, the original record will be deleted and a new record with a new start time will be created in its place.   */

            IF @tessitura_perf_type_name = 'Drop-In Activity' AND @activity_end_time <> @existing_end_time BEGIN
        
                IF @existing_inv_no <= 0 BEGIN
                    IF @log_message <> '' SELECT @log_message = @log_message + '  '
                    SELECT @log_message = @log_message + 'Unable to determine inventory number for performance ' + @tessitura_performance_code + '.'
                    SELECT @log_status = 'err'
                END ELSE BEGIN

                    BEGIN TRY

                       BEGIN TRANSACTION
                        
                           UPDATE [dbo].[T_PERF] SET [doors_close] = @activity_end_time WHERE perf_no = @existing_inv_no

                        COMMIT TRANSACTION
        
                        IF @log_message <> '' SELECT @log_message = @log_message + '  '
                        SELECT @log_message = @log_message + 'End time on ' + @tessitura_performance_code + ' changed from ' + convert(char(8),@existing_end_time,108) 
                                                   + ' to ' + convert(char(8),@activity_end_time,108) + '.'
                        SELECT @log_status = 'cmp'
    
                    END TRY
                    BEGIN CATCH

                        WHILE @@TRANCOUNT > 0 ROLLBACK TRAN
                        IF @log_message <> '' SELECT @log_message = @log_message + '  '
                        SELECT @log_message = @log_message + 'Error changing end time on ' + @tessitura_performance_code + ' from ' + CONVERT(CHAR(8),@existing_end_time,108) 
                                                       + ' to ' + CONVERT(CHAR(8),@activity_end_time,108) + '.'
                       SELECT @log_status = 'err'

                    END CATCH

                END

            END

            IF @log_message <> '' BEGIN
                IF @log_status = '' SELECT @log_status = 'err'
                INSERT INTO [admin].[dbo].[tessitura_events_import_log]
                SELECT @log_date, GETDATE(), @log_status, @log_message, @show_type, @show_date, @show_start_time, @show_end_time, @tessitura_title_name, @tessitura_title_no, @tessitura_production_name, 
                       @tessitura_production_name_long, @tessitura_production_name_short,  @tessitura_production_no, @tessitura_production_season_name, @tessitura_production_season_no, @Location_name, @duration, 
                       @existing_production_name, @existing_prod_no, @existing_prod_season_no
                SELECT @log_status = '', @log_message = ''
            END
        END
                    
        GOTO BEGIN_PRESENTATION_CREATE_LOOP
    
    END_PRESENTATION_CREATE_LOOP:
    CLOSE PresentationCreateCursor
    DEALLOCATE PresentationCreateCursor

    

    DONE:
        
        WHILE @@TRANCOUNT > 0 ROLLBACK TRAN

        IF NOT EXISTS (SELECT * FROM [admin].[dbo].[tessitura_events_import_log] WHERE log_date = @log_date)
            INSERT INTO [admin].[dbo].[tessitura_events_import_log] SELECT @log_date, GETDATE(), 'non', 'No Changes Made', '', '', '', '', '', 0, '', '', '', 0, '', 0, '', 0, '', '', 0

        IF @display_results = 'Y' SELECT * FROM [admin].[dbo].[tessitura_events_import_log] WHERE log_date = @log_date

        /* Delete Log Entries after 30 days so that the table doesn't grow to be huge  */
        DELETE FROM [admin].[dbo].[tessitura_events_import_log] WHERE DATEDIFF(DAY,[log_date], GETDATE()) > 60

END
GO

GRANT EXECUTE ON [dbo].[LP_ImportPresentationsAndActivities] to impusers
GO

