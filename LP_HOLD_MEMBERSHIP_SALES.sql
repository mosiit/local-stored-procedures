USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jonathan Smillie, Tessitura Network
-- Create date: March 2016 (this version 4/1/16) 
-- Description:	
/* Script to put membership holds on a renewal performance for One-Step members. 
Working assumptions: 
1. The procedure is being run some time after the end of membership sales in the originating performance but 
before the beginning of sales in the target performance 
2. The target performance is built in the same facility as the originating performance. 
3. A specific hold code has been created to reserve "seats" for membership renewals.
This process is necessary because the rollover procedure that creates orders from one season to the next does
so by reserving the SAME SEAT from one event to the next. So the renewal procedure will fail if there isn't a 
seat available that correlates to the one sold to this member last year.
*/
-- =============================================
ALTER PROCEDURE [dbo].[LP_HOLD_MEMBERSHIP_SALES] @orig_season int, 
@dest_season int, 
--@orig_perf int,
@dest_perf int, 
@hold_code int, 
@zone_str varchar(100)=null, 
@order_cat varchar(30)=null,
@mode varchar(1), 
@list_no int
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--
-- First we make a table of all the seats that are paid, ticketed or marked attended 
-- in the originating performance 
--  
create table #zones_to_rollover 
(zone_no int, zone_desc varchar(30)) 
create table #order_cat
(order_cat int,description varchar(30)) 


if @zone_str is not null or len(@zone_str) > 0 
insert #zones_to_rollover (zone_no,zone_desc) 
select a.element,b.description 
from FT_SPLIT_LIST(@zone_str,',') a join t_zone b on a.element = b.zone_no 
join t_perf c on b.zmap_no = c.zmap_no 
where c.perf_no = @dest_perf 
--
-- if no explicit list of zones supplied, exclude the known values for upgrade and lost-card zones 
--
else 
insert #zones_to_rollover (zone_no,zone_desc) 
select a.element,b.description 
from FT_SPLIT_LIST(@zone_str,',') a join t_zone b on a.element = b.zone_no 
where b.zmap_no in (select zmap_no from t_perf where perf_no = @dest_perf) AND 
(b.description not like 'Upgrade%' or b.description not like '%lost%' or b.description not like '%legacy%') 

if @order_cat is not null or len(@order_cat) > 0 
insert #order_cat 
(order_cat, description) 
select a.element, b.category_desc 
from FT_SPLIT_LIST(@order_cat,',') a join LTR_ORDER_CAT_FOR_PARAMETER b 
on a.Element = b.category_id

--
-- if no explicit list of categories supplied, populate table with all values except that for gift memberships 
-- 
else 
insert #order_cat 
(order_cat, description) 
select category_id, category_desc 
from LTR_ORDER_CAT_FOR_PARAMETER
where category_id = 0 or category_desc not like '%Gift%' 


--select * from #order_cat

declare @memb_perf_type int = ( select id from tr_perf_type where id in (select default_value from t_defaults 
 where field_name like 'Membership_Perf_Type')   )
create table #hold_for_rollover
(orig_perf int, 
 seat_no int, 
 section_ind int, 
 seat_row varchar(10), 
 seat_num varchar(10), 
 dest_perf int, 
 hold_code int, 
 class int, 
 customer_no int,
 cust_name varchar(60)  
  ) 
insert #hold_for_rollover 
 (seat_no,orig_perf,section_ind,seat_row,seat_num,dest_perf,hold_code,class,customer_no) 
select a.seat_no,a.perf_no,b.section,a.seat_row,a.seat_num,@dest_perf,@hold_code,isnull(o.class,0),a.customer_no
from tx_perf_seat a 
join t_seat b on a.seat_no = b.seat_no  
join t_order o on a.order_no = o.order_no
join #order_cat oc on isnull(o.class,0) = oc.order_cat
where (a.perf_no in (select perf_no from t_perf where season = @orig_season)
and seat_status in (8,13,22))
and a.perf_no in (select perf_no from t_perf where perf_type = @memb_perf_type) 
and a.zone_no in (select zone_no from #zones_to_rollover) 
and a.customer_no in (select customer_no from t_list_contents where list_no = @list_no) 
and isnull(o.class,0) in (select order_cat from #order_cat) 
-- logic added 072717 to not rollover orders that are gift memberships 



update #hold_for_rollover set cust_name = dbo.FS_GET_CONSTITUENT_DISPLAY_NAME(a.customer_no) 
from #hold_for_rollover a -- 
-- then we get rid of anything we've found where a membership hold already exists on the seat in the 
-- destination performance (in case you run this more than once for a given membership renewal perf) 
-- 

--select * from #hold_for_rollover 

/* And now we determine if you really want to do this. If you've given the "insert" instruction to the 'mode" 
parameter, we'll actually put rows in the TX_PERF_HC table for this stuff. 
*/  

 if @mode = 'I' 
 begin  
  delete #hold_for_rollover 
where seat_no in 
(select seat_no from tx_perf_hc where perf_no = @dest_perf and hc_no = @hold_code) 
  
  insert tx_perf_hc 
(seat_no,hc_no,perf_no,start_dt,priority) 
select distinct(seat_no),@hold_code,@dest_perf,getdate(),'1'  
from #hold_for_rollover 
where seat_no not in (select distinct(seat_no) from tx_perf_hc 
 where perf_no in (select perf_no from t_perf where season = @dest_season) 
 and perf_no = @dest_perf and hc_no = @hold_code) 
 --print 'Seats added to hold code table' 
 end 
 -- else 
 -- begin 
 ---- print 'No action taken - run in review mode only' 
 -- end 
 ;

with data_for_output_cte 
as 
(
 select distinct(a.seat_no),
 z.description as zone,
 a.seat_row as row, 
 a.seat_num as seat_num,
 isnull(b.hc_no,@hold_code) as hc_no, 
 isnull(q.description,'Membership Holds') as hold_code,
 a.dest_perf as perf_no, 
 x.description as perf_desc,
 y.perf_dt, 
 a.cust_name, 
 action = case when @mode = 'I' then 'Inserted' when @mode = 'R' then 'Review Only' 
  end 
 from #hold_for_rollover a 
 left join tx_perf_hc b on a.dest_perf = b.perf_no and a.seat_no = b.seat_no 
 join tx_perf_seat c on a.orig_perf = c.perf_no and a.seat_no = c.seat_no 
 join t_zone z on c.zone_no = z.zone_no 
 left join t_hc q on b.hc_no = q.hc_no 
 join t_inventory x on x.inv_no = @dest_perf 
 join t_perf y on y.perf_no = @dest_perf 
 )
 select seat_no,
 zone,
 row,seat_num,
 hc_no,
 hold_code,
 perf_no,
 perf_desc,
 perf_dt, 
 cust_name,
 action from data_for_output_cte
END

