USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[NP_UPDATE_EVENT_MEMBERSHIP_COUNTS] (
	@perf_no INT,
	@customer_no INT,
	@cust_memb_no INT,
	@adult_count INT,
	@child_count INT,
	@other_count INT,
	@control_id INT,
	@attendance_id INT = NULL)
AS

/****************************************************************************
N-Scan Version 6.0
Tessitura Versions 12.1
RJW - 12/28/2013

Procedure to update membership counts for event scans.

NP_UPDATE_EVENT_MEMBERSHIP_COUNTS @perf_no = 66, @customer_no = 3, @cust_memb_no = 38,
	@adult_count = 1, @child_count = 1, @other_count = 0,
	@control_id = 17, @attendance_id = null

-- 03/13/2018 - Modified J Smillie Tessitura Network 
-- Added code lines 137-138 and 149-151 to handle cases where a 0 is entered for all 
-- values of entry 
-- Return an error to the device and don't write a zero-value entry record to the N-Scan 
-- data table 

***************************************************************************/
DECLARE	@status VARCHAR(50),
	@adult_limit INT,
	@child_limit INT,
	@other_limit INT,
	@memb_org_no INT,
	@memb_level_no INT,
	@update_dt DATETIME,
	@device VARCHAR(30)

SELECT	@memb_org_no = COALESCE(a.memb_org_no, 0),
	@memb_level_no = COALESCE(b.memb_level_no, 0),
	@adult_limit = COALESCE(b.admission_adult, 0),
	@child_limit = COALESCE(b.admission_child, 0),
	@other_limit = COALESCE(b.admission_other, 0)
FROM	TX_CUST_MEMBERSHIP a
	JOIN T_MEMB_LEVEL b ON a.memb_org_no = b.memb_org_no AND a.memb_level = b.memb_level
WHERE	a.cust_memb_no = @cust_memb_no
	AND a.customer_no = @customer_no
	AND a.cur_record = 'Y'

IF COALESCE(@memb_org_no, 0) = 0
  BEGIN
	SELECT	@status = 'Active membership not found'
	GOTO final
  END

IF NOT EXISTS(SELECT * FROM T_PERF a JOIN TX_PROD_SEASON_MEMB_ORG b ON a.prod_season_no = b.prod_season_no
		WHERE a.perf_no = @perf_no AND b.memb_org_no = @memb_org_no)
  BEGIN
	SELECT	@status = 'Memberships not allowed for performance'
	GOTO final
  END

DECLARE	@temp_msg VARCHAR(30) = ''
IF @adult_count > @adult_limit
	SELECT	@temp_msg = 'Adult'

IF @child_count > @child_limit
	SELECT	@temp_msg = @temp_msg + CASE WHEN @temp_msg <> '' THEN ', ' ELSE '' END + 'Child'

IF @other_count > @other_limit
	SELECT	@temp_msg = @temp_msg + CASE WHEN @temp_msg <> '' THEN ', ' ELSE '' END + 'Other'

IF @temp_msg <> ''
  BEGIN
	SELECT	@status = @temp_msg + ' limit exceeded'
	GOTO final
  END

SELECT	@update_dt = update_dt,
	@device = device_name
FROM	T_NSCAN_EVENT_CONTROL
WHERE	id = @control_id

IF @update_dt IS NULL
  BEGIN
	SELECT	@status = 'Invalid control record'
	GOTO final
  END

IF COALESCE(@attendance_id, 0) = 0
	SELECT	@attendance_id = id
	FROM	T_ATTENDANCE
	WHERE	customer_no = @customer_no
		AND perf_no = @perf_no
		AND cust_memb_no = @cust_memb_no

IF COALESCE(@attendance_id, 0) > 0
  BEGIN
	UPDATE	T_ATTENDANCE
	SET	admission_adult = @adult_count,
		admission_child = @child_count,
		admission_other = @other_count
	WHERE	id = @attendance_id
	
	IF @@ROWCOUNT = 0
	  BEGIN
		SELECT	@status = 'Invalid attendance id'
		GOTO final
	  END
  END
ELSE
  BEGIN
	INSERT	T_ATTENDANCE(customer_no, perf_no, attend_dt, device, admission_adult, admission_child,
			admission_other, cust_memb_no, memb_level_no)
	SELECT	@customer_no,
		@perf_no,
		@update_dt,
		@device,
		@adult_count,
		@child_count,
		@other_count,
		@cust_memb_no,
		@memb_level_no
	
	SELECT	@attendance_id = SCOPE_IDENTITY()
  END
--- 
--- Customization 03/13/2018 JTS Tessitura Network 
--- Don't write a zero-attendance record if the number entered for all membership-type 
--- entries is 0 
--- 
IF @adult_count + @child_count + @other_count > 0 
BEGIN  
UPDATE	T_NSCAN_EVENT_CONTROL
SET	perf_no = @perf_no,
	status = 'X',
	ticket_ok = 'Y',
	ticket_updated = 'Y',
	ticket_msg = 'OK',
	attendance_id = @attendance_id
WHERE	id = @control_id

SELECT	@status = 'OK'
END 
ELSE 
SELECT @status = 'Invalid count selected'

final:

SELECT	perf_no = @perf_no,
	customer_no = @customer_no,
	cust_memb_no = @cust_memb_no,
	adult_count = @adult_count,
	child_count = @child_count,
	other_count = @other_count,
	control_id = @control_id,
	attendance_id = @attendance_id,
	status = @status


GO

