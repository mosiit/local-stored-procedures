USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[AP_ACKNOWLEDGEMENT_LETTERS]    Script Date: 5/22/2019 12:52:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER            PROCEDURE [dbo].[LP_ACKNOWLEDGEMENT_LETTERS]
(
	@letter_id int ,
	@list_no int = 0,
	@reprint_ind Int,  -- 1 Yes, 2 No (do not reprint)
	@start_dt datetime,
	@end_dt datetime,
	@mail_dt datetime = null, 
	@mail_type varchar(8) = null, 
	@label varchar(1) = 'N',
	@signor int = null,
	@eaddress_type int = null,
	@eaddress_purpose varchar(8) = null,
	@eaddress_market_ind_no char(1) = null
)
 
 AS
Set NoCount On  -- added auto CWR 8/9/2001
 
/**********************************Acknowledgement Selection Stored Proc************************************
 
This is called from all Acknowledgement Datawindow formats
 
Modified 2/4/99 CWR added columns for solicitor, source_no, and payment_type.  Payment
	type gets filled in with '***Mixed***' if more than one payment method was used.  It will
	be NULL if no payment methods were used
Modified 8/30/99 CWR changed logic when list_no is used to up front when creating #LETTERS instead of 
	at the end when updating.  There was an 'Insert Into' clause at that point.
Modified 3/30/00 CWR added column Printed_dt to be updated when letters are run the first time.  
	This column does not get updated in reprint mode, and is used mainly for the contact
	log.
Modified 4/26/00 by CWR removed some of the update conditions at end of procedure
Modified 11/3/2000 by CWR -- changed null comparisons to 7.0 behaviour
Modified 1/23/2001 by CWR -- added 2 new columns: declined_ind and constituencies.  Declined_ind is from
	the t_ack_ext table and constituencies is a comma delimited list of constituences
	retrieved by calling AP_CONST_STRING
Modified 1/26/2000 by CWR -- added 2 new columns: fund_no and fund_desc
Modified 6/4/2001 by CWR -- changed method of getting addresses and salutations to use AP_GET_ADDRESS and
	AP_GET_SAL.  Also added 4 new parameters for this.  Note that if there is no specific signor parameter, then
	the signor from the t_ack_rules table is used.
Modified 8/29/2001 by CWR -- added new syntax for getting formatted postal codes, also now returning
	business_title
Modified 12/3/2001 by CWR -- If the signer was left blank on the Report Parameter window, it was not
	picking up the signer indicated in the Ack rules.
Modified 9/9/2002 by CWR -- now returning contribution notes as last column
Modified 10/21/2002 by CWR -- allowing contributions notes to be null (mistake from last mod)
Modified 1/12/2003 (3.30) by CWR -- returning two new columns, ack_no and duplicate_of
Modified 9/18/2003 (3.40) by CWR -- now returning two new columns, tax_receipt_no and orig_tax_receipt_no.  Code
	here sets the tax_receipt_no if one is called for in t_format_info.
Modified 9/22/2003 by CWR -- code that was updating d_customer_no for a 'Donor' record was also updating
	rows where the creditee_type was 'R'
Modified CWR 2/15/2005; updated for address structure changes
Modified CWR 11/1/2005 -- removed soon to be obsolete *= and =* Outer Join syntax
Modified CWR 11/7/2005 -- added new custom columns to output
Modified CWR 1/21/2006 -- added owner name to table references
Modified LB 2/24/2006 Added ands update for the payment type to fix problem with payment type being null in the output table.
Modified CWR 2/25/2006 -- made Liz's change the default behavior, with a T_DEFAULT setting to make it work the
	old way
Modified CWR 9/26/2006 -- changed this so that the code that updates the related donor or recipient will not
	execute if the customer_no's are the same.  Also added some indexes to the #LETTERS table for performance
	and limited the scope of the grouping of the creditee table (previously it was considering all rows)
Modified CWR 12/14/2006 -- replaced cursor with call to new scalar function FS_CONST_STRING
Modified RWC 1/31/2011 FP1832. -- Using standard function for default values.
	Few syntax updates here, consolidation of update statements into initial insert where possible.
	Removed some old commented code, updated all join syntax.
	Control group check on List now.
Modified CWR 9/2/2011 #4213 -- added street3 to output
Modified RWC 9/23/2011 FP1969.  -- Notes field not populated in original insert after moved from a previous
	Update statement.
Modified RWC 11/1/2011 FP1997.  -- Removed cursors here as we can do this using FT_GET_ADDRESS 
	and more set-based means now for tax receipts.
Modified RWC 1/20/2012 #5161 -- Left off formatting for postal code in refactoring.
Modified RAP 7/9/2012 #1221 -- Restore previous method of assigning tax receipt numbers
Modified CWR 7/16/2012 #1250 -- Restored previous method of assigning saluatations for creditors
Modified CWR 3/22/2013 #2075 -- fixed SQL in code getting salutations
Modified CWR 4/20/2013 #2236 -- changed @mail_type to varchar(8) for backward compatibility
Modified RAP 4/22/2013 #2231 -- multiple contact point param in FT_GET_ADDRESS
Modified DCA 4/22/2013 #2333 -- now returning display_name_short on worker_customer_no for solicitor column
Modified CWR 7/7/2013 #2671 -- now returning initiator_no and initiator_name
Modified CWR 7/21/2013 #2687 -- now returning sort_name and initiator_sort_name
Modified CWR 7/30/2013 #2687 -- forgot to return sortname data
Modified RWC 8/4/2013 #2687 -- was using worker sort name instead of owner
Modified RAP 8/16/2013 #2156 -- Add initiator_salutation
Modified DWW 11/21/2013 #7382 -- Make join to get initiator salutation a left join 
Modified CWR 12/19/2016 #5911 -- added email_address and phone columns
	
***********************************************************************************************************/
 
/**********************************Select customers based on letter_id***************************************/
Declare @receipt_counter int, @receipt_counter_desc varchar(30), @next_receipt_no int, @ack_no int
DECLARE @REPRINT_YES int = 1, @REPRINT_NO int = 2

Select	@receipt_counter = receipt_counter, @receipt_counter_desc = b.description
From	[dbo].T_FORMAT_INFO a
	JOIN [dbo].TR_TAX_RECEIPT_COUNTER b ON a.receipt_counter = b.id
Where	a.id = @letter_id

Declare @ack_dt datetime = GETDATE()

If @list_no > 0 and Not Exists (Select * From dbo.VS_LIST Where list_no = @list_no)
  Begin
	Raiserror('List provided does not exist or user does not have permission to use.', 11, 2) 
	RETURN
  End
  
DECLARE 
	@cur_cno int,
	@lname varchar(55),
	@fname varchar(20),
	@default_country int,
	@eaddress_market_ind char(1),
	@phone1 varchar(32),
	@phone2 varchar(32),
	@fax varchar(32),
	@address_no int

If @eaddress_market_ind_no = 1 
	Select @eaddress_market_ind = 'Y'
Else
	Select @eaddress_market_ind = 'N'

	
Create Table 	#LETTERS(
	id int identity(1,1),
	customer_no int null, 
	trn_no int null,
	cont_ref_no int null, 
	trn_desc varchar(30) null, 
	letter_id int null,
	trn_dt datetime null, 
	camp_desc varchar(30) null, 
	appeal_desc varchar(30) null, 
	memb_desc varchar(30) null,
	prem1_amt money null , 
	prem2_amt money null, 
	prem3_amt money null, 
	prem4_amt money null, 
	prem5_amt money null,
	prem1_desc varchar(30) null, 
	prem2_desc varchar(30) null, 
	prem3_desc varchar(30) null, 
	prem4_desc varchar(30) null,
	prem5_desc varchar(30) null, 
	address_type int null, 
	salutation int null , 
	street1 varchar(64) null ,
	street2 varchar(64) null, 
	street3 varchar(64) null, 
	city varchar(30) null, 
	state varchar(20) null,	
	postal_code varchar(10) null,
	country int null, 
	esal1_desc varchar(55) null,	
	esal2_desc varchar(55) null,	
	lsal_desc varchar(55) null,
	country_desc varchar(30) null, 
	cont_amt money null, 
	recd_amt money null, 
	inception_dt datetime null,
	goods_services_value money null ,
	prem_text varchar(30) null, 
	expr_dt datetime null,
	creditee_ind char(1) null,
	d_customer_no int null,
	d_esal1_desc varchar(55) null,
	cont_designation int null,
	designation_desc varchar(30) null,
	solicitor varchar(60) null,
	source_no int null,
	payment_type varchar(30) null,
	constituencies varchar(255) null,
	declined_ind char(1) null,
	fund_no int null,
	fund_description char(30) null,
	business_title varchar(55) null,
	notes varchar(255) null,
	ack_no int null,
	duplicate_of int null,
	tax_receipt_no int null,
	orig_tax_receipt_no int null,
	initiator_no int null,
	initiator_name varchar(160),
	sort_name varchar(55),
	initiator_sort_name varchar(55),
	initiator_esal1_desc varchar(55),
	initiator_esal2_desc varchar(55),
	initiator_lsal_desc varchar(55),
	initiator_business_title varchar(55),
	email_address varchar(80),
	phone1 varchar(32) null,
	phone2 varchar(32) null,
	address_no int,
	country_id int,
	cont_dt datetime,
	channel varchar(30),
	pmt_dt datetime)


Create clustered index temp1 ON #LETTERS(cont_ref_no)
Create index temp2 ON #LETTERS(customer_no)
Create index temp3 ON #LETTERS(trn_no)
 
--FP1382.  Using default function now
Select @default_country = Coalesce((Select max(id) from dbo.TR_COUNTRY
	Where ID = Convert(int,dbo.FS_GET_DEFAULT_VALUE(dbo.FS_GET_PARAM_FROM_APPNAME('UG'), 'IMPRESARIO', 'DEFAULT COUNTRY'))),1)
 
Insert into #LETTERS (customer_no, trn_no, cont_ref_no, trn_desc, letter_id, trn_dt, camp_desc, appeal_desc, memb_desc, 
					prem1_amt, prem2_amt, prem3_amt, prem4_amt, prem5_amt, prem1_desc, prem2_desc, prem3_desc, prem4_desc, prem5_desc,
					address_type, salutation, street1, street2, street3, city, state, postal_code, country, esal1_desc, esal2_desc, lsal_desc,
					country_desc, cont_amt, recd_amt, inception_dt, goods_services_value, prem_text, expr_dt, 
					creditee_ind, d_customer_no, d_esal1_desc, cont_designation, designation_desc, solicitor, source_no,
					payment_type, constituencies, declined_ind, fund_no, fund_description, business_title, notes, ack_no,
					duplicate_of, tax_receipt_no, orig_tax_receipt_no, initiator_no, initiator_name, sort_name, initiator_sort_name,
					initiator_esal1_desc, initiator_esal2_desc, initiator_lsal_desc, initiator_business_title, address_no, country_id, email_address,
					cont_dt,channel)
Select 	a.customer_no,
	a.trn_no,
	a.cont_ref_no,
	a.trn_desc,
	a.letter_id,
	a.trn_dt,
	a.camp_desc,
	a.appeal_desc,
	a.memb_desc,
	Coalesce(a.prem1_amt, 0),
	Coalesce(a.prem2_amt, 0),
	Coalesce(a.prem3_amt, 0),
	Coalesce(a.prem4_amt, 0),
	Coalesce(a.prem5_amt, 0),
	a.prem1_desc,
	a.prem2_desc,
	a.prem3_desc,
	a.prem4_desc,
	a.prem5_desc,
	a.address_type,
	a.salutation,
	ad.street1,
	ad.street2,
	ad.street3,
	ad.city,
	ad.state,
	postal_code = dbo.AF_FORMAT_STRING(ad.postal_code, co.zip_mask),
	ad.country,
	esal1_desc = Case When ads.customer_no is not null Then ads.esal1_desc When rqs.customer_no is not null Then rqs.esal1_desc Else ds.esal1_desc End,
	esal2_desc = Case When ads.customer_no is not null Then ads.esal2_desc When rqs.customer_no is not null Then rqs.esal2_desc Else ds.esal2_desc End,
	lsal_desc = Case When ads.customer_no is not null Then ads.lsal_desc When rqs.customer_no is not null Then rqs.lsal_desc Else ds.lsal_desc End,
	country_desc = Case When co.id = @default_country Then '' Else co.description End,
	a.cont_amt,
	a.recd_amt,
	a.inception_dt,
	Coalesce(a.goods_services_value, 0),
	prem_text = 'The Good Faith estimate of',
	a.expr_dt,
	a.creditee_ind,
	d_customer_no = case When a.creditee_ind = 'R' Then c.customer_no Else null end,
	d_esal1_desc = ' ',
	a.cont_designation,
	designation_desc = d.description,
	solicitor = dn.display_name_short,
	c.source_no,
	null,
	dbo.FS_CONST_STRING(a.customer_no),
	declined_ind = CASE WHEN a.declined_ind = 'Y' THEN 'Y' ELSE 'N' END,
	c.fund_no,
	fund_description = f.description,
	business_title = Case When ads.customer_no is not null Then ads.business_title When rqs.esal1_desc is not null Then rqs.business_title Else ds.business_title End,
	c.notes,
	ack_no,
	duplicate_of,
	tax_receipt_no,
	null,
	c.initiator_no,
	di.display_name,
	cu.sort_name,
	di.sort_name,
	initiator_esal1_desc = Case When irqs.customer_no is not null Then irqs.esal1_desc Else ids.esal1_desc End,
	initiator_esal2_desc = Case When irqs.customer_no is not null Then irqs.esal2_desc Else ids.esal2_desc End,
	initiator_lsal_desc = Case When irqs.customer_no is not null Then irqs.lsal_desc Else ids.lsal_desc End,
	initiator_business_title = Case When irqs.esal1_desc is not null Then irqs.business_title Else ids.business_title End,
	ad.address_no,
	co.id,
	ead.address,
	c.cont_dt, 
	ch.description as channel 
From	[dbo].T_ACK_EXT a With (NOLOCK)
	OUTER APPLY dbo.FT_GET_ADDRESS(@mail_dt, Convert(int, @mail_type), @label, a.customer_no, NULL) ad
	OUTER APPLY [dbo].ft_get_eaddress(@mail_dt, @eaddress_type, @eaddress_purpose, @eaddress_market_ind, a.customer_no, NULL) ead
	LEFT JOIN dbo.TR_COUNTRY co (NOLOCK) on ad.country = co.id
	JOIN dbo.T_CONTRIBUTION c With (NOLOCK) on a.cont_ref_no = c.ref_no
	JOIN dbo.T_FUND f WITH (NOLOCK) on c.fund_no = f.fund_no
	JOIN dbo.TR_SALES_CHANNEL ch on c.channel = ch.id 
	LEFT JOIN dbo.TR_CONT_DESIGNATION d With (NOLOCK) on a.cont_designation = d.id
	JOIN dbo.T_CUSTOMER cu WITH (NOLOCK) on a.customer_no = cu.customer_no
	LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() dn ON c.worker_customer_no = dn.customer_no
	LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() di ON c.initiator_no = di.customer_no
	LEFT OUTER JOIN [dbo].TX_CUST_SAL ads ON a.customer_no = ads.customer_no and ad.alt_signor = ads.signor and ad.alt_signor > 0	-- join based on signor from t_address
	LEFT OUTER JOIN [dbo].VXS_CUST_SAL rqs ON a.customer_no = rqs.customer_no and Coalesce(@signor, a.salutation) = rqs.signor 	-- join based on requested signor and label
												and (@label = 'N' or rqs.label = 'Y')
	JOIN [dbo].TX_CUST_SAL ds ON a.customer_no = ds.customer_no and ds.default_ind = 'Y'			-- join guaranteed to get default salutation
	LEFT OUTER JOIN [dbo].TR_SIGNOR sg ON Coalesce(ads.signor, rqs.signor, ds.signor) = sg.id
	LEFT OUTER JOIN [dbo].VXS_CUST_SAL irqs ON c.initiator_no = irqs.customer_no and Coalesce(@signor, a.salutation) = irqs.signor 	-- join based on requested signor and label for initiator
												and (@label = 'N' or irqs.label = 'Y')
	LEFT JOIN [dbo].TX_CUST_SAL ids ON c.initiator_no = ids.customer_no and ids.default_ind = 'Y'	-- get default salutation for initiator, if one exists
Where	(a.letter_id = @letter_id) 
	and	(a.printed_ind = case when @reprint_ind = @REPRINT_YES then 'Y' else 'N' end ) 
	and	(a.trn_dt between @start_dt and @end_dt)
	and (Coalesce(@list_no, 0) = 0 OR a.customer_no in (Select customer_no From [dbo].T_LIST_CONTENTS Where list_no = @list_no))
 

Declare @done char(1)
Select @done = 'N', @cur_cno = 0

While @done = 'N'
  Begin
	Select @cur_cno = min(customer_no) from #letters where customer_no > @cur_cno
	
	If @cur_cno is null 
		Select @done = 'Y'
	Else
	  Begin

		Select @address_no = address_no from #letters where customer_no = @cur_cno
	
		Select @phone1 = null, @phone2 = null, @fax = null
	
		Exec [dbo].AP_GET_PHONES @customer_no = @cur_cno, 
				@address_no = @address_no,
				@day_phone = @phone1 OUTPUT,
				@eve_phone = @phone2 OUTPUT,
				@fax_phone = @fax OUTPUT,
				@called_from = 'AP_ACKNOWLEDGEMENT_LETTERS'
	
	
		UPDATE a
		Set	phone1 = [dbo].AF_FORMAT_STRING(@phone1, c.phone_mask),		-- Now use function, @phonemask was not being populated before FP498.
			phone2 = [dbo].AF_FORMAT_STRING(@phone2, c.phone_mask)
		FROM	#letters a
			LEFT JOIN [dbo].TR_COUNTRY c (NOLOCK) on a.country_id = c.id		--to get phonemask FP498.
		WHERE 	customer_no = @cur_cno
	  End
  End

--select creditee_ind, d_customer_no, * from #letters
Declare @row_count int
Select @row_count = COUNT(*) From #LETTERS 
/**************************************Get tax receipt numbers (CWR 9/18/2003)*******************************************/
If @receipt_counter is not null and @row_count > 0
  Begin
  	If @reprint_ind = @REPRINT_NO
	  Begin
		DECLARE	receipt_cursor cursor for
		SELECT ack_no, customer_no FROM #LETTERS order by ack_no OPTION (KEEP PLAN)
		
		OPEN 	receipt_cursor
		
		FETCH 	receipt_cursor into @ack_no, @cur_cno
		
		WHILE 	@@fetch_status >= 0
		  begin
			exec @next_receipt_no = [dbo].ap_get_next_receipt_no  @type = @receipt_counter

			Update	#LETTERS
			Set	tax_receipt_no = @next_receipt_no
			Where	ack_no = @ack_no and customer_no = @cur_cno
 
			FETCH 	NEXT FROM receipt_cursor into @ack_no, @cur_cno
		  end

		CLOSE receipt_cursor
		DEALLOCATE receipt_cursor
	  End
	  
	Update	a
	Set	orig_tax_receipt_no = b.tax_receipt_no
	From	#LETTERS a
		JOIN [dbo].t_ack_ext b ON a.duplicate_of = b.ack_no
  End


/*********************************Get ID for Recipients, if donor record***************************/
Create table #tcreds(
	customer_no int null,
	trn_no int null, 
	creditee_no int null)


 
INSERT INTO #tcreds (customer_no, trn_no, creditee_no)
SELECT	a.customer_no,
	a.trn_no,
	MAX(c.creditee_no)
FROM	[dbo].t_ack_ext a (NOLOCK)
	JOIN #LETTERS x on a.trn_no = x.trn_no
	JOIN [dbo].t_transaction b (NOLOCK) on a.trn_no = b.transaction_no
	JOIN [dbo].t_creditee c (NOLOCK) on c.ref_no = b.ref_no and a.creditee_ind in ('D', 'B', 'G') and c.creditee_no <> a.customer_no
GROUP BY a.customer_no, a.trn_no

UPDATE	a
SET	d_customer_no = b.creditee_no
FROM	#LETTERS a
	JOIN #tcreds b on a.customer_no = b.customer_no and	a.trn_no = b.trn_no and a.creditee_ind in ('D', 'B', 'G')

 
/*********************************Get Salutations for Donor/Receipients**********************/
UPDATE	a
SET	d_esal1_desc = b.esal1_desc
FROM	#LETTERS a
	JOIN [dbo].tx_cust_sal b (NOLOCK) on a.d_customer_no = b.customer_no and b.default_ind = 'Y' and a.d_customer_no is not null

/*********************************Get the payment method**********************************/
 
CREATE	TABLE #tpay(trn_no int, pmt_method int null, pmt_dt datetime null)
 
INSERT	INTO #tpay
SELECT	DISTINCT a.trn_no,
	b.pmt_method, 
	b.pmt_dt 
FROM	#LETTERS a
	JOIN [dbo].t_payment b (NOLOCK) on a.trn_no = b.transaction_no 
 
CREATE INDEX sss on #tpay(trn_no)

UPDATE	#tpay
SET	pmt_method = 0
WHERE	trn_no in (Select trn_no from #tpay Group by trn_no Having count(*) > 1)


-- Code For an old Met Issue
If (Select Coalesce(dbo.FS_GET_DEFAULT_VALUE(dbo.FS_GET_PARAM_FROM_APPNAME('UG'), 'IMPRESARIO', 'Acknowledgement Patch'), 'No') )= 'Yes'
  Begin
	Update	a
	Set	a.payment_type = 'ST'
	From	#LETTERS a 
		JOIN #tpay b ON a.trn_no = b.trn_no 
	Where	b.pmt_method = 21
  End
Else
  Begin
	Update	a
	Set	a.payment_type = CASE WHEN b.pmt_method = 0 THEN '***Mixed***' ELSE c.description END
	From	#LETTERS a
		JOIN #tpay b ON a.trn_no = b.trn_no
		LEFT OUTER JOIN [dbo].tr_payment_method c (NOLOCK) ON b.pmt_method = c.id
  End
 update a 
	set a.pmt_dt = b.pmt_dt 
		from #LETTERS a 
		JOIN #tpay b on a.trn_no=b.trn_no 


--- Now update the table if this is not a reprint
If @reprint_ind = @REPRINT_NO
  Begin
	Update b 
	Set 	printed_ind = 'Y' , 
			printed_dt = @ack_dt, 
			tax_receipt_no = a.tax_receipt_no
	From 	#LETTERS a
		JOIN [dbo].t_ack_ext b ON a.customer_no = b.customer_no 
			and a.trn_no = b.trn_no 
			and a.letter_id = b.letter_id 
			and Coalesce(a.ack_no, 0) = Coalesce(b.ack_no, 0) 
			and	a.trn_dt = b.trn_dt
	Where	b.printed_ind = 'N' 
  End
 
Insert 	[dbo].T_ACK_ERRORS(letter_id, reprint_ind, start_dt, end_dt, list_no, cur_datetime, error_no, numrows)
Select 	@letter_id, @reprint_ind, @start_dt, @end_dt, @list_no, @ack_dt, @@error, @@rowcount
 
Select 	a.customer_no,
	a.trn_no,
	a.trn_desc,
	a.trn_dt,
	a.camp_desc,
	a.appeal_desc,
	a.memb_desc,
	a.prem1_amt,
	a.prem2_amt,
	a.prem3_amt,
	a.prem4_amt,
	a.prem5_amt,
	a.prem1_desc,
	a.prem2_desc,
	a.prem3_desc,
	a.prem4_desc, 
	a.prem5_desc,
	a.street1, 
	a.street2,
	a.city,
	a.state,
	a.postal_code, 
	country = a.country_desc,
	a.esal1_desc,
	a.esal2_desc, 
	a.lsal_desc, 
	a.cont_amt, 
	a.recd_amt,
	a.goods_services_value, 
	a.inception_dt,
	a.prem_text, 
	a.expr_dt,
	a.d_esal1_desc,
	a.designation_desc, 
	a.solicitor,
	a.source_no,
	a.payment_type,
	a.constituencies,
	a.declined_ind,
	a.fund_no,
	a.fund_description,
	a.business_title,
	a.notes,
	a.ack_no,
	a.duplicate_of,
	a.tax_receipt_no,
	a.orig_tax_receipt_no,
	tax_receipt_counter = @receipt_counter_desc,
	b.custom_1,
	b.custom_2,
	b.custom_3,
	b.custom_4,
	b.custom_5,
	b.custom_6,
	b.custom_7,
	b.custom_8,
	b.custom_9,
	b.custom_0,
	a.street3,
	a.initiator_no,
	a.initiator_name,
	a.sort_name,
	a.initiator_sort_name,
	a.initiator_esal1_desc,
	a.initiator_esal2_desc,
	a.initiator_lsal_desc,
	a.initiator_business_title,
	a.email_address,
	a.phone1,
	a.phone2,
	f.printable_fund, 
	a.cont_dt, 
	a.channel,
	a.pmt_dt 
FROM #LETTERS a
	JOIN [dbo].T_CONTRIBUTION b ON a.cont_ref_no = b.ref_no
	JOIN [dbo].LTR_FUND_DETAIL AS f on a.fund_no = f.fund_no
ORDER BY a.memb_desc, country, a.customer_no
