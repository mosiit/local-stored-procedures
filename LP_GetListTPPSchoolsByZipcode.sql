USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_GetListTPPSchoolsByZipcode]
(
	@Zipcode VARCHAR(5)
)
AS
SET NOCOUNT ON 
/*
Created by: D. Jacob 12/15/15
Purpose: Returns a list of public and private schools in a certain zipcode. 
Used by MadeMedia for TPP registration
*/

DECLARE @SubjectList VARCHAR(500)

SET @SubjectList = 'Integrated Science,Earth Science / Astronomy,Life Science / Biology,Physical Science / Physics,Chemistry,Technology / Engineering,Computer Science,Mathematics,Social Studies / History,Language Arts,Foreign Language,Art,Special Education,All subjects,Other'

SELECT DISTINCT ISNULL(ky.key_value, c.customer_no) AS Proxid, 
	c.customer_no, 
	c.lname AS schnam, 
	adr.street1 AS LSTREET, 
	adr.city AS LCITY, 
	adr.state AS STATE, 
	dbo.AF_FORMAT_STRING(adr.postal_code, '@@@@@') AS LZIP, 
	[dbo].[LFS_TPP_GetGradesList](c.customer_no) GradeList, 
	@SubjectList SubjectList, 
	[dbo].[LFS_TPP_GetDomainList](c.customer_no) AS domain
FROM dbo.T_CUSTOMER c
	LEFT JOIN dbo.TX_CUST_KEYWORD ky
		ON c.customer_no = ky.customer_no
		AND ky.keyword_no = 514
	--LEFT JOIN dbo.TX_CUST_KEYWORD openClosed
	--	ON c.customer_no = ky.customer_no
	--	AND ky.keyword_no = 518
	left JOIN t_address adr
		ON adr.customer_no = c.customer_no
		AND adr.primary_ind = 'Y' AND adr.inactive = 'N'
WHERE c.cust_type IN (21, 22) -- School Offical Record & School District Official Rec
	AND adr.postal_code like @Zipcode + '%'
	--AND openClosed.key_value NOT IN ('Closed', 'Temp Closed')
ORDER BY schnam
GO

GRANT EXECUTE ON [dbo].[LP_GetListTPPSchoolsByZipcode] to impusers
GO 