USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_GET_AVAILABLE_ENTITLEMENT_GIFTS_BOCA_TEXT]    Script Date: 12/30/2018 2:04:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tessitura Network, Jonathan Smillie
-- Create date: 12/14/2018 - 12/21/2018 
-- Description:	Returns data from a selection of entitlements formatted with Boca FGL commands 
-- to print from a text file 
-- =============================================
CREATE  PROCEDURE [dbo].[LP_GET_AVAILABLE_ENTITLEMENT_GIFTS_BOCA_TEXT]
	@customer_no int = null,
	@list_no int = null,
	@entitlement_str varchar(100) = null

	
AS
BEGIN

create table #entitlements
(entitlement_no int) 

if @entitlement_str is not null
	begin  
	insert #entitlements (entitlement_no) 
	select b.entitlement_no 
		from FT_SPLIT_LIST(@entitlement_str,',') a join LTR_ENTITLEMENT b 
		on a.element = b.entitlement_no
	end 
if @entitlement_str is null 
	begin
		insert #entitlements (entitlement_no) 
	select entitlement_no from LTR_ENTITLEMENT 
	end 

create table #ent_to_print 
	 (id_key int,
	  entitlement_no int,
	  entitlement_desc varchar(60),
	  entitlement_code varchar(60),
	  custom_desc varchar(60), 
	  allow_nscan char(1), 
	  num_items int,
	  init_dt datetime,
	  expr_dt varchar(20), 
	  group_gift_code varchar(30),
	  gift_code varchar(30),
	  tkw_description varchar(30),
	  customer_no int,
	  lname varchar(80),
	  fname varchar(30)
	  )
	if @customer_no is not null or @list_no is not null
		begin 
			if @customer_no is not null 
				begin
				 INSERT #ent_to_print 
					select 
						b.id_key,
						a.entitlement_no,
						a.entitlement_desc,a.entitlement_code,a.custom_1 as custom_desc,a.allow_nscan, 
						b.num_items,
						b.init_dt,
						expr_dt = convert(varchar(12),b.expr_dt), 
						b.group_gift_code,
						b.gift_code,
						c.description as tkw_description,
						cust.customer_no,
						cust.lname,
						cust.fname
					from LTR_ENTITLEMENT a
					join LT_ENTITLEMENT_GIFT b on a.entitlement_no = b.entitlement_no
				--		and a.entitlement_no = coalesce(@entitlement_no,a.entitlement_no)
						and b.gift_status='C'
					join TR_TKW c on c.id = a.ent_tkw_id
					join T_CUSTOMER (nolock) cust on cust.customer_no = b.customer_no
					where b.customer_no=@customer_no and 
					a.entitlement_no in (select entitlement_no from #entitlements) 

				end
			if @list_no is not null
				begin
					INSERT #ent_to_print 
					select 
						b.id_key,
						a.entitlement_no,
						a.entitlement_desc,a.entitlement_code,a.custom_1 as custom_desc,a.allow_nscan, 
						b.num_items,
						b.init_dt,
						expr_dt = convert(varchar(8),b.expr_dt), 
						b.group_gift_code,
						b.gift_code,
						c.description as tkw_description,
						cust.customer_no,
						cust.lname,
						cust.fname
					from LTR_ENTITLEMENT a
					join LT_ENTITLEMENT_GIFT b on a.entitlement_no = b.entitlement_no
						--and a.entitlement_no = coalesce(@entitlement_no,a.entitlement_no)
						and b.gift_status='C'
					join TR_TKW c on c.id = a.ent_tkw_id
					join T_LIST_CONTENTS (nolock) d on d.customer_no = b.customer_no
						and d.list_no = @list_no
					join T_CUSTOMER (nolock) cust on cust.customer_no = b.customer_no
					WHERE a.entitlement_no in (select entitlement_no from #entitlements) 
				end

		end 
	else
		begin
			INSERT #ent_to_print 
			select 
				b.id_key,
				a.entitlement_no,
				a.entitlement_desc,a.entitlement_code,a.custom_1 as custom_desc,a.allow_nscan, 
				b.num_items,
				b.init_dt,
				expr_dt = convert(varchar(8),b.expr_dt), 
				b.group_gift_code,
				b.gift_code,
				c.description as tkw_description,
				cust.customer_no,
				cust.lname,
				cust.fname
			from LTR_ENTITLEMENT a
					join LT_ENTITLEMENT_GIFT b on a.entitlement_no = b.entitlement_no
					--	and a.entitlement_no = coalesce(@entitlement_no,a.entitlement_no)
						and b.gift_status='C'
			join TR_TKW c on c.id = a.ent_tkw_id
			join T_CUSTOMER (nolock) cust on cust.customer_no = b.customer_no
			WHERE a.entitlement_no in (select entitlement_no from #entitlements) 
		end
	     delete from [dbo].[LT_ENT_TO_TEXT_EXPORT]
		 INSERT [dbo].[LT_ENT_TO_TEXT_EXPORT]
		SELECT 
		venue_name='<RC65,225><F3><HW1,1>'+custom_desc+' Pass',
		exp_dt='<RC120,225><F2><HW2,1><NR>Expiration Date: '+expr_dt,
		cust_name='<RC150,225><F3><HW1,1>'+coalesce(fname+' ','')+lname,
		resale_stub='<RC337,50><F3><HW1,1><RL>Not For Resale',
		where_to1=case when allow_nscan='Y' then
		'<RC200,225><F2><HW2,2><NR>Bring this pass directly to the'
		when allow_nscan='N' then '<F2><HW2,2><RC200,225><NR>' end,
		where_to2 = case when allow_nscan='Y' 
		then'<RC230,225><F2><HW2,2><NR>Exhibit Halls Entrance'
		when allow_nscan='N' then '<F2><HW2,2><RC230,225><NR>' end,  
		gc1=case when allow_nscan='Y' then 
		'<RC337,80><F2><HW1,1><RL>'+gift_code
		when allow_nscan='N' then 
		'<RC65,600><F2><HW2,1><NR>Gift Code:'+gift_code end, 
		gc2=case when allow_nscan= 'Y' then 
		'<RC375,940><F3><HW1,1><RL>'+custom_desc+' Pass'+
		'<RC350,1000><F5><HW1,1><RL>Expires: '+expr_dt+
		'<RC100,1100><F1><HW1,1><RR><X1><OL5>^'+gift_code+'^'
		when allow_nscan='N' then 
		'<RC375,940><F3><HW1,1><RL>'+custom_desc+' Pass'+
		'<RC350,1000><F2><HW2,1><RL>Expires: '+expr_dt+
		'<RC350,1050><F2><HW2,1><RL>Gift Code: '+gift_code end 
		,
		gc3=case when allow_nscan='Y' then 
		'<RC50,700><NR><F2><HW1,1><OP5>^'+gift_code+'^'
		when allow_nscan='N' then null end,
		valid1=
			case when allow_nscan = 'Y' then 
			'<F2><HW1,1><RC300,225><NR>Valid for one general admission Exhibit Halls'
			when allow_nscan='N' then 
			'<RC200,225><F2><HW2,1><NR>Exchange this pass for a'+
			--'<RC200,225><F2><HW1,1><NR>Must exchange this pass for a ticket in advance of'+
			'<RC200,470><F2><HW2,1><NR>ticket at www.mos.org/redeem,'+
			'<RC235,225><F2><HW2,1><NR>at the box office, or by '+
			'<RC235,475><F2><HW2,1><NR>calling 617-723-2500.'
	--		'<RC240,225><F2><HW1,1><NR>617-723-2500 during business hours or by '+
--			'<RC260,225><F2><HW1,1>presenting this pass in person at the Museum''s'+
	--		'<RC280,225><F2><HW1,1>box office.'
			end,  
		valid2=
		case when allow_nscan = 'Y' then 
		'<RC320,225><F2><HW1,1><NR>admission. Not valid for separately'+
		'<RC340,225><F2><HW1,1><NR>ticketed exhibits or venues.'
		-- Not for resale.'
		when allow_nscan='N' then 
		'<RC320,225><F2><HW1,1><NR>Not valid for special programs or events.'
	--	+'<RC340,225><F2><HW1,1><NR>Not for resale.' 
		end,
		ff='<p>'
		 FROM #ent_to_print

	select * from  [dbo].[LT_ENT_TO_TEXT_EXPORT]
END

