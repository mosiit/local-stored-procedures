USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_CLEANUP_PURSUANT_EMAILS]    Script Date: 3/5/2021 4:23:43 PM ******/
DROP PROCEDURE [dbo].[LP_CLEANUP_PURSUANT_EMAILS]
GO

/****** Object:  StoredProcedure [dbo].[LP_CLEANUP_PURSUANT_EMAILS]    Script Date: 3/5/2021 4:23:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_CLEANUP_PURSUANT_EMAILS] @FileName VARCHAR(1000)
AS
BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @nsql NVARCHAR(1000)
	DECLARE @ExcelFileName NVARCHAR(300)
	DECLARE @StepName VARCHAR(100)
	DECLARE @StepNumber INT

	SELECT @ExcelFileName = LTRIM(RTRIM(SUBSTRING(@FileName, 1, PATINDEX('%.%', @FileName) - 1)))

	IF NOT EXISTS
	(
		SELECT	*
		FROM	[TemporaryData].[dbo].sysobjects
		WHERE	name = @ExcelFileName
				AND xtype = 'U'
	)
		BEGIN
			SELECT @nsql = 'CREATE TABLE [TemporaryData].[dbo].' + @ExcelFileName + '(
			[Emailaddress] [nvarchar](255) NULL,
			[EventType] [nvarchar](255) NULL,
			[OptOutAll] [nvarchar](255) NULL,
			[GroupType] [nvarchar](255) NULL
			) ON [PRIMARY]'

			EXECUTE sp_executesql @nsql

		END
	ELSE
		BEGIN
			SELECT @nsql = 'TRUNCATE TABLE [TemporaryData].[dbo].' + @ExcelFileName
			EXECUTE sp_executesql @nsql
		END

	-- ===============================================================================================
	-- For grouptype = UpdateType_NoGood, change eaddress_type = No Good (10), uncheck Allow Marketing 
	-- box,  uncheck the Primary box, and remove all affiliated Contact Point Purposes
	-- ===============================================================================================

	SELECT @StepName = 'Update email to no good'
	SELECT @StepNumber = 1

	UPDATE e
	SET e.eaddress_type = 10,
	    e.market_ind = 'N',
	    e.primary_ind = 'N'
	FROM T_EADDRESS AS e
	INNER JOIN [TemporaryData].[dbo].[MOSOptOutsStaging] AS j ON e.address = j.Emailaddress
	WHERE j.GroupType = 'UpdateType_NoGood';

	INSERT INTO [TemporaryData].[dbo].[MOSOptOutsLogging]
		SELECT	@ExcelFileName,
				@StepNumber,
				@StepName,
				@@ROWCOUNT,
				GETDATE()

	SELECT @StepName = 'Delete CPP for no good email'
	SELECT @StepNumber = @StepNumber + 1

	DELETE cpp
	FROM TX_CONTACT_POINT_PURPOSE AS cpp
	INNER JOIN T_EADDRESS AS E ON E.eaddress_no = cpp.contact_point_id
	INNER JOIN [TemporaryData].[dbo].[MOSOptOutsStaging] AS j ON E.address = j.Emailaddress
	WHERE j.GroupType = 'UpdateType_NoGood'
	        AND cpp.purpose_id IN ( 10, 12, 25, 26, 30, 31 );

	INSERT INTO [TemporaryData].[dbo].[MOSOptOutsLogging]
		SELECT	@ExcelFileName,
				@StepNumber,
				@StepName,
				@@ROWCOUNT,
				GETDATE()

	-- ===============================================================================================
	-- For grouptype = Uncheck_market_ind, uncheck Allow Marketing box, uncheck the Primary box, and 
	-- remove all affiliated Contact Point Purposes  
	-- ===============================================================================================
	SELECT @StepName = 'Uncheck market and primary indicators'
	SELECT @StepNumber = @StepNumber + 1

	UPDATE e
	SET e.market_ind = 'N',
	    e.primary_ind = 'N'
	FROM T_EADDRESS AS e
	INNER JOIN [TemporaryData].[dbo].[MOSOptOutsStaging] AS j ON e.address = j.Emailaddress
	WHERE j.GroupType = 'Uncheck_market_ind';

	INSERT INTO [TemporaryData].[dbo].[MOSOptOutsLogging]
		SELECT	@ExcelFileName,
				@StepNumber,
				@StepName,
				@@ROWCOUNT,
				GETDATE()

	SELECT @StepName = 'Delete CPP for uncheck market and primary indicators'
	SELECT @StepNumber = @StepNumber + 1

	DELETE cpp
	FROM TX_CONTACT_POINT_PURPOSE AS cpp
	INNER JOIN T_EADDRESS AS E ON E.eaddress_no = cpp.contact_point_id
	INNER JOIN [TemporaryData].[dbo].[MOSOptOutsStaging] AS j ON E.address = j.Emailaddress
	WHERE j.GroupType = 'Uncheck_market_ind'
	        AND cpp.purpose_id IN ( 10, 12, 25, 26, 30, 31 );

	INSERT INTO [TemporaryData].[dbo].[MOSOptOutsLogging]
		SELECT	@ExcelFileName,
				@StepNumber,
				@StepName,
				@@ROWCOUNT,
				GETDATE()

	-- ===============================================================================================
	-- For grouptype = Remove_SupportUs, remove Support Us Contact Point Purpose
	-- ===============================================================================================

	SELECT @StepName = 'Delete support us email'
	SELECT @StepNumber = @StepNumber + 1

	DELETE cpp
	FROM TX_CONTACT_POINT_PURPOSE AS cpp
	INNER JOIN T_EADDRESS AS E ON E.eaddress_no = cpp.contact_point_id
	INNER JOIN [TemporaryData].[dbo].[MOSOptOutsStaging] AS j ON E.address = j.Emailaddress
	WHERE j.GroupType = 'Remove_SupportUs'
	        AND cpp.purpose_id = 12;

	INSERT INTO [TemporaryData].[dbo].[MOSOptOutsLogging]
		SELECT	@ExcelFileName,
				@StepNumber,
				@StepName,
				@@ROWCOUNT,
				GETDATE()

 	-- =========================================================================================
	-- Save the table from staging status to it's real name
	-- =========================================================================================
	SELECT @nsql = 'INSERT INTO [TemporaryData].[dbo].' + @ExcelFileName + ' SELECT [Emailaddress], [EventType], [OptOutAll], [GroupType] FROM [TemporaryData].[dbo].[MOSOptOutsStaging]'

	EXECUTE sp_executesql @nsql

END;
GO


