USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_DetermineExhibitHallCloseTime]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_DetermineExhibitHallCloseTime]
GO

CREATE PROCEDURE [dbo].[LP_DetermineExhibitHallCloseTime]
        @performance_dt datetime = null,        --Null @performance_dt = today
        @suppress_selection char(1) = 'N',
        @performance_date char(10) = null OUTPUT,
        @museum_close_time char(8) = null OUTPUT, 
        @museum_close_reason varchar(30) = null OUTPUT
AS BEGIN

    DECLARE @day_of_week varchar(20), @day_count int, @performance_year char(4)
    DECLARE @thanksgiving_wed char(10), @thanksgiving_thu char(10), @thanksgiving_sat char(10)
    DECLARE @christmas_eve char(10), @christmas_day char(10), @christmas_vac_start char(10), @christmas_vac_end char(10) 
    DECLARE @new_years_eve char(10), @new_years_day char(10)
    DECLARE @christmas_vac_end_date varchar(50), @new_years_day_open_til_7 varchar(50)
    DECLARE @presidents_day char(10), @feb_vac_start char(10), @feb_vac_end char(10)
    DECLARE @patriots_day char(10), @apr_vac_start char(10), @apr_vac_end char(10), @apr_vac_2nd_saturday varchar(50)
    DECLARE @independence_day char(10), @summer_start char(10), @labor_day char(10)

    /* Initialize Variables */

    IF @performance_dt is null SELECT @performance_dt = getdate()

    SELECT @performance_date = convert(char(10),@performance_dt,111)
    SELECT @performance_year = left(@performance_date,4)

    /* Check to see if there is a keyword on the public exhibit hall performance for this date.  If multiple keywords exist, the latest time is selected
       If the keyword exists and it's a valid time, stop right there and jump to the end of the procedure.  */

    SELECT @museum_close_time = max([keyword]) FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_KEYWORDS] 
    WHERE [performance_date] = @performance_date and [title_Name] = 'Exhibit Halls' and [Production_name] = 'Exhibit Halls' and [keyword_category] = 'Hours'
    SELECT @museum_close_time = IsNull(@museum_close_time, '')

    IF isDate(@museum_close_time) = 1 BEGIN
        SELECT @museum_close_reason = 'Keyword'
        GOTO DONE
    END ELSE BEGIN
        SELECT @museum_close_time = '17:00:00', @museum_close_reason = ''
    END

    /* Consider First week of January as the previous performance year for determining School Vacation Week Dates  */

    IF right(@performance_date,5) between '01/01' and '01/07' SELECT @performance_year = (@performance_year - 1)

    /* Settings From T_Defaults */

    SELECT @apr_vac_2nd_saturday = [default_value] FROM [dbo].[T_Defaults] WHERE [parent_table] = 'Museum of Science' and [field_name] = 'SC April Vacation 2nd Saturday'
    SELECT @apr_vac_2nd_saturday = IsNull(@apr_vac_2nd_saturday, 'Yes')
               
    SELECT @new_years_day_open_til_7 =  [default_value] FROM [dbo].[T_Defaults] WHERE [parent_table] = 'Museum of Science' and [field_name] = 'SC New Years Day Open Til 7:00'
    SELECT @new_years_day_open_til_7 = IsNull(@new_years_day_open_til_7, 'Yes')

    SELECT @christmas_vac_end_date = [default_value] FROM [dbo].[T_Defaults] WHERE [parent_table] = 'Museum of Science' and [field_name] = 'SC Xmas Vacation Last Date'
    SELECT @christmas_vac_end_date = IsNull(@christmas_vac_end_date, '')


    /* Determine Day of the Week */
        
    SELECT @day_of_week = datename(weekday,@performance_dt)

    /* Determine February Vacation dates for performance_dt year */

    SELECT @day_count = 0, @presidents_day = @performance_year + '/02/01'
    WHILE @day_count < 3 or datename(weekday,@presidents_day) <> 'Monday' BEGIN
        IF datename(weekday,@presidents_day) = 'Monday' SELECT @day_count = (@day_count + 1)
        IF @day_count < 3 or datename(weekday,@presidents_day) <> 'Monday' SELECT @presidents_day = convert(char(10),dateadd(day,1,@presidents_day),111)
    END
    SELECT @feb_vac_start = convert(char(10),dateadd(day,-2, @presidents_day),111)
    SELECT @feb_vac_end = convert(char(10),dateadd(day,5, @presidents_day),111)

    /* Determine April Vacation dates for performance_dt year  */

    SELECT @day_count = 0, @patriots_day = @performance_year + '/04/01'
    WHILE @day_count < 3 or datename(weekday,@patriots_day) <> 'Monday' BEGIN
        IF datename(weekday,@patriots_day) = 'Monday' SELECT @day_count = (@day_count + 1)
        IF @day_count < 3 or datename(weekday,@patriots_day) <> 'Monday' SELECT @patriots_day = convert(char(10),dateadd(day,1,@patriots_day),111)
    END
    SELECT @apr_vac_start = convert(char(10),dateadd(day,-2, @patriots_day),111)
    SELECT @apr_vac_end = convert(char(10),dateadd(day,5, @patriots_day),111)
    IF @apr_vac_2nd_saturday = 'No' SELECT @apr_vac_end = convert(char(10),dateadd(day, -1, @apr_vac_end),111)

    /* Determine Summer Hours dates for performance_dt year */

    SELECT @independence_day = @performance_year + '/07/04'
    SELECT @summer_start = @performance_year + '/07/05'
    
    SELECT @labor_day = @performance_year + '/09/01'
    WHILE datename(weekday,@labor_day) <> 'MONDAY' SELECT @labor_day = convert(char(10),dateadd(day,1,@labor_day),111)
    
    /* Determine Thanksgiving Weekend Dates for performance_dt year */

    SELECT @day_count = 0, @thanksgiving_thu = @performance_year + '/11/01'
    WHILE @day_count < 4 or datename(weekday,@thanksgiving_thu) <> 'Thursday' BEGIN
        IF datename(weekday,@thanksgiving_thu) = 'Thursday' SELECT @day_count = (@day_count + 1)
        IF @day_count < 4 or datename(weekday,@thanksgiving_thu) <> 'Thursday' SELECT @thanksgiving_thu = convert(char(10),dateadd(day,1,@thanksgiving_thu),111)
    END
    SELECT @thanksgiving_wed = convert(char(10),dateadd(day,-1,@thanksgiving_thu),111)
    SELECT @thanksgiving_sat = convert(char(10),dateadd(day,2,@thanksgiving_thu),111)

    /* Determine Christmas Week dates for performance_dt year */

    SELECT @christmas_eve = @performance_year + '/12/24', @christmas_day = @performance_year + '/12/25'
    SELECT @christmas_vac_start = @performance_year + '/12/26', @christmas_vac_end = @performance_year + '/12/30'
    IF isdate(@christmas_vac_end_date) = 0 SELECT @christmas_vac_end_date = @christmas_vac_end
    IF @christmas_vac_end <> @christmas_vac_end_date and isdate(@christmas_vac_end_date) = 1 SELECT @christmas_vac_end = convert(char(10),convert(datetime,@christmas_vac_end_date),111)

    SELECT @new_years_eve = @performance_year + '/12/31'
    SELECT @new_years_day = convert(varchar(4),(convert(int,@performance_year) + 1)) + '/01/01'

    
    /*  Check Performance Date */

    /* If it's a Friday, always return 21:00:00 (9:00 PM) unless it's Christmas Day when we're closed or 4th of July when we always close at 5:00 */

    IF @day_of_week = 'Friday' and @performance_date not in (@new_years_eve, @christmas_eve, @christmas_day, @independence_day) BEGIN
        SELECT @museum_close_reason = 'Friday', @museum_close_time = '21:00:00'
        GOTO DONE
    END

    /* February Vacation Week: Extended Hours Until 19:00:00 (7:00 PM) */

    IF @performance_date between @feb_vac_start and @feb_vac_end BEGIN
        SELECT @museum_close_time = '19:00:00', @museum_close_reason = 'February School Vacation'
        GOTO DONE
    END

    /* April Vacation Week: Extended Hours Until 19:00:00 (7:00 PM) */

    IF @performance_date between @apr_vac_start and @apr_vac_end BEGIN
        SELECT @museum_close_time = '19:00:00', @museum_close_reason = 'April School Vacation'
        GOTO DONE
    END

    /* Independence Day: Close at 17:00:00 (5:00 PM) regardless of day of week */

    IF @performance_date = @independence_day BEGIN
       SELECT @museum_close_time = '17:00:00', @museum_close_reason = 'Independence Day'
       GOTO DONE
    END

    /* Summer Hours: Extended Hours Until 19:00:00 (7:00 PM) */
    
    IF @performance_date between @summer_start and @labor_day BEGIN
        SELECT @museum_close_time = '19:00:00', @museum_close_reason = 'Summer Hours'
        GOTO DONE
    END
    
    /* Day before thanksgiving = 14:00:00 (2:00 PM) / Thanksgiving Day = CLOSED / Saturday after Thanksgiving = 19:00:00 (7:00 PM)  */

         IF @performance_date = @thanksgiving_wed SELECT @museum_close_time = '14:00:00', @museum_close_reason = 'Day Before Thanksgiving'
    ELSE IF @performance_date = @thanksgiving_thu SELECT @museum_close_time = '00:00:00', @museum_close_reason = 'Closed On Thanksgiving'
    ELSE IF @performance_date = @thanksgiving_sat SELECT @museum_close_time = '19:00:00', @museum_close_reason = 'Thanksgiving Weekend'

    IF @museum_close_reason <> '' GOTO DONE
     
     /* Christmas Eve = 14:00:00 (2:00 PM) / Christmas Day = CLOSED / Vacation Week = 19:00:00 (7:00 PM)  */

         --SELECT @performance_date, @christmas_vac_start, @christmas_vac_end

         IF @performance_date = @christmas_eve SELECT @museum_close_time = '14:00:00', @museum_close_reason = 'Christmas Eve'
    ELSE IF @performance_date = @christmas_day SELECT @museum_close_time = '00:00:00', @museum_close_reason = 'Closed On Christmas Day'
    ELSE IF @performance_date between @christmas_vac_start and @christmas_vac_end and @performance_date not in (@new_years_eve, @new_years_day) SELECT @museum_close_time = '19:00:00', @museum_close_reason = 'Christmas Vacation'

    /*  New Year's Eve = 17:00:00 (5:00 PM) / New Year's Day = 17:00:00 (5:00 PM) or 19:00:00 (7:00 PM) Depending on the year  */

    IF @performance_date = @new_years_eve SELECT @museum_close_time = '17:00:00', @museum_close_reason = 'New Year''s Eve'
    ELSE IF @performance_date = @new_years_day and @new_years_day_open_til_7 = 'No' SELECT @museum_close_time = '17:00:00', @museum_close_reason = 'New Year''s Day'
    ELSE IF @performance_date = @new_years_day and @new_years_day_open_til_7 <> 'No' SELECT @museum_close_time = '19:00:00', @museum_close_reason = 'Christmas Vacation'
                
    DONE:

    IF @museum_close_reason = '' SELECT @museum_close_reason = @day_of_week

    IF @suppress_selection <> 'Y'
        SELECT @performance_date as 'performance_date', @museum_close_time as 'museum_close_time', @museum_close_reason as 'museum_close_reason'

END
GO

GRANT EXECUTE ON [dbo].[LP_DetermineExhibitHallCloseTime] TO impusers
GO

