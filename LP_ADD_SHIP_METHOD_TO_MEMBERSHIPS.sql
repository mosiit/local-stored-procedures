USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_ADD_SHIP_METHOD_TO_MEMBERSHIPS]
AS 

-- =============================================
-- Author:		Aileen Duffy-Brown
-- Create date: 3-30-2018
-- Description:	Adds one-step ship_method to mostly 
-- renewing and some new one-step members with one-step constituency.   
-- Should be run nightly after midnight and after the job that 
-- runs looking for CSIs - LP_ADD_CONSTITUENCY_SHIP_METHOD_CLOSE_CSI.
-- Adapted the code from LP_ADD_CONSTITUENCY_SHIP_METHOD_CLOSE_CSI.
-- TrackIt! Work Order # 90653
-- =============================================

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

--update ship_method = 3 for one-step history
UPDATE   mem
SET      mem.ship_method = 3
FROM     dbo.TX_CUST_MEMBERSHIP AS mem
LEFT OUTER JOIN TX_CONST_CUST AS cus
ON       mem.customer_no = cus.customer_no
WHERE    mem.expr_dt >= CAST(GETDATE() AS DATE)
        AND cus.constituency = 30
        AND mem.ship_method IS NULL
        AND mem.current_status IN (2, 3)
        AND mem.memb_org_no = 4;

GO
--GRANT EXECUTE ON [dbo].[LP_ADD_SHIP_METHOD_TO_MEMBERSHIPS] TO ImpUsers;
--GO


