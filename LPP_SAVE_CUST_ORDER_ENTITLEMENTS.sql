USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LPP_SAVE_CUST_ORDER_ENTITLEMENTS]    Script Date: 11/27/2018 5:45:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[LPP_SAVE_CUST_ORDER_ENTITLEMENTS]

@order_no int,
@debug char(1) = 'N'

AS

SET NOCOUNT ON
SET ANSI_WARNINGS OFF

/************************************************************************
3/21/2014 Michael Reisman
Decrement entitlements

EXEC dbo.LPP_SAVE_CUST_ORDER_ENTITLEMENTS @order_no = 938

6/25/2014 MIR Recipients: customer no in the #order_entitlements insert select so that the recipient is being used if applicable.
9/19/2014 MIR Modified to include SLI details for displaying detailed history (original perf & price type)
12/4/2014 MIR Fix - removing orders when non-printed lineitems are "zapped" (Deleted not returned)
1/27/2015 MIR NOLOCK optimizer hints
2/3/2015 MIR section for Cascading entitlements (child-as-adult)
3/12/2015 MIR using STD membership level table, not zones.
4/13/2015 MIR Beginning of proc to get all possible customers includes cases without line items (for contribution only).
4/29/2015 MIR Fix to beginning of proc - entitlements deleted where no sub line items
4/29/2015 MIR MFA logic to apply cumulative member-reset pendings
5/7/2015 MIR Corrected flaw in removing the extra entitlements for multi-memberships: rownumber now partitions the data by entitlement + ent_key
5/21/2015 MIR Perf reset type entitlements should not get stored in LTX_CUST_ENTITLEMENT.  just order hist.
	THIS REMOVES OLD RECORDS:
			DELETE ce
			FROM LTX_CUST_ENTITLEMENT ce
				JOIN LTR_ENTITLEMENT e ON ce.entitlement_no = e.entitlement_no
			where e.reset_type = 'P'
6/17/2015 MIR Including Perf reset entitlement overrides (properly) by adding entitlement_perf_no to the LTX_CUST_ENTITLEMENT table.
6/17/2015 MIR Removing Recipient from the entitlements: Entitlements are related only to the order owner.
6/18/2015 MIR additional grouping to make sure reset type logs only the appropriate info to LTX_CUST_ENTITLEMENT
6/22/2015 MIR inactive status should not be granted entitlements!
6/29/2015 MIR fixed logic for subtracting pendings after currents are used
7/8/2015 MIR using LV_ENTITLEMENT view for distinct entitlement keywords
7/8/2015 MIR removing sli status of 2 (SUP) so that it doesn't write to usage history until it's seated & paid. (so that recalling the order and paying will work)
7/14/2014 MIR extra logic to make sure that Daily pendings are removed if extra
7/14/2014 MIR using perf dt in join on LTX_CUST_ORDER_ENTITLEMENT for daily usage, rather than the ce.entitlement_date (which may not exist)
8/5/2015 MIR Parenthesis fix when adding to LTX_CUST_ENTITLEMENT
2/1/2016 MIR Start & End date logic; Order or Perf date logic; Lapsed membership logic
************************************************************************/


DECLARE @selection_priority VARCHAR(1) = (SELECT dbo.FS_GET_DEFAULT_VALUE(NULL, 'Entitlements','SELECTION_PRIORITY'))


/* only order's customer */
SELECT
	customer_no = o.customer_no,
	sli_count = COUNT(sli_no)
INTO #order_custs
FROM dbo.T_ORDER o (NOLOCK)
	LEFT JOIN dbo.T_SUB_LINEITEM sli (NOLOCK) ON (sli.order_no = o.order_no AND sli.sli_status IN (2,3,12))
WHERE o.order_no = @order_no
GROUP BY o.customer_no



/* remove order history if no more sub-lineitems */
IF NOT EXISTS (SELECT TOP 1 * FROM #order_custs WHERE sli_count > 0)
  	DELETE LTX_CUST_ORDER_ENTITLEMENT
	WHERE order_no = @order_no


DECLARE @customer_no INT = (SELECT MIN(customer_no) FROM #order_custs)


			
if exists(
select 1 from LTX_GIFT_ORDER_LOCK a 
join LT_ENTITLEMENT_GIFT b on a.gift_no = b.id_key
where coalesce(b.gift_customer_no,0)!=@customer_no
and a.order_no=@order_no
and @customer_no !=0)
	begin										
		exec ap_set_context 'MERGING', 'Y'
		UPDATE  a set a.customer_no = @customer_no
		from LTX_CUST_ENTITLEMENT a
		join LTX_GIFT_ORDER_LOCK b on a.gift_no = b.gift_no					
		where b.order_no = @order_no

		update a set a.gift_customer_no =@customer_no
		from LT_ENTITLEMENT_GIFT a 
		join LTX_GIFT_ORDER_LOCK b on a.id_key = b.gift_no
		where b.order_no=@order_no

		update a set a.customer_no =@customer_no
		from LTX_CUST_ORDER_ENTITLEMENT a 
		join LTX_CUST_ENTITLEMENT b on a.ltx_cust_entitlement_id = b.id_key
		join LTX_GIFT_ORDER_LOCK c on c.gift_no = b.gift_no		
		where a.order_no=@order_no
					

		update a set a.customer_no =@customer_no
		from LTX_AUDIT_CUST_ORDER_ENTITLEMENT a 
		join LTX_CUST_ENTITLEMENT b on a.ltx_cust_entitlement_id = b.id_key
		join LTX_GIFT_ORDER_LOCK c on c.gift_no = b.gift_no		
		where a.order_no=@order_no

		update a set a.gift_customer_no =@customer_no
		from LT_AUDIT_ENTITLEMENT_GIFT a 
		join LTX_GIFT_ORDER_LOCK b on a.id_key = b.gift_no
		where b.order_no=@order_no

		UPDATE  a set a.customer_no = @customer_no
		from LTX_AUDIT_CUST_ENTITLEMENT a
		join LTX_GIFT_ORDER_LOCK b on a.gift_no = b.gift_no					
		where b.order_no = @order_no
		exec ap_set_context 'MERGING', 'N'
	end
			
	
if exists (select 1 from LTX_GIFT_ORDER_LOCK l where order_no =@order_no) and NULLIF(@customer_no,0) IS NULL
Begin		
			declare @association_type int
			select	@association_type = convert(int, isnull(default_value, '0'))  --4 --action used to close issue, if not auto-closed
			from	dbo.t_defaults
			where	parent_table = 'ENTITLEMENTS'
				and field_name = 'GIFT_PROXY_ASSOCIATION'
			
			select @association_type = coalesce(@association_type,-1)

			select 	@customer_no = coalesce(gift_customer_no,b.associated_customer_no, a.customer_no)			
			from [dbo].[LT_ENTITLEMENT_GIFT] a
			join LTX_GIFT_ORDER_LOCK l on a.id_key = l.gift_no
				and l.order_no =@order_no
			left outer join t_association b on a.customer_no = b.customer_no 
			and b.association_type_id= @association_type	
			
			update a set a.gift_status ='A' , gift_customer_no=@customer_no
			from LT_ENTITLEMENT_GIFT a 
			join LTX_GIFT_ORDER_LOCK b on a.id_key=b.gift_no
				and b.order_no=@order_no		
end


--Get All
IF NULLIF(@customer_no,0) IS NOT NULL
  BEGIN
  DELETE dbo.LTX_CUST_ORDER_ENTITLEMENT
	WHERE customer_no = @customer_no 
	AND order_no = @order_no
	
  
update a set a.gift_status ='A' , gift_customer_no=@customer_no
			from LT_ENTITLEMENT_GIFT a 
			join LTX_GIFT_ORDER_LOCK b on a.id_key=b.gift_no
				and b.order_no=@order_no	


	IF NULLIF(@customer_no,0) IS NOT NULL
		IF object_id('tempdb..#order_entitlements') IS NOT NULL
		DROP TABLE #order_entitlements

		CREATE TABLE #order_entitlements (
			oe_id int IDENTITY(1,1) NOT NULL,
			customer_no int NULL,
			order_no int NULL,
			cust_memb_no int NULL,
			memb_org_no int NULL,
			memb_level varchar(3) NULL,
			entitlement_no int NULL,
			reset_type char(1) NULL,
			li_seq_no int NULL,
			sli_no int NULL,
			perf_no int NULL,
			recipient_no int NULL,
			entitlement_date datetime NULL,
			init_dt datetime NULL,
			expr_dt datetime NULL,
			membership_status int NULL,
			num_ent int NULL,
			num_remain_before_order int NULL,
			num_used int NULL,
			order_used int NULL,
			to_decrement int NULL,
			ltx_cust_entitlement_id int null)	
		
		/* table of entitlements used in this order */
		INSERT #order_entitlements (
			customer_no,
			order_no,
			cust_memb_no,
			memb_org_no,
			memb_level,
			entitlement_no,
			reset_type,
			li_seq_no,
			sli_no,
			perf_no,
			recipient_no,
			entitlement_date,
			init_dt,
			expr_dt,
			membership_status,
			num_ent,
			num_remain_before_order,
			num_used,
			order_used,
			ltx_cust_entitlement_id)
		SELECT
			customer_no = @customer_no,
			order_no = @order_no,
			cm.cust_memb_no,
			cm.memb_org_no,
			cm.memb_level,
			e.entitlement_no,
			e.reset_type,
			sli.li_seq_no,
			sli.sli_no, /* MIR 9/12/2014 */
			sli.perf_no, /* MIR 9/19/2014 */
			sli.recipient_no, /* MIR 9/25/2017 */
			entitlement_date = CASE WHEN e.reset_type = 'D' THEN CAST(CONVERT(varchar(10),f.perf_dt,101) as datetime) ELSE NULL END,
			cm.init_dt,
			cm.expr_dt,
			membership_status = cm.current_status,
			num_ent = MIN(e.num_ent),		
			num_remain_before_order = MIN(COALESCE(ce.num_ent,e.num_ent) - ISNULL(coe.cust_num_used,0)),
			num_used = count(DISTINCT sli_no),
			order_used = ROW_NUMBER() OVER (
										PARTITION BY e.entitlement_no, cm.current_status, /* added 6/29/2015 to keep track of pendings */
											CASE
											WHEN e.reset_type = 'D' THEN CONVERT(varchar(10),f.perf_dt,101) 
											WHEN e.reset_type = 'M' THEN NULL
											WHEN e.reset_type = 'P' THEN CAST(sli.perf_no AS varchar(255))
											END
										ORDER BY sli.sli_no),
			ce.id_key
		FROM T_ORDER o (NOLOCK)
			JOIN dbo.T_SUB_LINEITEM sli (NOLOCK) ON o.order_no = sli.order_no
			JOIN dbo.TR_PRICE_TYPE pt ON sli.price_type = pt.id
			JOIN dbo.LTR_ENTITLEMENT e ON e.ent_price_type_group = pt.price_type_group
				and e.is_adhoc='N'
			JOIN dbo.T_MEMB_LEVEL ml ON e.memb_level_no = ml.memb_level_no
			JOIN dbo.LV_ENTITLEMENT_INV_TKW_LIST v ON v.tkw = e.ent_tkw_id AND v.perf_no = sli.perf_no
			JOIN dbo.TX_CUST_MEMBERSHIP cm (NOLOCK) ON (ml.memb_org_no = cm.memb_org_no AND ml.memb_level = cm.memb_level)
			JOIN dbo.T_PERF f (NOLOCK) ON f.perf_no = sli.perf_no /* to know which date to store & decrement from */
		LEFT JOIN dbo.LTX_CUST_ENTITLEMENT ce 
				ON (e.entitlement_no = ce.entitlement_no 
				AND ce.customer_no = cm.customer_no
				AND ce.cust_memb_no = cm.cust_memb_no
				AND (e.reset_type = 'M'
					OR
					(e.reset_type = 'D' AND CONVERT(VARCHAR(10),f.perf_dt,101) = ce.entitlement_date)
					OR
					(e.reset_type = 'P' AND CAST(sli.perf_no AS VARCHAR(255)) = ce.entitlement_perf_no) /* MIR 6/17/2015 */
					)
					)
			LEFT JOIN (
				SELECT
					lcoe.customer_no,
					lcoe.cust_memb_no,
					lcoe.entitlement_no, 
					entitlement_key = CASE
										WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
										WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
										WHEN le.reset_type = 'M' THEN NULL
										ELSE NULL
										END,
					cust_num_used = ISNULL(SUM(lcoe.num_used),0)
				FROM dbo.LTX_CUST_ORDER_ENTITLEMENT lcoe
					JOIN dbo.LTR_ENTITLEMENT le ON lcoe.entitlement_no = le.entitlement_no
				WHERE coalesce(order_no,0)  <> @order_no
				GROUP BY lcoe.customer_no, lcoe.cust_memb_no, lcoe.entitlement_no, CASE
										WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
										WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
										WHEN le.reset_type = 'M' THEN NULL
										ELSE NULL
										END) coe
			ON (
				coe.customer_no = cm.customer_no 
				AND coe.cust_memb_no = cm.cust_memb_no
				AND coe.entitlement_no = e.entitlement_no
				AND (
					(e.reset_type = 'D' AND coe.entitlement_key = CONVERT(varchar(10),f.perf_dt,101)) /* used to be ce.entitlement_dt 7/14/2015 */
					OR (e.reset_type = 'P' AND coe.entitlement_key = CAST(sli.perf_no AS varchar(255))) /* 3/16/2015 */
					OR e.reset_type = 'M'
					)
				)
		WHERE cm.customer_no = @customer_no
		AND sli.order_no = @order_no /* in this order */
		AND sli.sli_status IN (3,12) /* ...were bought.. 7/8/2015 removed 2. */
		/* splinter for MFA */
		AND (
			(cm.current_status in (2,3) and CASE WHEN e.date_to_compare = 'O' THEN o.order_dt ELSE f.perf_dt END BETWEEN CAST(CONVERT(varchar(10),cm.init_dt,101) as datetime) AND CAST(CONVERT(varchar(10),cm.expr_dt,101) as datetime) + '23:59:59')
			OR (cm.current_status = 3 AND CASE WHEN e.date_to_compare = 'O' THEN o.order_dt ELSE f.perf_dt END BETWEEN DATEADD(MONTH,0-ml.renewal,CAST(CONVERT(varchar(10),cm.init_dt,101) as datetime)) AND CAST(CONVERT(varchar(10),cm.expr_dt,101) as datetime) + '23:59:59')
			OR (cm.current_status = 7 AND ISNULL(e.allow_lapsed,'Y') = 'Y' AND CASE WHEN e.date_to_compare = 'O' THEN o.order_dt ELSE f.perf_dt END BETWEEN CAST(CONVERT(varchar(10),cm.init_dt,101) as datetime) AND dbo.FS_ADD_MONTHS(CAST(CONVERT(varchar(10),cm.expr_dt,101) as datetime) + '23:59:59',ISNULL(ml.lapse_susp,0)))
			)
		AND NOT (
			ISNULL(cm.declined_ind,'N') = 'Y'
			AND 
			ISNULL(e.decline_benefit,'Y') = 'Y')
		/* is an active entitlement */
		AND ISNULL(e.inactive,'N') = 'N'

		--AND @customer_no = o.customer_no /* where customer we gave is owner 6/29/2015 NOT RECIPIENT */
		GROUP BY
			cm.cust_memb_no, 
			cm.memb_org_no,
			cm.memb_level,
			e.entitlement_no,
			e.reset_type, 
			sli.li_seq_no,
			sli.sli_no, /* MIR 9/12/2014 */
			sli.perf_no, /* MIR 9/12/2014 */
			sli.recipient_no, /* MIR 9/25/2017 */
			CONVERT(varchar(10),f.perf_dt,101), /* 5/7/2015 */
			cm.init_dt,
			cm.expr_dt,
			cm.current_status,
			ce.id_key;

--select * into jb_order_entitlements from #order_entitlements
IF @debug = 'Y' begin print getdate()  SELECT '#order_entitlements member',  * from #order_entitlements end
		
		/* Gifted entitlements from a membership*/
		
		SELECT	
			customer_no=@customer_no,
			order_no=@order_no,
			cust_memb_no=0,
			memb_org_no=0,		
			e.reset_type,
			sli.li_seq_no,
			sli.sli_no,
			sli.perf_no,
			sli.recipient_no,
			e.ent_tkw_id, e.ent_price_type_group ,		
			2  membership_status/* as if it's current */		
			,row_number() over(partition by e.ent_tkw_id, e.ent_price_type_group order by sli_no ) row_no
			,ce.id_key
			into #gift_order_entitlements
		FROM dbo.T_ORDER o (NOLOCK)
			JOIN dbo.T_SUB_LINEITEM sli (NOLOCK) ON o.order_no = sli.order_no
			and sli.order_no = @order_no
			JOIN dbo.TR_PRICE_TYPE pt ON sli.price_type = pt.id
			JOIN dbo.LTR_ENTITLEMENT e ON e.ent_price_type_group = pt.price_type_group			
				and (e.is_adhoc='N')
			join LTX_CUST_ENTITLEMENT  ce on ce.entitlement_no = e.entitlement_no 
				and  coalesce(ce.cust_memb_no,0)=0 and ce.customer_no=@customer_no
			JOIN dbo.LV_ENTITLEMENT_INV_TKW_LIST v ON v.tkw = e.ent_tkw_id AND v.perf_no = sli.perf_no		
			JOIN dbo.T_PERF f (NOLOCK) ON f.perf_no = sli.perf_no /* to know which date to store & decrement from */					
		where sli.order_no = @order_no
		AND sli.sli_status IN (3,12)		
		/*AND NOT (			
			ISNULL(e.decline_benefit,'Y') = 'Y')*/
		/* is an active entitlement */
		AND ISNULL(e.inactive,'N') = 'N'
		GROUP BY			
			e.ent_tkw_id, e.ent_price_type_group ,
			e.reset_type, 
			sli.li_seq_no,
			sli.sli_no,
			sli.perf_no,
			sli.recipient_no,
			CONVERT(varchar(10),f.perf_dt,101)
			,ce.id_key

		
		
		INSERT #order_entitlements (
		customer_no,
		order_no,
		cust_memb_no,
		memb_org_no,
		entitlement_no,
		reset_type,
		li_seq_no,
		sli_no,
		perf_no,
		recipient_no,
		init_dt,
		expr_dt,
		membership_status,
		num_ent,
		num_remain_before_order,
		num_used,
		order_used,
		ltx_cust_entitlement_id,
		to_decrement)
			select sub.customer_no,
			sub.order_no,
			sub.cust_memb_no,
			sub.memb_org_no,
			a.entitlement_no,
			sub.reset_type,
			sub.li_seq_no,
			sub.sli_no,
			sub.perf_no,
			sub.recipient_no,
			a.init_dt,
			a.expr_dt,
			sub.membership_status,
			num_ent=a.num_ent,
			a.num_remain_before_order,
			a.num_used ,
			order_used = 1,
			ltx_cust_entitlement_id = a.id_key,
			null 
	from #gift_order_entitlements sub 
	join (
	 
	SELECT		
		e.entitlement_no,
		e.reset_type,
		sli.li_seq_no,
		sli.sli_no,
		sli.perf_no,
		sli.recipient_no,
		ce.init_dt,
		ce.expr_dt,
		2 current_status, /* as if it's current */
		num_ent = MIN(e.num_ent),		
		num_remain_before_order = MIN(COALESCE(ce.num_ent,e.num_ent) - ISNULL(coe.cust_num_used,0)),
		num_used = count(DISTINCT sli.sli_no),
		order_used = ROW_NUMBER() OVER (PARTITION BY e.entitlement_no ORDER BY sli.sli_no),
		ce.id_key
	FROM dbo.T_ORDER o (NOLOCK)
		JOIN dbo.T_SUB_LINEITEM sli (NOLOCK) ON o.order_no = sli.order_no
		JOIN dbo.TR_PRICE_TYPE pt ON sli.price_type = pt.id
		JOIN dbo.LTR_ENTITLEMENT e ON e.ent_price_type_group = pt.price_type_group			
		JOIN dbo.LV_ENTITLEMENT_INV_TKW_LIST v ON v.tkw = e.ent_tkw_id AND v.perf_no = sli.perf_no
		--CROSS APPLY dbo.LFT_INV_KEYWORD (sli.perf_no, e.ent_tkw_id) v
		JOIN dbo.T_PERF f (NOLOCK) ON f.perf_no = sli.perf_no /* to know which date to store & decrement from */
		LEFT JOIN dbo.LTX_CUST_ENTITLEMENT ce 
			ON (e.entitlement_no = ce.entitlement_no 
			AND ce.customer_no = @customer_no --o.customer_no
			--AND ce.cust_memb_no = 0
			--AND (e.is_adhoc = 'Y')
				and ((e.is_adhoc='Y')
				or
				(e.is_adhoc='N' and coalesce(ce.cust_memb_no,0)=0 and ce.gift_no>1 ))
			)
		LEFT JOIN (
			SELECT
				customer_no,
				cust_memb_no,
				entitlement_no,
				cust_num_used = ISNULL(SUM(num_used),0),
				ltx_cust_entitlement_id
			FROM dbo.LTX_CUST_ORDER_ENTITLEMENT
			WHERE coalesce(order_no,0)  <> @order_no
			and customer_no=@customer_no
			GROUP BY customer_no, cust_memb_no, entitlement_no,ltx_cust_entitlement_id
			) coe
		ON (
			coe.customer_no = ce.customer_no 
				AND coalesce(coe.cust_memb_no,0) = coalesce(ce.cust_memb_no,0)
			AND coe.entitlement_no = e.entitlement_no
			AND e.is_adhoc = 'Y'
			and ltx_cust_entitlement_id=ce.id_key)
	--WHERE o.customer_no = @customer_no
	--AND sli.order_no = @order_no
	join #gift_order_entitlements x on x.order_no = sli.order_no and x.sli_no=sli.sli_no
	where 
	sli.sli_status IN (3,12)	
	AND	(
		CASE WHEN e.date_to_compare = 'O' THEN o.order_dt ELSE f.perf_dt END
		BETWEEN CAST(CONVERT(varchar(10),ce.init_dt,101) as datetime) AND CAST(CONVERT(varchar(10),ce.expr_dt,101) as datetime) + '23:59:59'
		)
	GROUP BY
		e.entitlement_no,
		e.reset_type, 
		sli.li_seq_no,
		sli.sli_no,
		sli.perf_no,
		sli.recipient_no,
		CONVERT(varchar(10),f.perf_dt,101),
		ce.init_dt,
		ce.expr_dt,
		ce.id_key) a on a.sli_no = sub.sli_no;


IF @debug = 'Y' begin print getdate()  SELECT '#order_entitlements gift',  * from #order_entitlements end
	IF object_id('tempdb..#order_entitlements') IS NULL
		GOTO END_ME;

	
 
	/* for redeeming Adhoc */
		with    cte(entitlement_no, num_aval,id_key, i) as 
			(
			select  entitlement_no
			,       num_aval
			, id_key
			,       1
			from    (select entitlement_no,id_key, num_aval = Sum(num_ent-coalesce(num_used,0)) from dbo.[LFT_CUSTOMER_ENTITLEMENT_COUNT](@customer_no,@ORDER_NO) group by entitlement_no,id_key) a 
			union all
			select  entitlement_no
			,       num_aval
			, id_key
			,       i + 1
			from    cte
			where   cte.i < cte.num_aval
			)
	select  a.* ,ce.init_dt,ce.expr_dt,e.date_to_compare,ROW_NUMBER() over(partition by E.entitlement_no order by
	E.entitlement_no 
	) row_no into #working
	from    cte A
	join LTR_ENTITLEMENT e on e.enTITLEMENT_NO=A.entitlement_no
	JOIN LTX_CUST_ENTITLEMENT ce on ce.id_key = a.id_key
	order by
			E.entitlement_no
	,       i

	

	select a.*,b.ent_tkw_id, b.ent_price_type_group,h.gift_no,
	sort_key = ROW_NUMBER() OVER (partition by b.ent_tkw_id, b.ent_price_type_group ORDER BY CASE 			
				when h.gift_no is not null then 0
				WHEN b.is_adhoc='Y' and 'A'=@selection_priority then 1
				WHEN b.is_adhoc='Y'  then 2
				WHEN b.is_adhoc='N' and 'M'=@selection_priority then 1
				WHEN b.is_adhoc='N' then 2					
				ELSE '12/31/2999' END ,a.init_dt,a.id_key  ) into #working2
	 from #working a
	join LTR_ENTITLEMENT b on a.entitlement_no = b.entitlement_no
		and b.is_adhoc='Y' 
	left join LTX_CUST_ENTITLEMENT g on g.id_key = a.id_key
			left join LT_ENTITLEMENT_GIFT i on i.id_key = g.gift_no
			left join LTX_GIFT_ORDER_LOCK h on i.id_key = h.gift_no
				and h.order_no = @order_no
	INSERT #order_entitlements (
		customer_no,
		order_no,
		cust_memb_no,
		memb_org_no,
		entitlement_no,
		reset_type,
		li_seq_no,
		sli_no,
		perf_no,
		recipient_no,
		init_dt,
		expr_dt,
		membership_status,
		num_ent,
		num_remain_before_order,
		num_used,
		order_used,
		ltx_cust_entitlement_id,
		to_decrement)
		select 
		sub.customer_no,
			sub.order_no,
			sub.cust_memb_no,
			sub.memb_org_no,
			b.entitlement_no,
			sub.reset_type,
			sub.li_seq_no,
			sub.sli_no,
			sub.perf_no,
			sub.recipient_no,
			b.init_dt,
			b.expr_dt,
			sub.membership_status,
			num_ent=c.num_ent,
			num_remain_before_order = b.num_aval,
			num_used = c.num_ent - b.num_aval,
			order_used = 1,
			ltx_cust_entitlement_id = b.id_key,
			1

	  from (
	SELECT	
			customer_no=@customer_no,
			order_no=@order_no,
			cust_memb_no=0,
			memb_org_no=0,		
			e.reset_type,
			sli.li_seq_no,
			sli.sli_no,
			sli.perf_no,
			sli.recipient_no,
			e.ent_tkw_id, e.ent_price_type_group ,		
			2  membership_status/* as if it's current */		
			,row_number() over(partition by e.ent_tkw_id, e.ent_price_type_group order by sli_no ) row_no
		FROM dbo.T_ORDER o (NOLOCK)
			JOIN dbo.T_SUB_LINEITEM sli (NOLOCK) ON o.order_no = sli.order_no
			and sli.order_no = @order_no
			JOIN dbo.TR_PRICE_TYPE pt ON sli.price_type = pt.id
			JOIN dbo.LTR_ENTITLEMENT e ON e.ent_price_type_group = pt.price_type_group			
				and (e.is_adhoc='Y')
			JOIN dbo.LV_ENTITLEMENT_INV_TKW_LIST v ON v.tkw = e.ent_tkw_id AND v.perf_no = sli.perf_no		
			JOIN dbo.T_PERF f (NOLOCK) ON f.perf_no = sli.perf_no /* to know which date to store & decrement from */		
		where sli.order_no = @order_no
		AND sli.sli_status IN (3,12)
		and exists (select 1 from #working x where x.entitlement_no = e.entitlement_no)
		and  NOT (			
			ISNULL(e.decline_benefit,'Y') = 'Y')
		AND ISNULL(e.inactive,'N') = 'N'
		GROUP BY
			e.ent_tkw_id, e.ent_price_type_group ,
			e.reset_type, 
			sli.li_seq_no,
			sli.sli_no,
			sli.perf_no,
			sli.recipient_no,
			CONVERT(varchar(10),f.perf_dt,101)
			) sub
	join #working2 b on sub.ent_price_type_group = b.ent_price_type_group and sub.ent_tkw_id=b.ent_tkw_id and b.sort_key=sub.row_no
	join LTX_CUST_ENTITLEMENT c on c.id_key = b.id_key
	

	
	
--delete from LTX_GIFT_ORDER_LOCK where order_no=@order_no



IF @debug = 'Y' begin print getdate()  SELECT '#order_entitlements_after_adhoc', * FROM #order_entitlements ORDER BY cust_memb_no, order_used end

	/* 11/6 get the max for each entitlement kw/pt (without entitlement #) */
	SELECT
		dl_id = ROW_NUMBER() OVER (ORDER BY e.ent_tkw_id, e.ent_price_type_group),
		oe.sli_no,
		e.ent_tkw_id,
		e.ent_price_type_group
	INTO #decrement_list
	FROM #order_entitlements oe
		JOIN dbo.LTR_ENTITLEMENT e ON oe.entitlement_no = e.entitlement_no
	WHERE --coalesce(oe.reset_type,'M') = 'M' 
	coalesce(oe.membership_status,2) IN (2,7,3)
	GROUP BY oe.sli_no, e.ent_tkw_id, e.ent_price_type_group
	HAVING COUNT(*) > =1


	IF @debug = 'Y' begin print getdate() 		
		SELECT '#decrement_list'
		select * from #decrement_list
		end

	SELECT
		oe.oe_id,
		oe.num_remain_before_order,
		oe.sli_no,
		oe.entitlement_no,
		sort_key = ROW_NUMBER() OVER (ORDER BY CASE 
			when h.gift_no is not null then 0
			WHEN e.is_adhoc='Y' and 'A'=@selection_priority then 1
			WHEN e.is_adhoc='Y'  then 2
			WHEN e.is_adhoc='N' and 'M'=@selection_priority then 1
			WHEN e.is_adhoc='N' then 2		
			when h.gift_no is not null then 0
		   ELSE '12/31/2999' END ,oe.init_dt,oe.ltx_cust_entitlement_id ),
		processed = CAST(0 as int),
		oe.ltx_cust_entitlement_id	,
		 h.gift_no 	
	INTO #decrement_list2
	FROM #order_entitlements oe 
		JOIN dbo.LTR_ENTITLEMENT e ON oe.entitlement_no = e.entitlement_no
		JOIN #decrement_list dl ON (dl.sli_no = oe.sli_no AND dl.ent_tkw_id = e.ent_tkw_id AND dl.ent_price_type_group = e.ent_price_type_group)
		left join LTX_CUST_ENTITLEMENT g on g.id_key = oe.ltx_cust_entitlement_id			
		left join LTX_GIFT_ORDER_LOCK h on g.gift_no = h.gift_no
			and h.order_no = @order_no
					
				
	IF @debug = 'Y' begin print getdate()  SELECT '#decrement_list2',   * from #decrement_list2 end
	

DECLARE @sli int = (SELECT MIN(sli_no) FROM #decrement_list2)
DECLARE @oe_id int,
		@num_remain int,
		@entitlement_no int,
		@ltx_cust_entitlement_id int

WHILE @sli IS NOT NULL
 BEGIN	
	/* get the lowest ranked (to use first) entitlement for this sli */
	SELECT TOP 1 
			@oe_id = oe_id,
			@num_remain = num_remain_before_order,
			@entitlement_no = entitlement_no,
			@ltx_cust_entitlement_id = ltx_cust_entitlement_id
	FROM #decrement_list2
	WHERE sli_no = @sli
	AND num_remain_before_order > 0
	ORDER BY sort_key
 
	UPDATE #order_entitlements
	SET to_decrement = 1
	WHERE oe_id = @oe_id
	
	UPDATE #decrement_list2
	SET num_remain_before_order = num_remain_before_order - 1	
	WHERE entitlement_no = @entitlement_no
	and ltx_cust_entitlement_id=@ltx_cust_entitlement_id

	SET @sli = (SELECT MIN(sli_no) FROM #decrement_list2 WHERE sli_no > @sli)
 END;







	IF @debug = 'Y' begin print getdate()  SELECT '#order_entitlements',   * from #order_entitlements end
	IF @debug = 'Y' begin print getdate() select * from #decrement_list end
	IF @debug = 'Y' begin print getdate()  select * from #decrement_list2 end

/*	drop table order_entitlements
	drop table decrement_list2
	drop table decrement_list

select * into order_entitlements from #order_entitlements
select * into 	 decrement_list2 from #decrement_list2
	select * into decrement_list from #decrement_list
	*/

	/* remove those that were dups based on lower priority (not yet being used) */
	DELETE oe
	FROM #order_entitlements oe
		JOIN #decrement_list2 dl ON dl.oe_id = oe.oe_id
	WHERE ISNULL(oe.to_decrement,0) = 0

	

	IF @debug = 'Y' begin print getdate() SELECT '#order_entitlements final',   * from #order_entitlements end
	
	/* update entitlements used in this order  */
	IF NOT EXISTS (SELECT TOP 1 * FROM #order_entitlements) 
		GOTO END_ME

	
	INSERT dbo.LTX_CUST_ORDER_ENTITLEMENT (
		customer_no,
		order_no,
		sli_no, /* MIR 9/12/2014 */
		perf_no, /* MIR 9/19/2014 */
		cust_memb_no,
		entitlement_no,
		entitlement_date,
		num_used,
		ltx_cust_entitlement_id)
	SELECT
		customer_no,
		order_no,
		sli_no, /* MIR 9/12/2014 */
		perf_no, /* MIR 9/19/2014 */
		cust_memb_no,
		entitlement_no,
		entitlement_date,
		1,
		ltx_cust_entitlement_id
	FROM  #order_entitlements a
	where coalesce(to_decrement,1)=1
	GROUP BY customer_no, order_no, sli_no, perf_no, cust_memb_no, entitlement_no, entitlement_date,ltx_cust_entitlement_id


	
	
	/* Add customer+entitlement records where they exist in the order but not in the CUST table */
	INSERT dbo.LTX_CUST_ENTITLEMENT (customer_no, cust_memb_no, entitlement_no, entitlement_date, entitlement_perf_no, num_ent)
	SELECT
		oe.customer_no, 
		oe.cust_memb_no, 
		oe.entitlement_no, 
		CASE WHEN oe.reset_type = 'D' THEN oe.entitlement_date ELSE NULL END,  /* 6/18/2015 */
		CASE WHEN oe.reset_type = 'P' THEN oe.perf_no ELSE NULL END,
		MAX(oe.num_ent) /* 9/12/2014 MIR total # ents is MIN instead of SUM revised 2/5/2015 */		
	FROM #order_entitlements oe
		LEFT JOIN LTX_CUST_ENTITLEMENT ce ON 
		(oe.customer_no = ce.customer_no
		AND oe.cust_memb_no = ce.cust_memb_no
		AND oe.entitlement_no = ce.entitlement_no
		AND (
			(oe.reset_type = 'D' AND oe.entitlement_date  = ce.entitlement_date)
			OR
			(oe.reset_type = 'P' AND oe.perf_no = ce.entitlement_perf_no)
			OR

			(oe.reset_type = 'M'))
			)
	WHERE ce.customer_no is NULL /* non matches */
		and oe.LTX_CUST_ENTITLEMENT_id !=ce.id_key
	AND coalesce(oe.reset_type,'M')in ('D','P','M') /* 6/17/2015 including perf */
	and to_decrement=1
	GROUP BY oe.customer_no, oe.cust_memb_no, oe.entitlement_no, oe.reset_type, oe.entitlement_date, oe.perf_no


	
	IF @debug = 'Y' begin print getdate() select 'the first insert of cust ents', * FROM LTX_CUST_ENTITLEMENT where customer_no = @customer_no end

	/* Add membership-reset-type entitlements where they don't exist */
	INSERT dbo.LTX_CUST_ENTITLEMENT (customer_no, cust_memb_no, entitlement_no, entitlement_date, entitlement_perf_no, num_ent)
	SELECT
		@customer_no,
		cm.cust_memb_no,
		e.entitlement_no, 
		NULL,
		NULL,
		MAX(e.num_ent)
	FROM dbo.LTR_ENTITLEMENT e
		JOIN dbo.T_MEMB_LEVEL ml ON ml.memb_level_no = e.memb_level_no
		JOIN dbo.TX_CUST_MEMBERSHIP cm ON (ml.memb_org_no = cm.memb_org_no AND ml.memb_level = cm.memb_level AND cm.customer_no = @customer_no)
		LEFT JOIN #order_entitlements oe on e.entitlement_no = oe.entitlement_no
		LEFT JOIN dbo.LTX_CUST_ENTITLEMENT ce 
			ON (ce.customer_no = @customer_no
			AND ce.cust_memb_no = cm.cust_memb_no
			AND ce.entitlement_no = e.entitlement_no)
	WHERE ce.customer_no is null -- nonmatched in override table
	and oe.LTX_CUST_ENTITLEMENT_id !=ce.id_key
	AND oe.entitlement_no is NULL -- and nonmatched in order
	AND coalesce(e.reset_type,'M') = 'M'
	AND cm.cur_record = 'Y'
	and to_decrement=1
	GROUP BY cm.cust_memb_no, e.entitlement_no


	--Gift Processing
	/* Add entitlement gifts:*/
	
	
	DECLARE @gift_kw int = (SELECT CAST(dbo.FS_GET_DEFAULT_VALUE(NULL, 'Entitlements','ENTITLEMENT_GIFT_KEYWORD') as int))
	
	
	

--select * into order_entitlements from #order_entitlements

--return

	--Entitlementst that are at sublineitem level.
	insert into LT_ENTITLEMENT_GIFT(
			--id_key,
			[customer_no],
			[gift_customer_no],
			[gift_recip_email],
			[gift_recip_name],
			[gift_message],
			[gift_status],
			[li_seq_no],
			[entitlement_no],
			[num_items],			
			[ltx_cust_entitlement_id],
			[init_dt],
			[expr_dt] ,
			[created_by],
			[create_dt],
			[last_updated_by],
			[last_update_dt],
			custom_screen_ctrl,
			gift_code,
			sli_no)
		select a.customer_no,
				[gift_customer_no]=a.recipient_no,
				[gift_recip_email]=null,
				[gift_recip_name] = null,
				[gift_message] = null,
				[gift_status] = 'C',
				[li_seq_no]= a.li_seq_no,
				[entitlement_no]=a.entitlement_no,
				 [num_items]=1,
				 [ltx_cust_entitlement_id] = a.[ltx_cust_entitlement_id],
				 a.init_dt,
				 a.expr_dt,
				 created_by=null,
				 [create_dt],
			[last_updated_by],
			[last_update_dt],
			custom_screen_ctrl=null,
			gift_code=null	,
			a.sli_no		
		from  #order_entitlements a 
		join LTR_ENTITLEMENT b on b.entitlement_no = a.entitlement_no
			and b.transferrable='Y'
			and to_decrement=1
			and gift_level='S'
		where not exists(select 1 from LT_ENTITLEMENT_GIFT x where x.sli_no=a.sli_no )
		and  EXISTS (SELECT TOP 1 * 
				FROM dbo.LV_ENTITLEMENT_INV_TKW_LIST v 
				WHERE v.perf_no = a.perf_no
				AND v.tkw = @gift_kw)
	
	update  aa set aa.[gift_code]=bb.gift_code
	from LTX_CUST_ORDER_ENTITLEMENT aa
	join [dbo].[LT_ENTITLEMENT_GIFT]  bb on aa.sli_no=bb.sli_no
	join  #order_entitlements a on aa.sli_no=a.sli_no
		join LTR_ENTITLEMENT b on b.entitlement_no = a.entitlement_no
			and b.transferrable='Y'
			and to_decrement=1
			and gift_level='S'


	insert into LT_ENTITLEMENT_GIFT(
			--id_key,
			[customer_no],
			[gift_customer_no],
			[gift_recip_email],
			[gift_recip_name],
			[gift_message],
			[gift_status],
			[li_seq_no],		
			[entitlement_no],
			[num_items],
			[ltx_cust_entitlement_id],
			[init_dt],
			[expr_dt] ,
			[created_by],
			[create_dt],
			[last_updated_by],
			[last_update_dt],
			custom_screen_ctrl,
			gift_code,
			sli_no)
	select a.customer_no,
				[gift_customer_no]=max(a.recipient_no),
				[gift_recip_email]=null,
				[gift_recip_name] = null,
				[gift_message] = null,
				[gift_status] = 'C',
				[li_seq_no]= a.li_seq_no,
				[entitlement_no]=a.entitlement_no,
				 [num_items]=sum(a.num_ent),
				 [ltx_cust_entitlement_id] = a.[ltx_cust_entitlement_id],
				 a.init_dt,
				 a.expr_dt,
				 created_by=null,
				 [create_dt],
			[last_updated_by],
			[last_update_dt],
			custom_screen_ctrl=null,
			gift_code=null	,
			sli_no =null
		from  #order_entitlements a 
		join LTR_ENTITLEMENT b on b.entitlement_no = a.entitlement_no
			and b.transferrable='Y'
			and to_decrement=1
			and gift_level='L'
		where not exists(select 1 from LT_ENTITLEMENT_GIFT x where x.li_seq_no=a.li_seq_no and x.sli_no is null )
		and  EXISTS (SELECT TOP 1 * 
					FROM dbo.LV_ENTITLEMENT_INV_TKW_LIST v 
					WHERE v.perf_no = a.perf_no
					AND v.tkw = @gift_kw)	
		group by a.customer_no,a.li_seq_no,a.entitlement_no,a.ltx_cust_entitlement_id,	 [create_dt], a.init_dt,a.expr_dt,
		[last_updated_by],[last_update_dt]


			update  aa set aa.[gift_code]=bb.gift_code
from LTX_CUST_ORDER_ENTITLEMENT aa	
	join  #order_entitlements a on aa.order_no=a.order_no 
		and aa.entitlement_no=a.entitlement_no
	join [dbo].[LT_ENTITLEMENT_GIFT]  bb on a.li_seq_no=bb.li_seq_no and bb.sli_no is null
		join LTR_ENTITLEMENT b on b.entitlement_no = a.entitlement_no
			and b.transferrable='Y'
			and to_decrement=1
			and gift_level='L'




  END

END_ME:
IF object_id('tempdb..#order_entitlements') IS NOT NULL
	DROP TABLE #order_entitlements




GO

