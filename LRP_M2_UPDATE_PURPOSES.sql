SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO





/****** Object:  Stored Procedure dbo.    Script Date: ??/??/????******/
CREATE      PROCEDURE  LRP_M2_UPDATE_PURPOSES
		(@range int)
 
AS
Set NoCount On  

 
/******************************************************************************************************
Created 4/14/14 Mail2 by JC: This procedure is to update the purposes for every new email added to the 
system that has a corresponding Mail2 preference set. Range is in minutes.

grant exec on LRP_M2_Update_Purposes to impusers
exec LRP_M2_Update_Purposes 5
******************************************************************************************************/
--declare @range int = 120
Declare @Email Table (customer_no int, eaddress_no int, Address varchar(80) null)

Insert @Email (customer_no, eaddress_no, Address)
Select customer_no, eaddress_no,Address
From [dbo].T_EADDRESS e (nolock)
where create_dt between DATEADD(MI,-@range,GETDATE()) and GETDATE()

--select * from @email

if (select default_value from T_DEFAULTS where field_name = 'M2UpdateContactPurpose' ) = 'YES'

begin 
	Declare @ContactPurposes Table (Id int identity(1,1), m2_list_no int, purpose_id int, address varchar(80), status varchar(30), eaddress_no int)
	
	Insert	@ContactPurposes (m2_list_no, purpose_id, address, status, eaddress_no)
	Select	b.m2_list_no, c.contact_point_purpose, a.address, b.status, a.eaddress_no
	from	@Email a 
			join LTX_m2_Elists_eaddress b (nolock) on a.address = b.address
			join LTR_m2_elists c (nolock) on b.m2_list_no = c.id

	--Select * from @ContactPurposes

	--If status is inactive (Delete)
	Delete from TX_CONTACT_POINT_PURPOSE
	--select	'inactive',*	 
	from	TX_CONTACT_POINT_PURPOSE a 
			join @ContactPurposes b on a.contact_point_id = b.eaddress_no  and a.purpose_id = b.purpose_id
	where	b.status = 'inactive'
			and exists (select 1 from TX_CONTACT_POINT_PURPOSE a1 (Nolock)where b.eaddress_no = a1.contact_point_id and b.purpose_id = a1.purpose_id)

	
	--If status is active (Insert)
	INSERT INTO dbo.TX_CONTACT_POINT_PURPOSE (contact_point_id, contact_point_category, purpose_id, created_by, create_loc) 
	select	Distinct b.eaddress_no, d.contact_point_category, b.purpose_id, dbo.FS_USER(), [dbo].fs_location()
	--select	'active',*	
	from	@ContactPurposes b
			join TX_CONTACT_POINT_category_PURPOSE d (Nolock) on b.purpose_id = d.purpose_id
	where	b.status = 'active'
			and not exists (select 1 from TX_CONTACT_POINT_PURPOSE a1 (Nolock)where b.eaddress_no = a1.contact_point_id and b.purpose_id = a1.purpose_id)

end 

else

begin 
	RETURN
End









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

