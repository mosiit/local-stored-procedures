USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE LP_ADD_ERC_RANKINGS_TO_HH
AS

SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- Find all INDIVIDUALS wtih ERC RANKING (Rank_Type = 7) without a HOUSEHOLD Rank_Type = 7 AND ADD HOUSEHOLD RANK_TYPE = 7  

--An individual on a household record cannot be the "owner" of an order. 
--Performances with a required ranking look for the ranking on the owner's record. 
--Currently, Teacher Partners who are part of households are unable to reserve tickets to TPP events. 

INSERT INTO dbo.T_CUST_RANK
(
    customer_no,
    rank_type,
    rank,
    create_loc,
    created_by,
    create_dt,
    last_updated_by,
    last_update_dt
)
SELECT DISTINCT -- need distinct in case there are two TPP teachers in 1 household 
	hh.customer_no,
	7 AS rank_type,
	50 AS rank,
	@@SERVERNAME,
	[dbo].[FS_USER](),
	GETDATE(),
	[dbo].[FS_USER](),
	GETDATE()
FROM dbo.T_CUST_RANK r
INNER JOIN dbo.T_CUSTOMER ind
	ON ind.customer_no = r.customer_no
	AND ind.cust_type = 1 -- Individual
INNER JOIN dbo.V_CUSTOMER_EXPAND_HOUSEHOLD exhs
	ON ind.customer_no = exhs.expanded_customer_no
INNER JOIN dbo.T_CUSTOMER hh
	ON exhs.customer_no = hh.customer_no
	AND hh.cust_type = 7 -- Household
LEFT JOIN dbo.T_CUST_RANK hhrank
	ON hh.customer_no = hhrank.customer_no
	AND hhrank.rank_type = 7 -- ERC Ranking
WHERE r.rank_type = 7 -- ERC Ranking
AND hhrank.rank IS NULL 

