USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_GIFT_MEMB_CHANGE_EXPR_DATE]    Script Date: 11/22/2016 2:56:07 PM ******/
DROP PROCEDURE [dbo].[LP_GIFT_MEMB_CHANGE_EXPR_DATE]
GO

/****** Object:  StoredProcedure [dbo].[LP_GIFT_MEMB_CHANGE_EXPR_DATE]    Script Date: 11/22/2016 2:56:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_GIFT_MEMB_CHANGE_EXPR_DATE]
	@InitDateStart	DATETIME,
	@InitDateEnd 	DATETIME,
	@ExprDateStart	DATETIME,
	@ExprDateEnd 	DATETIME,
	@ExprDateChange	DATETIME
AS

SET NOCOUNT ON

	BEGIN

		BEGIN TRY

			BEGIN TRANSACTION

				INSERT INTO LT_GIFT_MEMB_EXP_DATE_CHANGES ([cust_memb_no],[customer_no],[campaign_no],[memb_org_no],[memb_level],[memb_amt],[ben_provider],[init_dt],[expr_dt],[current_status],[NRR_status],[declined_ind],[AVC_amt],[orig_expiry_dt],[orig_memb_level],[cur_record],[ben_holder_ind],[recog_amt],[category_trend],[notes])
					SELECT	cm.cust_memb_no,
							cm.customer_no,
							cm.campaign_no,
							cm.memb_org_no,
							cm.memb_level,
							cm.memb_amt,
							cm.ben_provider,
							cm.init_dt,
							cm.expr_dt,
							cm.current_status,
							cm.NRR_status,
							cm.declined_ind,
							cm.AVC_amt,
							cm.orig_expiry_dt,
							cm.orig_memb_level,
							cm.cur_record,
							cm.ben_holder_ind,
							cm.recog_amt,
							cm.category_trend,
							cm.notes
					FROM	TX_CUST_MEMBERSHIP cm
					WHERE	cm.nrr_status IN ('NE', 'RI')
					AND		cm.init_dt BETWEEN @InitDateStart AND @InitDateEnd
					AND		cm.expr_dt BETWEEN @ExprDateStart AND @ExprDateEnd
					AND		cm.ben_provider > 0
					AND		cm.memb_org_no = 4 -- household membership

				UPDATE	TX_CUST_MEMBERSHIP
				SET		expr_dt = @ExprDateChange
				WHERE	nrr_status IN ('NE', 'RI')
				AND		init_dt BETWEEN @InitDateStart AND @InitDateEnd
				AND		expr_dt BETWEEN @ExprDateStart AND @ExprDateEnd
				AND		ben_provider > 0
				AND		memb_org_no = 4

			COMMIT TRANSACTION

		END TRY

		BEGIN CATCH

			SELECT	ERROR_NUMBER() AS ErrorNumber,
					ERROR_SEVERITY() AS ErrorSeverity,  
					ERROR_STATE() AS ErrorState,  
					ERROR_PROCEDURE() AS ErrorProcedure,  
					ERROR_LINE() AS ErrorLine,  
					ERROR_MESSAGE() AS ErrorMessage; 

			IF(@@TRANCOUNT > 0)
				ROLLBACK TRANSACTION

		END CATCH


	END
GO


