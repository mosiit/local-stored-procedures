USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_SHOW_SCHEDULE_CONFLICT_CHECKER]    Script Date: 2/3/2016 9:42:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE [dbo].[LRP_SHOW_SCHEDULE_CONFLICT_CHECKER]
GO

CREATE PROCEDURE [dbo].[LRP_SHOW_SCHEDULE_CONFLICT_CHECKER]
	@PerfStartDate		DATETIME,
	@PerfEndDate		DATETIME,
	@TitleString		VARCHAR(MAX) -- comma delimited list, empty = all
AS

-- ================================================================================================
-- Created:  H. Sheridan (MOS), 1/28/2016
--
-- Notes:
--
--		Performance dates have to be cut down to date format only (mm/dd/yyyy) in the WHERE clause.
--		Otherwise, the end date isn't included in the results.
--
--		Content information is found by going here in the Tessitura application:
--				Main Menu >  GoTo > Ticketing Setup  
--				Double click on a title
--				The Title Details page opens
--				Click on the Content tab
--
-- ================================================================================================

BEGIN

	SET NOCOUNT ON

		DECLARE	@tblTitles TABLE	(
									TitleDesc	VARCHAR(30)
									)

		DECLARE @tblShows TABLE		(
									[RowID]											INT IDENTITY(1,1),
									[Title]											VARCHAR(30),
									[Title Number]									INT,
									[Performance Code]								VARCHAR(10),
									[Performance Name]								VARCHAR(30),
									[Performance Date]								VARCHAR(10),
									[Performance Date Report]						VARCHAR(100),
									[Performance Time]								VARCHAR(30),
									[Performance Time Display]						VARCHAR(10),
									[Facility]										VARCHAR(30),
									[Performance Number]							INT,	
									[Season Name]									VARCHAR(30),
									[Production Season Name]						VARCHAR(30),
									[Content Type]									VARCHAR(30),
									[Content Value]									VARCHAR(MAX)
									)

		DECLARE	@tblTimeBorders TABLE	(
										[RowID]				INT IDENTITY(1,1),
										[Title]				VARCHAR(30),
										--[Performance Name]	VARCHAR(30),
										[Performance Date]	VARCHAR(10),
										[MinTime]			VARCHAR(10),
										[MaxTime]			VARCHAR(10),
										[EntryInterval]		INT
										)

		DECLARE	@tblTimeInts TABLE	(
									[Title]				VARCHAR(30),
									[Performance Name]	VARCHAR(30),
									[Performance Date]	VARCHAR(10),
									[Time Slot]			TIME
									)

		DECLARE @tblPerfDates TABLE	(
									[RowID]				INT IDENTITY(1,1),
									[Title]				VARCHAR(30),
									[Performance Name]	VARCHAR(30),
									[Performance Date]	VARCHAR(10)
									)
									
		DECLARE @tblCnts TABLE	(
								[Title]					VARCHAR(30),
								[Performance Date]		VARCHAR(10),
								[Performance Time]		VARCHAR(30),
								[ShowCount]				INT
								)

		DECLARE @tblDates TABLE	(
								[Title]					VARCHAR(30),
								[Performance Date]		DATETIME
								)

		DECLARE	@tblResults TABLE	(
									[Title]						VARCHAR(30),
									[Performance Date]			VARCHAR(10),
									[Performance Date Report]	VARCHAR(100),
									[Performance Time Display]	VARCHAR(10),
									[Performance Name]			VARCHAR(30),
									[Performance Time]			VARCHAR(30),
									[Show Status]				VARCHAR(60)
									)

		DECLARE	@TitleStringTrim		VARCHAR(MAX)

		SET	@TitleStringTrim = LTRIM(RTRIM(REPLACE(REPLACE(@TitleString, ', ', ','), ' ,', ',')))
		
		IF @TitleStringTrim <> 'All'
			INSERT INTO @tblTitles (TitleDesc)
				SELECT element FROM FT_SPLIT_LIST (@TitleStringTrim,',') WHERE element IS NOT NULL

		INSERT INTO @tblShows
			SELECT	DISTINCT pep.title_name AS 'Title',
					pep.title_no AS 'Title Number',
					pep.performance_code AS 'Performance Code',
					pep.performance_name AS 'Performance Name',
					--pep.performance_dt AS 'Performance Date', 
					CONVERT(VARCHAR(10), pep.performance_dt, 101) AS 'Performance Date', 
					DATENAME(dw, pep.performance_dt) + ', ' + DATENAME(mm, pep.performance_dt) + ' ' + DATENAME(dd, pep.performance_dt) + ', ' + DATENAME(yy, pep.performance_dt) AS 'Performance Date Report', 
					pep.performance_time AS 'Performance Time',
					pep.performance_time_display AS 'Performance Time Display',
					pep.performance_facility_name AS 'Facility',
					pep.performance_no AS 'Performance Number',
					pep.season_name AS 'Season Name',
					pep.production_season_name AS 'Production Season Name',
					pet.content_type AS 'Content Type',
					pet.content_value AS 'Content Value'
			FROM	[dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] pep
			JOIN	[dbo].[LV_PRODUCTION_ELEMENTS_TITLE] pet
			ON		pet.title_no = pep.title_no
			WHERE	CONVERT(VARCHAR(10), pep.performance_dt, 101) >= @PerfStartDate
			AND		CONVERT(VARCHAR(10), pep.performance_dt, 101) <= @PerfEndDate
			AND		(@TitleStringTrim = 'All' OR pep.performance_facility_name IN (SELECT TitleDesc FROM @tblTitles))
			AND		pet.content_type = 'Entry Interval'
			ORDER BY 'Performance Date', 'Performance Time'

		--SELECT  'debug', [Title],
		--		[Title Number],
		--		[Performance Code],
		--		[Performance Name],
		--		[Performance Date],
		--		[Performance Date Report],
		--		[Performance Time],
		--		[Performance Time Display],
		--		[Facility],
		--		[Performance Number],
		--		[Season Name],
		--		[Production Season Name],
		--		[Content Type],
		--		[Content Value] 
		--FROM	@tblShows
		--ORDER BY [Performance Date], [Performance Time]

		INSERT INTO @tblTimeBorders
			SELECT	[Title], [Performance Date], MIN([Performance Time]), MAX([Performance Time]), [Content Value]
			FROM	@tblShows
			GROUP BY [Title], [Performance Date], [Content Value]

		--SELECT 'debug', [Performance Date], [MinTime], [MaxTime] FROM @tblTimeBorders ORDER BY Title, [Performance Date]

		DECLARE	@PerfDate		VARCHAR(10),
				@PerfName		VARCHAR(30),
				@Title			VARCHAR(30),
				@MinTime		TIME,
				@MaxTime		TIME,
				@TimeSlot		TIME,
				@NextTime		TIME,
				@LastTime		TIME,
				@EntryInt		INT,
				@RowID			INT,
				@PerfDateCnt	INT,
				@PerfDateMax	INT,
				@TimeCnt		INT,
				@TimeMax		INT

		INSERT INTO @tblPerfDates
			SELECT	DISTINCT tb.[Title],
					s.[Performance Name],
					tb.[Performance Date]
			FROM	@tblTimeBorders tb
			JOIN	@tblShows s
			ON		tb.[Title] = s.[Title]
			AND		tb.[Performance Date] = s.[Performance Date]

		--SELECT 'debug', * from @tblPerfDates ORDER BY [Title], [Performance Date], [Performance Name]

		SELECT	@PerfDateCnt = 1
		SELECT	@PerfDateMax = MAX([RowID]) FROM @tblPerfDates

		WHILE @PerfDateCnt <= @PerfDateMax
			BEGIN

				SELECT	@Title = [Title],
						@PerfName = [Performance Name],
						@PerfDate = [Performance Date]
				FROM	@tblPerfDates
				WHERE	[RowID] = @PerfDateCnt

				SELECT	@MinTime = [MinTime],
						@MaxTime = [MaxTime],
						@EntryInt = [EntryInterval]
				FROM	@tblTimeBorders
				WHERE	[Title] = @Title
				AND		[Performance Date] = @PerfDate
				--AND		[Performance Name] = @PerfName

				--SELECT	'debug', @PerfDate AS 'Perf Date', @MinTime AS 'Min Time', @MaxTime AS 'Max Time'

				SELECT	@TimeSlot = @MinTime
				SELECT	@NextTime = @MinTime

				INSERT INTO @tblTimeInts
					SELECT	@Title, @PerfName, @PerfDate, @TimeSlot

				WHILE @NextTime < @MaxTime
					BEGIN
						SELECT	@TimeSlot = MAX([Time Slot]) FROM @tblTimeInts WHERE [Performance Date] = @PerfDate AND [Performance Name] = @PerfName

						SELECT	@NextTime = DATEADD(mi, @EntryInt, CONVERT(TIME(0), @TimeSlot, 8))

						--SELECT 'debug', @PerfDate AS 'Perf Date', @TimeSlot AS 'Time Slot', @NextTime AS 'Next Time'

						INSERT INTO @tblTimeInts
							SELECT	@Title, @PerfName, @PerfDate, @NextTime
					END

				--SELECT 'debug', * FROM @tblTimeInts

				SELECT	@PerfDateCnt = @PerfDateCnt + 1

			END

		--SELECT 'debug', [Title], [Performance Date], [Time Slot], CONVERT(VARCHAR(5), [Time Slot]) FROM @tblTimeInts ORDER BY [Title], [Performance Date], [Time Slot]

	END

	DECLARE	@NextDate	DATETIME,
			@PrevDate	DATETIME

	INSERT INTO @tblDates
		SELECT	DISTINCT s.Title, 
				@PerfStartDate
		FROM	@tblShows s

	SET @PrevDate = @PerfStartDate
	SET @NextDate = @PerfStartDate

	WHILE @NextDate < @PerfEndDate
		BEGIN
			SET @NextDate = DATEADD(dd, 1, @PrevDate)

			SET @PrevDate = @NextDate

			INSERT INTO @tblDates
				SELECT	DISTINCT s.[Title],
						@NextDate
				FROM	@tblShows s
		END

	--SELECT 'debug', * FROM @tblDates

	-- 2016/05/16, H. Sheridan - Added code to consolidate Thrill Ride performances
	UPDATE	@tblShows
	SET		[Title] = CASE
						WHEN [Title] = 'Thrill Ride 360' AND [Performance Name] LIKE '%Capsule A' THEN 'Thrill Ride 360/Capsule A'
						WHEN [Title] = 'Thrill Ride 360' AND [Performance Name] LIKE '%Capsule B' THEN 'Thrill Ride 360/Capsule B'
						ELSE [Title]
					  END

	--UPDATE	@tblDates
	--SET		[Title] = CASE
	--					WHEN [Title] = 'Thrill Ride 360' AND [Performance Name] LIKE '%Capsule A' THEN 'Thrill Ride 360/Capsule A'
	--					WHEN [Title] = 'Thrill Ride 360' AND [Performance Name] LIKE '%Capsule B' THEN 'Thrill Ride 360/Capsule B'
	--					ELSE [Title]
	--				  END

	UPDATE	@tblTimeInts
	SET		[Title] = CASE
						WHEN [Title] = 'Thrill Ride 360' AND [Performance Name] LIKE '%Capsule A' THEN 'Thrill Ride 360/Capsule A'
						WHEN [Title] = 'Thrill Ride 360' AND [Performance Name] LIKE '%Capsule B' THEN 'Thrill Ride 360/Capsule B'
						ELSE [Title]
					  END

	INSERT INTO @tblCnts
		SELECT	[Title],
				[Performance Date],
				[Performance Time],
				COUNT(*)
		FROM	@tblShows
		GROUP BY [Title], [Performance Date], [Performance Time]

	--SELECT 'debug', * FROM @tblCnts		
	INSERT INTO @tblResults
		SELECT	d.[Title] AS 'Title',
				CONVERT(VARCHAR(10), d.[Performance Date], 101) AS 'Performance Date',
				DATENAME(dw, d.[Performance Date]) + ', ' + DATENAME(mm, d.[Performance Date]) + ' ' + DATENAME(dd, d.[Performance Date]) + ', ' + DATENAME(yy, d.[Performance Date]) AS 'Performance Date Report',
				'' AS 'Performance Time Display',
				'' AS 'Performance Name',
				'' AS 'Performance Time',
				'Missing Entire Day' AS 'Show Status'
		FROM	@tblDates d
		WHERE	d.[Performance Date] NOT IN (SELECT [Performance Date] FROM @tblShows)

		UNION

		SELECT	s.[Title] AS 'Title',
				s.[Performance Date] AS 'Performance Date',
				s.[Performance Date Report] AS 'Performance Date Report',
				s.[Performance Time Display] AS 'Performance Time Display',
				s.[Performance Name] AS 'Performance Name',
				s.[Performance Time] AS 'Performance Time',
				CASE 
					WHEN c.[ShowCount] > 1 THEN 'Duplicate'
					ELSE 'Scheduled Show' 
				END AS 'Show Status'
		FROM	@tblShows s
		JOIN	@tblCnts c
		ON		s.[Title] = c.[Title]
		AND		s.[Performance Date] = c.[Performance Date]
		AND		s.[Performance Time] = c.[Performance Time]

		UNION

		SELECT	i.[Title] AS 'Title',
				--i.[Performance Name] AS 'Performance Name',
				i.[Performance Date] AS 'Performance Date',
				DATENAME(dw, i.[Performance Date]) + ', ' + DATENAME(mm, i.[Performance Date]) + ' ' + DATENAME(dd, i.[Performance Date]) + ', ' + DATENAME(yy, i.[Performance Date]) AS 'Performance Date Report',
				[dbo].[LF_Format_Time](i.[Time Slot], '') AS 'Performance Time Display',
				'' AS 'Performance Name',
				i.[Time Slot] AS 'Performance Time',
				'Missing Show' AS 'Show Status'
		FROM	@tblTimeInts i
		WHERE	NOT EXISTS (
							SELECT	s.[Title Number]
							FROM	@tblShows s
							WHERE	s.[Title] = i.[Title]
							--AND		s.[Performance Name] = i.[Performance Name]
							AND		s.[Performance Date] = i.[Performance Date]
							AND		CONVERT(VARCHAR(5), s.[Performance Time]) = CONVERT(VARCHAR(5), i.[Time Slot])
							)
		ORDER BY 'Title', 'Performance Date', 'Performance Time', 'Performance Name'

		IF (SELECT COUNT([Title]) FROM @tblResults) = 0
			INSERT INTO @tblResults ([Title]) 
				SELECT	'No data found'

		SELECT	[Title],
				[Performance Date],
				[Performance Date Report],
				[Performance Time Display],
				[Performance Name],
				[Performance Time],
				[Show Status]
		FROM	@tblResults
		ORDER BY [Title], CAST([Performance Date] AS DATETIME), [Performance Time], [Performance Name]


GO


