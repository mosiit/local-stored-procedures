USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_NSCAN_AUTOMATIC_ATTENDANCE_WITH_PRINT]    Script Date: 1/17/2017 5:44:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================================================================================

Author:			Natasha Purkiss (Tessitura Network)
Create date:	6/12/2014
Description:	Prints and scans tickets that have been seated and meet the parameters selected. 
				
 ===================================================================================================================================================*/
Create PROCEDURE [dbo].[LP_NSCAN_AUTOMATIC_ATTENDANCE]

(	

--Declare
	@mos_str varchar(max), -- = ('2, 3, 4'),
	@perf_type_str varchar(max), -- = '1',
	@mins_past int = null, -- e.g. '15' means the ticket must have been created in the last 15mins. 
							--If null then gets all. This value will depend on how often the job is being run
	@channel_str varchar(max) = null
)


AS
BEGIN
SET NOCOUNT ON;

Declare 
	@selected_perf_no int, 
	@ticket_no int,
	@sli_no int,
	@perf_no_str VARCHAR(20),
	@perf_no int
		
	
Declare @tickets_to_scan as table
	(
		ticket_no int, 
		selected_perf_no int,
		printed_ind char(1),
		sli_no int
	)
	
Declare @results as table
	(
		ticket_no int,
		perf_no int,	
		success char(1), 
		error varchar(max)
	)
		
--Get all the tickets to scan/print
Insert into @tickets_to_scan
	Select distinct 
		a.ticket_no,
		a.perf_no,
		case when a.ticket_no is not null then 'Y' else 'N' end,
		a.sli_no
	from 
		T_SUB_LINEITEM a 
		join T_PERF b on a.perf_no=b.perf_no
		join T_ORDER c on a.order_no=c.order_no	
		left outer join T_ORDER_SEAT_HIST d on d.ticket_no=a.ticket_no and a.order_no=d.order_no and d.event_code = 22
	where 1 = 1
		and a.sli_status in (3, 12) --seated and paid, or ticketed and paid
		and (charindex(',' + convert(varchar,c.MOS) + ',' , ',' + @mos_str + ',') > 0 or @mos_str is null)
		and (charindex(',' + convert(varchar,b.perf_type) + ',' , ',' + @perf_type_str + ',') > 0 or @perf_type_str is null)
		and	(charindex(',' + convert(varchar,c.channel) + ',' , ',' + @channel_str + ',') > 0 or @channel_str is null)
		and (a.create_dt > DATEADD(minute,-@mins_past, GETDATE()) or @mins_past is null) --required


--Begin Cursor
	IF @@ROWCOUNT>0
	Begin
	
		DECLARE ticketcursor CURSOR FOR
		SELECT
			sli_no,
			selected_perf_no
		FROM
			@tickets_to_scan
			
		OPEN ticketcursor
		
		FETCH NEXT FROM ticketcursor INTO @sli_no,@perf_no
		
		WHILE @@FETCH_STATUS=0
		BEGIN

			BEGIN TRY
				--If the ticket isn't printed yet, print it so it can be scanned
				IF NOT EXISTS (SELECT 1 FROM TX_SLI_TICKET WITH(NOLOCK) WHERE sli_no = @sli_no AND perf_no = @perf_no) --has not been printed yet

				BEGIN
			
					Execute @ticket_no = [dbo].AP_Get_NextID_function 'TN', @increment = 1

					EXEC TP_UPDATE_SLI_WITH_TICKET
					@sli_no	= @sli_no,
					@new_tkt_no	= @ticket_no,
					@update_tx_perf_seat = 'Y'
					
				End
				ELSE
					SELECT @ticket_no = ticket_no FROM TX_SLI_TICKET WITH(NOLOCK) WHERE sli_no = @sli_no and perf_no = @perf_no
			
				SET @perf_no_str = CONVERT(VARCHAR,@perf_no)
				
				--Scan the ticket
				IF @ticket_no IS NOT NULL		
				EXEC [NP_SCAN_EVENTS]
						@scan_str = @ticket_no,
						@selected_perf_str = @perf_no_str,
						@user_entrance = NULL,
						@update = N'Y', --updates T_ORDER_SEAT_HIST and inserts into T_ATTENDANCE 
						@online = N'Y',
						@exit_mode = N'N',
						@user_id = N'nscan',
						@update_dt = null,
						@device_name = NULL,
						@control_id = 0,
						@suppress_log_updates = N'N'
		
			END TRY

			BEGIN CATCH
			
				print 'failed to run NP_SCAN_EVENTS'
				print @ticket_no
				print error_message()	
							
			END CATCH		
		
			FETCH NEXT FROM ticketcursor INTO @sli_no,@perf_no
		
		End;
		
		CLOSE ticketcursor;
		DEALLOCATE ticketcursor;
	
	END; --IF @@ROWCOUNT>0

Exec NP_SEND_EMAIL

END


