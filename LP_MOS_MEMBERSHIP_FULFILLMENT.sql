USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_MOS_MEMBERSHIP_FULFILLMENT]    Script Date: 12/18/2018 12:51:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_MOS_MEMBERSHIP_FULFILLMENT]
(
	@customer_no int,
	@affiliation_type int = 0,  -- ASH 09/20/2016
	@list_no int = 0,
	@reprint_ind Int,  -- 1 Yes, 2 No (do not reprint)
	@start_dt datetime,
	@end_dt datetime,
	@memb_org_str varchar(4000),
	@memb_level_str varchar(4000),
	@signor int,
	@new_pin_ind int, -- 1 Yes, 2 No
	@gift_memb int = 3,	-- 1 is gift, 2 not gift, 3 all
	@preview char(1) --U = Update, R = Review Only (code = 84 from TR_GOOESOFT_DROPDOWN)
)
 
 AS

Set NoCount On  -- added auto CWR 8/9/2001

/**********************************Customized Acknowledgement Selection Stored Proc*************************
Customized membership fulfillment utility, to be used with Memberships as Products plugin.

Created by Alex Harris, Tessitura Network, for Museum of Science

This customization is loosely based on the standard Print Acknowledgement Letters utility, but is
designed to be used to create membership data for memberships sold via the Memberships as Products
custom plugin.

Updated 09/13/2016 TSR to parse the "message" part of the special request note for return in the dataset.
updated 09/19/2016 TSR to increase field length in #letters for member and giver postal codes. Accomodates a truncation error for postal codes with zip4 data.
Updated 09/20/2016 ASH to incorporate school information for TPP memberships, based on a new @affiliation_type parameter.
updated 10/25/2016 TSR to add parameter for returning gift memberships, non gift memberships, or both in result set
updated 10/25/2016 TSR to always return recipient info in esal1_desc
updated 11/02/2016 to fine tune gift membership identification TSR
updated 11/03/2016 to include benefactor_id > 0 AND special request type as ways to ID whether a membership is a gift membership.
udpated 11/03/2016 to account for instances in which a primary affiliate of a household with an inherited address has a TPP membership on their individual record. They were originally being excluded because there is no primary address attached to the individual record.
updated 12/01/2016 BG to exclude cards where the customer and campaign have already been printed. When new transactions are made against an existing membership, or even if a membership is deleted and replaced, those actions should not result in a new card.
updated 2/2/2017 BG (TSR) to identify gift memberships earlier, *before* inserting into LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS
updated 2/16/2017 BG (TSR) to exclude MaP cards already printed ONLY if they were the same level (allow printing if the same membership has been upgraded)
updated 7/11/2017 BG (TSR) added grouping to Annual Fund/Advancement Memberships selection so that multiple contributions don't result in duplicate cards
updated 7/11/2017 BG (TSR) updating printed_dt for reprint = Y
updated 7/19/2017 BG(TSR) now updating printed_dt for both reprint Y and N
updated 7/20/2017 BG(TSR) added join to #Memberships table during #letters insert and removed gift parameter filter from final select - it *should* be redundant, but if for any reason it's not, we want to see every card that was processed
updated 8/29/2017 BG(TSR) added filter for Annual Fund memberships only, current_status must be Active or Pending
updated 9/13/2017 BG(TSR) update from 12/1/16 now only applies to Annual Fund memberships (memb org 5,24)
updated 10/2/2018 MP (TSR) update to include Affiliation type for the ESal2 export
udpated 12/18/2018 TSR(bg) restored 7/20 updates which were missing for some reason
udpated 12/18/2018 TSR(bg) updated is_gift case statement so *either* a benefactor *or* special request indicates a gift


Sample Execute:

exec LP_MOS_MEMBERSHIP_FULFILLMENT
	@customer_no = null,
	@affiliation_type  = null,  -- ASH 09/20/2016
	@list_no = null,
	@reprint_ind  = 1,  -- 1 Yes, 2 No (do not reprint)
	@start_dt = '09/19/2015' ,
	@end_dt = '9/24/2016',
	@memb_org_str = null ,
	@memb_level_str = null,
	@signor = null ,
	@new_pin_ind  = 2,-- 1 Yes, 2 No
	@gift_memb = 1,
	@preview = 'R'

Execute 

***********************************************************************************************************/
if OBJECT_ID('tempdb..#Memberships') is not null begin drop table #Memberships end 
if OBJECT_ID('tempdb..#special_Req_text') is not null begin drop table #special_Req_text end 
if OBJECT_ID('tempdb..#letters') is not null begin drop table #letters end 
if OBJECT_ID('tempdb..#Member_Cards') is not null begin drop table #Member_Cards end 

/*
if OBJECT_ID('LT_CARD_FULFILL_AUDIT') is null 
begin
	create table LT_CARD_FULFILL_AUDIT(
	  id int identity(1,1)
	, parameter varchar(200)
	, value varchar(max)
	, create_dt datetime default(getdate())
	) 
end

insert into LT_CARD_FULFILL_AUDIT(parameter, value)
values('@customer_no', cast(@customer_no as varchar))
	,('@affiliation_type', cast(@affiliation_type as varchar))  
	,('@list_no', cast(@list_no as varchar))
	,('@reprint_ind', cast(@reprint_ind as varchar))
	,('@start_dt', cast(@start_dt as varchar))
	,('@end_dt', cast(@end_dt as varchar))
	,('@memb_org_str', cast(@memb_org_str as varchar))
	,('@memb_level_str', cast(@memb_level_str as varchar))
	,('@signor', cast(@signor as varchar))
	,('@new_pin_ind', cast(@new_pin_ind as varchar))
	,('@gift_memb', cast(@gift_memb as varchar))
	,('@preview', cast(@preview as varchar))
*/

If @customer_no > 0 And Not Exists (Select * From dbo.T_CUSTOMER Where customer_no = @customer_no
	And ISNULL(inactive,1) = 1)
  Begin
	Raiserror('Customer number provided is either invalid or inactive.',11,2)
	RETURN
  End


If @list_no > 0 and Not Exists (Select * From dbo.VS_LIST Where list_no = @list_no)
  Begin
	Raiserror('List provided does not exist or user does not have permission to use.', 11, 2) 
	RETURN
  End

  
Declare 
	@membership_perf_type int,
	@send_to_giver int,
	@send_to_recipient int,
	@memb_card_prefix varchar(10),
	@memb_card_customer_length int,
	@card_name_override_kw int,
	@one_step_constituency int

Set		@send_to_giver = 3
Set		@send_to_recipient = 2
Set		@card_name_override_kw = 482
Set		@one_step_constituency = 30

Select @memb_card_prefix = Coalesce(dbo.FS_GET_DEFAULT_VALUE(dbo.FS_GET_PARAM_FROM_APPNAME('UG'), 'IMPRESARIO', 'NSCAN_MEMBER_CUSTOMER_PREFIX'),'')
Select @membership_perf_type = Coalesce(dbo.FS_GET_DEFAULT_VALUE(dbo.FS_GET_PARAM_FROM_APPNAME('UG'), 'IMPRESARIO', 'Membership_Perf_Type'),0)
Select @memb_card_customer_length = Coalesce(dbo.FS_GET_DEFAULT_VALUE(dbo.FS_GET_PARAM_FROM_APPNAME('UG'), 'IMPRESARIO', 'NSCAN_MEMBER_CUSTOMER_LENGTH'),0)

create table #Memberships(customer_no	int,sli_no	int,cust_memb_no	int,trn_dt	datetime,camp_desc	varchar(30)
,appeal_desc	varchar(30),memb_desc	varchar(30),inception_dt	datetime,due_amt	money,paid_amt	money,goods_service_value	money
,order_no	int,source_no	int,benefactor_no	int,declined_ind	char(1),printed_ind	char(1),expr_dt	datetime,printed_dt	datetime
,created_by	varchar(8),create_dt	datetime,create_loc	varchar(16))

If @reprint_ind = 2--No Reprint
  Begin
	Insert into #Memberships (customer_no, sli_no, cust_memb_no, trn_dt, camp_desc, appeal_desc,
		memb_desc, inception_dt, due_amt, paid_amt, goods_service_value, order_no, source_no, benefactor_no, declined_ind,
		printed_ind, expr_dt)
	
	--Memberships via Memberships as Products (Household Memberships):
	Select	cm.customer_no,
			sli.sli_no,
			cm.cust_memb_no,
			sli.create_dt,
			c.description,
			a.description,
			ml.description,
			cm.inception_dt,
			sli.due_amt,
			sli.paid_amt,
			ml.goods_services_value,
			sli.order_no,
			o.source_no,
			ISNULL(cm.ben_provider,0),
			cm.declined_ind,
			'Y',
			cm.expr_dt
	From	T_SUB_LINEITEM As sli WITH (NOLOCK)
	Left Join T_SPECIAL_REQ As sr WITH (NOLOCK) On sli.li_seq_no = sr.li_seq_no
	Join	T_PERF AS p WITH (NOLOCK) On sli.perf_no = p.perf_no
	Join	LX_SLI_MEMB As sm WITH (NOLOCK) On sli.sli_no = sm.sli_no
	Join	TX_CUST_MEMBERSHIP As cm WITH (NOLOCK) On sm.cust_memb_no = cm.cust_memb_no
	Join	T_ORDER As o WITH (NOLOCK) On sli.order_no = o.order_no
	Join	TX_APPEAL_MEDIA_TYPE As amt On o.source_no = amt.source_no
	Join	T_CAMPAIGN As c On p.campaign_no = c.campaign_no
	Join	T_APPEAL As a On amt.appeal_no = a.appeal_no
	Join	T_MEMB_LEVEL As ml On cm.memb_level = ml.memb_level
	Join	TR_Batch_Period AS bp ON cm.expr_dt BETWEEN bp.start_dt AND bp.end_dt
	Where	p.perf_type in (@membership_perf_type)
	And		sli.create_dt Between @start_dt And @end_dt
	And		(Isnull(@customer_no,0) = 0 Or cm.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where o.customer_no = lc.customer_no And lc.list_no = @list_no))
	And		sli.sli_status in (2,3,6,12) -- Any sli statuses that are seated and/or paid
	And Not Exists (Select 1 From dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As amp -- For MemAsProd only, do not print card if a card 
								Where cm.customer_no = amp.customer_no			-- for the same customer and 
								and cm.cust_memb_no = amp.cust_memb_no			-- for the same membership has already been printed
								and amp.memb_desc = ml.description				-- unless it was for a different level (to accommodate mid-year upgrades) 
								)--2/16/2017 BG(TSR)
	And		(isnull(@memb_org_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
	And		(isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
	And		(
				(isnull(@gift_memb,3) in(2) and (isnull(cm.ben_provider,0) = 0 and isnull(sr.category,0) = 0)) --not gift
				or
				(isnull(@gift_memb,3) in(1) and (isnull(cm.ben_provider,0) > 0 or isnull(sr.category,0) > 0)) --is gift
				or
				(isnull(@gift_memb,3) in(3)) --all
			)

/*Two previous versions here:
--	And Not Exists (Select 1 From dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As amp Where sli.sli_no = amp.sli_no)
	And Not Exists (Select 1 From dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As amp 
								Join TR_Batch_Period mbp on amp.expr_dt BETWEEN mbp.start_dt and mbp.end_dt 
								Where cm.customer_no = amp.customer_no 
								and bp.fyear = mbp.fyear)--12/1/2016 BG(TSR)


*/

  Union All

	--Memberships via contributions (Annual Fund/Advancement Memberships):
	Select mem.customer_no,
			0,
			mem.cust_memb_no,
			MAX(c.cont_dt),
			camp.description,
			MAX(app.description),
			ml.description,
			mem.inception_dt,
			SUM(c.cont_amt),
			SUM(c.recd_amt),
			ml.goods_services_value,
			MAX(c.ref_no),
			MAX(c.source_no),
			ISNULL(mem.ben_provider,0),
			mem.declined_ind,
			'Y',
			mem.expr_dt
	From	T_CONTRIBUTION AS c
	Join	T_TRANSACTION AS t ON c.ref_no = t.ref_no AND t.trn_type in (1,2) --Gifts and pledges only
	Join	TX_CONT_MEMB AS cm ON c.ref_no = cm.cont_ref_no
	Join	TX_CUST_MEMBERSHIP AS mem ON cm.cust_memb_no = mem.cust_memb_no
	Join	T_CAMPAIGN AS camp ON /*c.campaign_no = camp.campaign_no*/ mem.campaign_no = camp.campaign_no
	Join	T_APPEAL AS app ON c.appeal_no = app.appeal_no
	Join	T_MEMB_LEVEL AS ml ON mem.memb_org_no = ml.memb_org_no AND mem.memb_level = ml.memb_level
	Join	TR_Batch_Period AS bp ON mem.expr_dt BETWEEN bp.start_dt AND bp.end_dt
	Where	c.cont_dt Between @start_dt And @end_dt
	And		(Isnull(@customer_no,0) = 0 Or mem.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where mem.customer_no = lc.customer_no And lc.list_no = @list_no))
	And Not Exists (Select 1 From dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As amp 
								join TX_CUST_MEMBERSHIP z on amp.cust_memb_no = z.cust_memb_no
								Join TR_Batch_Period mbp on z.expr_dt BETWEEN mbp.start_dt and mbp.end_dt 
								--Join TR_Batch_Period mbp on amp.expr_dt BETWEEN mbp.start_dt and mbp.end_dt --4/10/2018 BG(Tess) since exp_dt can change, now looking at the membership
								Where mem.customer_no = amp.customer_no 
								and bp.fyear = mbp.fyear--12/1/2016 BG(TSR)
								and REPLACE(mem.memb_org_no,24,5) = REPLACE(z.memb_org_no,24,5) --only compare same-org membs. 24 and 5 are both AF, should be treated as same org here
								)
--	And Not Exists (Select 1 From dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As amp Where c.ref_no = amp.order_no)
	And		(Isnull(@memb_org_str,'') = '' OR Charindex(','+Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
	And		(isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
	And		(
				(isnull(@gift_memb,3) in(2) and isnull(mem.ben_provider,0) = 0) --not gift
				or
				(isnull(@gift_memb,3) in(1) and isnull(mem.ben_provider,0) > 0) --is gift
				or
				(isnull(@gift_memb,3) in(3)) --all
			)
	And		mem.current_status in(2,3) --Active, Pending --8/29/2017
	group by mem.customer_no, mem.cust_memb_no, camp.description, ml.description, mem.inception_dt, ml.goods_services_value, ISNULL(mem.ben_provider,0), mem.declined_ind, mem.expr_dt
  
	Union All

	--Memberships manually added to constituent records:
	Select	mem.customer_no,
			0,
			mem.cust_memb_no,
			mem.create_dt,
			camp.description,
			'',
			ml.description,
			mem.inception_dt,
			mem.memb_amt,
			mem.memb_amt,
			ml.goods_services_value,
			0,
			0,
			ISNULL(mem.ben_provider,0),
			mem.declined_ind,
			'Y',
			mem.expr_dt
	From	TX_CUST_MEMBERSHIP AS mem
	Join	T_CAMPAIGN AS camp ON mem.campaign_no = camp.campaign_no
	Join	T_MEMB_LEVEL AS ml ON mem.memb_org_no = ml.memb_org_no AND mem.memb_level = ml.memb_level
	Join	TR_Batch_Period AS bp ON mem.expr_dt BETWEEN bp.start_dt AND bp.end_dt
	Where	mem.create_dt Between @start_dt And @end_dt
	And Not Exists (Select 1 From TX_CONT_MEMB As cont Where mem.cust_memb_no = cont.cust_memb_no)
	And Not Exists (Select 1 From LX_SLI_MEMB As smem Where mem.cust_memb_no = smem.cust_memb_no)
	And		(Isnull(@customer_no,0) = 0 Or mem.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where mem.customer_no = lc.customer_no And lc.list_no = @list_no))
	And Not Exists (Select 1 From dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As amp 
								join TX_CUST_MEMBERSHIP z on amp.cust_memb_no = z.cust_memb_no
								Join TR_Batch_Period mbp on z.expr_dt BETWEEN mbp.start_dt and mbp.end_dt 
								--Join TR_Batch_Period mbp on amp.expr_dt BETWEEN mbp.start_dt and mbp.end_dt --4/10/2018 BG(Tess) since exp_dt can change, now looking at the membership
								Where mem.customer_no = amp.customer_no 
								and bp.fyear = mbp.fyear--12/1/2016 BG(TSR)
								and REPLACE(mem.memb_org_no,24,5) = REPLACE(z.memb_org_no,24,5) --only compare same-org membs. 24 and 5 are both AF, should be treated as same org here
								)
--	And Not Exists (Select 1 From dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As amp Where mem.cust_memb_no = amp.cust_memb_no)
	And		(Isnull(@memb_org_str,'') = '' OR Charindex(','+Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
	And		(isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
	And		mem.create_dt >= '2016-05-18' --Added this here so that only memberships created post-conversion will be picked up.
	And		(
				(isnull(@gift_memb,3) in(2) and isnull(mem.ben_provider,0) = 0) --not gift
				or
				(isnull(@gift_memb,3) in(1) and isnull(mem.ben_provider,0) > 0) --is gift
				or
				(isnull(@gift_memb,3) in(3)) --all
			)

  End

if @preview = 'U' --Update 
	BEGIN
	insert into LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS (customer_no, sli_no, cust_memb_no, trn_dt, camp_desc, appeal_desc,
		memb_desc, inception_dt, due_amt, paid_amt, goods_service_value, order_no, source_no, benefactor_no, declined_ind,
		printed_ind, expr_dt)
	select customer_no, sli_no, cust_memb_no, trn_dt, camp_desc, appeal_desc,
		memb_desc, inception_dt, due_amt, paid_amt, goods_service_value, order_no, source_no, benefactor_no, declined_ind,
		printed_ind, expr_dt 
	from #Memberships
	END

  -- temp table for special request notes, including an indicator to help identify parse point. TSR 09/13/2016
  create table #special_Req_text (li_seq_no int, notes varchar(255), category int, message_ind varchar(1), send_memb_ind varchar(1))
  insert #special_Req_text
  select li_seq_no, notes, category,
	 message_ind = case when notes like '%message:%' then 'Y' else 'N' end,
	 send_memb_ind = case when notes like '%send membership%' then 'Y' else 'N' end
	 from t_special_req


Create table	#letters (
	memb_ack_no int null,
	customer_no int null,
	benefactor_no int null,
	order_no int null,
	sli_no int null,
	cust_memb_no int null,
	esal1_desc varchar(55) null,
	esal2_desc varchar(55) null,
	current_trn_amt money null,
	current_trn_dt datetime null,
	goods_service_value money null,
	gift_memb_role_id int null,
	gift_memb_role varchar(100) null,
	gift_memb_to varchar(55) null,
	gift_memb_from varchar(55) null,
	gift_memb_msg varchar(255) null,
	member_street1 varchar(64) null,
	member_street2 varchar(64) null,
	member_street3 varchar(64) null,
	member_city varchar(30) null,
	member_state varchar(20) null,
	--member_postal_code varchar(10) null,	--TSR 09/19/2016
	member_postal_code varchar(11) null,
	member_country varchar(30) null,
	giver_street1 varchar(64) null,
	giver_street2 varchar(64) null,
	giver_street3 varchar(64) null,
	giver_city varchar(30) null,
	giver_state varchar(20) null,
	--giver_postal_code varchar(10) null,	-- TSR 09/19/2016
	giver_postal_code varchar(11) null,
	giver_country varchar(30) null,
	source_no int null,
	one_step_flag varchar(30) null,
	memb_desc varchar(30) null,
	expr_dt datetime null,
	a1_name varchar(55) null,
	a2_name varchar(55) null,
	household_customer_no int null,
	card_number varchar(30) null,
	pin char(4) null,
	mag_stripe1 varchar(255) null,
	mag_stripe2 varchar(255) null,
	is_gift int )		--10/25/2016 TSR 1= is gift, 2= not a gift


Create clustered index temp1 ON #LETTERS(order_no)
Create index temp2 ON #LETTERS(customer_no)
Create index temp3 ON #LETTERS(sli_no)


If @reprint_ind = 2 and @preview = 'R' --No reprint, Review Only
  Begin
	--Insert memberships as products base data into #letters
	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
	Select	0,
			map.customer_no,
			map.benefactor_no,
			map.order_no,
			map.sli_no,
			map.cust_memb_no,
			map.due_amt,
			Convert(datetime,Convert(date,map.trn_dt)),
			map.goods_service_value,
			Isnull(sr.category,0),
			Isnull(lic.description,''),
			--Isnull(sr.notes,''),
			case when sp.message_ind = 'y' and send_memb_ind = 'y' then isnull(substring(sp.notes,(charindex('Message:',sp.notes)+8),((charindex('Send membership',sp.notes))-(charindex('Message:',sp.notes))-8)),'')
				 when sp.message_ind = 'y' and send_memb_ind = 'N' then isnull(substring(sp.notes,(charindex('Message:',sp.notes)+8),255),'')
				 else isnull(sr.notes,'') end as notes,		--09/13/2016 TSR
			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
			a2.street1,
			a2.street2,
			a2.street3,
			a2.city,
			a2.state,
			a2.postal_code,
			c2.description,
			map.source_no,
			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
			map.memb_desc,
			Convert(datetime,Convert(date,map.expr_dt)),
			case when (isnull(map.benefactor_no,0) >0 or isnull(sp.category,0)>0) then 1 --updated from below 12/18/2018 TSR(bg)
				 else 2 end 
			--case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
			--	 when (isnull(map.benefactor_no,0) >0 or isnull(sp.category,0)>0) then 1 end 
	From	#Memberships As map
	Join	dbo.T_SUB_LINEITEM As sli WITH (NOLOCK) On map.sli_no = sli.sli_no
	Left Join dbo.T_SPECIAL_REQ As sr WITH (NOLOCK) On sli.li_seq_no = sr.li_seq_no
	left join #special_Req_text sp on sp.li_seq_no = sr.li_seq_no		-- 09/13/2016 to facilitate parse
	Left Join dbo.TR_LINEITEM_CATEGORY As lic On sr.category = lic.id
	Left Join	dbo.T_ADDRESS As a1 WITH (NOLOCK) On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'  --left join added ASH 11/03/2016
	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id
	Left Join dbo.T_ADDRESS As a2 WITH (NOLOCK) On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'
	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id
	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
	Where	sli.create_dt Between @start_dt And @end_dt
	And		Isnull(map.printed_dt,'01/01/1900') = '01/01/1900'
	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
  End

if @reprint_ind = 2 and @preview = 'U' --Reprint No, Update Records
BEGIN 
	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
	Select	map.memb_ack_no,
			map.customer_no,
			map.benefactor_no,
			map.order_no,
			map.sli_no,
			map.cust_memb_no,
			map.due_amt,
			Convert(datetime,Convert(date,map.trn_dt)),
			map.goods_service_value,
			Isnull(sr.category,0),
			Isnull(lic.description,''),
			--Isnull(sr.notes,''),
			case when sp.message_ind = 'y' and send_memb_ind = 'y' then isnull(substring(sp.notes,(charindex('Message:',sp.notes)+8),((charindex('Send membership',sp.notes))-(charindex('Message:',sp.notes))-8)),'')
				 when sp.message_ind = 'y' and send_memb_ind = 'N' then isnull(substring(sp.notes,(charindex('Message:',sp.notes)+8),255),'')
				 else isnull(sr.notes,'') end as notes,		-- 09/13/2016 TSR
			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
			a2.street1,
			a2.street2,
			a2.street3,
			a2.city,
			a2.state,
			a2.postal_code,
			c2.description,
			map.source_no,
			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
			ml.description,  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
			Convert(datetime,Convert(date,cm.expr_dt)),  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
			case when (isnull(map.benefactor_no,0) >0 or isnull(sp.category,0)>0) then 1 --updated from below 12/18/2018 TSR(bg)
				 else 2 end 
			--case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
			--	 when (isnull(map.benefactor_no,0) >0 or isnull(sp.category,0)>0) then 1 end 
	From	LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As map
	Join	dbo.T_SUB_LINEITEM As sli On map.sli_no = sli.sli_no
	Join	dbo.LX_SLI_MEMB As sm On sli.sli_no = sm.sli_no
	Join	dbo.TX_CUST_MEMBERSHIP As cm ON sm.cust_memb_no = cm.cust_memb_no
	Join	dbo.T_MEMB_LEVEL As ml On cm.memb_org_no = ml.memb_org_no And cm.memb_level = ml.memb_level
	Left Join dbo.T_SPECIAL_REQ As sr On sli.li_seq_no = sr.li_seq_no
	left join #special_Req_text sp on sp.li_seq_no = sr.li_seq_no		-- 09/13/2016 TSR to facilitate parse
	Left Join dbo.TR_LINEITEM_CATEGORY As lic On sr.category = lic.id
	Left Join	dbo.T_ADDRESS As a1 On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.T_ADDRESS As a2 On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'
	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id
	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
	JOIN #Memberships x on map.customer_no = x.customer_no and map.cust_memb_no = x.cust_memb_no -- BG 7/20/2017
	Where	sli.create_dt Between @start_dt And @end_dt
	And		Isnull(map.printed_dt,'01/01/1900') = '01/01/1900'
	And		(Isnull(@memb_org_str,'') = '' OR charindex(',' +Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
	And		(Isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))

END
--if @reprint_ind = 1 and @preview = 'Y'
--  Begin
--	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
--		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
--		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
--		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
--	Select	0,
--			map.customer_no,
--			map.benefactor_no,
--			map.order_no,
--			map.sli_no,
--			map.cust_memb_no,
--			map.due_amt,
--			Convert(datetime,Convert(date,map.trn_dt)),
--			map.goods_service_value,
--			Isnull(sr.category,0),
--			Isnull(lic.description,''),
--			--Isnull(sr.notes,''),
--			case when sp.message_ind = 'y' and send_memb_ind = 'y' then isnull(substring(sp.notes,(charindex('Message:',sp.notes)+8),((charindex('Send membership',sp.notes))-(charindex('Message:',sp.notes))-8)),'')
--				 when sp.message_ind = 'y' and send_memb_ind = 'N' then isnull(substring(sp.notes,(charindex('Message:',sp.notes)+8),255),'')
--				 else isnull(sr.notes,'') end as notes,		-- 09/13/2016 TSR
--			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
--			a2.street1,
--			a2.street2,
--			a2.street3,
--			a2.city,
--			a2.state,
--			a2.postal_code,
--			c2.description,
--			map.source_no,
--			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
--			ml.description,  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
--			Convert(datetime,Convert(date,cm.expr_dt)),  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
--			case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
--				 when (isnull(map.benefactor_no,0) >0 or isnull(sp.category,0)>0) then 1 end 
--	From	#Memberships As map
--	Join	dbo.T_SUB_LINEITEM As sli On map.sli_no = sli.sli_no
--	Join	dbo.LX_SLI_MEMB As sm On sli.sli_no = sm.sli_no
--	Join	dbo.TX_CUST_MEMBERSHIP As cm ON sm.cust_memb_no = cm.cust_memb_no
--	Join	dbo.T_MEMB_LEVEL As ml On cm.memb_org_no = ml.memb_org_no And cm.memb_level = ml.memb_level
--	Left Join dbo.T_SPECIAL_REQ As sr On sli.li_seq_no = sr.li_seq_no
--	left join #special_Req_text sp on sp.li_seq_no = sr.li_seq_no		-- 09/13/2016 TSR to facilitate parse
--	Left Join dbo.TR_LINEITEM_CATEGORY As lic On sr.category = lic.id
--	Left Join	dbo.T_ADDRESS As a1 On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
--	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id  --11/03/2016 ASH. Join turned into Left Join
--	Left Join dbo.T_ADDRESS As a2 On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'
--	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id
--	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
--	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
--				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
--				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
--	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
--	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
--	Where	map.printed_dt Between @start_dt And @end_dt
--	And		(Isnull(@memb_org_str,'') = '' OR charindex(',' +Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
--	And		(Isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
--	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
--	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
--  End
if @reprint_ind = 1 --and @preview = 'Y'
  Begin
	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
	Select	map.memb_ack_no,
			map.customer_no,
			map.benefactor_no,
			map.order_no,
			map.sli_no,
			map.cust_memb_no,
			map.due_amt,
			Convert(datetime,Convert(date,map.trn_dt)),
			map.goods_service_value,
			Isnull(sr.category,0),
			Isnull(lic.description,''),
			--Isnull(sr.notes,''),
			case when sp.message_ind = 'y' and send_memb_ind = 'y' then isnull(substring(sp.notes,(charindex('Message:',sp.notes)+8),((charindex('Send membership',sp.notes))-(charindex('Message:',sp.notes))-8)),'')
				 when sp.message_ind = 'y' and send_memb_ind = 'N' then isnull(substring(sp.notes,(charindex('Message:',sp.notes)+8),255),'')
				 else isnull(sr.notes,'') end as notes,		-- 09/13/2016 TSR
			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
			a2.street1,
			a2.street2,
			a2.street3,
			a2.city,
			a2.state,
			a2.postal_code,
			c2.description,
			map.source_no,
			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
			ml.description,  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
			Convert(datetime,Convert(date,cm.expr_dt)),  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
			case when (isnull(map.benefactor_no,0) >0 or isnull(sp.category,0)>0) then 1 --updated from below 12/18/2018 TSR(bg)
				 else 2 end 
			--case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
			--	 when (isnull(map.benefactor_no,0) >0 or isnull(sp.category,0)>0) then 1 end 
	From	LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As map
	Join	dbo.T_SUB_LINEITEM As sli On map.sli_no = sli.sli_no
	Join	dbo.LX_SLI_MEMB As sm On sli.sli_no = sm.sli_no
	Join	dbo.TX_CUST_MEMBERSHIP As cm ON sm.cust_memb_no = cm.cust_memb_no
	Join	dbo.T_MEMB_LEVEL As ml On cm.memb_org_no = ml.memb_org_no And cm.memb_level = ml.memb_level
	Left Join dbo.T_SPECIAL_REQ As sr On sli.li_seq_no = sr.li_seq_no
	left join #special_Req_text sp on sp.li_seq_no = sr.li_seq_no		-- 09/13/2016 TSR to facilitate parse
	Left Join dbo.TR_LINEITEM_CATEGORY As lic On sr.category = lic.id
	Left Join	dbo.T_ADDRESS As a1 On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.T_ADDRESS As a2 On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'
	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id
	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
	Where	map.printed_dt Between @start_dt And @end_dt
	And		(Isnull(@memb_org_str,'') = '' OR charindex(',' +Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
	And		(Isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
  End


If @reprint_ind = 2 and @preview = 'R' --No reprint, Review Only
  Begin
	--Insert annual fund base data into #letters
	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
	Select	0,
			map.customer_no,
			map.benefactor_no,
			map.order_no,
			map.sli_no,
			map.cust_memb_no,
			map.due_amt,
			Convert(datetime,Convert(date,map.trn_dt)),
			map.goods_service_value,
			0,
			'',
			'',
			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
			a2.street1,
			a2.street2,
			a2.street3,
			a2.city,
			a2.state,
			a2.postal_code,
			c2.description,
			map.source_no,
			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
			map.memb_desc,
			Convert(datetime,Convert(date,map.expr_dt)),
			case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
				 when isnull(map.benefactor_no,0) > 0 then 1 end 
	From	#Memberships As map
	Join	dbo.T_CONTRIBUTION AS c ON map.order_no = c.ref_no
	Left Join	dbo.T_ADDRESS As a1 WITH (NOLOCK) On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'
	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id
	Left Join dbo.T_ADDRESS As a2 WITH (NOLOCK) On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
	Where	c.cont_dt Between @start_dt And @end_dt
	And		Isnull(map.printed_dt,'01/01/1900') = '01/01/1900'
	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
  End
If @reprint_ind = 2 and @preview = 'U' --No reprint, Update Records
  Begin
	--Insert annual fund base data into #letters
	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
	Select	map.memb_ack_no,
			map.customer_no,
			map.benefactor_no,
			map.order_no,
			map.sli_no,
			map.cust_memb_no,
			map.due_amt,
			Convert(datetime,Convert(date,map.trn_dt)),
			map.goods_service_value,
			0,
			'',
			'',
			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
			a2.street1,
			a2.street2,
			a2.street3,
			a2.city,
			a2.state,
			a2.postal_code,
			c2.description,
			map.source_no,
			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
			map.memb_desc,
			Convert(datetime,Convert(date,map.expr_dt)),
			case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
				 when isnull(map.benefactor_no,0) > 0 then 1 end 
	From	LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As map
	Join	dbo.T_CONTRIBUTION AS c ON map.order_no = c.ref_no
	Left Join	dbo.T_ADDRESS As a1 WITH (NOLOCK) On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'
	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id
	Left Join dbo.T_ADDRESS As a2 WITH (NOLOCK) On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
	JOIN #Memberships x on map.customer_no = x.customer_no and map.cust_memb_no = x.cust_memb_no -- BG 7/20/2017
	Where	c.cont_dt Between @start_dt And @end_dt
	And		Isnull(map.printed_dt,'01/01/1900') = '01/01/1900'
	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
  End


--if @reprint_ind = 1 and @preview = 'Y'
--  Begin
--	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
--		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
--		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
--		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
--	Select	0,
--			map.customer_no,
--			map.benefactor_no,
--			map.order_no,
--			map.sli_no,
--			map.cust_memb_no,
--			map.due_amt,
--			Convert(datetime,Convert(date,map.trn_dt)),
--			map.goods_service_value,
--			0,
--			'',
--			'',
--			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
--			a2.street1,
--			a2.street2,
--			a2.street3,
--			a2.city,
--			a2.state,
--			a2.postal_code,
--			c2.description,
--			map.source_no,
--			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
--			ml.description,  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
--			Convert(datetime,Convert(date,cm.expr_dt)),  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
--			case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
--				 when isnull(map.benefactor_no,0) > 0 then 1 end 
--	From	#Memberships As map
--	Join	dbo.T_CONTRIBUTION AS c ON map.order_no = c.ref_no
--	Join	dbo.TX_CONT_MEMB AS com ON c.ref_no = com.cont_ref_no
--	Join	dbo.TX_CUST_MEMBERSHIP As cm ON com.cust_memb_no = cm.cust_memb_no
--	Join	dbo.T_MEMB_LEVEL As ml On cm.memb_org_no = ml.memb_org_no And cm.memb_level = ml.memb_level
--	Left Join	dbo.T_ADDRESS As a1 On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
--	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id  --11/03/2016 ASH. Join turned into Left Join
--	Left Join dbo.T_ADDRESS As a2 On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'
--	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id
--	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
--	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
--				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
--				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
--	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
--	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
--	Where	map.printed_dt Between @start_dt And @end_dt
--	And		(Isnull(@memb_org_str,'') = '' OR charindex(',' +Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
--	And		(Isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
--	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
--	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
--   End
if @reprint_ind = 1 --and @preview = 'U'
  Begin
	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
	Select	map.memb_ack_no,
			map.customer_no,
			map.benefactor_no,
			map.order_no,
			map.sli_no,
			map.cust_memb_no,
			map.due_amt,
			Convert(datetime,Convert(date,map.trn_dt)),
			map.goods_service_value,
			0,
			'',
			'',
			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
			a2.street1,
			a2.street2,
			a2.street3,
			a2.city,
			a2.state,
			a2.postal_code,
			c2.description,
			map.source_no,
			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
			ml.description,  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
			Convert(datetime,Convert(date,cm.expr_dt)),  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
			case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
				 when isnull(map.benefactor_no,0) > 0 then 1 end 
	From	LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As map
	Join	dbo.T_CONTRIBUTION AS c ON map.order_no = c.ref_no
	Join	dbo.TX_CONT_MEMB AS com ON c.ref_no = com.cont_ref_no
	Join	dbo.TX_CUST_MEMBERSHIP As cm ON com.cust_memb_no = cm.cust_memb_no
	Join	dbo.T_MEMB_LEVEL As ml On cm.memb_org_no = ml.memb_org_no And cm.memb_level = ml.memb_level
	Left Join	dbo.T_ADDRESS As a1 On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.T_ADDRESS As a2 On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'
	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id
	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
	Where	map.printed_dt Between @start_dt And @end_dt
	And		(Isnull(@memb_org_str,'') = '' OR charindex(',' +Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
	And		(Isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
   End



If @reprint_ind = 2 and @preview = 'R' --No reprint, Review Only
  Begin
	--Insert manually added memberships into #letters
	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
	Select	0,
			map.customer_no,
			map.benefactor_no,
			map.order_no,
			map.sli_no,
			map.cust_memb_no,
			map.due_amt,
			Convert(datetime,Convert(date,map.trn_dt)),
			map.goods_service_value,
			0,
			'',
			'',
			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
			a2.street1,
			a2.street2,
			a2.street3,
			a2.city,
			a2.state,
			a2.postal_code,
			c2.description,
			map.source_no,
			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
			map.memb_desc,
			Convert(datetime,Convert(date,map.expr_dt)),
			case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
				 when isnull(map.benefactor_no,0) > 0 then 1 end 
	From	#Memberships As map
	Join	dbo.TX_CUST_MEMBERSHIP AS mem ON map.cust_memb_no = mem.cust_memb_no
	Left Join	dbo.T_ADDRESS As a1 WITH (NOLOCK) On map.customer_no = a1.customer_no And a1.primary_ind = 'Y' --11/03/2016 ASH. Join turned into Left Join
	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.T_ADDRESS As a2 WITH (NOLOCK) On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
	Where	mem.create_dt Between @start_dt And @end_dt
	And		Not Exists (Select 1 From dbo.TX_CONT_MEMB As cont Where mem.cust_memb_no = cont.cust_memb_no)
	And		Not Exists (Select 1 From dbo.LX_SLI_MEMB As slim Where mem.cust_memb_no = slim.cust_memb_no)
	And		Isnull(map.printed_dt,'01/01/1900') = '01/01/1900'
	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
  End
If @reprint_ind = 2 and @preview = 'U' --No reprint, Update Records
  Begin
	--Insert manually added memberships into #letters
	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
	Select	map.memb_ack_no,
			map.customer_no,
			map.benefactor_no,
			map.order_no,
			map.sli_no,
			map.cust_memb_no,
			map.due_amt,
			Convert(datetime,Convert(date,map.trn_dt)),
			map.goods_service_value,
			0,
			'',
			'',
			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
			a2.street1,
			a2.street2,
			a2.street3,
			a2.city,
			a2.state,
			a2.postal_code,
			c2.description,
			map.source_no,
			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
			map.memb_desc,
			Convert(datetime,Convert(date,map.expr_dt)),
			case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
				 when isnull(map.benefactor_no,0) > 0 then 1 end 
	From	LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As map
	Join	dbo.TX_CUST_MEMBERSHIP AS mem ON map.cust_memb_no = mem.cust_memb_no
	Left Join	dbo.T_ADDRESS As a1 WITH (NOLOCK) On map.customer_no = a1.customer_no And a1.primary_ind = 'Y' --11/03/2016 ASH. Join turned into Left Join
	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.T_ADDRESS As a2 WITH (NOLOCK) On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
	JOIN #Memberships x on map.customer_no = x.customer_no and map.cust_memb_no = x.cust_memb_no -- BG 7/20/2017
	Where	mem.create_dt Between @start_dt And @end_dt
	And		Not Exists (Select 1 From dbo.TX_CONT_MEMB As cont Where mem.cust_memb_no = cont.cust_memb_no)
	And		Not Exists (Select 1 From dbo.LX_SLI_MEMB As slim Where mem.cust_memb_no = slim.cust_memb_no)
	And		Isnull(map.printed_dt,'01/01/1900') = '01/01/1900'
	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
  End



--if @reprint_ind = 1 and @preview = 'Y' 
--  Begin
--	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
--		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
--		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
--		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
--	Select	0,
--			map.customer_no,
--			map.benefactor_no,
--			map.order_no,
--			map.sli_no,
--			map.cust_memb_no,
--			map.due_amt,
--			Convert(datetime,Convert(date,map.trn_dt)),
--			map.goods_service_value,
--			0,
--			'',
--			'',
--			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
--			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
--			a2.street1,
--			a2.street2,
--			a2.street3,
--			a2.city,
--			a2.state,
--			a2.postal_code,
--			c2.description,
--			map.source_no,
--			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
--			ml.description,  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
--			Convert(datetime,Convert(date,cm.expr_dt)),  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
--			case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
--				 when isnull(map.benefactor_no,0) > 0 then 1 end 
--	From	#Memberships As map
--	Join	dbo.TX_CUST_MEMBERSHIP As cm ON map.cust_memb_no = cm.cust_memb_no
--	Join	dbo.T_MEMB_LEVEL As ml On cm.memb_org_no = ml.memb_org_no And cm.memb_level = ml.memb_level
--	Left Join	dbo.T_ADDRESS As a1 On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
--	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id  --11/03/2016 ASH. Join turned into Left Join
--	Left Join dbo.T_ADDRESS As a2 On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
--	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id  --11/03/2016 ASH. Join turned into Left Join
--	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
--	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
--				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
--				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
--	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
--	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
--	Where	map.printed_dt Between @start_dt And @end_dt
--	And	Not Exists (Select 1 From dbo.TX_CONT_MEMB As cont Where cm.cust_memb_no = cont.cust_memb_no)
--	And	Not Exists (Select 1 From dbo.LX_SLI_MEMB As slim Where cm.cust_memb_no = slim.cust_memb_no)
--	And		(Isnull(@memb_org_str,'') = '' OR charindex(',' +Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
--	And		(Isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
--	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
--	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
--   End
if @reprint_ind = 1 --and @preview = 'U' 
  Begin
	Insert into #letters (memb_ack_no, customer_no, benefactor_no, order_no, sli_no, cust_memb_no, current_trn_amt, current_trn_dt,
		goods_service_value, gift_memb_role_id, gift_memb_role, gift_memb_msg, member_street1, member_street2, member_street3, 
		member_city, member_state, member_postal_code, member_country, giver_street1, giver_street2, giver_street3,
		giver_city, giver_state, giver_postal_code, giver_country, source_no, one_step_flag, memb_desc, expr_dt, is_gift)
	Select	map.memb_ack_no,
			map.customer_no,
			map.benefactor_no,
			map.order_no,
			map.sli_no,
			map.cust_memb_no,
			map.due_amt,
			Convert(datetime,Convert(date,map.trn_dt)),
			map.goods_service_value,
			0,
			'',
			'',
			Case When @affiliation_type > 0 Then a3.street1 Else a1.street1 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street2 Else a1.street2 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.street3 Else a1.street3 End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.city Else a1.city End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.state Else a1.state End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then a3.postal_code Else a1.postal_code End,  -- ASH 09/20/2016
			Case When @affiliation_type > 0 Then c3.description Else c1.description End,  -- ASH 09/20/2016
			a2.street1,
			a2.street2,
			a2.street3,
			a2.city,
			a2.state,
			a2.postal_code,
			c2.description,
			map.source_no,
			Case When Isnull(cc.constituency_no,0) > 0 And Getdate() Between Isnull(cc.start_dt,'1900-01-01') And Isnull(cc.end_dt,'2999-12-31') Then 'Y' Else 'N' End,
			ml.description,  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
			Convert(datetime,Convert(date,cm.expr_dt)),  -- For reprints, pull from the current membership information, in case manual adjustments have been made.
			case when isnull(map.benefactor_no,0) = 0  then 2	--- 11/02/2016 TSR
				 when isnull(map.benefactor_no,0) > 0 then 1 end 
	From	LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As map
	Join	dbo.TX_CUST_MEMBERSHIP As cm ON map.cust_memb_no = cm.cust_memb_no
	Join	dbo.T_MEMB_LEVEL As ml On cm.memb_org_no = ml.memb_org_no And cm.memb_level = ml.memb_level
	Left Join	dbo.T_ADDRESS As a1 On map.customer_no = a1.customer_no And a1.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
	Left Join	dbo.TR_COUNTRY As c1 On a1.country = c1.id  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.T_ADDRESS As a2 On map.benefactor_no = a2.customer_no And a2.primary_ind = 'Y'  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TR_COUNTRY As c2 On a2.country = c2.id  --11/03/2016 ASH. Join turned into Left Join
	Left Join dbo.TX_CONST_CUST As cc On map.customer_no = cc.customer_no And cc.constituency = @one_step_constituency
	Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On map.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type 
				And (Isnull(af.end_dt,'2999-12-31') > Getdate()
				And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016
	Left Join dbo.T_ADDRESS As a3 WITH (NOLOCK) On af.group_customer_no = a3.customer_no And a3.primary_ind = 'Y'  -- ASH 09/20/2016
	Left Join dbo.TR_COUNTRY As c3 On a3.country = c3.id  -- ASH 09/20/2016
	Where	map.printed_dt Between @start_dt And @end_dt
	And	Not Exists (Select 1 From dbo.TX_CONT_MEMB As cont Where cm.cust_memb_no = cont.cust_memb_no)
	And	Not Exists (Select 1 From dbo.LX_SLI_MEMB As slim Where cm.cust_memb_no = slim.cust_memb_no)
	And		(Isnull(@memb_org_str,'') = '' OR charindex(',' +Convert(varchar,ml.memb_org_no)+',',','+@memb_org_str+',') > 0)
	And		(Isnull(@memb_level_str,'') = '' OR charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
	And		(Isnull(@customer_no,0) = 0 Or map.customer_no = @customer_no)
	And		(Isnull(@list_no,0) = 0 Or Exists (Select 1 From t_list_contents As lc Where map.customer_no = lc.customer_no And lc.list_no = @list_no))
   End

--If this is a gift membership, update gift membership to and gift membership from fields 
Update l
Set	l.gift_memb_to = Coalesce(cs2.esal1_desc,cs.esal1_desc)
From #letters As l
Join dbo.TX_CUST_MEMBERSHIP As cm WITH (NOLOCK) On l.cust_memb_no = cm.cust_memb_no
Join dbo.TX_CUST_SAL As cs WITH (NOLOCK) On l.customer_no = cs.customer_no And cs.default_ind = 'Y'
Left Join dbo.tx_cust_sal As cs2 On l.customer_no = cs2.customer_no And cs2.signor = Isnull(@signor,0)
Where Isnull(l.gift_memb_role_id,0) > 0

Update l
Set l.gift_memb_from = Coalesce(cs2.esal1_desc,cs.esal1_desc)
From #letters As l
Join dbo.TX_CUST_MEMBERSHIP As cm WITH (NOLOCK) On l.cust_memb_no = cm.cust_memb_no
Join dbo.TX_CUST_SAL As cs WITH (NOLOCK) On cm.ben_provider = cs.customer_no And cs.default_ind = 'Y'
Left Join dbo.TX_CUST_SAL As cs2 On cm.ben_provider = cs2.customer_no And cs2.signor = Isnull(@signor,0)
Where Isnull(l.gift_memb_role_id,0) > 0

--Update esal1_desc and esal2_desc with the name of the person to whom the letter should be addressed.
Update l
--Set l.esal1_desc = Case			-- 10/25/2016 TSR to always return recipient for esal1_desc
--				   When l.gift_memb_role_id = @send_to_giver Then Coalesce(cs4.esal1_desc,cs3.esal1_desc) 
--				   Else Coalesce(cs2.esal1_desc,cs1.esal1_desc) End,
Set l.esal1_desc = Coalesce(cs2.esal1_desc,cs1.esal1_desc),
	l.esal2_desc = Case
				   When l.gift_memb_role_id = @send_to_giver Then Coalesce(cs4.esal2_desc,cs3.esal2_desc)
				   When @affiliation_type > 0 Then cs5.esal1_desc  -- ASH 09/20/2016
				   Else Coalesce(cs2.esal2_desc,cs1.esal2_desc) End
From #letters As l
Join dbo.TX_CUST_MEMBERSHIP As cm WITH (NOLOCK) On l.cust_memb_no = cm.cust_memb_no
Join dbo.TX_CUST_SAL As cs1 WITH (NOLOCK) On cm.customer_no = cs1.customer_no And cs1.default_ind = 'Y'
Left Join dbo.TX_CUST_SAL As cs2 WITH (NOLOCK) On cm.customer_no = cs2.customer_no And cs2.signor = @signor
Left Join dbo.TX_CUST_SAL As cs3 WITH (NOLOCK) On cm.ben_provider = cs3.customer_no And cs3.default_ind = 'Y'
Left Join dbo.TX_CUST_SAL As cs4 WITH (NOLOCK) On cm.ben_provider = cs4.customer_no And cs4.signor = @signor
Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On cm.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type AND (Isnull(af.end_dt,'2999-12-31') > Getdate() And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016 w/ updates by MP 10/2/18 to add Affilitation Type
Left Join dbo.TX_CUST_SAL As cs5 WITH (NOLOCK) On af.group_customer_no	= cs5.customer_no And cs5.default_ind = 'Y'  -- ASH 09/20/2016


--Update A1/A2 flag.
Update l
Set l.a1_name = Coalesce(kw1.key_value,cs2.esal1_desc,cs.esal1_desc),
	l.a2_name = Coalesce(kw2.key_value,cs3.esal1_desc)
From #letters As l
Join dbo.TX_CUST_SAL AS cs WITH (NOLOCK) On l.customer_no = cs.customer_no And cs.default_ind = 'Y'
Left Join dbo.T_AFFILIATION As a1 WITH (NOLOCK) On l.customer_no = a1.group_customer_no And a1.primary_ind = 'Y' And a1.name_ind = -1
Left Join dbo.T_AFFILIATION As a2 WITH (NOLOCK) On l.customer_no = a2.group_customer_no And a2.primary_ind = 'Y' And a2.name_ind = -2
Left Join dbo.TX_CUST_SAL As cs2 WITH (NOLOCK) On a1.individual_customer_no = cs2.customer_no And cs2.default_ind = 'Y'
Left Join dbo.TX_CUST_SAL As cs3 WITH (NOLOCK) On a2.individual_customer_no = cs3.customer_no And cs3.default_ind = 'Y'
Left Join dbo.TX_CUST_KEYWORD As kw1 WITH (NOLOCK) On a1.individual_customer_no = kw1.customer_no And kw1.keyword_no in (@card_name_override_kw)
Left Join dbo.TX_CUST_KEYWORD As kw2 WITH (NOLOCK) On a2.individual_customer_no = kw2.customer_no And kw2.keyword_no in (@card_name_override_kw)

--Update Household Customer No
Update l
Set l.household_customer_no = pg.expanded_customer_no
From #letters As l
Join dbo.V_CUSTOMER_WITH_PRIMARY_GROUP As pg WITH (NOLOCK) On l.customer_no = pg.customer_no


create table #Member_Cards(customer_no	int,cust_memb_no	int,pin	char(4),printed_dt	datetime
,sli_no	int,inactive	char(1))

--If a new pin is needed, generate that now.
If (@reprint_ind = 2 Or @new_pin_ind = 1) -- Not a reprint or specifically requested new pins with reprint
  Begin
	if @preview = 'U' 
	begin
	Update mc
	Set mc.inactive = 'Y'
	From dbo.LT_MOS_MEMBER_CARDS As mc
	Join #letters As l On mc.cust_memb_no = l.cust_memb_no
	end
		
	Declare @current_cust_memb_no int
	Declare @pins Table (cust_memb_no int not null, pin varchar(4) null)
	
	Insert into @pins (cust_memb_no)
	Select Distinct cust_memb_no
	From	#letters

	Select @current_cust_memb_no = min(cust_memb_no) From @pins

	While Isnull(@current_cust_memb_no,0) <> 0
	  Begin
	    Update	@pins
		Set		pin = case when @preview = 'U' then Right('0000' + Convert(Varchar,Convert(Decimal(4,0),Rand()*10000)),4) else 'NEW*' end
		Where	cust_memb_no = @current_cust_memb_no 

		Insert into #Member_Cards (customer_no, cust_memb_no, pin, printed_dt, inactive)
		Select	l.customer_no,
				l.cust_memb_no,
				p.pin,
				getdate(),
				'N'
		From	#letters As l
		Join	@pins As p On l.cust_memb_no = p.cust_memb_no And p.cust_memb_no = @current_cust_memb_no

		Delete From @pins Where cust_memb_no = @current_cust_memb_no

		Select @current_cust_memb_no = min(cust_memb_no) From @pins
	  End
  End

if @preview = 'U' --Update
BEGIN
		Insert into dbo.LT_MOS_MEMBER_CARDS (customer_no, cust_memb_no, pin, printed_dt, sli_no, inactive)
		select * from #Member_Cards
END	

--Update #letters with card number, pin, and mag stripe info
Update l
Set l.card_number = @memb_card_prefix + Right('0000000000'+Convert(Varchar,mc.customer_no),@memb_card_customer_length-Len(@memb_card_prefix))+'-'+Convert(varchar,mc.pin),
	l.pin = Right('0000'+Convert(varchar,mc.pin),4),
	l.mag_stripe1 = CASE WHEN ISNULL(c2.lname,'') <> '' THEN 'B639647' + RIGHT('00000000'+CONVERT(VARCHAR,l.customer_no),8)
							+ mc.pin + '0^' + UPPER(ISNULL(c2.lname,'')) + '/' + UPPER(ISNULL(c2.fname,'')) +'^'
							+ SUBSTRING(REPLACE(CONVERT(VARCHAR,l.expr_dt,101),'/',''),1,4) + '101'
							+ CONVERT(varchar,GETDATE(),112)
						WHEN ISNULL(c3.lname,'') <> '' THEN 'B639647' + RIGHT('00000000'+CONVERT(VARCHAR,l.customer_no),8)
							+ mc.pin + '0^' + UPPER(ISNULL(c3.lname,'')) + '/' + UPPER(ISNULL(c3.fname,'')) +'^'
							+ SUBSTRING(REPLACE(CONVERT(VARCHAR,l.expr_dt,101),'/',''),1,4) + '101'
							+ CONVERT(varchar,GETDATE(),112)
						ELSE 'B639647' + RIGHT('00000000'+CONVERT(VARCHAR,l.customer_no),8)
							+ mc.pin + '0^' + UPPER(ISNULL(c.lname,'')) + '/' + UPPER(ISNULL(c.fname,'')) +'^'
							+ SUBSTRING(REPLACE(CONVERT(VARCHAR,l.expr_dt,101),'/',''),1,4) + '101'
							+ CONVERT(varchar,GETDATE(),112)
						END,
	l.mag_stripe2 = '639647' + RIGHT('00000000'+CONVERT(VARCHAR,l.customer_no),8) + mc.pin +'0' + '='
							+ SUBSTRING(REPLACE(CONVERT(VARCHAR,l.expr_dt,101),'/',''),1,4) + '101'
							+ CONVERT(varchar,GETDATE(),112) + '='
From #letters As l
Join #Member_Cards As mc On l.customer_no = mc.customer_no And l.cust_memb_no = mc.cust_memb_no
Join dbo.T_CUSTOMER As c On l.customer_no = c.customer_no
Left Join dbo.T_AFFILIATION As af On mc.customer_no = af.group_customer_no And af.primary_ind = 'Y' And af.name_ind = -1
Left Join dbo.T_CUSTOMER As c2 On af.individual_customer_no = c2.customer_no
Left Join dbo.T_AFFILIATION As af2 ON mc.customer_no = af2.group_customer_no And af2.primary_ind = 'Y' And af2.name_ind = -2
Left Join dbo.T_CUSTOMER As c3 On af2.individual_customer_no = c3.customer_no
Where mc.inactive = 'N'


If -- @reprint_ind = 1 and --updated 7/19/2017 BG(Tess) we need to update this for both reprint Yes and No
@preview = 'U'
  Begin
    Update map
    Set map.printed_dt = getdate()
    From dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS AS map
	Join #letters As l On map.memb_ack_no = l.memb_ack_no
  End


--Format postal codes
UPDATE #letters
SET member_postal_code = SUBSTRING(member_postal_code,1,5) +'-' +SUBSTRING(member_postal_code,6,5)
WHERE LEN(member_postal_code) > 5 AND member_postal_code NOT LIKE '%[A-Z]%'

UPDATE #letters
SET giver_postal_code = SUBSTRING(giver_postal_code,1,5) +'-' +SUBSTRING(giver_postal_code,6,5)
WHERE LEN(giver_postal_code) > 5 AND giver_postal_code NOT LIKE '%[A-Z]%'

/*FINAL SELECT*/
	Select Distinct * 
	From #letters a

	Union All

	Select Distinct * 
	From #letters a
	Where Isnull(a.a2_name,'') <> ''
	Order by current_trn_dt, customer_no

/* -- removing the @gift_memb filter from the final select 
--this *should* be redundant because the filter is applied before 
--cards are processed. But if for any reason additional cards are processed (which would probably indicate a bug)
--we should return all the cards.

if @gift_memb = 3	-- for returning all membership records 10/25/2016 TSR
Begin
		--Select the final results. Pull the row twice if A2 is populated
	Select Distinct * 
	From #letters a

	Union All

	Select Distinct * 
	From #letters a
	Where Isnull(a.a2_name,'') <> ''
	Order by current_trn_dt, customer_no
End


if @gift_memb <> 3		-- for returning only gift or only non gift membership records 10/25/2016 TSR
Begin
		--Select the final results. Pull the row twice if A2 is populated
	Select Distinct * 
	From #letters a
	where a.is_gift = @gift_memb

	Union All

	Select Distinct * 
	From #letters a
	Where Isnull(a.a2_name,'') <> ''
	and a.is_gift = @gift_memb
	Order by current_trn_dt, customer_no
End
*/

