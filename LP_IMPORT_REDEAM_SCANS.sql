USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_IMPORT_REDEAM_SCANS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_IMPORT_REDEAM_SCANS] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_IMPORT_REDEAM_SCANS] TO impusers';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_IMPORT_REDEAM_SCANS] TO tessitura_app';
END;
GO

/*  This procedure assumes the most up to date information has already been uploaded into the attendance_redeam table
    in the admin database.  That file will be periodically updated by Finance.  It is not a problem for the data in
    that table to be processed if no changes have been made.  If there is nothing to update.  The procuedure checks for
    updated data and makes adjustments only if necessary.  */

ALTER PROCEDURE [dbo].[LP_IMPORT_REDEAM_SCANS]
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Note the start date so that it is easly to determine what records were added and what records were updated  
        Set to one minute ago just in case things happen instantly  */

        DECLARE @start_dt DATETIME = DATEADD(MINUTE,-1,GETDATE())

    /*  Redeam data is all pulled in as text and then converted.  
        Delete any record where the text in the date field is not a date  */

        DELETE FROM [admin].[dbo].[attendance_redeam]
        WHERE ISDATE(ISNULL([redeam_date],'')) = 0 

    /*  The Redeam numbers should not contain futre dates.  Excel file often has future dates 
        with zeros listed.  Delete those records.  They are not needed  */

        DELETE FROM [admin].[dbo].[attendance_redeam]
        WHERE CAST([redeam_date] AS DATE) > CAST(GETDATE() AS DATE)

    /*  On any remaining record, change the scan count to 0 where it is null  */

        UPDATE [admin].[dbo].[attendance_redeam]
        SET [redeam_number] = ISNULL([redeam_number],'0')
        WHERE redeam_number IS NULL

    /*  The first step is to insert any records into the LT_REDEAM_SCANS table that do not
        already exist.  The unique identifier on the table is redeam_dt.  There can only be
        one record for each date.  There is a unique index on the table to prevent duplicates  */

        INSERT INTO [impresario].[dbo].[LT_REDEAM_SCANS] ([redeam_dt], [redeam_count])
        SELECT CAST(tmp.[redeam_date] AS DATE),
               CAST(tmp.[redeam_number] AS INT)
        FROM [admin].[dbo].[attendance_redeam] AS tmp
             LEFT OUTER JOIN [impresario].[dbo].[LT_REDEAM_SCANS] AS liv ON CAST(liv.[redeam_dt] AS DATE) = CAST(tmp.[redeam_date] AS DATE)
        WHERE liv.[redeam_id] IS NULL

    /*  The second step is to update the existing records in LT_REDEAM_SCANS that have changed.  
        If the scan count has not changed, the record is not updated with the same value.  
        That way the last updated date is more accurate.  That becomes important with the next step.  */

        UPDATE liv
        SET liv.[redeam_count] = tmp.[redeam_number]
        FROM [admin].[dbo].[attendance_redeam] AS tmp
             LEFT OUTER JOIN [impresario].[dbo].[LT_REDEAM_SCANS] AS liv ON CAST(liv.[redeam_dt] AS DATE) = CAST(tmp.[redeam_date] AS DATE)
        WHERE liv.[redeam_count] <> tmp.[redeam_number]

    /*  Note: There is no third step to delete the records in LT_REDEAM_SCANS that are no longer there.  
        Once a Reseam record exists, it is not removed by the automated process.  If there is ever a reason that a
        Redeam record needs to be deleted from LT_REDEAM_SCANS, that has to be done manually.  */
        
    
    /*  Once the records have been processed, they need to be added to the ticket and visit count history
        The entire table has is checked for missing history records and any record that does not already exist is created.  */

        --New History Records
        INSERT INTO [dbo].[LT_HISTORY_TICKET] ([attendance_type], [performance_type], [order_no], [sli_no], [li_seq_no], [title_name], [perf_no], [perf_date], [perf_time], [zone_no],
                                               [production_name], [production_name_short], [production_name_long], [comp_code], [comp_code_name], [order_payment_status], [price_type],
                                               [price_type_name], [due_amt], [paid_amt], [sale_total], [performance_date], [performance_time], [scan_admission_date], [scan_admission_time],
                                               [scan_device], [scan_admission_adult], [scan_admission_child], [scan_admission_other], [scan_admission_auto], [scan_admission_total], 
                                               [sli_status], [customer_no], [create_dt])
        SELECT 'Gate B (Non-Ticketed)',
               'Public',
               0,
               999999,
               rdm.[redeam_id],
               'Show and Go',
               0,
               '',
               '',
               0,
               'Redeam',
               'Redeam',
               'Redeam',
               0,
               '',
               '',
               0,
               '',
               0.0,
               0.0,
               0,
               CONVERT(CHAR(10), [redeam_dt], 111),
               '09:00:00',
               CONVERT(CHAR(10), [redeam_dt], 111),
               '09:00:00',
               'Redeam Scanner',
               0,
               0,
               rdm.[redeam_count],
               0,
               rdm.[redeam_count],
               0,
               0,
               GETDATE()
        FROM [dbo].[LT_REDEAM_SCANS] AS rdm
             LEFT OUTER JOIN [dbo].[LT_HISTORY_TICKET] AS his ON his.[performance_date] =  CONVERT(CHAR(10),rdm.[redeam_dt],111) AND his.[production_name] = 'Redeam'
        WHERE his.[history_no] IS NULL 
               
        --Update existing History records
        UPDATE his
        SET his.[scan_admission_other] = rdm.[redeam_count],
            his.[scan_admission_total] = rdm.[redeam_count]
        FROM [dbo].[LT_REDEAM_SCANS] AS rdm
             LEFT OUTER JOIN [dbo].[LT_HISTORY_TICKET] AS his ON his.[performance_date] =  CONVERT(CHAR(10),rdm.[redeam_dt],111) AND his.[production_name] = 'Redeam'
        WHERE rdm.[last_update_dt] > @start_dt 
          AND his.[scan_admission_total] <> rdm.[redeam_count]

        --New Visit Count Records
        INSERT INTO [dbo].[LT_HISTORY_VISIT_COUNT] ([run_dt], [history_dt], [history_date], [attendance_type], [transaction_count], [visit_count], [visit_count_scan])
        SELECT GETDATE(),
               CAST([performance_date] AS DATE),
               [performance_date],
               'Redeam',
               1,
               [scan_admission_total],
               [scan_admission_total]
        FROM dbo.LT_HISTORY_TICKET AS his
             LEFT OUTER JOIN [dbo].[LT_HISTORY_VISIT_COUNT] AS vct ON vct.[history_date] = his.[performance_date] AND vct.[attendance_type] = 'Redeam'
        WHERE his.[production_name] = 'Redeam'
          AND vct.[id] IS NULL

        --Update existing visit count records          
        UPDATE vct
        SET vct.[visit_count] = his.[scan_admission_total],
            vct.[visit_count_scan] = his.[scan_admission_total]
        FROM dbo.LT_HISTORY_TICKET AS his
             LEFT OUTER JOIN [dbo].[LT_HISTORY_VISIT_COUNT] AS vct ON vct.[history_date] = his.[performance_date] AND vct.[attendance_type] = 'Redeam'
        WHERE his.[production_name] = 'Redeam'
          AND (his.[scan_admission_total] <> vct.[visit_count] or his.[scan_admission_total] <> vct.[visit_count_scan])

END
GO


            
            EXECUTE [dbo].[LP_IMPORT_REDEAM_SCANS]


            