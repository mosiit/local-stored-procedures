USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_EMAIL_SIGN_UP]    Script Date: 5/14/2021 10:02:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_EMAIL_SIGN_UP]
        @email_address VARCHAR(500) = NULL,
        @first_name VARCHAR(30) = NULL,
        @last_name VARCHAR(30) = NULL,
        @cpp_1 INT = NULL,
        @cpp_2 INT = NULL,
        @cpp_3 INT = NULL
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
        
    /*  Procedure Variables  */

        DECLARE @email_exists INT = 0,
                @name_exists INT = 0

        DECLARE @contact_point_category INT = -2;    --   -2 = Email Address
        DECLARE @customer_no INT = 0;
        DECLARE @eaddress_no INT = 0;
        DECLARE @salutation_no INT = 0;

        DECLARE @original_source INT = 258   --258 = Web Email Sign Up

        DECLARE @next_id_ac INT = 0,
                @act_category_web INT = 15,         -- 15 = Web Enquiry CSI
                @act_type_no_mismatch INT = 207,    --207 = Email Sign-up Name Mismatch
                @act_contact_type_web INT = 7,      --  7 = Web Enquiry CSI
                @act_origin_web INT = 3,            --  3 = Web
                @act_notes VARCHAR(255) = 'The ' + @email_address + ' email address on this account matches an account already in Tessitura, '
                                        + 'but the constituent names do not match the existing account.'                            

        DECLARE @eaddress_list1 TABLE ([eaddress_no] INT NOT NULL DEFAULT (0),
                                       [customer_no] INT NOT NULL DEFAULT (0),
                                       [inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                       [eaddress] VARCHAR(500) NOT NULL DEFAULT (''));

        DECLARE @eaddress_list2 TABLE ([eaddress_no] INT NOT NULL DEFAULT (0),
                                       [customer_no] INT NOT NULL DEFAULT (0),
                                       [inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                       [eaddress] VARCHAR(500) NOT NULL DEFAULT (''));

        DECLARE @eaddress_list3 TABLE ([eaddress_no] INT NOT NULL DEFAULT (0),
                                       [customer_no] INT NOT NULL DEFAULT (0),
                                       [inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                       [eaddress] VARCHAR(500) NOT NULL DEFAULT (''));
         
    /*  Check Parameters  */    

        --The above parameters are required - If any of then are blank or null, jump to the end of the procedure
        IF ISNULL(@email_address, '') = '' GOTO DONE;
        SELECT @first_name = ISNULL(@first_name, '');
        SELECT @last_name = ISNULL(@last_name, '');
        
        --Only one of these three is required.  The other two are optional Parameters - Set default values if null is passed      
        SELECT @cpp_1 = ISNULL(@cpp_1, 0);
        SELECT @cpp_2 = ISNULL(@cpp_2, 0);  
        SELECT @cpp_3 = ISNULL(@cpp_3, 0);

        IF @cpp_1 = 0 AND @cpp_2 = 0 AND @cpp_3 = 0 BEGIN
            RAISERROR ('No contact point purposes.', 16, 1);        
            GOTO DONE;
        END;

    /*  Contact Point Purposes  */

        --Get list of email address id numbers for the messaging address
        INSERT INTO @eaddress_list1 ([eaddress_no], [customer_no], [inactive], [eaddress])
        SELECT [eaddress_no],
               [customer_no],
               [inactive],
               @email_address
        FROM [dbo].[T_EADDRESS] 
        WHERE [address] = @email_address;

        SELECT @email_exists =  @@ROWCOUNT;

        INSERT INTO @eaddress_list2 ([eaddress_no], [customer_no], [inactive], [eaddress])
        SELECT [eaddress_no], [customer_no], [inactive], [eaddress] FROM @eaddress_list1

        INSERT INTO @eaddress_list1 ([eaddress_no], [customer_no], [inactive], [eaddress])
        SELECT [eaddress_no], [customer_no], [inactive], [eaddress] FROM @eaddress_list1

        IF @email_exists > 0 BEGIN

            WITH [CTE_EMAIL_LIST] ([email_address], [customer_no], [first_name], [last_name])
            AS (SELECT ead.[address], ead.[customer_no], cus.[fname], cus.[lname] 
                FROM [dbo].[T_EADDRESS] AS ead INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ead.[customer_no]
                WHERE ead.[address] = @email_address
                UNION SELECT ead.[address], ead.[customer_no], inf.[customer_first_name], inf.[customer_last_name]
                FROM [dbo].[T_EADDRESS] AS ead INNER JOIN [dbo].[LV_CUSTOMER_WITH_HOUSEHOLD_INFO] AS inf ON inf.[household_no] = ead.[customer_no]
                WHERE ead.[address] = @email_address)
            SELECT @name_exists = COUNT(*) FROM [CTE_EMAIL_LIST] WHERE [first_name] = @first_name AND [last_name] = @last_name;

        END      

        --Validate email address
		IF @email_exists = 0 OR @name_exists = 0 BEGIN

            BEGIN TRY;

                EXECUTE [dbo].[AP_GET_NEXTID_function] @type = 'CU', @nextid = @customer_no OUTPUT, @increment = 1;
                IF ISNULL(@customer_no, 0) <= 0 RAISERROR ('Unable to get next customer number.', 16, 1);

                EXECUTE [dbo].[AP_GET_NEXTID_function] @type = 'CS', @nextid = @salutation_no OUTPUT, @increment = 1;
                IF ISNULL(@salutation_no, 0) <= 0 RAISERROR ('Unable to get next salutation number.', 16, 1);

                EXECUTE [dbo].[AP_GET_NEXTID_function] @type = 'AD', @nextid = @eaddress_no OUTPUT, @increment = 1;
                IF ISNULL(@eaddress_no, 0) <= 0 RAISERROR ('Unable to get next email address number.', 16, 1);

                IF @email_exists > 0 AND @name_exists = 0 BEGIN
                    EXECUTE [dbo].[AP_GET_NEXTID_function] @type = 'AC', @nextid = @next_id_ac OUTPUT, @increment = 1
                    IF ISNULL(@next_id_ac, 0) <= 0 RAISERROR ('Unable to get next CSI action number.', 16, 1);
                END

                --MUST HAVE A LAST NAME VALUE
                IF @last_name = '' AND @first_name <> '' SELECT @last_name = @first_name, @first_name = ''
                IF @last_name = '' AND @email_address <> '' SELECT @last_Name = SUBSTRING(@email_address,0, CHARINDEX('@', @email_address))
                IF @last_name = '' SELECT @last_name = 'Unknown User'

                --INSERT NEW RECORD INTO T_CUSTOMER
                INSERT INTO [dbo].[T_CUSTOMER] ([customer_no], [cust_type], [prefix], [fname], [mname], [lname], [suffix], [name_status], 
                                                [original_source_no], [last_activity_dt], [emarket_ind], [inactive],[sort_name])
                VALUES (@customer_no, 1, -1, @first_name, '', @last_name, -1, 1, @original_source, GETDATE(), 3, 1, @last_name + '/' + @first_name);
                      
                --INSERT NEW RECORD INTO T_CUST_SAL
                INSERT INTO [dbo].[TX_CUST_SAL] ([signor], [customer_no], [esal1_desc], [lsal_desc], [default_ind], [label], [salutation_no])
                VALUES (2, @customer_no, @first_name + ' ' + @last_name, @first_name, 'Y', 'Y', @salutation_no);

                --INSERT NEW RECORD INTO T_ADDRESS
                INSERT INTO [dbo].[T_EADDRESS] ([eaddress_no],[customer_no],[eaddress_type],[address],[months],[primary_ind],[inactive],[market_ind],[html_ind])
                OUTPUT [Inserted].[eaddress_no], @customer_no, 'N', @email_address INTO @eaddress_list
                VALUES (@eaddress_no, @customer_no, 1, @email_address,'YYYYYYYYYYYY','Y','N','Y','Y');
                         
                IF @email_exists > 0 AND @name_exists = 0 AND @next_id_ac > 0
                    INSERT INTO [dbo].[T_CUST_ACTIVITY] ([activity_no], [category], [activity_type], [customer_no],
                                                             [contact_type], [notes], [issue_dt], [urg_ind], [origin])
                    VALUES (@next_id_ac, @act_category_web, @act_type_no_mismatch, @customer_no, 
                            @act_contact_type_web, @act_notes, GETDATE(), 'N', @act_origin_web)

            END TRY
            BEGIN CATCH

                THROW

            END CATCH;

        END;

        BEGIN TRY
                        
            IF @cpp_1 > 0 BEGIN

                --Only opt in Active Email Addresses - Delete inactive eaddress numbers
                DELETE FROM @eaddress_list1 WHERE [inactive] <> 'N';

                --Remove email address numbers from the list where the Contact Point Purpose already exists (already opted in)
                DELETE FROM @eaddress_list1
                WHERE [eaddress_no] IN (SELECT contact_point_id FROM [dbo].[tx_contact_point_purpose]
                                        WHERE [contact_point_id] IN (SELECT [eaddress_no] FROM @eaddress_list1)
                                          AND [purpose_id] = @cpp_1 
                                          AND [contact_point_category] = @contact_point_category);

                --If any are left in the list that are not already opted in, add the contact point purpose to those email records
                IF EXISTS (SELECT [eaddress_no] FROM @eaddress_list1)
                    INSERT INTO [dbo].[TX_CONTACT_POINT_PURPOSE] (contact_point_id, contact_point_category, purpose_id)
                    SELECT [eaddress_no], 
                           @contact_point_category, 
                           @cpp_1
                    FROM @eaddress_list1;

            END;

            IF @cpp_2 > 0 BEGIN

                --Only opt in Active Email Addresses - Delete inactive eaddress numbers
                DELETE FROM @eaddress_list2 WHERE [inactive] <> 'N';

                --Remove email address numbers from the list where the Contact Point Purpose already exists (already opted in)
                DELETE FROM @eaddress_list2
                WHERE [eaddress_no] IN (SELECT contact_point_id FROM [dbo].[tx_contact_point_purpose]
                                        WHERE [contact_point_id] IN (SELECT [eaddress_no] FROM @eaddress_list2)
                                          AND [purpose_id] = @cpp_2
                                          AND [contact_point_category] = @contact_point_category);

                --If any are left in the list that are not already opted in, add the contact point purpose to those email records
                IF EXISTS (SELECT [eaddress_no] FROM @eaddress_list2)
                    INSERT INTO [dbo].[TX_CONTACT_POINT_PURPOSE] (contact_point_id, contact_point_category, purpose_id)
                    SELECT [eaddress_no], 
                           @contact_point_category, 
                           @cpp_2
                    FROM @eaddress_list2;

            END;

            IF @cpp_3 > 0 BEGIN

                --Only opt in Active Email Addresses - Delete inactive eaddress numbers
                DELETE FROM @eaddress_list3 WHERE [inactive] <> 'N';

                --Remove email address numbers from the list where the Contact Point Purpose already exists (already opted in)
                DELETE FROM @eaddress_list3
                WHERE [eaddress_no] IN (SELECT contact_point_id FROM [dbo].[tx_contact_point_purpose]
                                        WHERE [contact_point_id] IN (SELECT [eaddress_no] FROM @eaddress_list3)
                                          AND [purpose_id] = @cpp_3
                                          AND [contact_point_category] = @contact_point_category);

                --If any are left in the list that are not already opted in, add the contact point purpose to those email records
                IF EXISTS (SELECT [eaddress_no] FROM @eaddress_list3)
                    INSERT INTO [dbo].[TX_CONTACT_POINT_PURPOSE] (contact_point_id, contact_point_category, purpose_id)
                    SELECT [eaddress_no], 
                           @contact_point_category, 
                           @cpp_3
                    FROM @eaddress_list3;

            END;

        END TRY
        BEGIN CATCH

             THROW

        END CATCH;

    DONE:

END;
GO

--EXECUTE [dbo].[LP_EMAIL_SIGN_UP]
--        @email_address = 'jtestuserx@comcast.net',     @first_name = 'Julie',          @last_name = 'Testperson',
--        @cpp_1 = 10,                                   @cpp_2 = 12,                   @cpp_3 = 26

--EXECUTE [dbo].[LP_EMAIL_SIGN_UP]
--        @email_address = 'msherwoodtest@mos.org',     @first_name = 'Mark',          @last_name = 'Sherwood',
--        @cpp_1 = 10,                                   @cpp_2 = 12,                   @cpp_3 = 0
        
--SELECT * FROM [dbo].[T_EADDRESS] WHERE [address] = 'msherwoodtest@mos.org'
--SELECT * FROM [dbo].[T_CUSTOMER] WHERE [customer_no] = (SELECT [customer_no] FROM [dbo].[T_EADDRESS] WHERE [address] = 'msherwoodtest@mos.org')

--[dbo].[LFS_ActiveMemberInDateRange] @customer_no = 0,                  -- int
--                                    @memb_org = 0,                     -- int
--                                    @start_dt = '2020-10-23 17:10:54', -- datetime
--                                    @end_dt = '2020-10-23 17:10:54'    -- datetime

