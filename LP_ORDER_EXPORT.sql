USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_ORDER_EXPORT]    Script Date: 10/1/2015 3:43:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_ORDER_EXPORT](
		@session_id INT
	)
	AS
	/*	This procedure is used for local processing during Order Export. @session_id points to the 
		ready to be exported rows in TW_ORDER_IMPORT with a matching value in session_id.

	*/

	-- Local code goes here:

	-- FROM Online Help: 
	-- Before the final export of data, the LP_ORDER_EXPORT local procedure is called. 
	-- This procedure can be used to used to add data to the local use fields for export and/or manipulate
	-- data that has already been staged for export by the standard export procedures.

	-- DSJ, 10/02/2015 NOTE. In TW_ORDER_IMPORT table, the order_no is stored in import_ref_no, not order_no 
	UPDATE ordimp
	SET ordimp.local_use0 = ord.custom_4
	FROM dbo.TW_ORDER_IMPORT ordimp
		INNER JOIN dbo.T_ORDER ord
		ON ordimp.import_ref_no = ord.order_no
		AND ordimp.session_id = @session_id

	RETURN
GO


