USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_PROCESS_TICKETING_MEMBERSHIPS]    Script Date: 11/9/2016 11:01:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LP_PROCESS_TICKETING_MEMBERSHIPS]
	
@order_no int,
@debug char(1) = 'N'

AS

/********************************************************************************************************************
	This procedure searches for membership related tickets in an order which have not yet been processed, and if 
	any are found, processes them.
	
	This is all controled via a local system table that maps zones to memberships (with options for Renew/New and Upgrade)
	and membership add ons (like additional children or Planetarium access).
	
	Written 6/13/2012 - David Woodall, Tessitura Network Consulting
	Modified 6/16/2014 to work with Recipient_no if provided.
	Modified 9/11/2014 - DWW - Changes to make this more generic and limited to a single order.
	Modified 10/21/2016 -- TAH (TN) - added a call to LRP_INITIALIZE_CUST_ENTITLEMENTS so that entitlements will be added immediatley

exec ap_set_context 'ug', 'Admin'
LP_PROCESS_TICKETING_MEMBERSHIPS 514, @debug = 'Y'

Scripts for cleaning up and testing memberships:

Delete l
	from LX_SLI_MEMB  l
	join T_SUB_LINEITEM sli on l.sli_no = sli.sli_no
	join T_ORDER o on sli.order_no = o.order_no
Where o.order_no = 501

Delete from TX_CUST_MEMBERSHIP where customer_no = 789

Select * from tx_cust_membership where customer_no = 813298

Update tx_cust_membership
set init_dt = '6/1/2011', expr_dt = '5/31/2012 23:59:59',
	inception_dt= '6/1/2011', campaign_no = 16
where cust_memb_no = 8


*********************************************************************************************************************/

-- Some constants, and variables
DECLARE @memb_perf_type int,
		@num_to_process int,
		@counter int = 1,
		@batch_no int,
		@use_org_no int, 
		@memb_level_override char(3),	
		@renew_upgrade_override char(1),
		@addl_children int,
		@addl_benefit varchar(30),
		@sli_no int,
		@cust_memb_no int,
		@customer_no int

-- Get the perf type we are looking at:
Select	@memb_perf_type = dbo.FS_GET_DEFAULT_VALUE(NULL, NULL, 'Membership_Perf_Type')
if @debug = 'Y' select @memb_perf_type
		
-- Find all sli's for membership performances that we have not processed
Select	ROW_NUMBER() over (order by order_dt) as id,
		o.order_dt, sli.sli_no, mz.memb_org_no, mz.memb_level, mz.renew_upgrade,
		o.customer_no
into #needs_processed
	from dbo.T_SUB_LINEITEM (nolock) sli
	join dbo.T_ORDER (nolock) o on sli.order_no = o.order_no
	join dbo.T_PERF (NOLOCK) p on sli.perf_no = p.perf_no
	join dbo.LTR_MEMB_ZONES mz (NOLOCK) on sli.zone_no = mz.zone_no
	left join dbo.lx_sli_memb (nolock) lm on sli.sli_no = lm.sli_no
Where p.perf_type = @memb_perf_type		-- Membership performance
  and lm.sli_no is null					-- Not processed 
  and sli.sli_status in (3,12)			-- Seated,Paid and Ticketed only.
  and o.order_no = @order_no
Order by order_dt, sli.create_dt

-- None?  Then get outta here!
Select @num_to_process = @@rowcount
If @num_to_process = 0
	RETURN

if @debug = 'Y' select * from #needs_processed

-- Loop through all the unprocessed sli's
While @counter <= @num_to_process
  Begin

	--	Get the details for this row.	
	Select	@sli_no = sli_no,
			@use_org_no = memb_org_no,
			@memb_level_override = memb_level,
			@renew_upgrade_override = renew_upgrade,
			@cust_memb_no = NULL,
			@customer_no = customer_no
	From #needs_processed 
	where id = @counter

	-- See if this row is a membership. (It is has a level and renew flag, it is...)
	If ISNULL(@memb_level_override,'') <> '' and ISNULL(@renew_upgrade_override,'') <> ''
	  Begin
	  	
		-- Call the custom procedure to process this membership.
		Exec LP_CREATE_MEMBERSHIP_TICKETING @use_org_no = @use_org_no, @memb_level_override = @memb_level_override,
											@sli_no = @sli_no, @renew_upgrade_override = @renew_upgrade_override,
											@cust_memb_no = @cust_memb_no OUTPUT
		-- Call the custom procedure to create the entitlements
		Exec LRP_INITIALIZE_CUST_ENTITLEMENTS @customer_no = @customer_no

		
		-- Add a row to the LX table to mark this sli complete, using the cust_memb_no just returned, if we get one...
		If ISNULL(@cust_memb_no,0) <> 0
			Insert into LX_SLI_MEMB( sli_no, cust_memb_no)
				Values (@sli_no, @cust_memb_no)

	  End	

	-- Bump our counter and go get the next row, if any
	Select @counter = @counter + 1  
  
  End

RETURN

