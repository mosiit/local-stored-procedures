USE [impresario]
GO

DROP PROCEDURE [dbo].[LP_CREATE_LOGIN]
GO

/****** Object:  StoredProcedure [dbo].[LP_CREATE_LOGIN]    Script Date: 1/4/2017 1:57:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE        PROCEDURE [dbo].[LP_CREATE_LOGIN](
	@sessionkey varchar(64) = null,
	@customer_no int = null,	
	@login_type int = null,
	@new_login varchar(80) = null,
	@new_password varchar(80),
	@new_email_address varchar(80) = null,
	@html_ind char(1) = 'Y',
	@email_market_ind char(1) = 'Y',
	@temporary_ind char(1) = 'N')		-- added CWR 8/5/2008 FP783.

AS

SET NOCOUNT ON

/****************************************************************************************************************
New Procedure MRG 05/21/2013 #2044 New procedure to Create Web Logins.

This procedure will create a new login record provided it does not find an existing login for that customer


In all cases it returns a set consisting of customer_no, login, password, email address, with customer_no being
-1 if the validation fails to find an existing permanent or temporary login matching the old_login value.  If all
is successful, it returns the new values



Examples:
wp_update_login @sessionkey = '1',  @login_type = 1, @old_login = 'sjakcson', @new_login = 'sjackson', 
	@old_password = 'mypassword2', @new_password = 'mypassword2', @new_email_address = 'sjackson@aol.com'

wp_update_login @sessionkey = '1',  @login_type = 1, @old_login = '20156', @new_login = 'sjackson3', 
	@old_password = '20156', @new_password = 'mypassword2', @new_email_address = 'sjackson@aol2.com'

wp_update_login @sessionkey = '3',  @login_type = 1, @old_login = 'dddefhe3', @new_login = 'dddefhe3', 
	@old_password = 'test', @new_password = 'lash', @new_email_address = 'dhalford@mail.com'

select	b.address, a.* 
from	t_cust_login a
	JOIN t_eaddress b ON a.eaddress_no = b.eaddress_no
where a. customer_no = 3

*****************************************************************************************************************/

Declare	@errmsg varchar(200), @login_no int, @eaddress_type int, @email_address varchar(80), @login varchar(80),
	@mos int, @default_mos int, @default_source_no int, @status char(1), @password varchar(32),
	@eaddress_no int, @web_user char(8), @ug_id char(8)

/*
-- validate sessionkey parameter
If @sessionkey is null OR not exists (select * from [dbo].t_web_session_session where sessionkey = @sessionkey)
  Begin
	select @errmsg = 'Invalid Session ID'
	RAISERROR(@errmsg, 11, 2) WITH SETERROR
	return -101
  End

Exec WP_SET_WEB_CONTEXT @sessionkey=@sessionkey	
*/

-- you must pass a login type
If @login_type is null or Not Exists (Select * from VRS_LOGIN_TYPE where id = @login_type)
  Begin
	select @errmsg = 'Invalid Login Type'
	RAISERROR(@errmsg, 11, 2) WITH SETERROR
	return -101
  End


If @new_login is null 
  Begin
	select @errmsg = 'Login must be provided'
	RAISERROR(@errmsg, 11, 2) WITH SETERROR
	return -101
  End

If @new_email_address is null 
  Begin
	select @errmsg = 'Email Address must be provided'
	RAISERROR(@errmsg, 11, 2) WITH SETERROR
	return -101
  End
  
If @customer_no is null 
  Begin
    select @errmsg = 'Customer Number must be provided'
    RAISERROR(@errmsg, 11, 2) WITH SETERROR
    return -101
  End  

If @customer_no > 0
  Begin

	-- make sure the login is not used elsewhere
	If Exists (Select * 
		From 	[dbo].t_cust_login a
		Where	a.login = @new_login 
			and a.customer_no <> @customer_no
			and a.login_type = @login_type)		
	  Begin
		select @errmsg = 'Login is already in use.  Please select another login'
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -102
	  End

	-- make sure the login doesn't already exist for this customer
	If Exists (Select * 
		From 	[dbo].t_cust_login a
		Where	a.login = @new_login 
			and a.customer_no = @customer_no
			and a.login_type = @login_type)		
	  Begin
		select @errmsg = 'Login already exists for this constituent.'
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -102
	  End		

	-- make sure the email address is valid
	If Exists (Select * 
		From 	[dbo].t_cust_login a
			JOIN [dbo].t_eaddress b ON a.eaddress_no = b.eaddress_no
		Where	b.address = @new_email_address 
			and a.login_type = @login_type)	
	  Begin
		select @errmsg = 'Supplied email address is associated with a login.  Please select a different email address.'
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -102
	  End	


	-- check to see if this customer already has this email address
	Select	@eaddress_no = eaddress_no
	From	[dbo].t_eaddress 
	Where	address = @new_email_address and customer_no = @customer_no
		and Coalesce(inactive, 'N') = 'N'		-- added CWR FP1976.

	-- no eaddress, so we have to add it
	If @eaddress_no is null
	
	  Begin

	  /*
		-- call to new procedure CWR 4/14/2006
		Exec [dbo].wp_get_web_user @sessionkey = @sessionkey, @web_user = @web_user OUTPUT, @ug_id = @ug_id OUTPUT
	*/

		-- get some defaults for this const type
		Select 	@eaddress_type = eaddress_type
		From	[dbo].tr_cust_type a
			JOIN [dbo].t_customer b ON a.id = b.cust_type
		Where	b.customer_no = @customer_no

		-- get an eaddress id
		exec @eaddress_no = [dbo].ap_get_nextid_function @type = 'AD'

		--insert into t_eaddress
		Insert	[dbo].t_eaddress(
			eaddress_no,
			customer_no,
			eaddress_type,
			address,
			months,
			primary_ind,
			inactive,
			market_ind,
			html_ind,
			alt_signor,
			created_by)
		Select	@eaddress_no,
			@customer_no,
			@eaddress_type,
			@new_email_address,
			'YYYYYYYYYYYY',
			CASE WHEN EXISTS (select * from [dbo].t_eaddress where customer_no = @customer_no and primary_ind = 'Y') Then 'N' ELSE 'Y' END,
			'N',
			@email_market_ind,
			@html_ind,
			0,
			@web_user

		If @@error > 0 or @eaddress_no is null
		  Begin
			select @errmsg = 'Error adding email address'
			RAISERROR(@errmsg, 11, 2) WITH SETERROR
			return -101
		  End		

	  End
	  
	exec @login_no  = [dbo].ap_get_nextid_function @type = 'LO'

	-- Update the login - include primary indicator in local version
    INSERT  [dbo].T_CUST_LOGIN
            (login,
             password,
             login_type,
             const_update_dt,
             customer_no,
             login_no,
             failed_attempts,
             temporary_ind,
             inactive,
             primary_ind,
             eaddress_no,
             n1n2_ind,
             created_by
            )
            SELECT  @new_login,
                    @new_password,
                    @login_type,
                    GETDATE(),
                    @customer_no,
                    @login_no,
                    0,
                    @temporary_ind,
                    'N',
                    'Y', -- change in local version to 'Y' (from 'N')
                    @eaddress_no,
                    3,
                    @web_user

	If @@error > 0 or @login_no is null
	  Begin
		select @errmsg = 'Error inserting into T_CUST_LOGIN table'
		RAISERROR(@errmsg, 11, 2) WITH SETERROR
		return -101
	  End		


	Select  @login = @new_login, @password = @new_password, @email_address = @new_email_address, @status = 'P'

  End
	

-- return result -- removed, caused an out of memory exception when run in bulk
--Select 	customer_no = @customer_no, login = @login, password = @password, 
--	email_address = @email_address


GO


