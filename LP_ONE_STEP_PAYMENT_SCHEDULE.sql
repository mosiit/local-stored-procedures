USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_ONE_STEP_PAYMENT_SCHEDULE]    Script Date: 6/28/2017 4:29:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonathan Smillie, Tessitura Network 
-- Create date: March 2016 (this version 3/31/2016) 
-- Description:	Procedure to create payment schedules for One-Step renewing members 
--				Based on the concept that One-Step members are renewed using the standard Tessitura
--				rollover process, and that after rollover we need to create payment schedules so the orders 
--				can be billed automatically. This process takes a list of constituents (the assumption is 
--				it's the same one that was used to do the rollover) and finds all of them that have credit 
--				cards in the T_ACCOUNT_DATA system table. It then creates a single-instance payment schedule 
--				referencing the credit card it found. 
--				The Ticket Order Billing utility still needs to be run after this to actually bill people's 
--				cards for their payment. 
-- =============================================
ALTER PROCEDURE [dbo].[LP_ONE_STEP_PAYMENT_SCHEDULE]
      @list_id INT = NULL,
      @season INT,
      @target_perf INT,
      @schedule_dt DATETIME = NULL 
	-- Add the parameters for the stored procedure here
AS 
      BEGIN
	-- create a list of customer numbers and acct_ids for credit cards 
-- based on the people in a list of rolled-over orders 
--

            IF @list_id NOT IN (SELECT  list_no
                                FROM    t_list
                                WHERE   LEN(criteria) = 0) 
               BEGIN 
                     PRINT 'Recalculating list' 
                     EXEC RP_GENERATE_LIST @list_no = @list_id, @display_names = 'N', @supress_errors = 'Y' 
                     PRINT 'Finished recalculating!' 
               END 
            ELSE 
               PRINT 'This is a manually-created list. No recalculation done.' 

 
/** NOTE: 
Payment methods are being picked up based on rules established by MOS. First priority is the acct_id from 
T_ACCOUNT_DATA based on the existence of a One-Step attribute. Second option is the existence of a credit 
card with the One-Step payment method group. Lastly, pick up the most recently added credit card on the
constituent if neither of the others exist. 
**/
            SELECT DISTINCT
                    (customer_no),
                    key_value AS act_id
            INTO    #order_acct
            FROM    tx_cust_keyword
            WHERE   keyword_no = 417
                    AND customer_no IN (SELECT  customer_no
                                        FROM    t_list_contents
                                        WHERE   list_no = @list_id) 

            INSERT  #order_acct
                    SELECT DISTINCT
                            (customer_no),
                            id AS act_id
                    FROM    t_account_data
                    WHERE   customer_no IN (SELECT  customer_no
                                            FROM    t_list_contents
                                            WHERE   list_no = @list_id
                                                    AND customer_no NOT IN (SELECT  customer_no
                                                                            FROM    #order_acct))
                            AND payment_method_group_id = 3
                            AND inactive <> 'Y' 

            INSERT  #order_acct
                    SELECT DISTINCT
                            (customer_no),
                            MAX(id) OVER (PARTITION BY customer_no) AS max_id
                    FROM    t_account_data 
--where customer_no in 
--(select customer_no from t_account_data group by customer_no having count(customer_No) > 1 )
                    WHERE   customer_no IN (SELECT  customer_no
                                            FROM    t_list_contents
                                            WHERE   list_no = @list_id)
                            AND customer_no NOT IN (SELECT  customer_no
                                                    FROM    #order_acct)
                            AND inactive <> 'Y'
                    GROUP BY customer_no,
                            id
                    ORDER BY customer_no 


--
-- make a temp table of orders to have their credit card references updated
-- based on who's in the list and their order number that's accepting rollovers 
-- need to add a parameter for season 

            CREATE TABLE #orders (id INT IDENTITY(1, 1),
                                  order_no INT,
                                  customer_no INT)  
            INSERT  #orders
                    SELECT DISTINCT
                            (a.order_no),
                            customer_no
                    FROM    t_order a
                    JOIN    T_LINEITEM b ON a.order_no = b.order_no
                    WHERE   a.customer_no IN (SELECT    customer_no
                                              FROM      t_list_contents
                                              WHERE     list_No = @list_id)
                            AND a.accepting_rollovers = 'Y'
                            AND a.tot_due_amt > 0
                            AND b.perf_no = @target_perf

--
-- now we go in and update the rolled-over order with the account_id from T_ACCOUNT_DATA. 
-- this is what actually attaches the credit card to the payment schedule. 
-- 

            UPDATE  t_order
            SET     act_id = b.act_id
            FROM    #order_acct b
            JOIN    #orders a ON a.customer_no = b.customer_no
            JOIN    t_order c ON c.order_no = a.order_no
            WHERE   c.order_no = a.order_no 

--select * from t_order where order_no in (select order_no from #orders)

-- now make the order schedules 
-- based on the order numbers from the table and the account IDs 
-- 


-- 
-- Making the assumption that if they already have a payment schedule on tis order, 
-- we should wipe it out. This is mostly for cleaning up any payment schedules created during 
-- testing, as newly-rolled-over orders shouldn't have an entry in this table to begin with. 
-- 

            DELETE  t_order_schedule
            WHERE   order_no IN (SELECT order_no
                                 FROM   #orders) 

            INSERT  t_order_schedule
                    (order_no,
                     order_sch_no,
                     sch_type,
                     billing_type,
                     due_dt,
                     amt_due,
                     mir_lock
                    )
                    SELECT  a.order_no,
                            (SELECT (next_id)
                             FROM   t_next_id
                             WHERE  type = 'OS') + b.id,
                            1 AS sch_type,
                            4 AS billing_type,
--
-- making an assumption about order date: if an order date wasn't supplied in the rollover (so order_dt is NULL)
-- then we use the end of the month in which the order was created 
-- however, if the order does have an order_dt, we use the end of the order month as the date for the 
-- payment schedule 
                            CASE WHEN @schedule_dt IS NULL THEN ISNULL(EOMONTH(a.order_dt), EOMONTH(a.create_dt))
                                 ELSE @schedule_dt
                            END AS due_dt,
                            tot_due_amt,
                            0 AS mir_lock
                    FROM    t_order a
                    JOIN    #orders b ON a.order_no = b.order_no
                    WHERE   a.order_no IN (SELECT   order_no
                                           FROM     #orders)
                            AND tot_due_amt > 0 

--
-- don't forget to update the next-id table because we don't want to break stuff 
-- 

            UPDATE  t_next_id
            SET     next_id = (SELECT   MAX(order_sch_no) + 1
                               FROM     t_order_schedule)
            WHERE   type = 'OS' 

-- 
-- and now we extract the data for the report (which constituents we created schedules for and for how much) 
-- 
            SELECT DISTINCT
                    (b.order_sch_no) AS order_sched_no,
                    a.order_no AS order_no,
                    a.customer_no AS customer_no,
                    d.esal1_desc,
                    b.billing_type AS billing_type,
                    c.description AS bill_type_desc,
                    b.due_dt AS due_dt,
                    b.amt_due AS amt_due
            FROM    t_order a
            JOIN    t_order_schedule b ON a.order_no = b.order_no
            JOIN    TR_BILLING_TYPE c ON b.billing_type = c.id
            JOIN    tx_cust_sal d ON a.customer_no = d.customer_no
            WHERE   b.order_no IN (SELECT   order_no
                                   FROM     #orders)
                    AND d.default_ind = 'Y' 

      END

GRANT EXECUTE ON [dbo].[LP_ONE_STEP_PAYMENT_SCHEDULE] TO ImpUsers
GO