USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_ChangeUnsoldSchoolButterflyToPublic]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_ChangeUnsoldSchoolButterflyToPublic] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_ChangeUnsoldSchoolButterflyToPublic] TO impusers';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_ChangeUnsoldSchoolButterflyToPublic] TO tessitura_app';
END;
GO

ALTER PROCEDURE [dbo].[LP_ChangeUnsoldSchoolButterflyToPublic]
        @specific_dt DATETIME = NULL,
        @return_message VARCHAR(1000) = NULL OUTPUT
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF;

    /*  Procedure Variables and Tables  */

    DECLARE @start_dt DATETIME = NULL,
            @end_dt DATETIME = NULL,
            @log_dt DATETIME = GETDATE();

    DECLARE @bg_title_no INT = 157,
            @bg_prod_no INT = 158,
            @bg_exp_prod_no INT = 5723;

    DECLARE @perf_dt DATETIME = NULL,
            @bg_exp_perf_no INT = 0,
            @bg_perf_no INT = 0,
            @perf_time VARCHAR(10) = '',
            @second_perf_time varchar(10) = '',
            @rtn VARCHAR(1000) = '';


        IF OBJECT_ID('tempdb..#bg_experience_perfs') IS NOT NULL DROP TABLE [#bg_experience_perfs]
    
        CREATE TABLE [#bg_experience_perfs] ([fiscal_year] INT NOT NULL DEFAULT(0),
                                             [perf_dt] DATETIME NULL,
                                             [perf_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                             [second_perf_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                             [bg_exp_perf_no] INT NOT NULL DEFAULT (0),
                                             [zone_no] INT NOT NULL DEFAULT (0),
                                             [second_zone_no] INT NOT NULL DEFAULT (0),
                                             [title_no] INT NOT NULL DEFAULT(0),
                                             [title_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [bg_exp_prod_no] INT NOT NULL DEFAULT (0),
                                             [bg_exp_prod_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [tickets_sold] INT NOT NULL DEFAULT (0),
                                             [bg_perf_no] INT NOT NULL DEFAULT (0), 
                                             [bg_prod_no] INT NOT NULL DEFAULT (0), 
                                             [bg_prod_name] VARCHAR(30) NOT NULL DEFAULT (''));
    

       /*  Check Parameters  */

        IF @specific_dt IS NULL
            SELECT @start_dt = GETDATE(),
                   @end_dt = DATEADD(WEEK,2,GETDATE());
        ELSE
            SELECT @start_dt = @specific_dt,
                   @end_dt = @specific_dt;

        SELECT @start_dt = CAST(@start_dt AS DATE),
               @end_dt = CONVERT(CHAR(10),@end_dt,111) + ' 23:59:59.957';


        SELECT @return_message = '';

 
    /*  Get Butterfly Garden Experience Shows  */

            WITH CTE_BG_EXPERIENCE
            AS (SELECT [performance_no],
                       [performance_zone],
                       [performance_dt],
                       [performance_time],
                       CASE [performance_time]
                            WHEN '10:15' THEN '10:00'
                            WHEN '10:45' THEN '10:30'
                            WHEN '11:15' THEN '11:00'
                            ELSE [performance_time] END AS [parent_perf_time],
                       CASE [performance_time]
                            WHEN '10:00' THEN '10:15'
                            WHEN '10:30' THEN '10:45'
                            WHEN '11:00' THEN '11:15'
                            ELSE [performance_time] END AS [second_perf_time],
                       [title_no],
                       [title_name],
                       [production_no],
                       [production_name],
                       [season_fiscal_year]
                FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE
                WHERE CAST([performance_dt] AS DATE) BETWEEN @start_dt AND @end_dt
                  AND [performance_time] IN ('10:00', '10:30', '11:00')
                  AND [title_no] = @bg_title_no
                  AND [production_no] = @bg_exp_prod_no)
        INSERT INTO [#bg_experience_perfs] ([fiscal_year], [perf_dt], [perf_time], [second_perf_time], [bg_exp_perf_no], [zone_no], [second_zone_no], [title_no],
                                            [title_name], [bg_exp_prod_no], [bg_exp_prod_name], [tickets_sold], [bg_perf_no], [bg_prod_no], [bg_prod_name])
        SELECT cte.[season_fiscal_year],
               cte.[performance_dt],
               cte.[parent_perf_time],
               cte.[second_perf_time],
               cte.[performance_no],
               cte.[performance_zone],
               (cte.[performance_zone] + 1),
               cte.[title_no],
               cte.[title_name],
               cte.[production_no],
               cte.[production_name],
               COUNT(DISTINCT sli.[sli_no]),
               prf.[perf_no],
               pro.[prod_no],
               inv.[description]
        FROM CTE_BG_EXPERIENCE AS cte
             LEFT OUTER JOIN dbo.[T_SUB_LINEITEM] AS sli ON sli.[perf_no] = cte.[performance_no] AND sli.[zone_no] = cte.[performance_zone]
             INNER JOIN [dbo].[T_PERF] AS prf ON prf.[perf_dt] = cte.[performance_dt]
             INNER JOIN [dbo].[T_PROD_SEASON] AS sea ON sea.[prod_season_no] = prf.[prod_season_no]
             INNER JOIN [dbo].[T_PRODUCTION] AS pro ON pro.[prod_no] = sea.[prod_no] AND pro.[title_no] = @bg_title_no AND pro.[prod_no] = @bg_prod_no
             INNER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = pro.[prod_no]
        WHERE ISNULL(sli.[sli_status], 0) IN (0, 2, 3, 12)
        GROUP BY cte.season_fiscal_year, cte.[performance_dt], cte.[parent_perf_time], cte.[second_perf_time], cte.[performance_no], cte.[performance_zone],
                 cte.[title_no], cte.[title_name], cte.[production_no], cte.[production_name], prf.[perf_no], pro.[title_no], pro.[prod_no], inv.[description]
        HAVING COUNT(DISTINCT sli.[sli_no]) = 0

        SET ANSI_WARNINGS ON;
    
    /*  If nothing found, jump to the end  */

        IF NOT EXISTS (SELECT * FROM [#bg_experience_perfs]) GOTO DONE

    /*  Records must be processed one at a time
        For each record, the LP_EnableDisableZone procedure is run three times
            1: Disable the school show
            2: Enable the public show that's at the same time as the school show
            3: Enable the second public show (the show after the school show time)  */

        DECLARE bg_cursor INSENSITIVE CURSOR FOR
        SELECT bge.perf_dt,
               bge.bg_exp_perf_no, 
               bge.[bg_perf_no],
               bge.[perf_time],
               bge.[second_perf_time]
         FROM [#bg_experience_perfs] AS bge
         OPEN bg_cursor
     
         FETCH NEXT FROM bg_cursor INTO @perf_dt, @bg_exp_perf_no, @bg_perf_no, @perf_time, @second_perf_time
         WHILE @@FETCH_STATUS <> -1 BEGIN

            EXECUTE [dbo].[LP_EnableDisableZone] @perf_no = @bg_exp_perf_no, @perf_time = @perf_time, @is_enabled = 'N', @force_disable = 'Y', @return_message = @rtn OUTPUT

            EXECUTE [dbo].[LP_EnableDisableZone] @perf_no = @bg_perf_no, @perf_time = @perf_time, @is_enabled = 'Y', @force_disable = 'N', @return_message = @rtn OUTPUT

            EXECUTE [dbo].[LP_EnableDisableZone] @perf_no = @bg_perf_no, @perf_time = @second_perf_time, @is_enabled = 'Y', @force_disable = 'N', @return_message = @rtn OUTPUT

            INSERT INTO [admin].[dbo].[tessitura_butterfly_school_to_public_log] ([log_dt], [performance_dt], [performance_date], [performance_time], [second_public_time],
                                                                                  [performance_no], [production_name], [public_performance_no], [public_production_name])
            VALUES (@log_dt, 
                    @perf_dt, 
                    CONVERT(CHAR(10),@perf_dt,111), 
                    @perf_time, 
                    @second_perf_time, 
                    @bg_exp_perf_no, 
                    'Butterfly Experience', 
                    @bg_perf_no, 
                    'Butterfly Garden');
              
            
            FETCH NEXT FROM bg_cursor INTO @perf_dt, @bg_exp_perf_no, @bg_perf_no, @perf_time, @second_perf_time

        END

        CLOSE bg_cursor
        DEALLOCATE bg_cursor

     DONE:

        IF @return_message = '' SELECT @return_message = 'success'

        IF OBJECT_ID('tempdb..#bg_experience_perfs') IS NOT NULL DROP TABLE [#bg_experience_perfs]

END
GO
     
--EXECUTE [dbo].[LP_ChangeUnsoldSchoolButterflyToPublic]
