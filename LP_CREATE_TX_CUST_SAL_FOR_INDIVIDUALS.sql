USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE LP_CREATE_TX_CUST_SAL_FOR_INDIVIDUALS
AS

-- THIS IS CREATING THE FAMILIAR SALU (ID = 2) RECORD IN THE TX_CUST_SAL TABLE FOR ALL INDIVIDUALS WHO DON'T ALREADY HAVE ONE
-- WO 83659 - Insert Familiar Salu on Individuals_Associated with RSVP report

/*  6/21/2017 - Removed SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  line from procedure.  
                Added a TRY/CATCH Structure and put the insert into TX_CUST_SAL into a transaction
                Changed SELECT @increment = COUNT(*) FROM @custSalTbl to SELECT @increment = COUNT(*) + 1 FROM @custSalTbl
    7/8/2021  - Added Mail Two to the list of email decriptors being excluded
*/

SET NOCOUNT ON  


DECLARE @custSalTbl TABLE
(
	[signor] [int] NOT NULL,
	[customer_no] [int] NOT NULL,
	[esal1_desc] [varchar](55) NULL,
	[esal2_desc] [varchar](55) NULL,
	[lsal_desc] [varchar](55) NULL,
	[default_ind] [char](1) NULL,
	[label] [char](1) NULL,
	[business_title] [varchar](55) NULL
)

INSERT INTO @custSalTbl
(
	signor,
	customer_no,
	esal1_desc,
	esal2_desc,
	lsal_desc,
	default_ind,
	label,
	business_title
)
SELECT DISTINCT
	2,
	c.customer_no,
	cs.esal1_desc,
	NULL,
	RTRIM(LTRIM(c.fname)),
	'N',
	'Y',
	NULL
-- select count(*)
FROM T_CUSTOMER AS c
INNER JOIN TX_CUST_SAL AS cs 
	ON c.customer_no = cs.customer_no
WHERE c.cust_type = 1 -- Individual
	AND (c.name_status != 2 OR c.name_status IS NULL )-- not deceased
	AND cs.default_ind = 'Y'
	AND c.customer_no NOT IN (SELECT customer_no FROM TX_CUST_SAL WHERE signor = 2) -- Familiar Salu does not already exist
	AND esal1_desc NOT IN ('Kiosk Customer', 'Guest User', 'Mailtwo Import')

DECLARE @increment INT,
	@next_id INT

SELECT @increment = COUNT(*) + 1 FROM @custSalTbl


BEGIN TRY

    BEGIN TRANSACTION

        EXECUTE @next_id = dbo.AP_GET_NEXTID_function @type = 'CS', @increment = @increment
        --SELECT @increment, @next_id


        INSERT INTO dbo.TX_CUST_SAL
        (
    	    signor,
	        customer_no,
	        esal1_desc,
    	    esal2_desc,
	        lsal_desc,
	        default_ind,
	        label,
    	    business_title,
	        salutation_no
        )
        SELECT 
	        signor,
    	    customer_no,
	        esal1_desc,
	        esal2_desc,
            lsal_desc,
	        default_ind,
	        label,
	        business_title,
	        ROW_NUMBER() Over (Order by customer_no) + @next_id
        FROM @custSalTbl

    COMMIT TRANSACTION

END TRY
BEGIN CATCH

    ROLLBACK TRANSACTION

END CATCH

