USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_TOKENIZE_CREDIT_CARD]    Script Date: 12/6/2016 1:04:44 PM ******/
DROP PROCEDURE [dbo].[LP_TOKENIZE_CREDIT_CARD]
GO

/****** Object:  StoredProcedure [dbo].[LP_TOKENIZE_CREDIT_CARD]    Script Date: 12/6/2016 1:04:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_TOKENIZE_CREDIT_CARD]
	   @month_year VARCHAR(30),
       @cust_no VARCHAR(30),
       @account_name VARCHAR(300),
       @account_no VARCHAR(200),
       @mill_exp_dt VARCHAR(4),
       @sage_exp_dt VARCHAR(4)
AS 
       BEGIN

			SET NOCOUNT ON

			DECLARE @account_type INT,
					@exp_dt DATETIME,
					@error_msg VARCHAR(4000)

			-- H. Sheridan, 12/12/2016 - Moved these two statements out of the 'try' because I need the values for both successful and failed attempt logging
			SELECT @account_type = CASE WHEN SUBSTRING(@account_no, 1, 1) = '4' THEN 1
										WHEN SUBSTRING(@account_no, 1, 1) IN (5, 2) THEN 2
										WHEN SUBSTRING(@account_no, 1, 1) = '3' THEN 3
										WHEN SUBSTRING(@account_no, 1, 1) = 6 THEN 4
										ELSE 0
									END

			SELECT @exp_dt = CASE WHEN SUBSTRING(@mill_exp_dt, 3, 2) + SUBSTRING(@mill_exp_dt, 1, 2) >= SUBSTRING(@sage_exp_dt, 3, 2) + SUBSTRING(@sage_exp_dt, 1, 2) THEN DATEADD(dd, -1, CONVERT(DATETIME, SUBSTRING(@mill_exp_dt, 1, 2) + '/1/' + SUBSTRING(@mill_exp_dt, 3, 2)))
									ELSE DATEADD(dd, -1, CONVERT(DATETIME, SUBSTRING(@sage_exp_dt, 1, 2) + '/1/' + SUBSTRING(@sage_exp_dt, 3, 2)))
								END

			IF (@month_year IS NULL OR @month_year = '')
				BEGIN
					INSERT INTO [LT_TOKENIZE_CREDIT_CARD_LOG] ([status], [month_year], [error_msg], [customer_no], [customer_name], [account_type], [mill_exp_date], [sage_exp_date], [exp_date_used])
						SELECT	'Failed', '', 'Month/year parameter is null or missing', @cust_no, @account_name, @account_type, @mill_exp_dt, @sage_exp_dt, @exp_dt
					RETURN
				END

			IF (@cust_no IS NULL OR @cust_no = '')
				BEGIN
					INSERT INTO [LT_TOKENIZE_CREDIT_CARD_LOG] ([status], [month_year], [error_msg], [customer_no], [customer_name], [account_type], [mill_exp_date], [sage_exp_date], [exp_date_used])
						SELECT	'Failed', @month_year, 'Customer number parameter is null or missing', '', @account_name, @account_type, @mill_exp_dt, @sage_exp_dt, @exp_dt
					RETURN
				END

			IF (@account_no IS NULL OR @account_no = '')
				BEGIN
					INSERT INTO [LT_TOKENIZE_CREDIT_CARD_LOG] ([status], [month_year], [error_msg], [customer_no], [customer_name], [account_type], [mill_exp_date], [sage_exp_date], [exp_date_used])
						SELECT	'Failed', @month_year, 'Account number parameter is null or missing', @cust_no, @account_name, @account_type, @mill_exp_dt, @sage_exp_dt, @exp_dt
					RETURN
				END

			IF (@account_name IS NULL OR @account_name = '')
				BEGIN
					INSERT INTO [LT_TOKENIZE_CREDIT_CARD_LOG] ([status], [month_year], [error_msg], [customer_no], [customer_name], [account_type], [mill_exp_date], [sage_exp_date], [exp_date_used])
						SELECT	'Failed', @month_year, 'Account name parameter is null or missing', @cust_no, '', @account_type, @mill_exp_dt, @sage_exp_dt, @exp_dt
					RETURN
				END
	
			IF (@mill_exp_dt IS NULL OR @mill_exp_dt = '')
				BEGIN
					INSERT INTO [LT_TOKENIZE_CREDIT_CARD_LOG] ([status], [month_year], [error_msg], [customer_no], [customer_name], [account_type], [mill_exp_date], [sage_exp_date], [exp_date_used])
						SELECT	'Failed', @month_year, 'Millennium date parameter is null or missing', @cust_no, @account_name, @account_type, '', @sage_exp_dt, ''
					RETURN
				END

			IF (@sage_exp_dt IS NULL OR @sage_exp_dt = '')
				BEGIN
					INSERT INTO [LT_TOKENIZE_CREDIT_CARD_LOG] ([status], [month_year], [error_msg], [customer_no], [customer_name], [account_type], [mill_exp_date], [sage_exp_date], [exp_date_used])
						SELECT	'Failed', @month_year, 'Sage date parameter is null or missing', @cust_no, @account_name, @account_type, @mill_exp_dt, '', ''
					RETURN
				END

			BEGIN TRY

				BEGIN TRANSACTION

					OPEN SYMMETRIC KEY TessituraSecureKey
					DECRYPTION BY CERTIFICATE TessCert

					-- H. Sheridan, 12/6/2016 - Added payment method group id = 3 (denotes One Step)
					EXEC AP_STORE_ACCOUNT_DATA @customer_no = @cust_no, @act_no = @account_no, @act_type = @account_type, @act_name = @account_name, @card_expiry_dt = @exp_dt, @payment_method_group_id = 3

					IF NOT EXISTS (SELECT [customer_no] FROM [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] WHERE [status] = 'Success' AND [month_year] = @month_year AND [customer_no] = @cust_no AND [account_type] = @account_type AND [exp_date_used] = @exp_dt) 
						BEGIN

							INSERT INTO [LT_TOKENIZE_CREDIT_CARD_LOG] ([status], [month_year], [customer_no], [customer_name], [account_type], [mill_exp_date], [sage_exp_date], [exp_date_used])
								SELECT	'Success', @month_year, @cust_no, @account_name, @account_type, @mill_exp_dt, @sage_exp_dt, @exp_dt

						END

				COMMIT TRANSACTION

			 END TRY

			 BEGIN CATCH

				SELECT	@error_msg = ERROR_MESSAGE()

				IF(@@TRANCOUNT > 0)
					ROLLBACK TRANSACTION

				IF NOT EXISTS (SELECT [customer_no] FROM [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] WHERE [status] = 'Failed' AND [month_year] = @month_year AND [customer_no] = @cust_no AND [account_type] = @account_type AND [exp_date_used] = @exp_dt) 
					BEGIN

						-- H. Sheridan, 12/12/2016 - This statement must come after the rollback, otherwise the 'insert' itself is rolled back as well.
						INSERT INTO [LT_TOKENIZE_CREDIT_CARD_LOG] ([status], [month_year], [error_msg], [customer_no], [customer_name], [account_type], [mill_exp_date], [sage_exp_date], [exp_date_used])
							SELECT	'Failed', @month_year, @error_msg, @cust_no, @account_name, @account_type, @mill_exp_dt, @sage_exp_dt, @exp_dt

					END

			 END CATCH
			 
       END
