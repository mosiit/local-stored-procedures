﻿USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_NSCAN_PROCESS_ENTITLEMENT_GIFT_CODE]    Script Date: 6/18/2019 3:24:23 PM ******/
DROP PROCEDURE [dbo].[LP_NSCAN_PROCESS_ENTITLEMENT_GIFT_CODE]
GO

/****** Object:  StoredProcedure [dbo].[LP_NSCAN_PROCESS_ENTITLEMENT_GIFT_CODE]    Script Date: 6/18/2019 3:24:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Tessitura Network COnsulting Jon Ballinger
-- Create date: 1/15/2018
-- Description:	<Description,,>
	/*
	declare 	@status varchar(50),
	@is_gift_code char(1),
	@stat_code int
	exec [dbo].[LP_NSCAN_PROCESS_ENTITLEMENT_GIFT_CODE] @scan_str='E998-3887-8B3B',@status  = @status,@is_gift_code = @is_gift_code,@stat_code=@stat_code,@user_id='jballing',@suppress_log_updates='Y'

	04/26/2019 - updated JTS to add logic for evaluating perf_dt as part of selecting the entitlement performance to redeem (line 115) 
	*/
	
-- =============================================
CREATE PROCEDURE [dbo].[LP_NSCAN_PROCESS_ENTITLEMENT_GIFT_CODE]
	@scan_str varchar(30),
	@exit_mode as varchar(1)= 'N',
	@status varchar(50) = null output,
	@stat_code int = null output,
	@update_dt as datetime = null,
	@device_name as varchar(20) = null,
	@is_gift_code char(1)='N' output,
	@user_id varchar(8),
	@suppress_log_updates char(1),
	@user_entrance varchar(255) = null,
	@update as varchar(1) = 'N',
	@online as varchar(1) = 'Y',
	@perf_no int output,
	@customer_no int output,
	@accepted as varchar(1) ='N' output, 
	@entitlement_perf_dt date = null output ,
	@gift_code varchar(30)= null output


AS
BEGIN
--Constants
declare @exhibit_hall_keyword  int 
select  @exhibit_hall_keyword = default_value
from T_DEFAULTS where field_name='EXHIBIT_HALL_TKW'


-- set entitlement performance date to today's date for later use
if @entitlement_perf_dt is null 
begin 
	set @entitlement_perf_dt = convert(date,getdate()) 
end 

--Check scan string has gift code pattern
if exists(SELECT 1 where @scan_str like '%[A-Za-z0-9]%-%[A-Za-z0-9]%-%[A-Za-z0-9]')
	Begin

			declare  @gift_id int,					
					@init_dt datetime,
					@expr_dt datetime,
					@entitlement_no int,
					@num_remain int,					
					@cnt int,
					@association_type int,
					@gift_status char(1),
					@num_ent int

			select	@association_type = convert(int, isnull(default_value, '0'))  --4 --action used to close issue, if not auto-closed
			from	dbo.t_defaults
			where	parent_table = 'ENTITLEMENTS'
				and field_name = 'GIFT_PROXY_ASSOCIATION'
			
			select @association_type = coalesce(@association_type,-1)

			select @gift_id = id_key,			
			@init_dt = init_dt,
			@expr_dt = expr_dt,
			@entitlement_no = entitlement_no,
			@customer_no = coalesce(gift_customer_no,b.associated_customer_no, a.customer_no),
			@gift_status = coalesce(a.gift_status,'C')
			from [dbo].[LT_ENTITLEMENT_GIFT] a
			left outer join t_association b on a.customer_no = b.customer_no 
			and b.association_type_id= @association_type
			where gift_code=@scan_str

			
			
			if @gift_id is null
			begin
				select @status ='Gift code scanned is not valid.'
				select	@stat_code = 1	
				goto gift_return
			end
			
			if exists(select 1 from LTR_ENTITLEMENT where entitlement_no =@entitlement_no and allow_nscan='N')
			begin
				select @status ='Gift is not allowed for NSCAN use.'
				select	@stat_code = 1
				goto gift_return
			end

			if GETDATE() not between @init_dt and @expr_dt
			begin
				if  GETDATE()>@expr_dt
					Begin
					select @status ='The Gift has expired.'
					end
				if  GETDATE()<@init_dt
					Begin
					select @status ='The Gift start date not met.'
					end
				select	@stat_code = 1	
				goto gift_return
			end

			--populate working table for possible group gifts
			select b.[id_key]
			  ,b.[gift_code]
			  ,b.[customer_no]
			  ,b.[gift_customer_no]
			  ,b.[gift_recip_email]
			  ,b.[gift_recip_name]
			  ,b.[gift_message]
			  ,b.[gift_status]
			  ,b.[li_seq_no]
			  ,b.[num_items]
			  ,b.[entitlement_no]
			  ,b.[ltx_cust_entitlement_id]
			  ,b.[init_dt]
			  ,b.[expr_dt]
			  ,b.[created_by]
			  ,b.[create_dt]
			  ,b.[last_updated_by]
			  ,b.[last_update_dt]
			  ,b.[custom_screen_ctrl]
			  ,b.[sli_no]
			  ,b.[group_gift_code]
			  ,0 num_used
			into #group_gifts
			from LT_ENTITLEMENT_GIFT a
			Join LT_ENTITLEMENT_GIFT b on a.gift_code = b.group_gift_code 	
			join LTR_ENTITLEMENT c on b.entitlement_no = c.entitlement_no
					and a.id_key =@gift_id
			
			
			


			--Check if gift code is a group gift code with child gift codes
			if(select count(1) from #group_gifts) >1
			Begin
				declare @exhibit_hall_count int =0
				declare @is_group_gift_eh int =0

				select @is_group_gift_eh =1
					from #group_gifts a
					join LTR_ENTITLEMENT b on a.entitlement_no = b.entitlement_no
					and a.id_key =@gift_id
					where b.ent_tkw_id =@exhibit_hall_keyword 

				if @is_group_gift_eh=0
				begin
					select @status ='The group gift must be an exhibit hall.'
					select	@stat_code = 1	
					goto gift_return
				end

				select @exhibit_hall_count = count(1) 
					from #group_gifts a
					join LTR_ENTITLEMENT b on a.entitlement_no = b.entitlement_no
					where b.ent_tkw_id =@exhibit_hall_keyword
				
				--Check if there are exibit halls records
				if @exhibit_hall_count = 0
				begin
					select @status ='There are no exhibit hall entitlements.'
					select	@stat_code = 1	
					goto gift_return
				end

				
				--Check if there is an available performance based on group gift code.
				Select top(1) @perf_no= p.perf_no
				From  [dbo].t_prod_season a (NOLOCK) 
  				  JOIN [dbo].t_production b (NOLOCK) ON a.prod_no = b.prod_no 
				  JOIN [dbo].T_PERF p on a.prod_season_no = p.prod_season_no
  				  JOIN [dbo].tx_inv_tkw c (NOLOCK) ON c.inv_no in (a.prod_season_no, b.prod_no, b.title_no,p.perf_no) 
				  join LTR_ENTITLEMENT d on d.ent_tkw_id = c.tkw				
					and d.entitlement_no = @entitlement_no
					and c.tkw = @exhibit_hall_keyword
				  where getdate() between p.doors_open and p.doors_close 
				  and convert(date,p.perf_dt)  between
				  DATEADD(DAY, 1, EOMONTH(getdate(), -1)) and 
				   EOMONTH(getdate())
				   --and convert(date,p.perf_dt) = @entitlement_perf_dt 
				  --order by p.perf_dt 

				if @perf_no is null
				begin
					select @status ='Performance could not be found for gift.'
					select	@stat_code = 1	
					goto gift_return
				end
				
			
				--get number of used				
				update a set a.num_used =sub.num_used
				from #group_gifts a
				join (select b.id_key,sum(coalesce(c.num_used,0)) num_used
						from LTX_CUST_ENTITLEMENT  a
						join #group_gifts b on a.gift_no = b.id_key
								left join LTX_CUST_ORDER_ENTITLEMENT c
								on a.id_key = c.ltx_cust_entitlement_id
						group by b.id_key) sub on sub.id_key=a.id_key
				
			
				
				declare @new_gift_id int
				--get gift to use
				select top(1) @new_gift_id=id_key , @gift_status = gift_status
				from #group_gifts a
				join LTR_ENTITLEMENT b on a.entitlement_no = b.entitlement_no
				where b.ent_tkw_id =@exhibit_hall_keyword				
				and  num_items>num_used
				and getdate() between init_dt and expr_dt
				order by expr_dt 

				select @gift_id=@new_gift_id
				
				--if gift was not found this means there are no more entitlements left.
				if @gift_id is null
				Begin
					select @status ='The Gift has no more entitlements left.'
					select	@stat_code = 1	
					goto gift_return
				end
				
				if @gift_status ='C'
				begin
					select @accepted='Y'
					update a set a.gift_status = 'A' , a.gift_customer_no = @customer_no
					from LT_ENTITLEMENT_GIFT a
					where a.id_key=@new_gift_id				
				end				

				
				select @gift_code = gift_code
				from LT_ENTITLEMENT_GIFT 
				where id_key=@gift_id
				
				INSERT dbo.LTX_CUST_ORDER_ENTITLEMENT (
					customer_no,				
					ltx_cust_entitlement_id,
					entitlement_no,
					entitlement_date,
					num_used,
					gift_code,
					cust_memb_no
					)
				SELECT 	customer_no,
					a.id_key,
					a.entitlement_no,
					getdate(),
					1,
					@gift_code,
					null			
				FROM  LTX_CUST_ENTITLEMENT a
				join LTR_ENTITLEMENT b on a.entitlement_no = b.entitlement_no
				where a.customer_no = @customer_no
				and a.gift_no = @gift_id
				and case when b.gift_level ='S'  and not exists (
					select 1
					from LTX_CUST_ENTITLEMENT  a
					left join LTX_CUST_ORDER_ENTITLEMENT b 
					on a.id_key = b.ltx_cust_entitlement_id
					where gift_no = @gift_id
				) then 1 else 0 end=0
			
					select @stat_code=0
					select @status ='OK Gift has been scanned.'
					
					goto gift_return
			end

			
			Select top(1) @perf_no= p.perf_no
			From  [dbo].t_prod_season a (NOLOCK) 
  			  JOIN [dbo].t_production b (NOLOCK) ON a.prod_no = b.prod_no 
			  JOIN [dbo].T_PERF p on a.prod_season_no = p.prod_season_no
  			  JOIN [dbo].tx_inv_tkw c (NOLOCK) ON c.inv_no in (a.prod_season_no, b.prod_no, b.title_no,p.perf_no) 
			  join LTR_ENTITLEMENT d on d.ent_tkw_id = c.tkw
				and d.entitlement_no = @entitlement_no
			  where getdate() between p.doors_open and p.doors_close 
			  --and convert(date,p.perf_dt) = @entitlement_perf_dt 
			  --order by p.perf_dt 
--
--			The above added 042619 J Smillie to accommodate requirement to scan into the EH performance only 
--		
			if @perf_no is null
			begin
				select @status ='Performance could not be found for gift.'
				select	@stat_code = 1	
				goto gift_return
			end
		
			select @gift_code = gift_code
				from LT_ENTITLEMENT_GIFT 
				where id_key=@gift_id

			--Accept the gift If not already accpeted
			if @gift_status ='C'
			begin
				select @accepted='Y'
				update LT_ENTITLEMENT_GIFT set gift_status = 'A' , gift_customer_no = @customer_no
				where id_key = @gift_id
			end

			
			/*select @cnt  =sum(coalesce(num_ent,0))
			FROM  LTX_CUST_ENTITLEMENT a
			where not exists(select 1 from LTX_CUST_ORDER_ENTITLEMENT x where x.ltx_cust_entitlement_id = a.id_key)
			and  a.gift_no = @gift_id*/
			
			declare @num_items int,@num_used int
			select @num_items = sum(b.num_items)
			from LTX_CUST_ENTITLEMENT a
			join LT_ENTITLEMENT_GIFT b on a.id_key = b.ltx_cust_entitlement_id
			and getdate() between b.init_dt and b.expr_dt
			and b.id_key=@gift_id

			select @num_used= sum(coalesce(b.num_used,0)) from LTX_CUST_ENTITLEMENT  a
					left join LTX_CUST_ORDER_ENTITLEMENT b 
					on a.id_key = b.ltx_cust_entitlement_id
			where gift_no = @gift_id


			if((@num_items-@num_used)<1)
			begin
				select @status ='The Gift has no more entitlements left.'
				select	@stat_code = 1	
				goto gift_return
			end
	
		--Get first free entitlment for gift to assign usage.
		INSERT dbo.LTX_CUST_ORDER_ENTITLEMENT (
			customer_no,				
			ltx_cust_entitlement_id,
			entitlement_no,
			entitlement_date,
			num_used,
			gift_code,
			cust_memb_no
			)
		SELECT 	customer_no,
			a.id_key,
			a.entitlement_no,
			getdate(),
			1,
			@scan_str,
			null			
		FROM  LTX_CUST_ENTITLEMENT a
		join LTR_ENTITLEMENT b on a.entitlement_no = b.entitlement_no
		where a.customer_no = @customer_no
		and a.gift_no = @gift_id
		and case when b.gift_level ='S'  and not exists (
			select 1
			from LTX_CUST_ENTITLEMENT  a
			left join LTX_CUST_ORDER_ENTITLEMENT b 
			on a.id_key = b.ltx_cust_entitlement_id
			where gift_no = @gift_id
		) then 1 else 0 end=0
			
			select @stat_code=0
			select @status ='OK Gift has been scanned.'

	
	gift_return:			
		select  @is_gift_code='Y'
		return
	end	
else
	begin 
		select  @is_gift_code='N'
		return
	end
end	




GO


GO

 
grant execute on [LP_NSCAN_PROCESS_ENTITLEMENT_GIFT_CODE] to impusers
go