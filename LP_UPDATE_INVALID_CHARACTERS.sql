USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LP_UPDATE_INVALID_CHARACTERS]
AS

SET NOCOUNT ON

-- This is along the same vein of Tessitura's UP_FIND_INVALID_CHARACTERS. We know invalide characters can show up in 
-- fields. This is to clean those up that we have encountered. 

-- 1. TX_CUST_KEYWORD
-- Values in key_value (which should be an INT) sometimes has a CR or LF with it. 
-- WO 88730 
-- Removing CR/LF
UPDATE dbo.TX_CUST_KEYWORD
SET key_value = REPLACE(REPLACE(key_value,CHAR(10),''),CHAR(13),'')
WHERE keyword_no = 417
AND ISNUMERIC(key_value) = 0