USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_CUSTOMER_RANK]    Script Date: 5/7/2021 2:45:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LP_CUSTOMER_RANK]
(
	@customer_no INT
)
AS
SET NOCOUNT ON  -- added auto CWR 8/9/2001
 
/***********************************************************************************************
2/28/00 by CWR
 
Designed to autmatically rank constituents.  Currently called from t_customer update trigger,
this can be called from any place.  Code for other automatic ranking schemes can also be placed
here
 
This version customized for Museum of Science by Jonathan Smillie, Tessitura Network 
Most recent modifications: April 13, 2016
Ranking based on membership organization and level to determin access to allocations for 
public events. 
Modified 4/17/2006 by RAP - use VX_CONST_CUST to filter inactive constituencies
Modified 11/20/2006 by SEB - use VX_CONST_CUST_ACTIVE instead of incorrect conversion view
 
***********************************************************************************************/
--RETURN -- Comment this out to use this procedure.

IF @customer_no IS NULL
	RETURN

-- **** ORIGINAL CODE - ADD REGISTRATION RANKING (RANK_TYPE = 3) **** 

DECLARE @ranking INT = 0
DECLARE	@rank_type INT = 3 -- Other rank types can be added
 
SELECT @ranking = MAX(rnking)
FROM 
(	-- ranking based on membership levels
	SELECT 	
		CASE 
			WHEN    memb_org_no = 5 AND memb_level IN ('ADP') THEN 1100 
			WHEN	memb_org_no = 24 AND memb_level IN ('LDP') THEN 1100
			WHEN    memb_org_no = 5 AND memb_level IN ('ADG') THEN 1000 
			WHEN    memb_org_no = 5 AND memb_level IN ('ADS') THEN 900 
			WHEN    memb_org_no = 5 AND memb_level IN ('ADB') THEN 800 
			WHEN    memb_org_no = 5 AND memb_level IN ('AEE') THEN 700 
			WHEN	memb_org_no = 5 AND memb_level IN ('AEM') THEN 600 
			WHEN	memb_org_no = 5 AND memb_level IN ('AEK') THEN 500 
			WHEN	memb_org_no = 4 AND memb_level IN ('P2','P5','P8') THEN 400 
			WHEN	memb_org_no = 4 AND memb_level IN ('B2','B5','B8') THEN 300 
			-- H. Sheridan, SCTASK0002210 - Add the new Staff and Volunteers Membership
			WHEN    memb_org_no = 27 AND memb_level IN ('FTS','NBS','PTS','ST2') THEN 300
			WHEN	memb_org_no = 10 AND memb_level IN ('EP') THEN 200 
			WHEN	memb_org_no = 13 AND memb_level IN ('TPP') THEN 100
		END AS rnking
	FROM VXS_CUST_MEMBERSHIP
	WHERE customer_no = @customer_no AND cur_record = 'Y' 
	UNION
	-- ranking based on constituency
	SELECT 
		CASE 
			WHEN constituency = 1 THEN 1500 -- Board Member
			WHEN constituency = 11 THEN 750 -- Innovator
		END	AS rnking
	FROM dbo.TX_CONST_CUST
	WHERE customer_no = @customer_no
) X


DELETE dbo.T_CUST_RANK
WHERE customer_no = @customer_no AND rank_type = @rank_type
 
IF @ranking > 0			-- 0 is the default ranking so we don't need to write these records
	INSERT dbo.T_CUST_RANK(customer_no, rank_type, rank)
	VALUES(@customer_no, @rank_type, @ranking)

-- **** WO 88973 - ADD ERC RANKING (RANK_TYPE = 7) **** 

SET @ranking = 0
SET @rank_type = 7 -- ERC Ranking

SELECT @ranking = 50 
FROM VXS_CUST_MEMBERSHIP
WHERE customer_no = @customer_no AND cur_record = 'Y' AND memb_org_no = 13  AND memb_level IN ('TPP') 

DELETE dbo.T_CUST_RANK
WHERE customer_no = @customer_no AND rank_type = @rank_type
 
IF @ranking > 0			-- 0 is the default ranking so we don't need to write these records
	INSERT dbo.T_CUST_RANK(customer_no, rank_type, rank)
	VALUES(@customer_no, @rank_type, @ranking)

RETURN

GO

