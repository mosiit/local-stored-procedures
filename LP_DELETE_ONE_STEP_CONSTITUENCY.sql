USE [impresario];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

DROP PROCEDURE [dbo].LP_DELETE_ONE_STEP_CONSTITUENCY
GO

CREATE PROCEDURE [dbo].LP_DELETE_ONE_STEP_CONSTITUENCY
AS 
-- =============================================
-- Author:		Aileen Duffy-Brown
-- Create date: 11/14/2017
-- Description:	Looks for expired household memberships with a one-step constituency & deletes the one-step constituency.  
-- Should be run after 7:45 am on the first of the month.
-- TrackIt! Work Order # 87895
-- Updated 1/24/2018 to exclude one month instead of two months of expired memberships.
-- =============================================
      SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
      SET NOCOUNT ON;

      BEGIN

            DECLARE @temp_expr_one_step TABLE (
                     customer_no INT,
                     constituency INT,
                     expr_dt DATETIME
                    );

            INSERT  INTO @temp_expr_one_step
                    (
                     customer_no,
                     constituency,
                     expr_dt
                    )
                    SELECT  memb.customer_no,
                            cc.constituency,
                            MAX(memb.expr_dt)
                    FROM    dbo.TX_CUST_MEMBERSHIP memb
                    INNER JOIN dbo.TX_CONST_CUST cc
                    ON      cc.customer_no = memb.customer_no
                    WHERE   memb.customer_no NOT IN (SELECT DISTINCT
                                                            customer_no
                                                     FROM   dbo.TX_CUST_MEMBERSHIP
                                                     WHERE  current_status IN (2, 3) -- active, pending
                                                            AND memb_org_no = 4)
                            AND cc.constituency = 30 -- one step
                            AND memb.memb_org_no = 4 -- household
                    GROUP BY memb.customer_no,
                            cc.constituency
                    HAVING  MAX(memb.expr_dt) <= EOMONTH(GETDATE(), -1)
                    ORDER BY memb.customer_no;
	-- 5599

            DELETE  cc
            FROM    dbo.TX_CONST_CUST cc
            INNER JOIN @temp_expr_one_step x
            ON      cc.customer_no = x.customer_no
                    AND x.constituency = cc.constituency


      END;
