USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LRP_INITIALIZE_CUST_ENTITLEMENTS]    Script Date: 11/21/2018 4:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LRP_INITIALIZE_CUST_ENTITLEMENTS]

@create_dt DATETIME = NULL,
@customer_no int = NULL

/* 
Internal Scheduled Procedure to initialize customer-level entitlements for view

* Only running off current records 
* Don't want it to run for memberships created a while ago, current entitlements in LTR_ENTITLEMENT may not apply.
* More or less the same code as in order-save, so it should match this anyway.  
* Only runs off M-level entitlements.

MIR 4/10/2015

Sample execution:
EXEC dbo.LRP_INITIALIZE_CUST_ENTITLEMENTS @create_dt = '1/1/2001'

Modified 4/22/2015 MIR handles UPGRADES made to the membership: removes old references in cust membership and 
Modified 6/2/2015 MIR runs on all current memberships (not date specific) in the first section. Upgrades still respects current date.
Modified 10/21/2016 TAH (TN) added @customer_no so that this could be called from LP_PROCESS_TICKETING_MEMBERSHIPS and add the 
						entitlements at the time the membership is created for one customer

Modified  11/9/2016 TAH (TN) added pending status so that pending membs will get entitlements
Modified  5/31/2017 JTS (TN) changed logic to eliminate create_dt evaluation when calculating upgrade entitlements 
because unless upgrades are done at exactly the time of sale, the create_dt will never match 
*/

AS

SET NOCOUNT ON


IF NULLIF(@create_dt,'') IS NULL
	SET @create_dt = CAST(CONVERT(VARCHAR(10),CURRENT_TIMESTAMP,101) AS DATETIME)

--SELECT @create_dt

INSERT LTX_CUST_ENTITLEMENT (customer_no, cust_memb_no, entitlement_no, entitlement_date, num_ent)
SELECT
	cm.customer_no,
	cm.cust_memb_no,
	e.entitlement_no, 
	NULL,
	MAX(e.num_ent) /* I feel like this should be MAX not SUM, though there shouldn't be multiple rows for the same ent */
FROM LTR_ENTITLEMENT e
	JOIN dbo.T_MEMB_LEVEL ml ON ml.memb_level_no = e.memb_level_no
	JOIN dbo.TX_CUST_MEMBERSHIP cm ON (ml.memb_org_no = cm.memb_org_no AND ml.memb_level = cm.memb_level)
	LEFT JOIN LTX_CUST_ENTITLEMENT ce ON  /* does not already exist */
	(ce.customer_no = cm.customer_no
	AND ce.cust_memb_no = cm.cust_memb_no
	AND ce.entitlement_no = e.entitlement_no)
WHERE ce.customer_no is null
AND e.reset_type = 'M'
AND (cm.cur_record = 'Y' or  cm.current_status = 3)  -- Current or pending 
AND (cm.customer_no = @customer_no or @customer_no is null)
/* AND cm.create_dt >= @create_dt commented out 6/2/2015 */
GROUP BY cm.customer_no, cm.cust_memb_no, e.entitlement_no				



/* FOR UPGRADES: gather outdated cust entitlements for the same cust_memb_no */
SELECT
	ce.id_key,
	cm.cust_memb_no, 
	old_memb_level_no = e.memb_level_no,
	new_memb_level_no = ml2.memb_level_no,
	old_entitlement_no = ce.entitlement_no, 
	new_entitlement_no = CAST(NULL AS INT),
	ce.entitlement_date, 
	old_num_ent = ce.num_ent,
	new_num_ent = CAST(NULL AS INT),
	new_id_key =CAST(NULL AS INT)
INTO #old_entitlements
FROM LTR_ENTITLEMENT e
	JOIN dbo.T_MEMB_LEVEL ml ON ml.memb_level_no = e.memb_level_no
	LEFT JOIN LTX_CUST_ENTITLEMENT ce ON ce.entitlement_no = e.entitlement_no
	LEFT JOIN TX_CUST_MEMBERSHIP cm
		JOIN dbo.T_MEMB_LEVEL ml2 ON (ml2.memb_org_no = cm.memb_org_no AND ml2.memb_level = cm.memb_level)
	ON cm.cust_memb_no = ce.cust_memb_no
WHERE e.reset_type = 'M'
AND (cm.cur_record = 'Y' or cm.current_status = 3) -- current or pending
AND cm.memb_level <> ml.memb_level /* different level */
and (cm.customer_no = @customer_no or @customer_no is null) --AND cm.create_dt >= @create_dt
/* equate old ents to new ents by our primary key (minus the level) */
UPDATE t
SET new_entitlement_no = ne.entitlement_no,
	new_num_ent = ne.num_ent
FROM #old_entitlements t
	JOIN dbo.LTR_ENTITLEMENT oe ON t.old_memb_level_no = oe.memb_level_no
	JOIN dbo.LTR_ENTITLEMENT ne ON t.new_memb_level_no = ne.memb_level_no
WHERE t.old_entitlement_no = oe.entitlement_no
AND oe.ent_tkw_id = ne.ent_tkw_id
AND oe.reset_type = ne.reset_type
AND oe.ent_price_type_group = ne.ent_price_type_group

/* we could CHECK here that the new # of ents is higher than the old # to prevent going negative */


	declare @id_key int
	DECLARE entitlements CURSOR  
    FOR
	select  distinct ce.id_key
	FROM dbo.LTX_CUST_ENTITLEMENT ce
	JOIN #old_entitlements oe ON ce.id_key = oe.id_key 
	OPEN entitlements  
	FETCH NEXT FROM entitlements into  @id_key;  
	WHILE @@FETCH_STATUS = 0  
	BEGIN 
		BEGIN TRY
		
		DELETE ce
		FROM dbo.LTX_CUST_ENTITLEMENT ce
			JOIN #old_entitlements oe ON ce.id_key = oe.id_key
			and oe.id_key=@id_key

		UPDATE coe
		SET entitlement_no = oe.new_entitlement_no
		FROM LTX_CUST_ORDER_ENTITLEMENT coe
			JOIN #old_entitlements oe ON (oe.old_entitlement_no = coe.entitlement_no AND oe.cust_memb_no = coe.cust_memb_no)
		where oe.id_key=@id_key
				FETCH NEXT FROM entitlements into @id_key ;
		end try
		begin catch
			delete from lt_failed_entitlement_upgrades where ent_id_key=@id_key
			insert into lt_failed_entitlement_upgrades
			select @id_key
					FETCH NEXT FROM entitlements into @id_key ;
		end catch

	end
	CLOSE entitlements;  
	DEALLOCATE entitlements; 	

/* drop the old one*/
--DELETE ce
--FROM dbo.LTX_CUST_ENTITLEMENT ce
--	JOIN #old_entitlements oe ON ce.id_key = oe.id_key

--/* Update the order entitlement history entitlement IDs*/	
--UPDATE coe
--SET entitlement_no = oe.new_entitlement_no
--FROM LTX_CUST_ORDER_ENTITLEMENT coe
--	JOIN #old_entitlements oe ON (oe.old_entitlement_no = coe.entitlement_no AND oe.cust_memb_no = coe.cust_memb_no)

