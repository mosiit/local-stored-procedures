USE [impresario]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_SchoolProgramsOffSale]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_SchoolProgramsOffSale]
GO

CREATE PROCEDURE [dbo].[LP_SchoolProgramsOffSale]
       @start_date datetime = Null,
       @return_message varchar(100) = Null OUTPUT
AS BEGIN

    DECLARE @title_no int, @perf_type_school int, @rtn varchar(100), @log_date datetime
    DECLARE @perf_no int, @perfCode varchar(10), @perf_day varchar(20), @perf_date char(10), @perf_time char(8), @prod_name varchar(30), @title_name varchar(30), @tix_sold int
    
    DECLARE @perfs_to_change table ([performance_no] int, [performance_code] varchar(10), [DayOfWeek] varchar(25), [performance_date] char(10), [performance_time] varchar(8), [production_name] varchar(30), [tix_sold] int)

    SELECT @return_message = '', @log_date = getdate()
    SELECT @start_date = IsNull(@start_date, getdate())

    /*  Determine performance type id for school only performances  */

    SELECT @perf_type_school = [id] FROM [dbo].[TR_PERF_TYPE] WHERE [description] = 'School Only'
    SELECT @perf_type_school = IsNull(@perf_type_school, 0)

    IF @perf_type_school <= 0 BEGIN
        SELECT @return_message = 'unable to determine school perf_type_no'
        GOTO DONE
    END

    DECLARE program_cursor INSENSITIVE CURSOR FOR
    SELECT [performance_no], [performance_code], datename(weekday,[performance_dt]), [performance_date], [performance_time], [production_name], [title_name]
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
    WHERE [performance_dt] between @start_date and dateadd(week,2,@start_date) 
      and [title_no] in (SELECT [title_no] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [title_name] in ('Cahners Theater', 'Current Science & Technology', 'Science Live! Stage'))
    OPEN program_cursor
    BEGIN_PROGRAM_LOOP:

        SELECT @rtn = null

        FETCH NEXT FROM program_cursor INTO @perf_no, @perfCode, @perf_day, @perf_date, @perf_time, @prod_name, @title_name
        IF @@FETCH_STATUS = -1 GOTO END_PROGRAM_LOOP

        EXECUTE [dbo].[LP_SS_GetPerformanceInfo] @perf_no = @perf_no, @perf_time = @perf_time,  @suppress_dataset = 'Y', @TixSold = @tix_sold OUTPUT

        IF @tix_sold > 0 GOTO BEGIN_PROGRAM_LOOP

        /*  Disable the performance/zone if there are no tickets reserved for it  */
        
        EXECUTE [dbo].[LP_EnableDisableZone] @perf_no = @perf_no, @perf_time = @perf_time, @is_enabled = 'N', @force_disable = 'N', @return_message = @rtn OUTPUT
        IF @rtn = 'success' SELECT @rtn = 'Perf no:' + CONVERT(VARCHAR(20),@perf_no) + ' (' + @perf_time + ') taken off sale'

        /*  Log the change in case anyone wants to know exactly when the show was changed  */

        INSERT INTO [admin].[dbo].[tessitura_school_program_off_sale_log] ([log_date], [time_stamp], [title_name], [performance_date], [performance_time], [performance_no], [production_name], [rtn_msg])
        VALUES (@log_date, getdate(), @title_name, @perf_date, @perf_time, @perf_no, @prod_name, @rtn)
        
        GOTO BEGIN_PROGRAM_LOOP
        
    END_PROGRAM_LOOP:
    CLOSE program_cursor
    DEALLOCATE program_cursor

    /*  Determine title number for the Planetarium  */
        
    SELECT @title_no = [title_no] FROM LV_PRODUCTION_ELEMENTS_TITLE WHERE title_name = 'Hayden Planetarium'
    SELECT @title_no = IsNull(@title_no, 0)
    
    IF @title_no <= 0 BEGIN
        SELECT @return_message = 'cannot determine title_no for the planetarium'
        GOTO DONE
    END

    /*  Select all school only planeterium performances for the next two weeks into a cursor and evaluate them one at a time  */

    DECLARE planetarium_cursor INSENSITIVE CURSOR FOR
    SELECT [performance_no], [performance_code], datename(weekday,[performance_dt]), [performance_date], [performance_time], [production_name] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
    WHERE [performance_dt] between @start_date and dateadd(week,2,@start_date) and [title_no] = @title_no and [performance_type] = @perf_type_school
    ORDER BY [performance_date], [performance_time]
    OPEN planetarium_cursor
    BEGIN_PLANETARIUM_LOOP:

        FETCH NEXT FROM planetarium_cursor INTO @perf_no, @perfCode, @perf_day, @perf_date, @perf_time, @prod_name
        IF @@FETCH_STATUS = -1 GOTO END_PLANETARIUM_LOOP

        /*  Use LP_SS_GetPerformanceInfo to determine if any tickets have been sold for this performance  */

        EXECUTE [dbo].[LP_SS_GetPerformanceInfo] @perf_no = @perf_no, @perf_time = @perf_time,  @suppress_dataset = 'Y', @TixSold = @tix_sold OUTPUT
        
        IF @tix_sold > 0 GOTO BEGIN_PLANETARIUM_LOOP
        
        /*  Disable the performance/zone if there are no tickets reserved for it  */
        
        EXECUTE [dbo].[LP_EnableDisableZone] @perf_no = @perf_no, @perf_time = @perf_time, @is_enabled = 'N', @force_disable = 'N', @return_message = @rtn OUTPUT
        IF @rtn = 'success' SELECT @rtn = 'Perf no:' + CONVERT(VARCHAR(20),@perf_no) + ' (' + @perf_time + ') taken off sale'
        
        /*  Log the change in case anyone wants to know exactly when the show was changed  */

        INSERT INTO [admin].[dbo].[tessitura_school_program_off_sale_log] ([log_date], [time_stamp], [title_name], [performance_date], [performance_time], [performance_no], [production_name], [rtn_msg])
        VALUES (@log_date, getdate(), 'Hayden Planetarium', @perf_date, @perf_time, @perf_no, @prod_name, @rtn)

        GOTO BEGIN_PLANETARIUM_LOOP

    END_PLANETARIUM_LOOP:
    CLOSE planetarium_cursor
    DEALLOCATE planetarium_cursor

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:
    
        --DELETE LOG RECORDS MORE THAN A YEAR OLD
        DELETE FROM [admin].[dbo].[tessitura_school_program_off_sale_log] WHERE datediff(day,[log_date],getdate()) >= 365

        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_SchoolProgramsOffSale] to impusers
GO


/*
--THE FOLLOWING CODE WILL PUT BACK ON SALE ANY SCHOOL PROGRAM TAKEN OFF SALE DURING A SPECIFIC SESSION
--CHECK THE tessitura_school_program_off_sale_log TABLE AND FIND THE LOG_DATE VALUE FOR THE SESSION YOU WANT REVERSED
--CHANGE THE SELECT @LOG_DATE = COMMAND TO BE THAT LOG DATE, THEN HIGHLIGHT EVERYHING BETWEEN /* AND */ AND RUN


DECLARE @log_date datetime       SELECT @log_date = '2016-08-30 13:14:47.263'
DECLARE @perf_no int, @perf_time varchar(8), @rtn varchar(100)
DECLARE on_sale_cursor INSENSITIVE CURSOR FOR
SELECT [performance_no], [performance_time] FROM [admin].[dbo].[tessitura_school_program_off_sale_log] WHERE [log_date] = @log_date
OPEN on_sale_cursor
BEGIN_ON_SALE_LOOP:
    
    SELECT @rtn = null

    FETCH NEXT FROM on_sale_cursor INTO @perf_no, @perf_time
    IF @@FETCH_STATUS = -1 GOTO END_ON_SALE_LOOP

    EXECUTE [dbo].[LP_SS_EnableDisableZone] @perf_no = @perf_no, @perf_time = @perf_time, @is_enabled = 'Y', @return_message = @rtn OUTPUT
    SELECT @perf_no, @perf_time, @rtn

    GOTO BEGIN_ON_SALE_LOOP

END_ON_SALE_LOOP:
CLOSE on_sale_cursor
DEALLOCATE on_sale_cursor
*/


--DECLARE @rtn varchar(100)
--EXECUTE  [dbo].[LP_SchoolProgramsOffSale] @start_date = '10-1-2016', @return_message = @rtn OUTPUT  PRINT @rtn
--SELECT * FROM [admin].[dbo].[tessitura_school_program_off_sale_log] WHERE log_date = (SELECT max(log_date) FROM [admin].[dbo].[tessitura_school_program_off_sale_log])
--TRUNCATE TABLE [admin].[dbo].[tessitura_school_program_off_sale_log]

DECLARE @rtn varchar(100)
EXECUTE  [dbo].[LP_SchoolProgramsOffSale] @return_message = @rtn OUTPUT  PRINT @rtn
SELECT * FROM [admin].[dbo].[tessitura_school_program_off_sale_log] WHERE log_date = (SELECT max(log_date) FROM [admin].[dbo].[tessitura_school_program_off_sale_log])

--SELECT * FROM T_SUB_LINEITEM WHERE perf_no in (11984, 12083, 11986,11984, 11987,12085)


