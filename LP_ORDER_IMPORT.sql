USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_ORDER_IMPORT]    Script Date: 7/14/2016 10:08:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_ORDER_IMPORT](
		@session_id INT,
		@run_mode INT,				-- 1=Import, 2=Review, 3=Reprint Session Output
		@mode CHAR(1),				-- (P)re-process, (B)efore, (A)fter
		@order_no INT
	)
	AS
	/*	This procedure is used for local processing during Order Import. 

		The Run Mode and Session ID are always passed in.

		This procedures is called at various points during the import process, indicated by @mode:

		(P)re-process - This mode indicates that the procedure is being called before any error checking
						occurs on the imported data.  The data is in TW_ORDER_IMPORT marked with the passed
						in session_no. 

		(B)efore	  - This mode indicates that the procedure is being called for a specific order (@order_no)
						before beginning to insert it, inside the transaction. Only called when run_mode = 1 (inserting)

		(A)fter		  - This mode indicates that the procedure is being called for a specific order (@order_no)
						after it has been inserted, inside the transaction. Only called when run_mode = 1 (inserting)

	*/

	-- You likely don't want to do anything in reprint session output mode.
	If @run_mode = 3
		RETURN

	-- 20160713, H. Sheridan - Added code per Tessitura help desk (Boann Petersen) to try to remove barrier to importing
	If @run_mode = 1
		RETURN

	-- Local code goes here:
	--
	-- local_use0 
	-- Copy school/org contact info into order custom field 1 
	-- 
	update b 
	set b.custom_1 = w.local_use0
	from t_order b join tw_order_import w
	on b.order_no = w.order_no  
	where w.order_no = @order_no 
	and w.session_id = @session_id 
	and @mode = 'A' 
	--
	-- local_use1
	-- Copy school/org billing info into order custom field 2
	-- 
	update b 
	set b.custom_2 = w.local_use1
	from t_order b join tw_order_import w
	on b.order_no = w.order_no  
	where w.order_no = @order_no 
	and w.session_id = @session_id 
	and @mode = 'A'
	--
	-- local_use2 
	-- Copy TP location info into order custom field 3
	-- 
	update b 
	set b.custom_3 = w.local_use2
	from t_order b join tw_order_import w
	on b.order_no = w.order_no  
	where w.order_no = @order_no 
	and w.session_id = @session_id 
	and @mode = 'A'
	--
	-- local_use4 
	-- Copy TP program code info into order custom field 4
	-- 
	UPDATE b 
	SET b.custom_4 = w.local_use4
	FROM t_order b JOIN tw_order_import w
	ON b.order_no = w.order_no  
	WHERE w.order_no = @order_no 
	AND w.session_id = @session_id 
	AND @mode = 'A'
	-- 
	-- local_use5
	-- Add attribute for organization context
	-- 
	INSERT tx_cust_keyword 
	(customer_no,keyword_no,key_value) 
	SELECT DISTINCT(b.customer_no), '415',b.local_use5
	FROM TW_ORDER_IMPORT b JOIN tx_cust_keyword a 
	ON a.customer_no = b.customer_no 
	WHERE ((order_no = @order_no 
	AND session_id = @session_id 
	AND @mode = 'A') AND b.customer_no NOT IN (SELECT customer_no FROM tx_cust_keyword 
	 WHERE customer_no = b.customer_no AND keyword_no = 415 AND key_value = b.local_use5) ) 
	-- 
	-- local_use6
	-- Copy TP program code info into order custom field 4
	-- 
	UPDATE b 
	SET b.custom_5 = w.local_use6
	FROM t_order b JOIN tw_order_import w
	ON b.order_no = w.order_no  
	WHERE w.order_no = @order_no 
	AND w.session_id = @session_id 
	AND @mode = 'A'
-- 
	-- local_use8
	-- Copy TP discount info into order custom field 7
	-- 
	UPDATE b 
	SET b.custom_7 = w.local_use8
	FROM t_order b JOIN tw_order_import w
	ON b.order_no = w.order_no  
	WHERE w.order_no = @order_no 
	AND w.session_id = @session_id 
	AND @mode = 'A'

	--
	--
	--
	--if @mode = 'A'  
	--begin 
	--exec [dbo].[LP_PARSE_CONTACT_AFFILIATION]
	--end 
	RETURN

GO


