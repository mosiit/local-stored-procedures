USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_TICKET_ELEMENTS](
	@ude_no			INT 	= NULL,  		-- 1-6
	@li_seq_no 		INT	= NULL,
	@cur_sli_no		INT 	= NULL,
	@payment_no		INT	= NULL,
	@print_unprinted 	CHAR(1) = NULL,
	@reprint_printed 	CHAR(1) = NULL,
	@sli_req_no		INT	= NULL,		
	@au_set_no		INT 	= NULL,
	@order_no  		INT	= NULL,
	@customer_no		INT 	= NULL,
	@design_no		INT 	= NULL,
	@ticket_no	INT = NULL,
	@composite_ticket_no	INT = NULL,
	@design_type		CHAR(1),			-- (T)icket, (H)eader, (R)eceipt, (F)orm
	@ude_value		VARCHAR(200) = '' OUTPUT		-- user defined element value
	)
AS

SET NOCOUNT ON

/********************************************************************************************************************************************
New Localized Procedure CWR 12/6/2004, designed to be called from ticket formatting procedures.  Allows the use of customized ticket
formatting elements.  All values must be returned as strings, formatted the way you want them to appear on the ticket

Modified CWR 4/18/2014 #8321.  Added additional parameters @ticket_no and @composite_ticket_no
Modified CWR 6/30/2014 #8730.  Expanded output parameter to 200 characters

Modified 6/28/2017 CCastilla/TN
For ude 1, for design type Header (H), insert into local table that audits header printing. Intended to audit group admission header tickets.
Currently the header design Group MOS Ticket (2098) contains ude1-5, so this logic can exist in any of those blocks. Ticket #8983

Modified 11/28/2017 CCastilla/TN
For ude 1, for design type Header (H), added if else for refund slip design numbers to pull most recent refund payment amount
********************************************************************************************************************************************/

-- comment this if you want the procedure to actually do something
-- Return


-- code for an element to produce a membership level based on constituency and membership org/level 
-- modified 022816 Jonathan Smillie, Tessitura Tech Services 
-- adding logic to pull Teacher Partner level (memb_org_no = 13) 

if @ude_no = 1 and @customer_no > 0 and @design_type = 'T'  
 select @ude_value = 
  case	when a.memb_org_no = 5 and a.memb_level in ('DI','ADP','ADG','ADS','ADB') and b.constituency = 37 then c.description 
		when a.memb_org_no = 5 and a.memb_level in ('AE','AEE','AEM','AEK') and b.constituency = 38 then c.description 
		when a.memb_org_no = 4 and a.memb_level in ('B2','B5','B8','P2','P5','P8') and b. constituency in (31,32,33,34,35,36) THEN c.description 
		when a.memb_org_no = 10 and a.memb_level = 'EP' and b.constituency = 40 then c.description	
		when a.memb_org_no = 13 and a.memb_level = 'TPP' then x.description																																														
																																													
   end
   from VXS_CUST_MEMBERSHIP a join vxs_const_cust b on a.customer_no = b.customer_no 
   join TR_CONSTITUENCY c on b.constituency = c.id 
   left join t_memb_level x on a.memb_level = x.memb_level
    where a.customer_no = @customer_no and a.cur_record='Y'  
    
--if @ude_no = 1 and @customer_no > 0 and @design_type = 'T'  
-- select @ude_value = 
--  case	when a.memb_org_no = 5 and a.memb_level in ('DI','ADP','ADG','ADS','ADB') and b.constituency = 37 then c.description 
--		when a.memb_org_no = 5 and a.memb_level in ('AE','AEE','AEM','AEK') and b.constituency = 38 then c.description 
--		when a.memb_org_no = 4 and a.memb_level in ('B2','B5','B8','P2','P5','P8') and b. constituency in (31,32,33,34,35,36) THEN c.description 
--		when a.memb_org_no = 10 and a.memb_level = 'EP' and b.constituency = 40 then c.description																																														
--   end
--   from VXS_CUST_MEMBERSHIP a join vxs_const_cust b on a.customer_no = b.customer_no 
--   join TR_CONSTITUENCY c on b.constituency = c.id 
--    where a.customer_no = @customer_no and a.cur_record='Y' 
    


-- Code for a temporary membership card that's valid for two weeks from today.

If @ude_no = 2 And @customer_no > 0 AND @design_type = 'T' 
	Select @ude_value =
		'T' +		--Leading character: T for temp card
		Right('0000000' + Convert(varchar,@customer_no),7) + --Turn the customer number into a ten character string
		Convert(varchar,Dateadd(day,14,getdate()),112) --Today's date + 14 days, in yyyymmdd format


-- Code for a temporary membership card that's valid for 24 hours.
If @ude_no = 3 And @customer_no > 0 and @design_type = 'T'
	Select @ude_value =
		'T' +		--Leading character: T for temp card
		Right('0000000' + Convert(varchar,@customer_no),7) + --Turn the customer number into a ten character string
		Convert(varchar,getdate(),112) --Today's date, in yyyymmdd format 

-- element #4 for tickets only 
-- prints price type category 
-- for use instead of price type name 

if @ude_no = 4 and @design_type = 'T' 
	select @ude_value = 
	  b.description 
from tr_price_type_category b join tr_price_type a 
on a.price_type_category = b.id
join t_sub_lineitem sl 
on a.id = sl.price_type  
where sl.sli_no = @cur_sli_no 

-- code added 07/18/2016 by Jonathan 
-- Modifications to make Header cards print summary info for counts of tickets 
-- Added 6/28/2017 CCastilla for ude_no = 1 begin/end and insert into lt_tkt_header_audit
-- Added 11/28/2017 CCastilla for ude_no = 1 if/else before begin/end for most recent refund amount

if @ude_no = 1 and @design_type = 'H' 
 --added 11/28/2017 CCastilla/TN for latest refund
	if @design_no in (2083)	--refund slip

		select 	@ude_value = 
		(
			select top 1 sum(b.pmt_amt)
			from 	[dbo].t_transaction a
					JOIN [dbo].t_payment  b ON a.transaction_no = b.transaction_no and a.sequence_no = b.sequence_no
					JOIN [dbo].tr_payment_method c ON b.pmt_method = c.id
			where	a.order_no = @order_no 
					and b.pmt_amt < 0
			group by 
					b.payment_no
			order by
					b.payment_no desc
		)

	ELSE
 --end of latest refund code

	BEGIN
		insert	into lt_tkt_header_audit
		select	@order_no, @customer_no, @design_no, getdate(), dbo.FS_USER(), dbo.FS_LOCATION()

		SELECT	@ude_value = 
		(
			SELECT	' '+ STUFF((SELECT 
					' ' + CONVERT(VARCHAR,perf_time)+' '+CONVERT(VARCHAR,count_by_perf)+' '     
			FROM	lv_sum_by_perf 
			WHERE	perf_type = 'EX' AND order_no = @order_no 
					FOR XML PATH('')) ,1,1,'') 
		)
	END

if @ude_no = 2 and @design_type = 'H' 
select @ude_value = 
(
SELECT ' '+ STUFF((SELECT 
	   ' ' + convert(varchar,perf_time)+' '+convert(varchar,count_by_perf)+' '     
	      FROM lv_sum_by_perf 
		  where perf_type = 'PL' and order_no = @order_no 
            FOR XML PATH('')) ,1,1,'') 
			)
if @ude_no = 3 and @design_type = 'H' 
select @ude_value = 
(
SELECT ' '+ STUFF((SELECT 
	   ' ' + convert(varchar,perf_time)+' '+convert(varchar,count_by_perf)+' '     
	      FROM lv_sum_by_perf 
		  where perf_type = 'OM' and order_no = @order_no 
            FOR XML PATH('')) ,1,1,'') 
			)
if @ude_no = 4 and @design_type = 'H' 
select @ude_value = 
(
SELECT ' '+ STUFF((SELECT 
	   ' ' + convert(varchar,perf_time)+' '+convert(varchar,count_by_perf)+' '     
	      FROM lv_sum_by_perf 
		  where perf_type = '4D' and order_no = @order_no 
            FOR XML PATH('')) ,1,1,'') 
			)
if @ude_no = 5 and @design_type = 'H' 
select @ude_value = 
(
SELECT ' '+ STUFF((SELECT 
	   ' ' + convert(varchar,perf_time)+' '+convert(varchar,count_by_perf)+' '     
	      FROM lv_sum_by_perf 
		  where perf_type = 'BG' and order_no = @order_no 
            FOR XML PATH('')) ,1,1,'') 
			)
-- Added by DSJ 9/17/19 for Nichols Gallery
if @ude_no = 6 and @design_type = 'H' 
select @ude_value = 
(
SELECT ' '+ STUFF((SELECT 
	   ' ' + convert(varchar,perf_time)+' '+convert(varchar,count_by_perf)+' '     
	      FROM lv_sum_by_perf 
		  where perf_type = 'NI' and order_no = @order_no 
            FOR XML PATH('')) ,1,1,'') 
			)

/** Elements for receipts start here
1 - programmed 9/28/16 by Jonathan Smillie, Tessitura Services 
	Calculates amount due on order from payment receipt - simple calculation based on subtraction of total due amount 
	and total paid amount
	 
**/ 
IF @ude_no = 1 AND @design_type = 'R' 
 SELECT @ude_value = (SELECT SUM(a.tot_due_amt - a.tot_ticket_paid_amt) FROM t_order a 
  WHERE  a.order_no = @order_no AND a.customer_no = @customer_no) 

---- Code for an NScan-formatted customer number for member validation.
---- commenting this one out as I created it just for testing 
--If @ude_no = 6 and @customer_no > 0 and @design_type = 'H' 
-- select @ude_value = 'C'+Right('0000000' + Convert(varchar,@customer_no),10)

--if @ude_no = 6 and @customer_no > 0 and @design_type = 'H' 
-- select @ude_value = 'C'+Right('0000000' + Convert(varchar,@customer_no),10)

--If @ude_no = 5 and @customer_no > 0 
--select @ude_value = 
-- (
-- select 'Adult CP: '+isnull(convert(varchar,Adult),0)+' '+'Child CP: '+isnull(convert(varchar,Child),0) from (
-- select distinct(order_no),b.short_desc as price_type,
-- count(price_type) over (partition by(order_no) order by price_type) as count_price 
-- from t_sub_lineitem a join tr_price_type b 
-- on a.price_type = b.id 
-- where order_no = @order_no and 
-- perf_no in (select perf_no from t_perf where prod_season_no = 7181) 
-- ) as s 
-- pivot 
-- (
-- sum(count_price) for price_type in (CPVouchA,CPVouchC,Child,Adult)
--  ) 
--  as pvt
--  )



/*
--Sample Code:

-- Return the value of a harcoded attribute for user defined element 1
If @ude_no = 1 and @customer_no > 0
	Select 	@ude_value = IsNull(key_value, '')
	From	tx_cust_keyword
	Where	customer_no = @customer_no
		and keyword_no = 3

-- Return membership level for memb org 1 in user defined element 2 only if this is a header
If @ude_no = 2 and @customer_no > 0 and @design_type = 'H'
	Select 	@ude_value = IsNull(memb_level_desc, '')
	From	ft_membership_info(@customer_no, 1, null)

-- Return the value of the mail marketing restriction in user defined element 3
If @ude_no = 3 and @customer_no > 0 
	Select 	@ude_value = IsNull(b.description, '')
	From	t_customer a
		JOIN tr_mail_ind b ON a.mail_ind = b.id
	Where	customer_no = @customer_no

-- Return the date of the last performance attended in user defined element 4 in mm/dd/yyyy format
If @ude_no = 4 and @customer_no > 0 
	Select 	@ude_value = convert(char, performance_dt, 101)
	From	t_tck_hist a
	Where	customer_no = @customer_no

-- Return the sum of giving in the last year in user defined element 5
If @ude_no = 5 and @customer_no > 0 
	Select 	@ude_value = '$' + convert(varchar(30), cont_amt)
	From	t_contribution a
	Where	customer_no = @customer_no
		and cont_dt >= DateAdd(yy, -1, getdate())
*/
GO


