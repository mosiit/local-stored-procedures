USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CreateProductsJSON]    Script Date: 1/13/2020 10:50:07 AM ******/
DROP PROCEDURE [dbo].[LRP_CreateProductsJSON]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CreateProductsJSON]    Script Date: 1/13/2020 10:50:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_CreateProductsJSON]
(
	@NumOfDays INT = 0 -- 0 for today's schedule
	--@XMLOut XML OUTPUT
)

AS
SET NOCOUNT ON 
/*
Created by: D. Jacob 05/02/2016
Purpose: Returns a list of activities plus available capacity ordered by date and time in XML format.
Used by Digital Signage
Pass in 0 for today's schedule 

Modified by:  H. Sheridan, 12/13/2018
Modification: Remove the venue capacity to see if it fixes issues with the today feed.
*/

DECLARE @OutputXML XML 

DECLARE @VenuesForDigitalSignage TABLE 
(
	Venue varchar(100)
)

INSERT INTO @VenuesForDigitalSignage
VALUES
('4-D Theater'),
('Butterfly Garden'),
('Hayden Planetarium'),
('Drop-In Activities'),
('Live Presentations'),
('Mugar Omni Theater'),
('School Lunch'),
-- 20190611, H. Sheridan - Work order 100080 - Add 'Special Exhibitions' to product XML feeds - for Body Worlds
('Special Exhibitions')
--('Thrill Ride 360')

--
-- Populating @prods table with all data needed for products.xml
--
DECLARE @prods TABLE (
	EventDate CHAR(10),
	EventTime VARCHAR(8),
	EventEnd VARCHAR(8),
	Venue VARCHAR(100),
	Location VARCHAR(100),
	SoftCapacity INT,
	Instock INT,
	EventNumber INT,
	MemberOnly INT,
	SchoolOnly INT,
	EventTitleLong VARCHAR(100),
	ItemLength INT
	UNIQUE CLUSTERED (EventDate, EventTime, EventTitleLong, EventNumber, Instock)
)

INSERT INTO @prods
	(EventDate,
	EventTime,
	EventEnd,
	Venue,
	Location,
	SoftCapacity,
	Instock,
	EventNumber,
	MemberOnly,
	SchoolOnly,
	EventTitleLong,
	ItemLength)
SELECT DISTINCT
CONVERT(VARCHAR(10),perf.performance_dt,111) AS EventDate,
	perf.performance_time AS EventTime,
	perf.performance_end_time AS EventEnd,
	CASE perf.title_name 
		WHEN 'Hayden Planetarium' THEN 'Charles Hayden Planetarium'
		ELSE perf.title_name 
	END AS Venue,
	CASE perf.title_name
		WHEN 'Drop-In Activities' THEN perf.performance_location 
		WHEN 'Live Presentations' THEN perf.performance_location 
		WHEN 'Hayden Planetarium' THEN 'Planetarium'
		ELSE perf.title_name
	END	AS location,
	CASE 
		WHEN perf.title_name = 'Drop-In Activities' THEN '30000'
		WHEN perf.title_name = 'Live Presentations' THEN '30000'
		ELSE ISNULL(cap.capacity, 0) 
	END AS SoftCapacity, 
	CASE 
		WHEN perf.title_name = 'Drop-In Activities' THEN '30000'
		WHEN perf.title_name = 'Live Presentations' THEN '30000'
		ELSE ISNULL(cap.available, 0) 
	END AS Instock, 
	perf.production_no AS EventNumber,
	CASE perf.performance_type_name
		WHEN 'Member Event' THEN 1
		ELSE 0
	END AS MemberOnly,
	CASE perf.performance_type_name 
		WHEN 'School Only' THEN 1
		ELSE 0
	END AS SchoolOnly,
	perf.production_name_long AS EventTitleLong,
	DATEDIFF(mi, CAST(perf.performance_time AS DATETIME), CAST(perf.performance_end_time AS DATETIME)) AS ItemLength 
FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE perf
	LEFT JOIN [dbo].[LV_PERFORMANCE_CAPACITY_AVAILABLITY] cap
		ON cap.production_no = perf.production_no
		AND cap.performance_date = perf.performance_date
		AND cap.performance_time = perf.performance_time
WHERE DATEDIFF(d, GETDATE(), performance_dt) >= 0 AND DATEDIFF(d, GETDATE(), performance_dt) <= @NumOfDays
	AND perf.performance_name_long <> ''
	AND perf.title_name IN (SELECT Venue FROM @VenuesForDigitalSignage)
	AND (perf.performance_type_name <> 'Buyout' AND perf.production_name NOT LIKE '%Buyout%')
	AND perf.is_generic_title = 'N' 

--select 'debug', * from @prods

UPDATE @prods
SET EventTitleLong = REPLACE(REPLACE(REPLACE(EventTitleLong, '&amp;', '&'), NCHAR(8211), '-'),NCHAR(8212), '-')
-- NCHAR(8211) is en dash
-- NCHAR(8212) is em dash

--SELECT 'prods', * FROM @prods
--
-- Populting venues table with all distinct venue + memberonly + softcapacity options
-- 

DECLARE @venues TABLE	(
	VenueId				INT IDENTITY(1,1),
	Venue				VARCHAR(100),
	MemberOnly			INT
	--SoftCapacity		INT
)

INSERT INTO @venues
(
	Venue,
	MemberOnly
	--SoftCapacity
)
SELECT DISTINCT 
	Venue,	
	MemberOnly
	--SoftCapacity
FROM @prods 

--SELECT 'venues', * FROM @venues

--
-- Getting all shows available in each venue
-- 

DECLARE @shows TABLE	
(
	showID	INT IDENTITY, 
	venueID INT,
	EventTitleLong		VARCHAR(500),
	Location			VARCHAR(100),
	EventNumber			INT,
	ItemLength			INT
)

INSERT INTO @shows
(
	v.VenueID,
	EventTitleLong,
	Location,
	EventNumber,
	ItemLength
)
SELECT	DISTINCT 
	v.Venueid,
	p.EventTitleLong,
	p.Location,
	p.EventNumber,
	p.ItemLength
FROM    @prods p
INNER JOIN	@venues v
	ON	p.Venue = v.Venue
	AND	 p.MemberOnly = v.MemberOnly
	--AND p.SoftCapacity = v.SoftCapacity

--SELECT 'shows', * FROM @shows

-- 20200114, H. Sheridan - add table of unique dates
DECLARE @feedDates TABLE	
(
	EventDate VARCHAR(10)
)

INSERT INTO @feedDates
	SELECT DISTINCT EventDate FROM @prods

--
-- Getting all unique show dates for each venue/Show combination
--
DECLARE @showDates TABLE	
(
	VenueId	INT,
	ShowId INT,
	EventDate VARCHAR(10)
)

INSERT INTO @showDates
(
	VenueId,
	ShowId,
	EventDate
)
SELECT	DISTINCT 
	v.VenueId,
	s.ShowID,
	p.EventDate
FROM    @prods p
INNER JOIN	@venues v
	ON	p.Venue = v.Venue
	AND	 p.MemberOnly = v.MemberOnly
--	--AND p.SoftCapacity = v.SoftCapacity
INNER JOIN @shows s
	ON s.EventTitleLong = p.EventTitleLong
	AND s.Location = p.Location
	AND s.EventNumber = p.EventNumber
	AND s.ItemLength = p.ItemLength

--SELECT 'showDates', * FROM @showDates

--SELECT 'feedDates', * FROM @feedDates

--
-- Getting all unique show times for each venue/Show/Date combination
--
DECLARE @showTimes TABLE	
(
	VenueId	INT,
	ShowId INT,
	EventDate VARCHAR(10),
	EventTime VARCHAR(8),
	EventEnd VARCHAR(8),
	SchoolOnly INT,
	InStock INT
)

INSERT INTO @showTimes
(
	VenueId,
	ShowId,
	EventDate,
	EventTime,
	EventEnd,
	SchoolOnly,
	InStock
)
SELECT	DISTINCT 
	v.VenueId,
	s.ShowID,
	p.EventDate,
	p.EventTime,
	p.EventEnd,
	p.SchoolOnly,
	p.InStock
FROM    @prods p
INNER JOIN	@venues v
	ON	p.Venue = v.Venue
	AND	 p.MemberOnly = v.MemberOnly
	--AND p.SoftCapacity = v.SoftCapacity
INNER JOIN @shows s
	ON s.EventTitleLong = p.EventTitleLong
	AND s.Location = p.Location
	AND s.EventNumber = p.EventNumber
	AND s.ItemLength = p.ItemLength

--SELECT 'showTimes', * FROM @showTimes

--SELECT 'venues', * FROM @venues WHERE Venue = '4-D Theater'
--SELECT 'products', * FROM @prods WHERE Venue = '4-D Theater' AND EventNumber = '79953' ORDER BY EventDate, Venue, EventTime, EventEnd
--SELECT 'shows', * FROM @shows WHERE location = '4-D Theater'
--SELECT 'showDates', * FROM @showDates WHERE VenueId = 1 AND showid = 1
--SELECT 'showTimes', * FROM @showTimes WHERE VenueId = 1 AND showid = 1

SELECT @OutputXML =
(
    SELECT DISTINCT
           products.EventDate AS 'eventdate',
           venue.Venue AS 'title',
           venue.MemberOnly AS 'memberonly',
           show.EventTitleLong AS 'title',
           show.Location AS 'location',
           show.ItemLength AS 'length',
           show.EventNumber AS 'eventnumber',
           time.SchoolOnly AS 'schoolonly',
           time.EventTime AS 'starttime',
           time.EventEnd AS 'endtime',
           time.InStock AS 'instock'
    FROM @feedDates uniqueDate
    JOIN @showDates products ON uniqueDate.EventDate = products.EventDate
    JOIN @feedDates fdt ON products.EventDate = fdt.EventDate
    JOIN @venues venue ON venue.VenueId = products.VenueId
    JOIN @shows show ON show.venueID = venue.VenueId
                        AND show.showID = products.ShowId
                        AND show.venueID = products.VenueId
    JOIN @showTimes time ON time.ShowId = show.showID
                            AND time.VenueId = show.venueID
                            AND time.EventDate = products.EventDate
    ORDER BY products.EventDate, venue.Venue, show.EventTitleLong, time.EventTime
    FOR XML AUTO, ELEMENTS
);

-- Original code
/*
SET @OutputXML = 
(SELECT v.venue AS 'title',
	v.MemberOnly AS 'memberonly',
	--v.SoftCapacity as 'capacity',
	(
	SELECT	s.EventTitleLong AS 'title',
			s.Location AS 'location',
			s.ItemLength AS 'length',
			s.EventNumber AS 'eventnumber',
			(
			SELECT sdt.EventDate AS 'eventdate',
					(
					SELECT	times.SchoolOnly AS 'starttime/@schoolonly',
							times.EventTime AS 'starttime',
							times.SchoolOnly AS 'endtime/@schoolonly',
							times.EventEnd AS 'endtime',
							times.InStock AS 'instock'
					FROM	@showTimes times
					WHERE	times.ShowId = sdt.ShowId
						AND		times.VenueId = sdt.VenueId
						AND		times.EventDate = sdt.EventDate
					ORDER BY times.EventDate, times.EventTime, times.EventEnd
					FOR XML PATH ('time'), ROOT ('times'), TYPE
					)
			FROM	@showDates sdt
			WHERE	sdt.ShowId = s.ShowId
			AND		sdt.VenueId = s.VenueId
			ORDER BY sdt.EventDate
			FOR XML PATH ('date'), TYPE
			)			
	FROM	@shows s
	WHERE	s.VenueId = v.VenueId
	FOR XML PATH ('show'), ROOT ('shows'), TYPE	
	)
FROM	@venues v
ORDER BY v.Venue
FOR XML PATH ('venue'), ROOT ('products'), TYPE
)
*/

SELECT @OutputXML = REPLACE(CONVERT(NVARCHAR(MAX), @OutputXML), '</products><products>', '')

--SELECT @XMLOut = @OutputXML;
--RETURN 0;

--SELECT @OutputXML;


DECLARE @MyHierarchy LUT_Hierarchy;
DECLARE @JSONString NVARCHAR(MAX);
DECLARE @JSONStripped NVARCHAR(MAX);

INSERT INTO @MyHierarchy
	SELECT *
	FROM dbo.LFT_ParseXML(@OutputXML);

SELECT @JSONString= dbo.LF_ToJSON(@MyHierarchy)

-- The formatting here is very, very specific.  Don't try to bring it all on one line or clean it up. Leave as is!
SELECT @JSONStripped = REPLACE(@JSONString, '},
    "time" :   ', '}
	')
SELECT @JSONStripped AS JSONFile;

GO


