USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTEND_WEATHER_CONDITIONS_DARKSKY]    Script Date: 3/9/2020 1:42:27 PM ******/
DROP PROCEDURE [dbo].[LRP_ATTEND_WEATHER_CONDITIONS_DARKSKY]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTEND_WEATHER_CONDITIONS_DARKSKY]    Script Date: 3/9/2020 1:42:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[LRP_ATTEND_WEATHER_CONDITIONS_DARKSKY]
	@WeekEnding	DATETIME,
	@WhichYear	VARCHAR(4) = 'This' -- 'This' or 'Last'
AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @tbl_weather TABLE
					(
					DateNative		DATETIME,
					DOW			VARCHAR(10),
					Budget			INT,
					TimeString		VARCHAR(7),
					Conditions		VARCHAR(500),
					Temperature		DECIMAL(18,2),
					ApparentTemp		DECIMAL(18,2)
					)

DECLARE @tbl_times TABLE (
						 [Times] VARCHAR(10)
						 )

DECLARE @tbl_dows TABLE (
						[DateNative]	DATE,
						 [DOW] VARCHAR(10),
						 Budget INT
						 )
						 
DECLARE @tbl_icon_list TABLE
						(
						[DateNative]	DATETIME,
						[DOW]			VARCHAR(10),
						[TimeString]	VARCHAR(10),
						[Budget]		INT,
						[Icon]			VARCHAR(100)
						)

DECLARE	@tbl_grossAttend TABLE (which_year VARCHAR(4), history_dt DATETIME, gross_attend INT, day_nbr INT)

DECLARE	@tbl_hilo TABLE (modified_dt VARCHAR(10), low_temp INT, high_temp INT)

DECLARE	@tbl_ind TABLE ([Date] DATETIME, [Count] INT)

DECLARE	@StartDate	DATETIME,
		@EndDate	DATETIME,
		@StartTime	INT = 9,
		@EndTime	INT = 17

IF @WhichYear = 'This'
	BEGIN

		SELECT	@StartDate = cur_week_start_dt,
				@EndDate = cur_week_end_dt
		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y')

		--SELECT 'debug',@StartDate,@EndDate,@WeekEnding

		INSERT INTO @tbl_grossAttend
			SELECT 'This', history_dt, SUM(visit_count), ROW_NUMBER () OVER(ORDER BY history_dt) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY history_dt

	END

IF @WhichYear = 'Last'
	BEGIN

		SELECT	@StartDate = cur_week_start_dt,
				@EndDate = cur_week_end_dt
		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y')

		INSERT INTO @tbl_grossAttend
			SELECT 'This', history_dt, SUM(visit_count), ROW_NUMBER () OVER(ORDER BY history_dt) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY history_dt

		SELECT	@StartDate = prv_week_start_dt,
				@EndDate = prv_week_end_dt
		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y')

		INSERT INTO @tbl_grossAttend
			SELECT 'Last', history_dt, SUM(visit_count), ROW_NUMBER () OVER(ORDER BY history_dt) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY history_dt

	END

INSERT INTO @tbl_weather (DateNative, DOW, Budget, TimeString, Conditions, Temperature, ApparentTemp)
	SELECT	ReadableDate,
			DATENAME(WEEKDAY, ReadableDate) AS 'Day of Week',
			[dbo].[LFS_ATTEND_BUDGET_DATA] (DATEADD(dd, DATEDIFF(dd, 0, ReadableDate), 0)) AS 'Budget Num',	
			LTRIM(RTRIM(RIGHT(CONVERT (VARCHAR, ReadableDate, 0), 7)))  AS 'Time',
			[Icon] AS 'Weather',
			Temperature AS 'Temp',
			ApparentTemperature AS 'Apparent Temp'
	FROM	[dbo].[LT_DARKSKY_LIVE_HOURLY]
	WHERE	ReadableDate BETWEEN @StartDate AND @EndDate
	AND		DATEPART(hh, ReadableDate) BETWEEN @StartTime AND @EndTime

INSERT INTO @tbl_dows
	SELECT DISTINCT CAST(DateNative AS DATE), DATENAME(WEEKDAY, DateNative), Budget FROM @tbl_weather

INSERT INTO @tbl_times ([Times]) VALUES 
	('9:00AM'),
	('10:00AM'),
	('11:00AM'),
	('12:00PM'),	
	('1:00PM'),	
	('2:00PM'),	
	('3:00PM'),	
	('4:00PM'),	
	('5:00PM')

INSERT INTO @tbl_icon_list
	SELECT	DISTINCT d.DateNative, d.[DOW], t.[Times], d.Budget, 'missing'
	FROM	@tbl_dows d
	OUTER APPLY (SELECT [Times]  FROM @tbl_times) t
	ORDER BY d.DateNative, d.DOW

INSERT INTO @tbl_weather
	SELECT	CONVERT(DATETIME, CONVERT(CHAR(8), DateNative, 112) + ' ' + CONVERT(CHAR(8), TimeString, 108)),
			ic.DOW, ic.Budget, ic.TimeString, ic.Icon, 0, 0
	FROM	@tbl_icon_list ic
	WHERE	NOT EXISTS (SELECT 1 FROM @tbl_weather w WHERE CONVERT(DATE, w.DateNative) = ic.DateNative AND w.TimeString = ic.TimeString)

--SELECT	'debug icon list', ic.DateNative, ic.DOW, ic.Budget, ic.TimeString, ic.Icon, 0, 0
--FROM	@tbl_icon_list ic
	
--SELECT 'debug weather', w.DateNative, w.TimeString FROM @tbl_weather w ORDER BY w.DateNative, w.TimeString

--SELECT 'debug', * FROM @tbl_dows
--SELECT 'debug', * FROM @tbl_times
--SELECT 'debug icon list', * FROM @tbl_icon_list
--SELECT 'debug', * FROM @tbl_weather
--SELECT 'debug', * FROM [dbo].[LT_WEATHER_MAPPING_DARKSKY]

--INSERT INTO @tbl_grossAttend
--	SELECT history_dt, SUM(visit_count) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY history_dt

--SELECT 'debug', * FROM @tbl_grossAttend

INSERT INTO @tbl_hilo
	SELECT	CONVERT(VARCHAR(10), ReadableDate, 101),
			CONVERT(INT, ROUND(MIN(Temperature),0)),
			CONVERT(INT, ROUND(MAX(Temperature),0))
	FROM	[dbo].[LT_DARKSKY_LIVE_HOURLY]
	WHERE	ReadableDate BETWEEN @StartDate AND @EndDate
	GROUP BY CONVERT(VARCHAR(10), ReadableDate, 101)

--SELECT 'debug', * FROM @tbl_hilo

-- 2019-10-16, H. Sheridan - Doing it this way to avoid having multiples show up for date/time columns in final output (i.e. 3 special dates = 3 rows for 9/3 @ 9 AM).  Didn't want to group the final SELECT.
INSERT INTO @tbl_ind
	SELECT	[Date], COUNT([Date])
	FROM	[dbo].[LTR_SPECIAL_DATES]
	WHERE	[Date] BETWEEN @StartDate AND @EndDate
	GROUP BY [Date]

IF @WhichYear = 'This'
	SELECT	CAST(w.DateNative AS DATE) AS DateNative, 
			DATEPART(YEAR, w.DateNative) AS 'Year',
			w.DOW AS [DayofWeek],
			CASE WHEN sd.[Count] > 0 THEN '*' ELSE '' END AS 'SpecialDate',
			w.Budget,
			ga.gross_attend AS GrossAttendance,
			ga.gross_attend AS GrossAttendanceThisYear,
			--CASE 
			--	WHEN DATEPART(hh, w.DateNative) < 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative)) + ' AM'
			--	WHEN DATEPART(hh, w.DateNative) = 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative)) + ' PM'
			--	WHEN DATEPART(hh, w.DateNative) > 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative) - 12) + ' PM'
			--END AS TimeNative,
			REPLACE(w.TimeString, ':00', ' ') AS TimeNative,
			--w.TimeString,
			ROW_NUMBER() OVER(PARTITION BY FORMAT (w.DateNative, 'd', 'en-US') ORDER BY DATEPART(hh, w.TimeString)) AS TimeOrder,
			wm.[Conditions] AS Conditions,
			wm.[Icon] AS Icon,
			LTRIM(RTRIM(hl.low_temp)) + '/' + LTRIM(RTRIM(hl.high_temp)) AS 'LowHighTemp',
			CASE 
				WHEN ISNULL(w.Budget,0) = 0 THEN 0
				ELSE CONVERT(DECIMAL(18,2), (CONVERT(FLOAT, ga.gross_attend - CONVERT(FLOAT, w.Budget))/CONVERT(FLOAT, w.Budget)) * 100) 
			END AS 'ActVsBud or ThisVsLast'
	FROM	@tbl_weather w
	JOIN	[dbo].[LT_WEATHER_MAPPING_DARKSKY] wm
	ON		w.Conditions = wm.[Conditions]
	LEFT JOIN	@tbl_grossAttend ga
	ON		CAST(ga.history_dt AS DATE) = CAST(w.DateNative AS DATE) AND @WhichYear = 'This'
	LEFT JOIN @tbl_hilo hl
	ON		hl.modified_dt = CONVERT(VARCHAR(10), w.DateNative, 101)
	LEFT JOIN	@tbl_ind sd
	ON			DATEDIFF(DAY, sd.[Date], w.DateNative) = 0
	--LEFT JOIN	[dbo].[LT_SPECIAL_DATES_MAPPING] sdm
	--ON			DATEDIFF(DAY, sdm.[Date], sd.[Date]) = 0
	WHERE	wm.[Conditions] IS NOT NULL
	ORDER BY w.DateNative, TimeOrder

--SELECT	'debug', CAST(gal.history_dt AS DATE), gal.gross_attend, gal.which_year
--FROM	@tbl_grossAttend gal
--WHERE	gal.which_year = 'This'

IF @WhichYear = 'Last'
	SELECT	CAST(w.DateNative AS DATE) AS DateNative, 
			DATEPART(YEAR, w.DateNative) AS 'Year',
			w.DOW AS [DayofWeek],
			CASE WHEN sd.[Count] > 0 THEN '*' ELSE '' END AS 'SpecialDate',
			w.Budget,
			ga.gross_attend AS GrossAttendance,
			gal.gross_attend AS GrossAttendanceThisYear,
			CAST(w.DateNative AS DATE),
			--CASE 
			--	WHEN DATEPART(hh, w.DateNative) < 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative)) + ' AM'
			--	WHEN DATEPART(hh, w.DateNative) = 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative)) + ' PM'
			--	WHEN DATEPART(hh, w.DateNative) > 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative) - 12) + ' PM'
			--END AS TimeNative,
			REPLACE(w.TimeString, ':00', ' ') AS TimeNative,
			--w.TimeString,
			ROW_NUMBER() OVER(PARTITION BY FORMAT (w.DateNative, 'd', 'en-US') ORDER BY DATEPART(hh, w.TimeString)) AS TimeOrder,
			wm.[Conditions] AS Conditions,
			wm.[Icon] AS Icon,
			LTRIM(RTRIM(hl.low_temp)) + '/' + LTRIM(RTRIM(hl.high_temp)) AS 'LowHighTemp',
			CASE 
				WHEN ISNULL(ga.gross_attend,0) = 0 THEN 0
				ELSE CONVERT(DECIMAL(18,2), (CONVERT(FLOAT, gal.gross_attend - CONVERT(FLOAT, ga.gross_attend))/CONVERT(FLOAT, ga.gross_attend)) * 100) 
			END AS 'ActVsBud or ThisVsLast'
	FROM	@tbl_weather w
	JOIN	[dbo].[LT_WEATHER_MAPPING_DARKSKY] wm
	ON		w.Conditions = wm.[Conditions]
	LEFT JOIN	@tbl_grossAttend ga
	ON		CAST(ga.history_dt AS DATE) = CAST(w.DateNative AS DATE) AND ga.which_year = 'Last'
	LEFT JOIN	@tbl_grossAttend gal
	ON		(gal.day_nbr = ga.day_nbr) AND gal.which_year = 'This'
	LEFT JOIN @tbl_hilo hl
	ON		hl.modified_dt = CONVERT(VARCHAR(10), w.DateNative, 101)
	LEFT JOIN	@tbl_ind sd
	ON			DATEDIFF(DAY, sd.[Date], w.DateNative) = 0
	--LEFT JOIN	[dbo].[LT_SPECIAL_DATES_MAPPING] sdm
	--ON			DATEDIFF(DAY, sdm.[Date], sd.[Date]) = 0
	WHERE	wm.[Conditions] IS NOT NULL
	ORDER BY w.DateNative, TimeOrder

-- Code to clean all non-printable characters courtesy of URL:  https://stackoverflow.com/questions/43148767/sql-server-remove-all-non-printable-ascii-characters
GO

EXECUTE [dbo].[LRP_ATTEND_WEATHER_CONDITIONS_DARKSKY] @WeekEnding = '3-14-2021', @WhichYear	= 'This'



