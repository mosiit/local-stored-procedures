USE [impresario]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_ChangeUnsoldSchoolShowsToPublic]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_ChangeUnsoldSchoolShowsToPublic]
GO

CREATE PROCEDURE [dbo].[LP_ChangeUnsoldSchoolShowsToPublic]
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

    DECLARE @title_no int, @perf_type_school int, @perf_type_public int, @tix_sold int
    DECLARE @perf_no int, @perfCode varchar(10), @perf_day varchar(20), @perf_date char(10), @perf_time char(8), @prod_name varchar(30)
    DECLARE @public_production_no varchar(30), @public_perf_no int, @rtn varchar(100)
    DECLARE @public_production_name varchar(30)

    SELECT @return_message = ''

    /*  Determine title number for the Mugar Omni Theater  */

        SELECT @title_no = [title_no] FROM LV_SS_PRODUCTION_ELEMENTS_TITLE WHERE title_name = 'Mugar Omni Theater'
        SELECT @title_no = IsNull(@title_no, 0)

        IF @title_no <= 0 BEGIN
            SELECT @return_message = 'cannot determine title_no'
            GOTO DONE
        END

    /*  Determine performance type id for school only performances  */

        SELECT @perf_type_school = [id] FROM [dbo].[TR_PERF_TYPE] WHERE [description] = 'School Only'
        SELECT @perf_type_school = IsNull(@perf_type_school, 0)

        IF @perf_type_school <= 0 BEGIN
            SELECT @return_message = 'unable to determine school perf_type_no'
            GOTO DONE
        END

    /*  Determine performance type id for public performances  */

        SELECT @perf_type_public = [id] FROM [dbo].[TR_PERF_TYPE] WHERE [description] = 'Public'
        SELECT @perf_type_public = IsNull(@perf_type_public, 0)

        IF @perf_type_public <= 0 BEGIN
            SELECT @return_message = 'cannot determine public perf_type_no'
            GOTO DONE
        END

    /*  Select all school only omni performances for the next two weeks into a cursor and evaluate them one at a time  */

        DECLARE PerformanceCursor INSENSITIVE CURSOR FOR
        SELECT [performance_no], [performance_code], datename(weekday,[performance_dt]), [performance_date], [performance_time], [production_name] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
        WHERE [performance_dt] between getdate() and dateadd(week,2,getdate()) and [title_no] = @title_no and [performance_type] = @perf_type_school
        ORDER BY [performance_date], [performance_time]
        OPEN PerformanceCursor
        BEGIN_PERFORMANCE_LOOP:

            FETCH NEXT FROM PerformanceCursor INTO @perf_no, @perfCode, @perf_day, @perf_date, @perf_time, @prod_name
            IF @@FETCH_STATUS = -1 GOTO END_PERFORMANCE_LOOP

            /*  Use LP_SS_GetPerformanceInfo to determine if any tickets have been sold for this performance  */

            EXECUTE [dbo].[LP_SS_GetPerformanceInfo] @perf_no = @perf_no, @perf_time = @perf_time,  @suppress_dataset = 'Y', @TixSold = @tix_sold OUTPUT
        
            IF @tix_sold = 0 BEGIN
        
                /*  If no tickets have been sold, determine the performance that the school only show will be changed to
                    NOTE: This performance has to already exist.  This procedure will not create a new performance, it will only enable a zone within an existing performance  */
                                    
                SELECT @public_production_no = MAX([public_production]) FROM [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI]
                WHERE CONVERT(DATE,@perf_date) BETWEEN CONVERT(DATE,[performance_start_dt]) AND CONVERT(DATE,[performance_end_dt])
                  AND [public_production] IN (SELECT [production_no] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                              WHERE [performance_date] = @perf_date AND [title_no] = @title_no AND [performance_type] <> @perf_type_school)

                SELECT @public_production_no = ISNULL(@public_production_no,0)

                IF @public_production_no <= 0 GOTO BEGIN_PERFORMANCE_LOOP

                SELECT @public_production_name = [production_name] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE production_no = @public_production_no

                SELECT @public_production_name = ISNULL(@public_production_name, '')
                                   
                SELECT @public_perf_no = max([performance_no]) FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                         WHERE [performance_date] = @perf_Date and [title_no] = @title_no and [production_no] = @public_production_no and [performance_type] = @perf_type_public

                SELECT @public_perf_no = IsNull(@public_perf_no, 0)

                IF @public_perf_no > 0 BEGIN

                    /*  If the public performance exists, execute the LP_EnableDisableZone procedure twice.
                        The first execution disables the zone within the school only performance  */

                    EXECUTE [dbo].[LP_EnableDisableZone] @perf_no = @perf_no, @perf_time = @perf_time, @is_enabled = 'N', @force_disable = 'Y', @return_message = @rtn OUTPUT

                    /*  The second execution enables the same zone within the public performance  */
    
                    EXECUTE [dbo].[LP_EnableDisableZone] @perf_no = @public_perf_no, @perf_time = @perf_time, @is_enabled = 'Y', @force_disable = 'N', @return_message = @rtn OUTPUT
                
                    /*  Log the change in case anyone wants to know exactly when the show was changed  */

                    INSERT INTO [admin].[dbo].[tessitura_omni_school_to_public_log] ([performance_date], [performance_time], [performance_no], [production_name], [public_performance_no], [public_production_name])
                    VALUES (@perf_date, @perf_time, @perf_no, @prod_name, @public_perf_no, @public_production_name)

                END

            END

            GOTO BEGIN_PERFORMANCE_LOOP

        END_PERFORMANCE_LOOP:
        CLOSE PerformanceCursor
        DEALLOCATE PerformanceCursor

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'
  
END
GO

GRANT EXECUTE ON [dbo].[LP_ChangeUnsoldSchoolShowsToPublic] to impusers
GO


--DECLARE @rtn VARCHAR(100) EXECUTE [dbo].[LP_ChangeUnsoldSchoolShowsToPublic] @return_message = @rtn OUTPUT   PRINT @rtn

--SELECT * FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_no IN (15070, 15071, 15072)
--SELECT * FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_no IN (14861, 14862, 14863, 14864)

--SELECT production_no, production_name FROM dbo.LV_PRODUCTION_ELEMENTS_PRODUCTION WHERE production_no IN (SELECT public_production FROM [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI])
--SELECT * FROM dbo.LTR_SCHOOL_TO_PUBLIC_OMNI
--SELECT performance_date, production_name, performance_time FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_Date between '2016/12/29' AND '2017/01/13' AND title_name = 'Mugar Omni Theater' AND performance_time = '11:00'
--EXECUTE [dbo].[LP_ChangeUnsoldSchoolShowsToPublic]
--SELECT performance_date, production_name, performance_time FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_Date between '2016/12/29' AND '2017/01/13' AND title_name = 'Mugar Omni Theater' AND performance_time = '11:00'