USE [impresario];
GO

/****** Object:  StoredProcedure [dbo].[LP_CREATE_TX_CUST_SAL_FOR_INDIVIDUALS]    Script Date: 6/19/2018 2:42:57 PM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO


ALTER PROCEDURE [dbo].[LP_CREATE_TX_CUST_SAL_FOR_HOUSEHOLDS_ONE_ADULT]
AS

-- THIS IS CREATING THE FAMILIAR SALU (ID = 2) RECORD IN THE TX_CUST_SAL TABLE FOR ALL INDIVIDUALS WHO DON'T ALREADY HAVE ONE
-- WO 83659 - Insert Familiar Salu on Individuals_Associated with RSVP report

/*  6/21/2017 - Removed SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  line from procedure.  
                Added a TRY/CATCH Structure and put the insert into TX_CUST_SAL into a transaction
                Changed SELECT @increment = COUNT(*) FROM @custSalTbl to SELECT @increment = COUNT(*) + 1 FROM @custSalTbl
	6/19/2018 - Aileen Duffy-Brown copied LP_CREATE_TX_CUST_SAL_FOR_INDIVIDUALS written by Debbie Jacob and edited it
	            for TrackIt! WO # 86665 for MKSHERWOOD.
	6/25/2018 - This version looks for household records where only one adult is present.
    7/8/2021  - Added Mail Two to the list of email decriptors being excluded

*/

SET NOCOUNT ON;


DECLARE @custSalTbl TABLE
(
    [signor] [INT] NOT NULL,
    [customer_no] [INT] NOT NULL,
    [esal1_desc] [VARCHAR](55) NULL,
    [esal2_desc] [VARCHAR](55) NULL,
    [lsal_desc] [VARCHAR](55) NULL,
    [default_ind] [CHAR](1) NULL,
    [label] [CHAR](1) NULL,
    [business_title] [VARCHAR](55) NULL
);

INSERT INTO @custSalTbl
(
    signor,
    customer_no,
    esal1_desc,
    esal2_desc,
    lsal_desc,
    default_ind,
    label,
    business_title
)
SELECT DISTINCT
       2,
       c.customer_no,
       cs.esal1_desc,
       NULL,
	   c1.fname + ' and ' + c2.fname,
      -- RTRIM(LTRIM(c1.fname)),
       'N',
       'Y',
       NULL
-- select count(*)
FROM T_CUSTOMER AS c
    INNER JOIN T_AFFILIATION AS a1
        ON c.customer_no = a1.group_customer_no
    INNER JOIN T_CUSTOMER AS c1
        ON a1.individual_customer_no = c1.customer_no
    INNER JOIN T_AFFILIATION AS a2
        ON c.customer_no = a2.group_customer_no
    INNER JOIN T_CUSTOMER AS c2
        ON a2.individual_customer_no = c2.customer_no
    INNER JOIN TX_CUST_SAL AS cs
        ON c.customer_no = cs.customer_no
WHERE c.cust_type = 7
      ----DJ ORIG c.cust_type = 1 -- Individual
      AND
      (
          c.name_status != 2
          OR c.name_status IS NULL
      ) -- not deceased
      AND cs.default_ind = 'Y'
      AND c.customer_no NOT IN
          (
              SELECT customer_no FROM TX_CUST_SAL WHERE signor = 2
          ) -- Familiar Salu does not already exist
      AND esal1_desc NOT IN ( 'Kiosk Customer', 'Guest User', 'Mailtwo Import')
      AND a1.affiliation_type_id = 10002
      AND a1.name_ind = '-1'
      AND c1.name_status = 1
      AND a2.name_ind = '-2'
      AND c2.name_status = 1;

	  --SELECT * FROM @custSalTbl

DECLARE @increment INT,
        @next_id INT;

SELECT @increment = COUNT(*) + 1
FROM @custSalTbl;


BEGIN TRY

    BEGIN TRANSACTION;

    EXECUTE @next_id = dbo.AP_GET_NEXTID_function @type = 'CS',
                                                  @increment = @increment;
    --SELECT @increment, @next_id


    INSERT INTO dbo.TX_CUST_SAL
    (
        signor,
        customer_no,
        esal1_desc,
        esal2_desc,
        lsal_desc,
        default_ind,
        label,
        business_title,
        salutation_no
    )
    SELECT signor,
           customer_no,
           esal1_desc,
           esal2_desc,
           lsal_desc,
           default_ind,
           label,
           business_title,
           ROW_NUMBER() OVER (ORDER BY customer_no) + @next_id
    FROM @custSalTbl;

    COMMIT TRANSACTION;

END TRY
BEGIN CATCH

    ROLLBACK TRANSACTION;

END CATCH;


GO

GRANT EXECUTE ON [dbo].[LP_CREATE_TX_CUST_SAL_FOR_HOUSEHOLDS_ONE_ADULT] TO ImpUsers;

