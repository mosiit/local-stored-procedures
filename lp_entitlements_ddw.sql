USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_ENTITLEMENTS_DDW]    Script Date: 11/27/2018 5:47:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[LP_ENTITLEMENTS_DDW]

AS
BEGIN
	declare @customer_no int
	select @customer_no = cast(dbo.FS_GET_PARAM_FROM_APPNAME('CT') as int)

select  sub.entitlement_desc,
		sub.entitlement_no,
		sub.num_remain 	,
		sub.id_key		
		 from (
	SELECT
		entitlement_desc=e.entitlement_desc,
		entitlement_no=ce.entitlement_no,
		num_remain = max(ce.num_ent) - sum(ISNULL(coe.num_used,0)),	
		ce.id_key	 
	FROM dbo.LTX_CUST_ENTITLEMENT ce
		JOIN dbo.LTR_ENTITLEMENT e on ce.entitlement_no = e.entitlement_no
			and ce.customer_no =@customer_no
			and e.transferrable='Y'
			 and  ce.inactive='N'
		JOIN dbo.TR_PRICE_TYPE_GROUP pt on pt.id = e.ent_price_type_group
		JOIN dbo.TR_TKW kw on kw.id = e.ent_tkw_id
		left JOIN dbo.T_MEMB_LEVEL ml ON ml.memb_level_no = e.memb_level_no
		left JOIN dbo.TX_CUST_MEMBERSHIP cm ON (cm.cust_memb_no = ce.cust_memb_no)
		LEFT JOIN T_PERF f ON f.perf_no = ce.entitlement_perf_no
		LEFT JOIN (
			SELECT
				lcoe.customer_no,
				lcoe.cust_memb_no,
				lcoe.entitlement_no, 
				lcoe.ltx_cust_entitlement_id,
				entitlement_key = CASE
							WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
							WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
							WHEN le.reset_type = 'M' THEN NULL
							ELSE NULL
							END,
				num_used = ISNULL(SUM(lcoe.num_used),0)
			FROM dbo.LTX_CUST_ORDER_ENTITLEMENT lcoe
				JOIN dbo.LTR_ENTITLEMENT le ON lcoe.entitlement_no = le.entitlement_no
			where lcoe.customer_no = @customer_no
			GROUP BY lcoe.customer_no, lcoe.cust_memb_no, lcoe.entitlement_no, 
			lcoe.ltx_cust_entitlement_id,
			CASE
											WHEN le.reset_type = 'D' THEN CONVERT(VARCHAR(10),lcoe.entitlement_date,101)
											WHEN le.reset_type = 'P' THEN CAST(lcoe.perf_no AS VARCHAR(255))
											WHEN le.reset_type = 'M' THEN NULL
											ELSE NULL
											END) coe
			ON (
			(isnull(coe.ltx_cust_entitlement_id,0) =ce.id_key) or 
			(
			isnull(coe.customer_no,0)= isnull(ce.customer_no,0)
			AND isnull(coe.cust_memb_no,0) = isnull(cm.cust_memb_no,0)
			AND coe.entitlement_no = e.entitlement_no
			AND (
				(e.reset_type = 'D' AND coe.entitlement_key = CONVERT(varchar(10),ce.entitlement_date,101)) /* Bug fix 2/3/2016 */
				OR (e.reset_type = 'P' AND coe.entitlement_key = CAST(ce.entitlement_perf_no AS varchar(255))) /* 3/16/2015 */
				OR e.reset_type = 'M'				

				))
			)
	WHERE  (
		is_adhoc='N' and CURRENT_TIMESTAMP  BETWEEN cm.init_dt AND cm.expr_dt
		OR (cm.current_status = 3 AND CURRENT_TIMESTAMP BETWEEN DATEADD(MONTH,0-ml.renewal,cm.init_dt) AND cm.expr_dt) --pending 
		) 
		or
		(is_adhoc='Y'and CURRENT_TIMESTAMP between ce.init_dt and ce.expr_dt)
	   or (is_adhoc='N' and coalesce(ce.cust_memb_no,0)=0)
	   group by 	e.entitlement_desc,
		ce.entitlement_no,		
		ce.id_key	 
) sub 
where sub.num_remain>0

END

GO

