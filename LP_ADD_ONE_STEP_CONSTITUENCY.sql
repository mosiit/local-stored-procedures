USE [impresario]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_ADD_ONE_STEP_CONSTITUENCY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_ADD_ONE_STEP_CONSTITUENCY]
GO

CREATE PROCEDURE [dbo].[LP_ADD_ONE_STEP_CONSTITUENCY]
        @return_message VARCHAR(100) = '' OUTPUT
AS BEGIN

    DECLARE @one_week_ago DATETIME, @today_date DATETIME
    DECLARE @one_step_const_no INT
    DECLARE @cust_table TABLE ([customer_no] INT)
    
    BEGIN TRY
    
        /* SET DATES AND VALUES  */

        SELECT @return_message = ''
        SELECT @one_week_ago = DATEADD(WEEK, -1, GETDATE())
        SELECT @today_date = GETDATE()
        SELECT @one_step_const_no = MIN([id]) FROM dbo.TR_CONSTITUENCY WHERE description = 'One-Step'

        /*  Generate a list of all customers that need the constituency record added  */
        
            INSERT INTO @cust_table ([customer_no])
            SELECT DISTINCT mem.[customer_no]
            FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] AS mem
                 LEFT OUTER JOIN [dbo].[TX_CONST_CUST]  AS con (NOLOCK) ON con.[customer_no] = mem.[customer_no] AND con.[constituency] = @one_step_const_no
            WHERE [membership_dt] BETWEEN @one_week_ago AND @today_date AND add_one_step = 'Y' AND con.constituency IS NULL
            
        /*  Insert records into the TX_CONST_CUST table.  */
            
            BEGIN TRANSACTION

                INSERT INTO [dbo].[TX_CONST_CUST] ([constituency], [customer_no], [default_ind], [n1n2_ind], [start_dt], [end_dt])
                SELECT @one_step_const_no, cus.[customer_no], 'N', 3, CONVERT(DATE,@today_date), NULL
                FROM @cust_table AS cus

            COMMIT TRANSACTION

    END TRY
    BEGIN CATCH

        WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        SELECT @return_message = left(Error_message(),100)
        SELECT @return_message = IsNull(@return_message, 'error while adding records to TX_CONST_CUST table.')
        
    END CATCH

    IF @return_message = '' SELECT @return_message = 'success'

    FINISHED:

        IF @@TRANCOUNT > 0 AND @return_message IN ('','success') SELECT @return_message = 'transaction error'

        WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION

        IF @return_message = '' SELECT @return_message = 'unknown error'

        IF @return_message NOT IN ('','success') RAISERROR (@return_message,18,1,N'number',5)

    

END
GO

GRANT EXECUTE ON [dbo].[LP_ADD_ONE_STEP_CONSTITUENCY] TO ImpUsers
GO


--DECLARE @rtn VARCHAR(100)     EXECUTE [dbo].[LP_ADD_ONE_STEP_CONSTITUENCY] @return_message = @rtn OUTPUT        --PRINT @rtn
