USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE LP_AddBusinessAddressToTPPRecords
AS

-- WHEN TPP TEACHERS SIGN UP, THEY DON'T ALWAYS ENTER THEIR SCHOOL ADDRESS. WE WANT TO SEND THEIR MEMBERSHIP CARDS TO THEIR SCHOOL ADDRESS.
-- THIS SP WILL CREATE THE BUSINESS/SCHOOL ADDRESS RECORD IF IT DNE AND WILL MARK IT AS "PRIMARY" IF A PRIMARY ADDRESS RECROD DOES NOT ALREADY EXIST
-- FOR THAT CONSTITUENT.


SET NOCOUNT ON 

-- Get all TPP Teacher School Addresses for teachers that do not have an Active current "Business" address_type
-- We will create a Business address_type record for them with the school address as the Business address. 
DECLARE @AddressTbl TABLE 
(
	[customer_no] [int] NULL,
	[address_type] [int] NOT NULL,
	[street1] [varchar](64) NOT NULL,
	[street2] [varchar](64) NULL,
	[street3] [varchar](64) NULL,
	[city] [varchar](30) NULL,
	[state] [varchar](20) NULL,
	[postal_code] [varchar](10) NULL,
	[country] [int] NULL,
	[primary_ind] [char](1) NOT NULL,
	[inactive] [char](1) NOT NULL,
	[label] [char](1) NOT NULL,
	[create_loc] [varchar](16) NULL,
	[created_by] [char](8) NOT NULL,
	[create_dt] [datetime] NULL,
	[last_updated_by] [char](8) NULL,
	[last_update_dt] [datetime] NOT NULL
)

INSERT INTO @AddressTbl
	(customer_no,
	address_type,
	street1,
	street2,
	street3,
	city,
	state,
	postal_code,
	country,
	primary_ind,
	inactive,
	label,
	create_loc,
	created_by,
	create_dt,
	last_updated_by,
	last_update_dt)
SELECT 
	aff.[individual_customer_no],
	2 AS address_type, -- business
	addr.street1 AS street1,
	addr.street2 AS street2,
	c.lname AS street3,
	addr.city,
	addr.state,
	addr.postal_code,
	addr.country,
	'N' AS primary_ind,
	'N' AS inactive,
	'Y' AS label,
	@@SERVERNAME,
	[dbo].[FS_USER](),
	GETDATE(),
	[dbo].[FS_USER](),
	GETDATE()
FROM [impresario].[dbo].[T_AFFILIATION] aff
LEFT JOIN dbo.T_CUSTOMER c
	ON aff.group_customer_no = c.customer_no
LEFT JOIN dbo.T_ADDRESS addr
	ON aff.group_customer_no = addr.customer_no
	AND addr.inactive = 'N' AND addr.primary_ind = 'Y'
	AND addr.address_type IN (2, 3) -- Business or Home
WHERE aff.[affiliation_type_id] = 10197
AND aff.individual_customer_no NOT IN (	SELECT pri.customer_no
											FROM dbo.T_ADDRESS add2
											INNER JOIN dbo.V_CUSTOMER_WITH_PRIMARY_AFFILIATES pri
												ON add2.customer_no = pri.expanded_customer_no
											WHERE add2.address_type = 2 -- Business
											AND add2.inactive = 'N'
											) 


-- If customer does not have a Primary address record, update @AddressTbl and set Primary_ind = 'Y'
UPDATE atbl
SET atbl.primary_ind = 'Y'
--SELECT atbl.*
FROM @AddressTbl atbl
WHERE atbl.customer_no NOT IN 
(	SELECT pri.customer_no
	FROM dbo.T_ADDRESS add2
	INNER JOIN dbo.V_CUSTOMER_WITH_PRIMARY_AFFILIATES pri
		ON add2.customer_no = pri.expanded_customer_no
	WHERE add2.primary_ind = 'Y'
	AND add2.inactive = 'N'
) 


DECLARE @increment INT,
	@next_id INT

SELECT @increment = COUNT(*) FROM @AddressTbl

EXECUTE @next_id = dbo.AP_GET_NEXTID_function @type = 'AD', @increment = @increment
--SELECT @increment, @next_id

INSERT INTO dbo.T_ADDRESS
	(address_no,
	customer_no,
	address_type,
	street1,
	street2,
	street3,
	city,
	state,
	postal_code,
	country,
	primary_ind,
	inactive,
	label,
	create_loc,
	created_by,
	create_dt,
	last_updated_by,
	last_update_dt
)
SELECT 
	ROW_NUMBER() Over (Order by customer_no) + @next_id AS address_no,
	customer_no,
	2 AS address_type, -- business
	street1,
	street2,
	street3,
	city,
	state,
	postal_code,
	country,
	primary_ind AS primary_ind,
	inactive,
	label,
	create_loc,
	created_by,
	create_dt,
	last_updated_by,
	last_update_dt
FROM @AddressTbl



