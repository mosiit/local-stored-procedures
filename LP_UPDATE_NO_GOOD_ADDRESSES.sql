USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_UPDATE_NO_GOOD_ADDRESSES]    Script Date: 12/1/2020 8:11:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_UPDATE_NO_GOOD_ADDRESSES]
AS 
-- ----------------------------------------------------------------------------------
-- Created by:			H. Sheridan
-- Created on:			12/1/2020
-- Service Now ticket:	SCTASK0001669 (Automatically update addresses in Tessitura)
-- Related SQL job:		T Daily - Update No Good Addresses
-- ----------------------------------------------------------------------------------

	BEGIN

	   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	   SET NOCOUNT ON

		INSERT INTO [LT_UPDATE_NO_GOOD_ADDRESSES] ([address_no], [customer_no], [address_type], [street1])
			SELECT address_no,
				   customer_no,
				   address_type,
				   street1
			FROM T_ADDRESS (NOLOCK)
			WHERE address_type <> 20
				  AND inactive = 'N'
				  AND street1 LIKE 'No Good%';

		UPDATE T_ADDRESS
		SET address_type = 20
		WHERE address_type <> 20
			  AND inactive = 'N'
			  AND street1 LIKE 'No Good%';

   END