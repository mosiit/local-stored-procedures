USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_ATTEND_SAVE_GOCITY_EMAIL_DATA]    Script Date: 1/9/2020 11:31:37 AM ******/
DROP PROCEDURE [dbo].[LP_ATTEND_SAVE_GOCITY_EMAIL_DATA]
GO

/****** Object:  StoredProcedure [dbo].[LP_ATTEND_SAVE_GOCITY_EMAIL_DATA]    Script Date: 1/9/2020 11:31:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LP_ATTEND_SAVE_GOCITY_EMAIL_DATA]
AS
BEGIN

    INSERT INTO dbo.LT_GOCITY_EMAILS
		SELECT	[Body],
				[BodyHTML],
				[Subject],
				[FromEmail],
				[DateSent],
				[To],
				[Size],
				[Priority],
				[Attachments],
				[RelatedItems],
				[MessageID]
		FROM	dbo.LT_GOCITY_EMAILS_STAGING s
		WHERE	s.[Subject] NOT IN (SELECT e.[Subject] FROM dbo.LT_GOCITY_EMAILS e);

END;
GO


