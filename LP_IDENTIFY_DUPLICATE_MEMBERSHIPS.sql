USE [impresario]
GO

DROP PROCEDURE [dbo].[LP_IDENTIFY_DUPLICATE_MEMBERSHIPS]
GO

/****** Object:  StoredProcedure [dbo].[LP_IDENTIFY_DUPLICATE_MEMBERSHIPS]    Script Date: 12/7/2016 11:45:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE        PROCEDURE [dbo].[LP_IDENTIFY_DUPLICATE_MEMBERSHIPS](
        @changed_since_days int = null,
        @identify_method1 int = 1,
        @identify_method2 int = null,
        @identify_method3 int = null,
        @include_inactive char(1) = 'Y',
        @list_no int = null
        )
AS
Set NoCount On  
/*******************************************************************************
Identify Duplicates Procedure - MEMBERSHIP FLAGS ADDED (H. Sheridan, 12/7/2016)

This procedure uses a criterion to identify the potential duplicate constituents 
and populates the t_potential_dups tables.  The data from this table is used later for customer merge.
This procedure is intended for use in a scheduled job.  There is no reference to this
in the application.

Modified 11/29/2000 by CWR -- for SQL 7.0 syntax.  Also fixed problem with calculating
	last run date if t_potential_dups was empty.
Modified 12/12/2000 by CWR -- added changed_since_days parameter which will tell the
	procedure how recently a record had to change in order to come back into the merge.
	If no value is passed, it uses the old logic.
Modified 8/30/2002 by CWR -- this procedure was looking for the keyword "Void Merge" with 
	a value of 'Y' and excluding them.  It should be looking for a value of 'Yes'
Modified 7/30/2003 by CWR -- now trimming blanks off of t_customer.lname in the match process
Modified 8/12/2003 by CWR -- now trimming blanks off of t_customer.lname in the match process (missed 2 places earlier)
Modified 6/8/2004 by CWR -- now populating street2 in t_potential_dups
Modified 3/4/2005 by CWR -- added IsNull() to fname column so that non-individuals would be found
Modified 6/28/2005 by CWR -- commented out code that excluded records with null fname
Modified 9/22/2006 by CWR -- now using T_DEFAULT setting Duplicate Matching to get substring values
Modified CWR 12/21/2009 FP1011. -- now trimming trailing spaces off of comparison elements
Modified RWC 5/6/2011 #4091 -- complete rewrite for version 11, including group/affiliations and 
	expanded identification modes.
	Optional parameter to exclude inactive constituents from duplicate consideration
	List parameter added to filter duplicate list so that at least ONE constituent in
		duplicate pair must be included in the list.
Modified RWC 5/11/2011 #4091 -- Added a few indexes for optimization.
	--Fixed having clause in dupe insert that was preventing constituents without an affiliation
Modified RWC 5/16/2011 #4091 -- small mistake in param check at line 76
	-- Added new identify method consideration here, -1 is for Custom Potential processes, 
		while -2 is for merges manually scheduled by a user.
Modified RWC 2/10/2012 #338 -- Changed sinced days was not working, as it was not using the temp table 
	for comparison.
	
Identify Methods (because mode may consider more than one of these)
	-2 - Manually scheduled
	-1 - Custom (default) - From custom processes and when a user manually assigns a pair for merge	
	1 - Name and Primary Address
	2 - Primary Email Address
	3 - Name and Primary EAddress

Exec [LP_IDENTIFY_DUPLICATE_MEMBERSHIPS]
select * from t_potential_dups
********************************************************************************/
BEGIN
SET ANSI_WARNINGS OFF
 
DECLARE	@void_merge_keyword_no int,
	@void_merge_description varchar(30) = 'Void Merge',
	@errmsg varchar(255), 
	@last_run_dt datetime

Declare @status_merged char(1) = 'M',
		@status_potential char(1) = 'P',
		@status_keep char(1) = 'K',
		@status_delete char(1) = 'D',
		@status_failed char(1) = 'F'
		
Declare @identify_method_custom int = -1,
		@identify_method_manually_scheduled int = -2,
		@identify_method_name_and_address int = 1,
		@identify_method_email_only int = 2,
		@identify_method_name_and_eaddress int = 3

If (@identify_method1 is NOT NULL and Not Exists (Select * From dbo.TR_DUPLICATE_IDENTIFY_METHOD Where id = @identify_method1 and id > 0))
	or (@identify_method2 is NOT NULL and Not Exists (Select * From dbo.TR_DUPLICATE_IDENTIFY_METHOD Where id = @identify_method2))
	or (@identify_method3 is NOT NULL and Not Exists (Select * From dbo.TR_DUPLICATE_IDENTIFY_METHOD Where id = @identify_method3))
  Begin
    SELECT @errmsg = 'One of the Identify Methods specified is not valid.'
    RAISERROR(@errmsg,1,2)
    RETURN -1
  End

Select @void_merge_keyword_no = keyword_no from dbo.T_KEYWORD where description = @void_merge_description

If @void_merge_keyword_no is null 
  begin
    select @errmsg = 'An entry with description ''Void Merge'' is missing in t_keyword table. '
    select @errmsg = @errmsg + char(10) + 'Aborting Duplicates Identifying Process!'
    RAISERROR(@errmsg,1,2) 
    RETURN -1
  end

-- get these values from the new T_DEFAULTS setting CWR 9/22/2006
Declare @lname_len int, @fname_len int, @street_len int, @postal_code_len int

declare @defaults varchar(255)
Select	@defaults = dbo.FS_GET_DEFAULT_VALUE(null, null, 'Duplicate Matching')

If IsNull(@defaults, '') = '' 
	Select	@defaults = 'fname=1,lname=20,postal_code=3,street1=3'

Select @fname_len = IsNull(convert(int, value), 1) from dbo.FT_PARSE_LIST(@defaults, ',') where name = 'fname'
Select @lname_len = IsNull(convert(int, value), 20) from dbo.FT_PARSE_LIST(@defaults, ',') where name = 'lname'
Select @street_len = IsNull(convert(int, value), 3) from dbo.FT_PARSE_LIST(@defaults, ',') where name = 'street1'
Select @postal_code_len = IsNull(convert(int, value), 3) from dbo.FT_PARSE_LIST(@defaults, ',') where name = 'postal_code'

/* First get the criterion strings that have count greater than 1 */
create table #potential_duplicates(criterion varchar(255) null, identify_method tinyint, customer_no1 int, customer_no2 int)
 
IF @changed_since_days is null
  BEGIN
	SELECT @last_run_dt = MAX(create_dt) FROM dbo.T_POTENTIAL_DUPS WHERE identify_method = @identify_method_manually_scheduled
 
	IF @last_run_dt is NULL
		Select @last_run_dt = MAX(merge_dt) from dbo.T_MERGED 
 
	IF @last_run_dt is NULL
		Select @last_run_dt = '1/1/1900'
  END
ELSE
  Begin
	Select @last_run_dt = dateadd(dd, -1 * @changed_since_days, getdate())
  End

/********** Duplicate Finding Process *************/

-- Method 1 - Name & Primary Address
If @identify_method_name_and_address in (@identify_method1, @identify_method2, @identify_method3)
  Begin
	INSERT INTO #potential_duplicates(criterion, identify_method, customer_no1, customer_no2)
	Select criterion,
			@identify_method_name_and_address,
			customer_no1,
			customer_no2
	From dbo.FT_DUPLICATES_BY_NAME_AND_ADDRESS(@lname_len, @fname_len, @street_len, @postal_code_len, @include_inactive)
  End

-- Primary Electronic Address ONLY
If  @identify_method_email_only in (@identify_method1, @identify_method2, @identify_method3)
  Begin
	INSERT INTO #potential_duplicates(criterion, identify_method, customer_no1, customer_no2)
	Select	criterion,
			@identify_method_email_only,
			customer_no1,
			customer_no2
	From dbo.FT_DUPLICATES_BY_EADDRESS(@include_inactive)
End 

-- Name and Primary Electronic Address
If  @identify_method_name_and_eaddress in (@identify_method1, @identify_method2, @identify_method3)
  Begin
	INSERT INTO #potential_duplicates(criterion, identify_method, customer_no1, customer_no2)
	SELECT 	criterion,
			@identify_method_name_and_eaddress,
			customer_no1,
			customer_no2
	From dbo.FT_DUPLICATES_BY_NAME_AND_EADDRESS(@lname_len, @fname_len, @include_inactive)
End 

CREATE index indt1 on #potential_duplicates(criterion)
Create index indt2 on #potential_duplicates(customer_no1, customer_no2)

/************ Process Potential Duplicates (filtering and cleanup) **************/

-- Cleaning duplicate matches found by multiple identify methods, priority is based on order of parameters (1,2,3)

-- Remove matches found in @identify_method2 and @identify_method3 where already found in @identify_method1
-- NOTE:  customer_no1 is always MIN and customer_no2 is always MAX, so we don't have to do multiple joins here
Delete a
From #potential_duplicates a
	JOIN #potential_duplicates b on a.customer_no1 = b.customer_no1 and a.customer_no2 = b.customer_no2 
									and b.identify_method = @identify_method1
Where a.identify_method in (@identify_method2, @identify_method3)

--Remove matches from method 3 where already found in method 2
Delete a
From #potential_duplicates a
	JOIN #potential_duplicates b on a.customer_no1 = b.customer_no1 and a.customer_no2 = b.customer_no2 
									and b.identify_method = @identify_method2
Where a.identify_method in (@identify_method3)

-- List parameter allows potential duplicates to be included only if ONE of the constituents is in the list.
If @list_no > 0
  Begin
	Delete From #potential_duplicates 
	Where Not Exists (Select * From dbo.T_LIST_CONTENTS Where list_no = @list_no and customer_no in (customer_no1, customer_no2))
  End

DELETE FROM #potential_duplicates
Where EXISTS (Select customer_no from dbo.TX_CUST_KEYWORD c (NOLOCK)
				WHERE c.customer_no in (customer_no1, customer_no2)
				and c.keyword_no = @void_merge_keyword_no and c.key_value = 'Yes')

--delete all records from t_potential_dups execept the scheduled ones.
--But, prior to that, store the criteria of 'F' records in a temp table
--so it can be used to "not delete" these records in the fresh result set.
 
CREATE TABLE #failed_criteria(criterion varchar(100), identify_method int)
 
INSERT #failed_criteria (criterion, identify_method)
SELECT 	DISTINCT criterion, identify_method
FROM 	dbo.T_POTENTIAL_DUPS 
WHERE 	status = @status_failed

DELETE FROM dbo.T_POTENTIAL_DUPS 
Where 	status in (@status_potential, @status_failed, @status_merged)
 
--delete from temp table already existing (in T table) criteria
DELETE FROM #potential_duplicates
Where criterion in (Select criterion From dbo.T_POTENTIAL_DUPS)

DELETE FROM #potential_duplicates
Where EXISTS (Select * From dbo.T_POTENTIAL_DUPS Where customer_no in (customer_no1, customer_no2))

/********************************************************************************************
  Delete the pair if no activity occurred on both of them since the last run of this proc 
  This was designed so that the same duplicate pair doesn't come back over and over.
*********************************************************************************************/
CREATE TABLE #delete_criterion(criterion varchar(100), last_activity_dt datetime)

INSERT 	#delete_criterion
SELECT 	a.criterion, 
		last_activity_dt = MAX(COALESCE(b.last_activity_dt, b.create_dt))
FROM 	#potential_duplicates a
	JOIN dbo.T_CUSTOMER b ON a.customer_no1 = b.customer_no or a.customer_no2 = b.customer_no
WHERE	a.identify_method != @identify_method_manually_scheduled
GROUP BY a.criterion
HAVING 	Coalesce(MAX(Coalesce(b.last_activity_dt, b.create_dt)), '1/1/1900') < @last_run_dt
 
CREATE INDEX del_criterion_ind ON #delete_criterion(criterion)
 
--Do not delete the criteria which failed during last merge attempt
DELETE	FROM #delete_criterion
WHERE criterion in (Select criterion From #failed_criteria)
 
DELETE FROM #potential_duplicates
Where criterion in (Select criterion From #delete_criterion)

/*************************************************************/

INSERT INTO dbo.T_POTENTIAL_DUPS(customer_no, criterion, status, identify_method)
SELECT customer_no1, criterion, @status_potential, identify_method FROM #potential_duplicates
UNION ALL
SELECT customer_no2, criterion, @status_potential, identify_method FROM #potential_duplicates

-- H. Sheridan, 12/7/2016 - Replace call to stored proc with code from stored proc so I can tie into membership status
--Execute dbo.AP_GET_POTENTIAL_DUPES

DECLARE	@tblMemb TABLE	(
						customer_no				INT,
						fname					VARCHAR(20),
						mname					VARCHAR(20),
						lname					VARCHAR(20),
						street1					VARCHAR(64),
						street2					VARCHAR(64),
						street3					VARCHAR(64),
						city					VARCHAR(30),
						[state]					VARCHAR(20),
						postal_code				VARCHAR(10),
						eaddress				VARCHAR(80),
						criterion				VARCHAR(255),
						[status]				CHAR(1),
						keep_cust				INT,
						identify_method			INT,
						identify_method_desc	VARCHAR(30),
						sort					VARCHAR(55),
						created_by				VARCHAR(8),
						create_dt				DATETIME,
						last_update_dt			DATETIME,
						last_updated_by			VARCHAR(8),
						current_memb			BIT,
						pending_memb			BIT,
						past_memb				BIT,
						expiration_date			VARCHAR(10)
						)

INSERT INTO @tblMemb
	SELECT d.customer_no,   
		c.fname,   
		c.mname,
		c.lname,   
		a.street1,   
		a.street2, 
		a.street3,
		a.city,
		a.state,  
		a.postal_code,   
		eaddress = e.address,
		d.criterion,   
		d.status,
		d.keep_cust,
		d.identify_method,
		identify_method_desc = m.description, 
		sort = c.sort_name,
		d.created_by,   
		d.create_dt,
		d.last_update_dt,
		d.last_updated_by ,
		0, 0, 0 , ''   
	FROM [dbo].T_POTENTIAL_DUPS d
		JOIN [dbo].T_CUSTOMER c ON d.customer_no = c.customer_no
		LEFT JOIN [dbo].FT_GET_PRIMARY_ADDRESS() a ON d.customer_no = a.customer_no
		LEFT JOIN [dbo].FT_GET_PRIMARY_EADDRESS() e ON d.customer_no = e.customer_no
		LEFT JOIN [dbo].TR_DUPLICATE_IDENTIFY_METHOD m ON d.identify_method = m.id
		--LEFT JOIN [dbo].FT_SPLIT_LIST(@status_str, ',') s ON d.status = s.Element		
	--WHERE @status_str IS NULL OR s.ElementId > 0

UPDATE	x
SET		x.current_memb = 1,
		x.expiration_date = CONVERT(VARCHAR(10), cmi.expiration_date, 101)
FROM	@tblMemb x
JOIN	[LV_CURRENT_MEMBERSHIP_INFO] cmi
ON		x.customer_no = cmi.customer_no

UPDATE	x
SET		x.pending_memb = 1
FROM	@tblMemb x
JOIN	[LV_PENDING_MEMBERSHIP_INFO] pmi
ON		x.customer_no = pmi.customer_no

UPDATE	x
SET		x.past_memb = 1
FROM	@tblMemb x
JOIN	[LV_PAST_MEMBERSHIP_INFO] pmi
ON		x.customer_no = pmi.customer_no

SELECT  ISNULL(customer_no, '') AS 'Customer Number',
        ISNULL(fname, '') AS 'First Name',
        ISNULL(mname, '') AS 'Middle Name',
        ISNULL(lname, '') AS 'Last Name',
        ISNULL(street1, '') AS 'Street 1',
        ISNULL(street2, '') AS 'Street 2',
        ISNULL(street3, '') AS 'Street 3',
        ISNULL(city, '') AS 'City',
        ISNULL([state], '') AS 'State',
        ISNULL(postal_code, '') AS 'Postal Code',
        ISNULL(eaddress, '') AS 'Email Address',
        ISNULL(criterion, '') AS 'Criterion',
        ISNULL([status], '') AS 'Status',
        ISNULL(keep_cust, '') AS 'Keep Customer',
        ISNULL(identify_method, '') AS 'Identify Method',
        ISNULL(identify_method_desc, '') AS 'Identify Method Description',
        ISNULL(sort, '') AS 'Sort Name',
        CASE WHEN current_memb = 1 THEN 'Yes' ELSE '' END AS 'Current Member',
        CASE WHEN pending_memb = 1 THEN 'Yes' ELSE '' END AS 'Pending Member',
        CASE WHEN past_memb = 1 THEN 'Yes' ELSE '' END AS 'Past Member',
        expiration_date AS 'Current Expiration Date',
        ISNULL(created_by, '') AS 'Created By',
        ISNULL(create_dt, '') AS 'Created Date',
        ISNULL(last_update_dt, '') AS 'Last Updated Date',
        ISNULL(last_updated_by, '') AS 'Last Updated By'
FROM @tblMemb

RETURN
END

GO


