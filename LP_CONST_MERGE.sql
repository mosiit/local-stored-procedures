USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_CONST_MERGE]    Script Date: 7/12/2019 1:21:12 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO



ALTER  PROCEDURE [dbo].[LP_CONST_MERGE](
	@merge_stage CHAR(1) = NULL,
	@kept_id INT = NULL,
	@deleted_id INT = NULL,
	@err_table VARCHAR(255) = NULL OUTPUT) 
 
AS
Set NoCount On 

 
/***********************************************************************************************
New 11/16/2001 by CWR
 
Designed to be called from AP_CUSTOMER_MERGE during the constituent merge process.  This procedure
is actually called twice during the procedure, once before any other data is merged and once after 
the final step in the merge which is when the T_CUSTOMER row for the deleted id is marked Merged.
Both calls are inside the merge transaction and will be rolled back if any error occurs.  Likewise,
if this procedure raises an error, then the entire merge process will be rolled back and the next
constituent merge will begin.  Failed merges are noted in the merge error log.
The @proc_stage parameter is either 'B' (before) or 'A' (after).  
 
NB: This example code merges a local table used on the custom tab.
 
***********************************************************************************************/
IF @kept_id is null or @deleted_id is null or @merge_stage is null
	RETURN

/*
-- sample code here for pre-merge processing
If @merge_stage = 'B'
  Begin
	do some processing here before the rows are merged

	If @@error <> 0 

	   Begin
		select @err_table = 'lt_custom_example (pre-merge)'

		goto GiveErrMsg
	   End
  End
*/

/*
If @merge_stage = 'A'
  Begin

	UPDATE	lt_custom_example set customer_no = @kept_id where customer_no = @deleted_id
	 
	If @@error <> 0 

	   Begin
		select @err_table = 'lt_custom_example (post_merge)'

		goto GiveErrMsg
	   End

   End

RETURN

GiveErrMsg:

RAISERROR 50000 'Error Occured in Localized Procedure'
 
RETURN
*/

-- =========================================================================================
-- Custom MOS code starts here
-- =========================================================================================

IF @merge_stage = 'A' 
   BEGIN

		-- H. Sheridan, 1/10/2017 - Initial additions (20 tables total)
		UPDATE  LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_TOKENIZE_CREDIT_CARD_LOG
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_TOKENIZE_CREDIT_CARD_LOG (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_HISTORY_TICKET
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_HISTORY_TICKET (post_merge)'
				 GOTO GiveErrMsg
		   END

		-- H. Sheridan, 6/15/2017 - remove this table per work order 87537 - Remove nightly summary table from local table merge
		--UPDATE  LT_NIGHTLY_SUMMARY
		--SET     customer_no = @kept_id
		--WHERE   customer_no = @deleted_id

		--IF @@error <> 0 
		--   BEGIN
		--		 SELECT @err_table = 'Error: LT_NIGHTLY_SUMMARY (post_merge)'
		--		 GOTO GiveErrMsg
		--   END

		UPDATE  LT_SCHOLARSHIP_ALLOCATION
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_SCHOLARSHIP_ALLOCATION (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_GIFT_MEMB_EXP_DATE_CHANGES
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_GIFT_MEMB_EXP_DATE_CHANGES (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_M2_Promotion_LOG
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_M2_Promotion_LOG (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_M2_Promotion_Update
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_M2_Promotion_Update (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_MOS_MEMBER_CARDS
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_MOS_MEMBER_CARDS (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LTR_SAVE_CARD
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LTR_SAVE_CARD (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LTR_CUSTOMER_CARD_TOKEN
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LTR_CUSTOMER_CARD_TOKEN (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_CUSTOM_EXAMPLE
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_CUSTOM_EXAMPLE (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LTR_WEB_CC_TOKEN
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LTR_WEB_CC_TOKEN (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_ATTRIBUTE_DETAIL
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_ATTRIBUTE_DETAIL (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  lt_member_examples_0923
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: lt_member_examples_0923 (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_COMMENTS
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_COMMENTS (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_SUB_HIST
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_SUB_HIST (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_TKT_HIST
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_TKT_HIST (post_merge)'
				 GOTO GiveErrMsg
		   END
		
		--Entitlements Merging
		exec ap_set_context 'MERGING', 'Y'
		UPDATE  LTX_CUST_ENTITLEMENT
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LTX_CUST_ENTITLEMENT (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LTX_CUST_ORDER_ENTITLEMENT
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LTX_CUST_ORDER_ENTITLEMENT (post_merge)'
				 GOTO GiveErrMsg
		   END
		
		UPDATE  LT_ENTITLEMENT_GIFT
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_ENTITLEMENT_GIFT (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_ENTITLEMENT_GIFT
		SET     gift_customer_no = @kept_id
		WHERE   gift_customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_ENTITLEMENT_GIFT (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_AUDIT_ENTITLEMENT_GIFT
		SET     gift_customer_no = @kept_id
		WHERE   gift_customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_AUDIT_ENTITLEMENT_GIFT (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LT_AUDIT_ENTITLEMENT_GIFT
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_ENTITLEMENT_GIFT (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LTX_AUDIT_CUST_ENTITLEMENT
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LTX_AUDIT_CUST_ENTITLEMENT (post_merge)'
				 GOTO GiveErrMsg
		   END

		UPDATE  LTX_AUDIT_CUST_ORDER_ENTITLEMENT
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LTX_AUDIT_CUST_ORDER_ENTITLEMENT (post_merge)'
				 GOTO GiveErrMsg
		   END
		exec ap_set_context 'MERGING', 'N'


		-- H. Sheridan, 3/2/2017 - Added new table
		UPDATE  LT_CORRESPONDENCE
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_CORRESPONDENCE (post_merge)'
				 GOTO GiveErrMsg
		   END

		-- H. Sheridan, 5/22/2017 - Added new table
		UPDATE  LT_ORDER_ACK_INVOICE_EMAIL
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		   BEGIN
				 SELECT @err_table = 'Error: LT_ORDER_ACK_INVOICE_EMAIL (post_merge)'
				 GOTO GiveErrMsg
		   END

		-- H. Sheridan, 6/28/2017 - Added new table
		UPDATE  LT_HISTORY_TICKET_ARCHIVE
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		BEGIN
			SELECT @err_table = 'Error: LT_HISTORY_TICKET_ARCHIVE (post_merge)'
			GOTO GiveErrMsg
		END

		-- H. Sheridan, 6/30/2017 - Added new table
		UPDATE  LT_TKT_HEADER_AUDIT
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		BEGIN
			SELECT @err_table = 'Error: LT_TKT_HEADER_AUDIT (post_merge)'
			GOTO GiveErrMsg
		END

		-- H. Sheridan, 9/27/2017 - Added new table
		UPDATE  LTW_ORDER_ACK_INVOICE_EMAIL
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		BEGIN
			SELECT @err_table = 'Error: LTW_ORDER_ACK_INVOICE_EMAIL (post_merge)'
			GOTO GiveErrMsg
		END

		-- D. Jacob, 10/12/2017 - Added new table
		UPDATE  LTR_CARD_UPDATES
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
		BEGIN
			SELECT @err_table = 'Error: LTR_CARD_UPDATES (post_merge)'
			GOTO GiveErrMsg
		END

		--D. Jacob, 11/20/2017 - Added new table
		UPDATE  dbo.LT_HISTORY_CUSTOMER_ARCHIVE
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
			BEGIN
				SELECT @err_table = 'Error: LT_HISTORY_CUSTOMER_ARCHIVE (post_merge)'
				GOTO GiveErrMsg
			END

		-- H. Sheridan,  6/18/2018 - Added new table
		UPDATE  dbo.LT_HISTORY_SALES_METRICS_RAW
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
			BEGIN
				SELECT @err_table = 'Error: LT_HISTORY_SALES_METRICS_RAW (post_merge)'
				GOTO GiveErrMsg
			END

		-- H. Sheridan,  7/11/2019 - Added new table
		UPDATE  dbo.LT_M2_WELCOME_SENT_LOG
		SET     customer_no = @kept_id
		WHERE   customer_no = @deleted_id

		IF @@error <> 0 
			BEGIN
				SELECT @err_table = 'Error: LT_M2_WELCOME_SENT_LOG (post_merge)'
				GOTO GiveErrMsg
			END

        -- M. Sherwood 4/18/2020 - Added new table
    	UPDATE  [dbo].[LT_HISTORY_TICKET_ANALYTICS]
	    SET     [Customer Number] = @kept_id
	    WHERE   [Customer Number] = @deleted_id

	    IF @@error <> 0 
	       BEGIN
		     SELECT @err_table = 'Error: LT_HISTORY_TICKET_ANALYTICS (post_merge)'
		     GOTO GiveErrMsg
	       END

        -- M. Sherwood 8/20/2020 - Added new table
    	UPDATE  [dbo].[LT_WF_PAGE_RESPONSE]         
	    SET     [customer_no] = @kept_id
	    WHERE   [customer_no] = @deleted_id

	    IF @@error <> 0 
	       BEGIN
		     SELECT @err_table = 'Error: LT_WF_PAGE_RESPONSE (post_merge)'
		     GOTO GiveErrMsg
	       END

   END

RETURN

GiveErrMsg:

--RAISERROR 50000 'Error occured in Localized Procedure '

-- Custom MOS code
RAISERROR (@err_table, 16, 1)
 
RETURN



GO


