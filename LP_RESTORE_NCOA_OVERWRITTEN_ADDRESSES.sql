USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RESTORE_NCOA_OVERWRITTEN_ADDRESSES]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_RESTORE_NCOA_OVERWRITTEN_ADDRESSES] AS' 
END
GO

/*
    LP_RESTORE_NCOA_OVERWRITTEN_ADDRESSES:  
    
            Written by: Mark Sherwood  Nov, 2017
    
            This procedure takes an ncoa session number and uses it to restore addresses that were overwritten by the National Change of Address utility that the museum runs every 
            six or so weeks to change the addresses of people who have moved.  That utility, instead of inactivating the current record and creating a new one, overwrites the existing 
            record with the new information.  The procedure uses the audit table to recover the data that was overwritten and create a new inactive record in the T_ADDRESS table so that 
            it can be seen as a former address on the constituent's record and can be searched on if necessary.

            Parameters: @ncoa_session:  Every time the NCOA utility is run, it creates a session.  Each record in T_ADDRESS updated will have this id number in the ncoa_session column
                                        If zero or null is passed to this parameter, the procedure will find the latest session number where records were updated.  There's also a table
                                        called T_NCOA_SESSION.  This should find the session you want:  SELECT MAX([ncoa_session]) FROM [dbo].[T_NCOA_SESSION] WHERE [process_record_count] > 0

                    @force_data_reset:  If procedure is run and fails before any records are added into the database, set this to Y to force the procedure to recreate the initial data set to work with.
                                        Do not pass Y to this parameter if any records from the session have been added to T_ADDRESS.  This will result in duplicate address records.

                    @gather_data_only:  If you want the procedure to figure out what addresses need to be put back but stop short of actually inserting the new records into the T_ADDRESS table,
                                        set this parameter to 'Y'.  The results will be in [TemporaryData].[dbo].[NCOA_changed_addresses] but no changes will be made to T_ADDRESS.

                     @process_counter:  This procedure will take some time to run, pass a number > 0 to this parameter to limit the number of records processed during the current run.
                                        Pass zero (or null) to this parameter to run for the entire data set.

                NOTE: I usually run this procedure with @gather_data_only set to 'Y' first.  Once I know all the records have been gathered and checked,
                      I run it again with @gather_data_only set to 'N'  As long as @force_data_reset is set to 'N', it will not reprocess all the records,
                      it will only add the new records to T_ADDRESS using the information that it already in [TemporaryData].[dbo].[NCOA_changed_addresses]

            This procedure is written in a way specifically designed to allow it to be run for a small subset of the data (determined by @process_counter parameter) or to be stopped, either on purpose
            or because of some kind of code failure.  The cursors that process the records one at a time may cause the overall run time to be a little longer than it might be otherwise, but if the procedure
            is partially run and is restarted, this coding ensures that the process picks up right where it left off and does not waste time reprocessing data it has already processed, which could
            potentially cause duplicate address records to be entered into the database.  It also uses an actual table created in the TemporaryData database to temporarily store address information, 
            rather than a temp table or a table variable defined within the procedure.

            To determine how many records have been fully or partially processed, use the following command (changing ## to whatever session # you are processing).  This will look at the record_status 
            column in the NCOA_changed_addresses table.  
            
                    SELECT [record_status], COUNT(*) AS 'record_count' FROM [TemporaryData].[dbo].[NCOA_changed_addresses] 
                    WHERE [session_no] = ## GROUP BY [record_status]

            record_status will be one of four values
                P = Pending:  The record has been added to the table but nothing has been done to it yet
                C = Checked:  The former address information has been extrapolated from the audit table but the record has not yet been added to T_ADDRESS
                I = Inserted: The record has been inserted into the T_ADDRESS table.  Processing for this record is complete.
                S = Same:  Record not added because the new values are the same as the old values in the Street1 and Street2 fields

*/

ALTER PROCEDURE [dbo].[LP_RESTORE_NCOA_OVERWRITTEN_ADDRESSES]
        @ncoa_session INT = 0,
        @force_data_reset CHAR(1) = 'N',
        @gather_data_only char(1) = 'N',
        @process_counter INT = 0,
        @err_message VARCHAR(4000) = '' OUTPUT
AS BEGIN

    PRINT 'Procedure Started  @ ' + CONVERT(VARCHAR(50),GETDATE(),113)

    /*  Procedure Variables  */
        
        DECLARE @address_no INT = 0, @ncoa_record_count INT = 0, @ncoa_import_date DATETIME = NULL, @sql_query VARCHAR(4000) = '', @primary_key_name VARCHAR(50)

        DECLARE @customer_no INT = 0, @former_address_type INT = 0, @former_street1 VARCHAR(64) = '', @former_street2 VARCHAR(64) = '', 
                @former_street3 VARCHAR(64) = '', @former_city VARCHAR(30) = '', @former_state VARCHAR(20) = '', @former_postal_code VARCHAR(10) = '', 
                @former_country INT = 0, @former_geo_area INT = 0, @former_geo_location GEOGRAPHY = NULL, @former_address_no INT = 0, @counter INT = 0

        DECLARE @force_table_create CHAR(1) = 'N'

        SELECT @primary_key_name = '[PK_NCOA_' + REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(30),GETDATE(),120),'-',''),':',''),' ','') + ']'
        
        /*  The @colulm_list table variable lists all the columns the procedure understands and monitors for changes.
            Data changed in columns not listed in this table is ignored.  */

        DECLARE @column_list TABLE ([changed_column_name] VARCHAR(50))

        INSERT INTO @column_list ([changed_column_name]) VALUES ('address_type'), ('street1'), ('street2'), ('street3'), ('city'), 
                                                                ('state'), ('postal_code'), ('country'), ('geo_area'), ('geo_location')

        
    /*  The TemporaryData database must exist on the server for this procedure to work.  */
    
        IF ISNULL(db_id('TemporaryData'),0) = 0 BEGIN

            SELECT @err_message = 'Procedure uses the TemporaryData database, which cannot be found on ' + @@SERVERNAME + '.  Cannot continue.'
            GOTO DONE

        END


    /*  Check Parameters  */

        SELECT @err_message = ISNULL(@err_message,'')
        
        SELECT @ncoa_session = ISNULL(@ncoa_session,0)
        SELECT @force_data_reset = ISNULL(@force_data_reset,'N')
        IF @force_data_reset <> 'Y' SELECT @force_data_reset = 'N'
        SELECT @process_counter = ISNULL(@process_counter,0)

        IF @ncoa_session <= 0 BEGIN
            SELECT @ncoa_session = MAX([ncoa_session]) FROM [dbo].[T_NCOA_SESSION] WHERE [process_record_count] > 0
            SELECT @ncoa_session = ISNULL(@ncoa_session,0)
        END

        IF @ncoa_session <= 0 BEGIN
            SELECT @err_message = 'Unable to determine most recent NCOA session number.'
            GOTO DONE   
        END


    /*  If the NCOA_changed_addresses database does not exist in the Temporarydata database, it will be created  
        @force_table_create used to be a parameter.  Now it's just a declared variable.  If it's set to Y, the temp table will be deleted, forcing to to be recreated  */

        IF EXISTS (SELECT * FROM [TemporaryData].[sys].[objects] WHERE name = 'NCOA_changed_addresses' AND type IN (N'U')) AND @force_table_create = 'Y'
            DROP TABLE [TemporaryData].[dbo].[NCOA_changed_addresses]
    
        IF NOT EXISTS (SELECT * FROM [TemporaryData].[sys].[objects] WHERE name = 'NCOA_changed_addresses' AND type IN (N'U')) BEGIN

            CREATE TABLE [TemporaryData].[dbo].[NCOA_changed_addresses] 
                        ([session_no] INT NOT NULL, [address_no] INT NOT NULL, [record_status] CHAR(1), [customer_no] INT, [address_type] INT, [address_type_name] VARCHAR(30), 
                         [street1] VARCHAR(64), [street2] VARCHAR(64), [street3] VARCHAR(64), [city] VARCHAR(30), [state] VARCHAR(20), [postal_code] VARCHAR(10), [country] int, 
                         [country_name] VARCHAR(30), [geo_area] INT, [geo_location] GEOGRAPHY, [former_address_type] INT, [former_street1] VARCHAR(64), 
                         [former_street2] VARCHAR(64), [former_street3] VARCHAR(64), [former_city] VARCHAR(30), [former_state] VARCHAR(20), [former_postal_code] VARCHAR(10), 
                         [former_country] INT, [former_geo_area] INT, [former_geo_location] GEOGRAPHY)
    
            SELECT @sql_query = 'ALTER TABLE [TemporaryData].[dbo].[NCOA_changed_addresses] ADD CONSTRAINT ' + @primary_key_name + ' PRIMARY KEY CLUSTERED ([session_no] ASC, [address_no] ASC) ON [PRIMARY]'
            EXECUTE (@sql_query)
    
        END
           
    /*  Check to make sure the necessary indexes exist on the NCOA_changed_addresses database  */
    
        IF NOT EXISTS (SELECT * FROM [TemporaryData].[sys].[indexes] WHERE [name] = 'ix_NCOA_changed_addresses_customer_no')
            CREATE NONCLUSTERED INDEX [ix_NCOA_changed_addresses_customer_no] ON [TemporaryData].[dbo].[NCOA_changed_addresses] ([customer_no] ASC) ON [PRIMARY]

        IF NOT EXISTS (SELECT * FROM [TemporaryData].[sys].[indexes] WHERE [name] = 'ix_NCOA_changed_addresses_record_status')
            CREATE NONCLUSTERED INDEX [ix_NCOA_changed_addresses_record_status] ON [TemporaryData].[dbo].[NCOA_changed_addresses] ([record_status] ASC) ON [PRIMARY]


    /*  Get the record count and import date for the NCOA session being processed  */
    
        SELECT @ncoa_record_count = ISNULL([process_record_count],0), @ncoa_import_date = [import_date]
        FROM [dbo].[T_NCOA_SESSION] WHERE ncoa_session = @ncoa_session


    /*  If no records processed, end procedure  */
    
        IF @ncoa_record_count = 0 BEGIN
            SELECT @err_message = 'No processed records found for NCOA Session # ' + CONVERT(VARCHAR(20),@ncoa_session)
            GOTO DONE
        END


    /* Task # 1  Get all the address records that were changed during the NCOA session  */

    BEGIN TRY

        BEGIN TRANSACTION

            IF @force_data_reset = 'Y' DELETE FROM [TemporaryData].[dbo].[NCOA_changed_addresses] WHERE [session_no] = @ncoa_session
           
            IF NOT EXISTS (SELECT * FROM [TemporaryData].[dbo].[NCOA_changed_addresses] WHERE [session_no] = @ncoa_session)
                INSERT INTO [TemporaryData].[dbo].[NCOA_changed_addresses] ([session_no],[record_status],[address_no],[customer_no],[address_type],[address_type_name],
                                                                            [street1],[street2],[street3],[city],[state],[postal_code],[country],[country_name],[geo_area],[geo_location])
                SELECT @ncoa_session, 'P', adr.[address_no], adr.[customer_no], adr.[address_type], typ.[description], adr.[street1], adr.[street2], adr.[street3], adr.[city], adr.[state], 
                       adr.[postal_code], adr.[country], cou.[description], adr.[geo_area], adr.[geo_location]
                FROM [dbo].[T_ADDRESS] AS adr
                     LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS typ (NOLOCK) ON typ.[id] = adr.[address_type]
                     LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK) ON cou.[id] = adr.[country]
                WHERE adr.[ncoa_session] = @ncoa_session AND adr.[ncoa_action] = 1   --ncoa_action 1 = changed addresses but not addresses that were just standardized

        COMMIT TRANSACTION

    END TRY
    BEGIN CATCH

        WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION

        SELECT @err_message = 'Error Inserting Address Records From NCOA session # ' + CONVERT(VARCHAR(20),@ncoa_session)
        GOTO DONE

    END CATCH

    PRINT 'Task # 1  Complete @ ' + CONVERT(VARCHAR(50),GETDATE(),113)
    
    /*  Task # 2 Check the audit trail for each address record to see what was changed
                 Processed one at a time in a cursor so that if the procedure stops, it can be started again (with the proper parameter values) a
                 and it will pick up where it left off and not have to redo processing that was already done.  */

        DECLARE address_cursor INSENSITIVE CURSOR FOR
        SELECT [address_no] FROM [TemporaryData].[dbo].[NCOA_changed_addresses] WHERE [session_no] = @ncoa_session AND [record_status] = 'P'
        OPEN address_cursor
        BEGIN_ADDRESS_LOOP:

            FETCH NEXT FROM address_cursor INTO @address_no
            IF @@FETCH_STATUS = -1 GOTO END_ADDRESS_LOOP

            BEGIN TRY

                BEGIN TRANSACTION

                    DECLARE query_cursor INSENSITIVE CURSOR FOR
                    SELECT 'UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses]' + CHAR(10)
                         + 'SET [former_' + [column_updated] + '] = ''' + [old_value] + '''' + CHAR(10)
                         + 'WHERE  [session_no] = ' + CONVERT(VARCHAR(20),@ncoa_session) + ' and [address_no] = ' + CONVERT(VARCHAR(20),@address_no) + CHAR(10) + CHAR(10)
                    FROM [dbo].[TA_AUDIT_TRAIL] 
                    WHERE CONVERT(DATE,[date]) = CONVERT(DATE,@ncoa_import_date) and [table_name] = 'T_ADDRESS' AND [alternate_key] = @address_no
                      AND [column_updated] IN (SELECT [column_updated] FROM @column_list)
                    OPEN query_cursor
                    BEGIN_QUERY_LOOP:
        
                        FETCH NEXT FROM query_cursor INTO @sql_query
                        IF @@FETCH_STATUS = -1 GOTO END_QUERY_LOOP

                        EXECUTE (@sql_query)
                        
                        GOTO BEGIN_QUERY_LOOP

                    END_QUERY_LOOP:
                    CLOSE query_cursor
                    DEALLOCATE query_cursor

                    /*  Reset the "former" data to be the same as the "current" data on any column that was not changed.  
                        The procedure knows the column wasn't changed if the value in it is null  */

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_address_type] = ISNULL([former_address_type],[address_type]) 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_street1] = ISNULL([former_street1],[street1]) 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_street2] = ISNULL([former_street2],[street2]) 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_street3] = ISNULL([former_street3],[street3]) 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_city] = ISNULL([former_city],[city]) 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_state] = ISNULL([former_state],[state]) 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_postal_code] = ISNULL([former_postal_code],[postal_code])
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_country] = ISNULL([former_country],[country]) 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_geo_area] = ISNULL([former_geo_area],[geo_area]) 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [former_geo_location] = ISNULL([former_geo_location],[geo_location]) 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                    /*  Change the status of this address record to "C" so that it is not processed again if the procedure is stopped and restarted  */

                        UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [record_status] = 'C' 
                        WHERE [session_no] = @ncoa_session and [address_no] = @address_no

                COMMIT TRANSACTION

            END TRY
            BEGIN CATCH
        
                WHILE @@TRANCOUNT > 0 ROLLBACK TRAN
                IF CURSOR_STATUS('global','query_cursor') > 0 CLOSE query_cursor
                IF CURSOR_STATUS('global','query_cursor') >= -1 DEALLOCATE query_cursor

                IF LEN(@err_message) + LEN(ERROR_MESSAGE()) < 3080
                    SELECT @err_message = @err_message + convert(VARCHAR(20),@address_no) + ' - ' + Error_message() + CHAR(10)
            
            END CATCH
                
            SELECT @counter = (@counter + 1)

            IF @counter < @process_counter OR @process_counter = 0  GOTO BEGIN_ADDRESS_LOOP

        END_ADDRESS_LOOP:
        CLOSE address_cursor
        DEALLOCATE address_cursor

        PRINT 'Task # 2  Complete @ ' + CONVERT(VARCHAR(50),GETDATE(),113)
    
    /*  Task # 3:  Create the new address records in the T_ADDRESS table
                   All the former information gathered from the audit table in the previous task is compiled into a new record for the T_ADDRESS table  */
    
        IF @gather_data_only <> 'Y' BEGIN

            SELECT @counter = 0

            /*  DO NOT INSERT NEW RECORDS IF THE STREET 1/FORMER STREET 1 AND STREET 2/FORMER STREET2 VALUES ARE THE SAME  */

            UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] 
            SET [record_status] = 'S'
            WHERE [session_no] = @ncoa_session AND [record_status] = 'C' 
              AND ISNULL([street1],'') = ISNULL([former_street1],'') 
              AND ISNULL([street2],'') = ISNULL([former_street2],'')

            DECLARE former_address_cursor INSENSITIVE CURSOR FOR
            SELECT [address_no], [customer_no], [former_address_type], [former_street1], [former_street2], [former_street3], [former_city], [former_state], [former_postal_code], 
                   [former_country], [former_geo_area], [former_geo_location]
            FROM [TemporaryData].[dbo].[NCOA_changed_addresses] 
            WHERE [session_no] = @ncoa_session AND [record_status] = 'C'
            ORDER BY address_no
            OPEN former_address_cursor
            BEGIN_FORMER_ADDRESS_LOOP:

                FETCH NEXT FROM former_address_cursor INTO @address_no, @customer_no, @former_address_type, @former_street1, @former_street2, @former_street3, 
                                @former_city, @former_state, @former_postal_code, @former_country, @former_geo_area, @former_geo_location
                IF @@FETCH_STATUS = -1 GOTO END_FORMER_ADDRESS_LOOP

                BEGIN TRY

                    BEGIN TRANSACTION
            

                        /*  Get the next ID number for T_ADDRESS using the procedure provided by Tessitura - If this fails to produce an id number, move on to the next record  */

                            EXECUTE dbo.AP_GET_NEXTID_function @type = 'AD', @nextid = @former_address_no OUTPUT, @increment = 0

                           IF ISNULL(@former_address_no,0) = 0 BEGIN
                                ROLLBACK TRANSACTION
                                GOTO BEGIN_FORMER_ADDRESS_LOOP
                            END

                            IF EXISTS (SELECT * FROM [dbo].[T_ADDRESS] WHERE [address_no] = @former_address_no) BEGIN
                                ROLLBACK TRANSACTION
                                GOTO BEGIN_FORMER_ADDRESS_LOOP
                            END

                        /*  Insert the new record into T_ADDRESS using all the information gathered from the audit tables and some defaults for the other columns
                            The start date for this former address is unknown but the end date is set to the current date.  
                            The ncoa_action column is set to 9 (a number not used by the NCOA utility) just to make it a value greater than zero for sorting and searching purposes  */

                            INSERT INTO [dbo].[T_ADDRESS] ([address_no],[customer_no],[address_type],[street1],[street2],[city],[state],[postal_code],[country],[start_dt],[end_dt],[months],[primary_ind],
                                                           [inactive], [alt_signor], [mail_purposes], [label], [geo_area], [delivery_point], [ncoa_session], [ncoa_action], [street3], [geo_location])
                            SELECT @former_address_no, @customer_no, @former_address_type, @former_street1, @former_street2, @former_city, @former_state, @former_postal_code, @former_country, NULL, GETDATE(), 
                                   'YYYYYYYYYYYY', 'N', 'Y', 0, NULL, 'Y', @former_geo_area, '', @ncoa_session, 9, @former_street3, @former_geo_location

                        /*  When the NCOA utility is run it sets the last modified user to one of three values.  
                                NCOA$CHG = Changed / NCOA$STD = Data not changed but standardized / NCOA$DNM = Can't figure it out, do not mail to this address 
                            I have created a fourth value:  NCOA$FMR to indicate that this is a former address re-added after it was overwritten by the NCOA utility
                        */

                            UPDATE [dbo].[T_ADDRESS] SET [last_updated_by] = 'FORMER' WHERE [address_no] = @former_address_no

                        /*  Change the status of this address record to "I" so that it is not processed again if the procedure is stopped and restarted  */

                            UPDATE [TemporaryData].[dbo].[NCOA_changed_addresses] SET [record_status] = 'I' WHERE [session_no] = @ncoa_session AND [address_no] = @address_no

                    COMMIT TRANSACTION

                END TRY
                BEGIN CATCH

                    WHILE @@TRANCOUNT > 0 ROLLBACK TRAN

                    IF LEN(@err_message) + LEN(ERROR_MESSAGE()) < 3080
                        SELECT @err_message = @err_message + convert(VARCHAR(20),@address_no) + ' - ' + Error_message() + CHAR(10)

                END CATCH

                SELECT @counter = (@counter + 1)

                IF @counter < @process_counter OR @process_counter = 0 GOTO BEGIN_FORMER_ADDRESS_LOOP

            END_FORMER_ADDRESS_LOOP:
            CLOSE former_address_cursor
            DEALLOCATE former_address_cursor

            PRINT 'Task # 3  Complete @ ' + CONVERT(VARCHAR(50),GETDATE(),113)
    
        END
        
    DONE:

        IF LTRIM(@err_message) = '' SELECT @err_message = 'success'

        WHILE @@TRANCOUNT > 0 ROLLBACK TRAN
      
        PRINT 'Procedure Complete @ ' + CONVERT(VARCHAR(50),GETDATE(),113)

END
GO

GRANT EXECUTE ON [dbo].[LP_RESTORE_NCOA_OVERWRITTEN_ADDRESSES] TO ImpUsers
GO

